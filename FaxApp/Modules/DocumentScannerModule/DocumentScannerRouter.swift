import Foundation
import UIKit
import VisionKit

protocol DocumentScannerRouter: AnyObject, AlertOptionsRoutable {
    func closeScreen(images pickedImages: [UIImage]?)
}

class DocumentScannerRouterImpl: BaseScreenRouter, ScreenRouter, DocumentScannerRouter {
    
    var routerIdentifiable: RouterIdentifiable {
        RouterType.singleImagePicker(.scanner)
    }
    
    var screen: UIViewController { _screen }
    
    var presentationType: ScreenPresentationType = .modal
    
    private lazy var delegate: Delegate = {
        let delegate = Delegate()
        delegate.presenter = DocumentScannerPresenterImpl(router: self)
        return delegate
    }()
    
    private lazy var _screen: UIViewController = {
        let view = VNDocumentCameraViewController()
        view.delegate = delegate
        return view
    }()
    
    private let model: SingleImagePickerRouterModel
    
    init(model: SingleImagePickerRouterModel) {
        self.model = model
    }
    
}

extension DocumentScannerRouterImpl {
    func closeScreen(images pickedImages: [UIImage]?) {
        closeLast { [model] in
            if let pickedImages = pickedImages {
                for image in pickedImages {
                    model.onPickImage(image)
                }
            }
        }
        lastClosed()
    }
}

private final class Delegate: NSObject, VNDocumentCameraViewControllerDelegate {
    
    var presenter: DocumentScannerPresenter?
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFinishWith scan: VNDocumentCameraScan) {
        presenter?.scanPicked(scan: scan)
    }
    
    func documentCameraViewController(_ controller: VNDocumentCameraViewController, didFailWithError error: Error) {
        presenter?.showAlert(error: error)
    }
    
    func documentCameraViewControllerDidCancel(_ controller: VNDocumentCameraViewController) {
        presenter?.dismissTapped()
    }
    
}
