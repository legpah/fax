//
//  PDFPreviewPresenter.swift
//  FaxApp
//
//  Created by Eugene on 22.04.2022.
//

import Foundation

protocol PDFPreviewPresenter: ViewOutput {
    func backButtonTapped()
    func didTapEditFrontPage()
}

final class PDFPreviewPresenterImpl {
    
    weak var view: PDFPreviewViewController?
    
    private var pdfObject: PDFObject
    private let onDocumentEdited: IClosure<PDFObject>?
    private weak var router: PDFPreviewPageRouter?
    
    init(pdfObject: PDFObject,
         onDocumentEdited: IClosure<PDFObject>?,
         router: PDFPreviewPageRouter) {
        self.pdfObject = pdfObject
        self.onDocumentEdited = onDocumentEdited
        self.router = router
    }
    
    private func openFontPage(_ model: FrontPageAttributesModel) {
        router?.openFrontPage(attributesModel: model, onCreate: { [weak self] pdfObject in
            guard let self = self else { return }
            self.pdfObject = pdfObject
            self.view?.updatePdf(pdfObject: pdfObject)
            self.onDocumentEdited?(pdfObject)
        })
    }
}

extension PDFPreviewPresenterImpl: PDFPreviewPresenter {
    func viewDidLoad() {
        view?.updatePdf(pdfObject: pdfObject)
    }
    
    func didTapEditFrontPage() {
        switch pdfObject.unerlyingSource {
        case .frontPage(let attributesModel):
            openFontPage(attributesModel)
        default:
            break
        }
    }
    
    func backButtonTapped() {
        router?.closeLast()
    }
}
