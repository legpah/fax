//
//  AnalyticsServiceImpl.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation
import FirebaseAnalytics
import Combine
import StoreKit

final class AnalyticsServiceImpl: AnalyticsService {
    
    static let shared = AnalyticsServiceImpl()
    
    private let service: AnalyticsService
    private let userUIDProvider: UserUIDProvider
    private let purchaseService: PurchasesService
    private let startupDateProvider = UDAccessFactory.startupDate
    private let timeZone = TimeZone
        .current
        .abbreviation()?
        .replacingOccurrences(of: "GMT", with: "")
    
    private var defaultParams: [String: Any] = [:] {
        didSet {
            if !defaultParams.isEqual(to: oldValue) {
                Analytics.setDefaultEventParameters(defaultParams)
            }
        }
    }
    
    private var cancelBag = Set<AnyCancellable>()
    
    init(amplitude: AnalyticsService = Amplitudes.shared,
         userUIDProvider: UserUIDProvider = UserUIDProviderImpl.shared,
         purchaseService: PurchasesService = PurchasesService.shared) {
        #if DEBUG
        service = DebugPrintAnalytics()
        #else
        service = amplitude
        #endif
        self.userUIDProvider = userUIDProvider
        self.purchaseService = purchaseService
        configureDefaultParams()
    }
    
    func send(_ event: AnalyticsEvent) {
        service.send(extendWithDefaultParams(event: event))
    }
    
    private func configureDefaultParams() {
//        var params: [String: Any] = [:]
//        params["user_id"] = userUIDProvider.uuid
//        startupDateProvider.value.flatMap {
//            params["startup_date"] = $0
//        }
//        timeZone.flatMap {
//            params["gmt"] = $0
//        }
//        params["subscription_type"] = purchaseService.currentState.description
//        defaultParams = params
//
//        subsribeOnSubscriptionType()
    }
    
    private func subsribeOnSubscriptionType() {
        purchaseService.subscriptionState.sinkSuccess { [weak self] state in
            self?.defaultParams["subscription_type"] = state.description
        }.store(in: &cancelBag)
    }
    
    private func extendWithDefaultParams(event: AnalyticsEvent) -> AnalyticsEvent {
        var params = event.parameters ?? [:]
        defaultParams.forEach {
            params[$0.key] = $0.value
        }
       // params["event_date"] = Int(Date().timeIntervalSince1970)
        return AnalyticsEventImpl(name: event.name, parameters: params)
    }
}

#if DEBUG
fileprivate class DebugPrintAnalytics: AnalyticsService {
    func send(_ event: AnalyticsEvent) {
        debugPrint("Analytics event:", event.name, event.parameters ?? [:], separator: "\n")
    }
}
#endif

extension AppleSubscriptionState {
    fileprivate var description: String {
        switch self {
        case .loading:
            return "loading_subscription"
        case .notSubscribed:
            return "not_subscribed"
        case let .subscribed(subscription):
            return subscription.identifier
        }
    }
}

extension Dictionary where Key == String, Value == Any {
    func isEqual(to another: Dictionary<Key, Value>) -> Bool {
        if self.count != another.count {
            return false
        }
        
        for (k, value) in self {
            if let anotherValue = another[k],
               isAnyEqual(value, b: anotherValue) {
                continue
            }
            return false
        }
        
        return true
    }
}
