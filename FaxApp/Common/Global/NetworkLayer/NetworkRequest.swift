//
//  NetworkRequest.swift
//  FaxApp
//
//  Created by Eugene on 11.05.2022.
//

import Foundation

public enum HTTPMethod: String {
    case get = "GET"
    case post = "POST"
    case put = "PUT"
    case delete = "DELETE"
}

public protocol NetworkRequest {
    associatedtype ReturnType: Decodable
    
    var path: String { get }
    var method: HTTPMethod { get }
    var queryParams: [String: String]? { get }
    var body: [String: Any]? { get }
    var headers: [String: String]? { get }
    var decode: (Data) throws -> ReturnType { get }
    
    func asURLRequest(baseURL: String) -> URLRequest?
}

extension NetworkRequest {
    var queryParams: [String: String]? { nil }
    var body: [String: Any]? { nil }
    var headers: [String: String]? {
        [
            .httpHeaders.contentType: .httpHeaders.applicationJson
        ]
    }
    var decode: (Data) throws -> ReturnType {
        return {
            try JSONDecoder().decode(ReturnType.self, from: $0)
        }
    }
}

extension NetworkRequest {
    func asURLRequest(baseURL: String) -> URLRequest? {
        guard var urlComponents = URLComponents(string: baseURL) else {
            return nil
        }
        
        urlComponents.path = "\(urlComponents.path)\(path)"
        urlComponents.queryItems = queryParams?.map{ k, v in
            URLQueryItem(name: k, value: v)
        }
        
        guard let finalURL = urlComponents.url else {
            return nil
        }
        
        var request = URLRequest(url: finalURL)
        request.httpMethod = method.rawValue
        
        request.allHTTPHeaderFields = self.headers
        request.httpBody = requestBodyFrom(params: body)
        
        return request
    }
    
    private func requestBodyFrom(params: [String: Any]?) -> Data? {
        guard let params = params else { return nil }
        guard let httpBody = try? JSONSerialization.data(withJSONObject: params, options: []) else {
            return nil
        }
        return httpBody
    }
}


enum HTTPHeadersTag {
    static let contentType = "content-type"
    static let contentLength = "content-length"
    static let applicationJson = "application/json"
    static let multipartData = "multipart/form-data"
}

extension String {
    static let httpHeaders = HTTPHeadersTag.self
}
