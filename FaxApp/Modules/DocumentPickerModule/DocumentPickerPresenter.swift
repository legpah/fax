//
//  DocumentPickerPresenter.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import Foundation

protocol DocumentPickerPresenter {
    func dismissTapped()
    func didSelectDocument(_ data: Data, fileType: DocumentPickerFileType)
    func didFailParseDocument()
}

final class DocumentPickerPresenterImpl {
    
    private weak var router: DocumentPickerRouter?
    
    init(router: DocumentPickerRouter) {
        self.router = router
    }
}

extension DocumentPickerPresenterImpl: DocumentPickerPresenter {
    func dismissTapped() {
        router?.screenClosed()
    }
    
    func didSelectDocument(_ data: Data, fileType: DocumentPickerFileType) {
        router?.closeScreen(documentResult: .success((data, fileType)))
    }
    
    func didFailParseDocument() {
        router?.closeScreen(documentResult: .failure(PickerError.failedToParseError))
    }
}

fileprivate enum PickerError: Error {
    case failedToParseError
}
