//
//  UIVIew+ConvertFramesHelper.swift
//  FaxApp
//
//  Created by Eugene on 14.06.2022.
//

import UIKit

extension UIView {
    
    /// Позиция относительно экрана
    var globalPoint: CGPoint {
        return self.superview?.convert(self.frame.origin, to: nil) ?? .zero
    }
}
