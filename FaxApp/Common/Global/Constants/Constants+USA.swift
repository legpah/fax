//
//  Constants+USA.swift
//  FaxApp
//
//  Created by Eugene on 3.05.2022.
//

import Foundation

extension ConstantsToken.Countries {
    static let usaCanadaCode = "+1"
    static let usaName = "United States"
    static let canadaName = "Canada"
}

extension String {
    static let countries = ConstantsToken.Countries.self
}
