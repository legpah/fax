//
//  UserDefaultsKeys.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation

enum UserDefaultsKeysTag {}

extension String {
    static let userDefaults = UserDefaultsKeysTag.self
}

extension UserDefaultsKeysTag {
    static let uiTheme = prefixed("stored_ui_theme")
    static let isOnboardingShowed = prefixed("is_onboarding_showed")
    static let isAlertForNotificationsShowed = prefixed("is_alert_for_notifications_showed")
    static let isFirstLaunch = prefixed("is_first_launch")
    static let selectedCountry = prefixed("selected_county_phone")
    static let startupDate = prefixed("startup_date")
    static let senderName = prefixed("sender_name")
    static let senderPhone = prefixed("sender_phone")
    static let notificationToken = prefixed("notification_token")
    static let isIDFARequested = prefixed("is_IDFA_requested")
    static let isPushAlertShowed = prefixed("is_push_alert_showed")
    
    private static func prefixed(_ key: String) -> String {
        return "com.supportlink.limited_\(key)"
    }
}
