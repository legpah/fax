//
//  UIThemeProviderImpl.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit

final class UIThemeProviderImpl: UIThemeProvider, UIThemeSwitcher {
    static let shared = UIThemeProviderImpl()
    
    private let analytics = AnalyticsServiceImpl.shared
    
    private init() {
        self.current = UIThemeProviderImpl.storedTheme() ?? .light
    }
    
    private(set) var current: UITheme {
        didSet {
            UIThemeProviderImpl.storeTheme(theme: current)
            notifyObservers()
            analytics.send(
                AnalyticsEventImpl(name: current == .light ?
                                   "tab3_user_switched_to_light_mode" :
                                   "tab3_user_switched_to_dark_mode"
                )
            )
        }
    }
    private var observers: NSHashTable<AnyObject> = NSHashTable.weakObjects()
    
    func switchTheme() {
        switch current {
        case .light:
            self.current = .dark
        case .dark:
            self.current = .light
        }
    }

    func register<Observer: UIThemable>(observer: Observer,
                                        applyOnRegister: Bool = true) {
        if applyOnRegister {
            observer.apply(theme: current)
        }
        self.observers.add(observer)
    }
    
    func unregister<Observer: UIThemable>(observer: Observer) {
        self.observers.remove(observer)
    }

    private func notifyObservers() {
        DispatchQueue.main.async {
            self.observers.allObjects
                .compactMap({ $0 as? UIThemable })
                .forEach({ $0.apply(theme: self.current) })
        }
    }
    
    private static func storeTheme(theme: UITheme) {
        UserDefaults.standard.setValue(theme.rawValue, forKey: .userDefaults.uiTheme)
    }
    
    private static func storedTheme() -> UITheme? {
        let stored = UserDefaults.standard.string(forKey: .userDefaults.uiTheme)
        return UITheme(rawValue: stored ?? "")
    }
}

