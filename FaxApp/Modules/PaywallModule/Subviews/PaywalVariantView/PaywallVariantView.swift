//
//  PaywallVariantView.swift
//  FaxApp
//
//  Created by Eugene on 14.07.2022.
//

import UIKit

protocol PaywallVariantView where Self: UIView {
    var itemsContainer: PremiumItemsContainer { get }
}
