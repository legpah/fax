//
//  ToSingleImagePickerRouterInterceptor.swift
//  FaxApp
//
//  Created by Eugene on 19.04.2022.
//

import UIKit

extension RoutingInterceptorToken {
    static func toSingleImagePicker(source: SingleImagePickerSource) -> RoutingInterceptor {
        ToSingleImagePickerRouterInterceptor(source: source)
    }
}

fileprivate struct ToSingleImagePickerRouterInterceptor: RoutingInterceptor {
    
    let source: SingleImagePickerSource
    
    func interceptRoute(from: ScreenRouter, to: ScreenRouter) -> ScreenRouter? {
        guard !UIImagePickerController.isSourceTypeAvailable(source.asNativeSouce) else {
            return to
        }
        return AlertOptionsRouter(model: .unableToOpenCamera())
    }
}
