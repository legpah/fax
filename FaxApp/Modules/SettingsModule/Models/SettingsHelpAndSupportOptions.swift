//
//  SettingsHelpAndSupportOptions.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import Foundation

enum SettingsHelpAndSupportOption: Int, CaseIterable {
    case faq
    case contactUs
    case privacyPolicy
    case rateApp
}
