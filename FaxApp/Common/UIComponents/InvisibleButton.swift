//
//  InvisibleButton.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import UIKit

final class InvisibleButton: UIView {
    
    var isActive: Bool = true
    
    var onTap: VoidClosure? {
        didSet {
            onTapChanged()
        }
    }
    
    private func onTapChanged() {
        if onTap == nil {
            self.gestureRecognizers?.removeAll()
        } else {
            self.addGestureRecognizer(
                UITapGestureRecognizer(target: self, action: #selector(handleTap))
            )
        }
    }
    
    @objc
    private func handleTap() {
        if isActive {
            onTap?()
        }
    }
}
