//
//  FirebaseMessaging.swift
//  FaxApp
//
//  Created by Eugene on 31.05.2022.
//

import FirebaseMessaging
import UserNotifications

final class FirMessaging {
    
    static let shared = FirMessaging()
    
    private let messaging = Messaging.messaging()
    private let delegate = FirMessagingDelegate()
    
    func configure() {
        messaging.delegate = self.delegate
        Messaging.messaging().subscribe(toTopic: "Test")
        UNUserNotificationCenter.current().delegate = self.delegate
    }
    
    func appRegisteredForRemoteNotifications(deviceToken: Data) {
        messaging.apnsToken = deviceToken
    }
}

final class FirMessagingDelegate: NSObject, MessagingDelegate, UNUserNotificationCenterDelegate {
    
    private let notificationRegisterer = NotificationsRegistererImpl()
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        if let fcmToken = fcmToken {
            notificationRegisterer.updateFirebase(token: fcmToken)
        }
    }
    
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        willPresent notification: UNNotification,
        withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void
    ) {
        if #available(iOS 14, *) {
            completionHandler([.banner, .badge])
        } else {
            completionHandler([.alert, .badge])
        }
    }
    
    func userNotificationCenter(
        _ center: UNUserNotificationCenter,
        didReceive response: UNNotificationResponse,
        withCompletionHandler completionHandler: @escaping () -> Void
    ) { 
        NotificationHandler.shared.handle(response.notification)
        completionHandler()
    }
}
