//
//  DocumentPickerViewController.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import UIKit
import PDFKit
import WebKit

final class DocumentPickerViewControllerImpl: UIDocumentPickerViewController {
    
    private let presenter: DocumentPickerPresenter
    private let webView = WKWebView(frame: PDFConstants.pageFrame)
    
    init(presenter: DocumentPickerPresenter,
         documentTypes: [DocumentPickerFileType],
         mode: UIDocumentPickerMode) {
        self.presenter = presenter
        super.init(documentTypes: documentTypes.map { $0.asPickerFileType }, in: mode)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSelf()
    }
    
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { .fullScreen }
        set {}
    }
    
    private func configureSelf() {
        self.delegate = self
        self.allowsMultipleSelection = false
        self.overrideUserInterfaceStyle = UITheme.current.asSystemTheme
        webView.navigationDelegate = self
    }
    
    private func accessingBlock(url: URL, block: VoidClosure) {
        let shouldStopAccessing = url.startAccessingSecurityScopedResource()
        block()
        if shouldStopAccessing {
            url.stopAccessingSecurityScopedResource()
        }
    }
    
    private func extractDataAndNotifyPresenter(url: URL) {
        accessingBlock(url: url) {
            guard let data = try? Data(contentsOf: url),
                  let fileType = DocumentPickerFileType(fileUrl: url)
            else {
                presenter.didFailParseDocument()
                return
            }
            presenter.didSelectDocument(data, fileType: fileType)
        }
    }
}

extension DocumentPickerViewControllerImpl: UIDocumentPickerDelegate {
    func documentPicker(_ controller: UIDocumentPickerViewController,
                        didPickDocumentAt url: URL) {
        guard let docType = DocumentPickerFileType(fileUrl: url) else {
            presenter.didFailParseDocument()
            return
        }

        let shouldStopAccessing = url.startAccessingSecurityScopedResource()

        defer {
            if shouldStopAccessing {
                url.stopAccessingSecurityScopedResource()
            }
        }

        switch docType {
        case .pdf:
            extractDataAndNotifyPresenter(url: url)
        case .doc, .docx:
            accessingBlock(url: url, block: {
                webView.load(URLRequest(url: url))
            })
        }
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        presenter.dismissTapped()
    }
}

extension DocumentPickerViewControllerImpl: WKNavigationDelegate {
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        presenter.didSelectDocument(webView.renderPdfImage(), fileType: .pdf)
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        presenter.didFailParseDocument()
    }
}
