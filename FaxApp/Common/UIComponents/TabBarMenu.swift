//
//  TabBarMenu.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import UIKit

final class TabBarMenu: UIView {
    
    // MARK: - Internal properties
    
    var onItemTap: IClosure<TabBarType>?
    
    // MARK: - Private properties
    
    private let items: [TabBarType]
    private let container = UIView()
    private var buttons: [TrunButton] = []
    
    private var selectedItem: TabBarType
    
    // MARK: - Init
    
    init(
        items: [TabBarType]
    ) {
        self.items = items
        selectedItem = items[0]
        super.init(frame: .zero)
        self.configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Internal methods
    
    func setSelected(item: TabBarType) {
        toggleSeleted(to: item)
    }
    
    // MARK: - Private methods
    
    private func configureUI() {
        configureTabsStackView()
        themeProvider.register(observer: self)
    }
    
    private func configureTabsStackView() {
        addSubview(container)
        container.pinEdgesToSuperviewEdges()
        
        var previousView: UIView = container
        
        buttons = items.enumerated().map { idx, item in
            let iconModel = TrunButton.IconModel(
                position: .top,
                offset: 3,
                sizelayout: .empty,
                image: item.selectedImage,
                inactiveImage: item.unselectedImage
            )
            let btn = TrunButton(title: item.title,
                                 titleColor: .tukesh,
                                 backroundColor: nil,
                                 borderColor: nil,
                                 titleInactiveColor: .gr65,
                                 font: .poppins(10),
                                 iconModel: iconModel,
                                 height: 45)
            btn.borderRadius = 0
            btn.bordersWidth = 0
            btn.onTap = { [weak self] in
                self?.handleTap(item: item)
            }
            
            container.addSubview(btn)
            btn.match(.width, to: .width, of: container, withMultiplier: 0.33)
            if idx == 0 {
                btn.pinEdge(.left, to: .left, of: previousView)
            } else {
                btn.pinEdge(.left, to: .right, of: previousView)
            }
            btn.pinEdge(toSuperviewEdge: .top, withInset: 5)
            
            previousView = btn
            
            return btn
        }
        toggleSeleted(to: selectedItem)
    }
    
    private func handleTap(item: TabBarType) {
        guard item != selectedItem else {
            return
        }
        
        toggleSeleted(to: item)
        onItemTap?(item)
    }
    
    private func toggleSeleted(to item: TabBarType) {
        buttons.enumerated().forEach { idx, btn in
            if items[idx] == item {
                btn.isSelected = true
            } else {
                btn.isSelected = false
            }
        }
        selectedItem = item
    }
}

extension TabBarMenu: UIThemable {
    func apply(theme: UITheme) {
        backgroundColor = .themed.flerio.value
        container.backgroundColor = .themed.flerio.value
    }
}

extension TabBarType {
    var title: String {
        switch self {
        case .fax:
            return L10n.Main.fax
        case .faxHistory:
            return L10n.History.title
        case .settings:
            return L10n.Settings.title
        }
    }
    
    var selectedImage: UIThemed<UIImage> {
        switch self {
        case .fax:
            return .faxTabSelectedIcon
        case .faxHistory:
            return .historySelectedTabIcon
        case .settings:
            return .settingsSelectedTabIcon
        }
    }
    
    var unselectedImage: UIThemed<UIImage> {
        switch self {
        case .fax:
            return .faxTabUnselectedIcon
        case .faxHistory:
            return .historyUnselectedTabIcon
        case .settings:
            return .settingsUnselectedTabIcon
        }
    }
}

