//
//  SendFaxServiceImpl.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation
import Combine

final class SendFaxServiceImpl {
    private let api = ApiClient()
}

extension SendFaxServiceImpl: SendFaxService {
    func sendFax(
        pdfData: Data,
        previewData: Data,
        recipient: String,
        pagesCount: Int
    ) -> AnyPublisher<SendFaxResponse, NetworkError> {
        let endpoint = SendFaxEndpoint(pdfData: pdfData,
                                       previewData: previewData,
                                       recipient: recipient,
                                       pagesCount: pagesCount)
        return api.request(request: endpoint)
    }
}
