//
//  DocumentPickerFileType.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import MobileCoreServices
import UniformTypeIdentifiers

enum DocumentPickerFileType {
    case pdf
    case doc
    case docx
    
    static var allTypes: [DocumentPickerFileType] {
        [.pdf, .doc, .docx]
    }
    
    init?(fileUrl: URL) {
        switch fileUrl.pathExtension {
        case "doc":
            self = .doc
        case "docx":
            self = .docx
        case "pdf":
            self = .pdf
        default:
            return nil
        }
    }
}

extension DocumentPickerFileType {
    
    var asPickerFileType: String {
        switch self {
        case .pdf:
            if #available(iOS 15, *) {
                return UTType.pdf.identifier
            } else {
                return kUTTypePDF as String
            }
        case .doc:
            return "com.microsoft.word.doc"
        case .docx:
            return "org.openxmlformats.wordprocessingml.document"
        }
    }
}

