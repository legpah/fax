//
//  Font.swift
//  FaxApp
//
//  Created by Eugene on 18.04.2022.
//

import UIKit

extension UIFont {
    static func poppins(_ size: CGFloat, _ weight: FontWeight = .regular) -> UIFont {
        guard let font = UIFont(name: "Poppins-\(weight.rawValue)", size: size) else {
            assertionFailure("poppins fonts not found")
            return .systemFont(ofSize: size, weight: .regular)
        }
        return font
    }
}
