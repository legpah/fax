//
//  OnboardingViewController+TitleView.swift
//  FaxApp
//
//  Created by Eugene on 15.06.2022.
//

import UIKit

extension OnboardingViewControllerImpl {
    final class TitleView: UIView {
        private let titleLabel = Label(font: .poppins(24, .semiBold),
                                       alignment: .center)
        private let subtitleLabel = Label(font: .poppins(16, .light),
                                          alignment: .center)
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            setupAdjust(label: titleLabel)
            
            addSubview(titleLabel)
            addSubview(subtitleLabel)
            
            setDimension(.height, toSize: 92)
            
            titleLabel.pinEdgesToSuperviewEdges(excludingEdge: .bottom)
            
            subtitleLabel.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 6)
            subtitleLabel.pinEdges(toSuperviewEdges: [.left, .right])
        }
        
        func set(title: String, subtitle: String) {
            if titleLabel.text == nil {
                titleLabel.text = title
                subtitleLabel.text = subtitle
                return
            }
            
            let copyTitleLabel = Label(text: title,
                                       font: .poppins(24, .semiBold),
                                       alignment: .center)
            let copySubtitleLabel = Label(text: subtitle,
                                          font: .poppins(16, .light),
                                          alignment: .center)
            copyTitleLabel.alpha = 0
            copySubtitleLabel.alpha = 0
            
            setupAdjust(label: copyTitleLabel)
            
            UIView.performWithoutAnimation {
                addSubview(copyTitleLabel)
                addSubview(copySubtitleLabel)
                
                copyTitleLabel.pinEdgesToSuperviewEdges(excludingEdge: .bottom)
                
                copySubtitleLabel.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 6)
                copySubtitleLabel.pinEdges(toSuperviewEdges: [.left, .right])
                
                self.setNeedsLayout()
                self.layoutIfNeeded()
            }

            UIView.animate(withDuration: .animations.onboardingCarousel, animations: {
                copyTitleLabel.alpha = 1
                copySubtitleLabel.alpha = 1
                self.titleLabel.alpha = 0
                self.subtitleLabel.alpha = 0
            }, completion: { _ in
                
                UIView.performWithoutAnimation {
                    self.titleLabel.text = title
                    self.subtitleLabel.text = subtitle
                    self.titleLabel.alpha = 1
                    self.subtitleLabel.alpha = 1
                    copyTitleLabel.removeFromSuperview()
                    copySubtitleLabel.removeFromSuperview()
                }

            })
        }
        
        private func setupAdjust(label: UILabel) {
            label.adjustsFontSizeToFitWidth = true
            label.minimumScaleFactor = 0.5
        }
    }
}
