import Foundation
import UIKit
import VisionKit

protocol DocumentScannerPresenter: ViewOutput {
    func scanPicked(scan: VNDocumentCameraScan?)
    func dismissTapped()
    func showAlert(error: Error)
}

final class DocumentScannerPresenterImpl {
    
    private weak var router: DocumentScannerRouter?
    
    init(router: DocumentScannerRouter) {
        self.router = router
    }
    
}

extension DocumentScannerPresenterImpl: DocumentScannerPresenter {

    func scanPicked(scan: VNDocumentCameraScan?) {
        guard let scan = scan else {
            router?.closeScreen(images: nil)
            return
        }
        
        var compressedImages: [UIImage] = []
        for page in 0..<scan.pageCount {
            let compressedData = scan.imageOfPage(at: page)
                .resizeImage(targetSize: PDFConstants.pageSize)?
                .jpeg(.lowest)
            
            guard let compressedImage = UIImage(data: compressedData ?? Data()) else { continue }
            compressedImages.append(compressedImage)
        }

        router?.closeScreen(images: compressedImages)
    }
    
    func dismissTapped() {
        router?.closeScreen(images: nil)
    }
    
    func showAlert(error: Error) {
        router?.closeScreen(images: nil)
    }
    
}
