//
//  TableView.swift
//  FaxApp
//
//  Created by Eugene on 22.04.2022.
//

import UIKit

final class TableView: UITableView {
    init(registerCells: [UITableViewCell.Type],
         style: UITableView.Style = .plain) {
        super.init(frame: .zero, style: style)
        registerCells.forEach {
            self.register($0)
        }
        self.showsHorizontalScrollIndicator = false
        self.showsVerticalScrollIndicator = false
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension UITableView {
    func register<Cell: UITableViewCell>(_ type: Cell.Type) {
        self.register(type, forCellReuseIdentifier: type.identifier)
    }
}
