//
//  PaywallOpenRouterResolver.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import Foundation

extension CanOpenRouterResolverToken {
    static var paywallResolver: CanOpenScreenRouterResolver {
        PaywallOpenRouterResolver()
    }
}

fileprivate struct PaywallOpenRouterResolver: CanOpenScreenRouterResolver {
    func canOpen(router: ScreenRouter, from: ScreenRouter) -> Bool {
        switch RouterType(indentifiable: router.routerIdentifiable) {
        case .alertOptions,
                .pdfPreview:
            return true
        default:
            return false
        }
    }
}
