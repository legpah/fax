//
//  SendFaxDocumentCell.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit
import Combine

final class SendFaxDocumentsCell: UITableViewCell {
    
    private let button = InvisibleButton()
    private let deleteButton = IconButton(image: .deleteFile, imageSizeRule: .scale(x: 1, y: 1))
    private let previewContainer = UIView()
    private let previewImage = UIImageView()
    private let documentTitleLabel = Label(font: .poppins(16, .semiBold), lines: 3)
    private let pagesCountTitleLabel = Label(font: .poppins(12))
    
    private var model: SendFaxDocumentCellModel?
    private var cancellable: AnyCancellable?
    private var loadedImage: UIImage? {
        didSet { updateImage() }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        configureUI()
    }
    
    deinit {
        themeProvider.unregister(observer: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        subviews[2].isHidden = true
        applyReorderingImageControl(color: .themed.grawh.value)
        reorderControlImageView?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
    }
    
    override func prepareForReuse() {
        cancellable?.cancel()
        loadedImage = nil
        super.prepareForReuse()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        updateImage()
        applyReorderingImageControl(color: .themed.grawh.value)
    }
    
    private func configureUI() {
        previewImage.contentMode = .center
        addSubview(deleteButton)
        contentView.addSubview(previewContainer)
        previewContainer.addSubview(previewImage)
        contentView.addSubview(documentTitleLabel)
        contentView.addSubview(pagesCountTitleLabel)
        contentView.addSubview(button)
        
        previewImage.alpha = 0
        previewImage.contentMode = .center
        previewContainer.applyBordersShape(width: 1.5, radius: 5)
        previewContainer.layer.borderColor = UIColor.ds_gr8.cgColor
        
        button.onTap = { [weak self] in
            guard let self = self, let model = self.model else { return }
            model.onTap(model)()
        }
        
        deleteButton.onTap = { [weak self] in
            guard let self = self, let model = self.model else { return }
            model.onDeleteButtonTap(model)()
        }
        
        configureConstraints()
        
        themeProvider.register(observer: self)
    }
    
    private func configureConstraints() {
        button.pinEdges(toSuperviewEdges: [.left, .right])
        button.pinEdge(.top, to: .top, of: previewImage)
        button.pinEdge(.bottom, to: .bottom, of: previewImage)
        
        deleteButton.pinEdge(toSuperviewEdge: .left, withInset: 16)
        deleteButton.alignAxis(toSuperviewAxis: .y)
        deleteButton.match(.width, to: .height, of: deleteButton)
        deleteButton.match(.height, to: .height, of: self, withMultiplier: 0.22)
        
        previewContainer.pinEdges(toSuperviewEdges: [.top, .bottom], withInset: 12)
        previewContainer.pinEdge(.left, to: .right, of: deleteButton, withOffset: 24)
        previewContainer.match(.width, to: .height, of: previewContainer, withMultiplier: 0.72)
        
        previewImage.pinEdgesToSuperviewEdges(with: .symmetric(value: 1.5))
        
        documentTitleLabel.pinEdge(.top, to: .top, of: previewContainer)
        documentTitleLabel.pinEdge(.left, to: .right, of: previewContainer, withOffset: 11)
        documentTitleLabel.pinEdge(toSuperviewEdge: .right, withInset: 15)
        
        pagesCountTitleLabel.pinEdge(.top, to: .bottom, of: documentTitleLabel, withOffset: 3)
        pagesCountTitleLabel.pinEdge(.left, to: .right, of: previewContainer, withOffset: 11)
    }
    
    private func updateImage() {
        guard previewContainer.frame.size != .zero else {
            return
        }
        
        let animated = previewImage.image == nil
        let targetSize = CGSize(
            width: self.previewImage.frame.size.width + 3,
            height: self.previewImage.frame.size.height + 3
        )
        let image = loadedImage?.resizeImage(targetSize: targetSize)
        
        if animated {
            self.previewImage.alpha = 0
            self.previewImage.image = image
            UIView.animate(withDuration: .animations.defaultDuration) {
                self.previewImage.alpha = 1
            }
        } else {
            self.previewImage.image = image
        }
    }
}


extension SendFaxDocumentsCell: SetupableCell {
    func setup(model: SendFaxDocumentCellModel) {
        self.model = model
        documentTitleLabel.text = model.pdfObject.title
        pagesCountTitleLabel.text = model.pagesCountTitle
        cancellable = model.pdfObject.pdfPreview.sink { [weak self] image in
            self?.loadedImage = image
        }
    }
}

extension SendFaxDocumentsCell: UIThemable {
    func apply(theme: UITheme) {
        backgroundColor = .themed.tikruk.value
        previewContainer.backgroundColor = .themed.tikruk.value
        previewImage.backgroundColor = .themed.tikruk.value
        applyReorderingImageControl(color: .themed.grawh.value)
    }
}
