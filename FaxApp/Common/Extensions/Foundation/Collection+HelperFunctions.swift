//
//  Collection+HelperFunctions.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import Foundation

extension Collection {
    var isNotEmpty: Bool {
        !isEmpty
    }
}

extension Bool {
    var analyticValue: String { self ? "yes" : "no" }
}
