//
//  PaywallViewController.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import UIKit

protocol PaywallViewControllerInterface: AnyObject, PremiumItemsContainer {
    func setup(variation: PaywallViewVariation)
    func update(isLoading: Bool)
    func showInformView(_ model: GenericInformViewModel)
    func setupViewAccordingPremiumModel(cost: String)
}

final class PaywallViewControllerImpl: UIViewController {
    
    private let presenter: PaywallPresenter
    
    private let closeButton = Button(title: L10n.Paywall.close,
                                     titleColor: .gr16,
                                     bgColor: nil,
                                     font: .poppins(14, .semiBold))
    private let backButton = Button(title: L10n.Sending.back,
                                     titleColor: .tukesh,
                                     bgColor: nil,
                                     font: .poppins(14, .semiBold))
    private let restoreButton = Button(title: L10n.Paywall.restore,
                                       titleColor: .tukesh,
                                       bgColor: nil,
                                       font: .poppins(14, .semiBold))
    private var variantView: PaywallVariantView?
    private let continueButton = TrunButton(title: L10n.Paywall.continue,
                                            titleColor: .bw,
                                            backroundColor: .tukesh,
                                            borderColor: nil)
    private let privacyPolicy = DocumentsButton(text: L10n.Paywall.privacyPolicy)
    private let termsOfUse = DocumentsButton(text: L10n.Paywall.termsOfUse)
    private let loadingSnipper = ActivityIndicator(color: .bw)
    
    private var variation: PaywallViewVariation = .first
    private var variantStatusBarTheme: UITheme = .current {
        didSet { UIApplication.shared.setStatusBarTheme(variantStatusBarTheme) }
    }
    
    init(presenter: PaywallPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { .overFullScreen }
        set {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureSelf()
        presenter.viewDidLoad()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        UIApplication.shared.setStatusBarTheme(nil)
    }
    
    private func configureSelf() {
        view.layer.masksToBounds = true
        view.addSubview(closeButton)
        view.addSubview(backButton)
        view.addSubview(restoreButton)
        view.addSubview(continueButton)
        continueButton.addSubview(loadingSnipper)
        view.addSubview(privacyPolicy)
        view.addSubview(termsOfUse)
        
        backButton.isHidden = true
        
        continueButton.bordersWidth = 0
        
        closeButton.onTap = { [weak self] in
            self?.presenter.didTapDismiss()
        }
        restoreButton.onTap = { [weak self] in
            self?.presenter.didTapRestorePurchases()
        }
        continueButton.onTap = { [weak self] in
            self?.presenter.didTapContinueButton()
        }
        privacyPolicy.onTap = { [weak self] in
            self?.presenter.didTapPrivacyPolicy()
        }
        termsOfUse.onTap = { [weak self] in
            self?.presenter.didTapTermsOfUse()
        }
        
        configureConstraints()
        
        themeProvider.register(observer: self)
    }
    
    private func configureConstraints() {
        closeButton.pin(toTopLayoutGuideOf: self, withInset: 5)
        closeButton.pinEdge(toSuperviewEdge: .left, withInset: .layout.xInset)
        
        backButton.pin(toTopLayoutGuideOf: self, withInset: 5)
        backButton.pinEdge(toSuperviewEdge: .left, withInset: .layout.xInset)
        
        restoreButton.pin(toTopLayoutGuideOf: self, withInset: 5)
        restoreButton.pinEdge(toSuperviewEdge: .right, withInset: .layout.xInset)
        restoreButton.setDimension(.height, toSize: 21)
        
        continueButton.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        continueButton.pinEdge(.bottom, to: .top, of: privacyPolicy, withOffset: -16, relation: .equal)
        
        privacyPolicy.pin(toBottomLayoutGuideOf: self, withInset: 5)
        privacyPolicy.pinEdge(toSuperviewEdge: .left, withInset: .layout.xInset)
        
        termsOfUse.pin(toBottomLayoutGuideOf: self, withInset: 5)
        termsOfUse.pinEdge(toSuperviewEdge: .right, withInset: .layout.xInset)
        
        loadingSnipper.centerInSuperview()
    }
}

extension PaywallViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        view.backgroundColor = .themed.flerio.value
        switch variation {
        case .fourth:
            closeButton.titleColor = .init(value: .ds_wh.withAlphaComponent(0.5))
            restoreButton.titleColor = .init(value: .ds_wh.withAlphaComponent(0.5))
        case .fifth:
            closeButton.titleColor = .init(value: .ds_wh.withAlphaComponent(0.5))
            restoreButton.titleColor = .init(value: .ds_wh.withAlphaComponent(0.5))
            continueButton.titleColor = .init(value: .ds_pur)
            continueButton.backroundThemedColor = .init(value: .ds_wh)
        case .sixth:
            continueButton.titleColor = .ww
            restoreButton.titleColor = .gr16 
        default:
            break
        }
    }
}

extension PaywallViewControllerImpl: PaywallViewControllerInterface {
    var allPlansIsShowed: Bool {
        variantView?.itemsContainer.allPlansIsShowed ?? true
    }
    
    func allPlans(isShowed: Bool) {
        variantView?.itemsContainer.allPlans(isShowed: isShowed)
    }
    
    func setupViewAccordingPremiumModel(cost: String) {
        switch variation {
        case .sixth:
            guard let view = variantView as? PaywallSixthVariantView else { return }
            view.changeCostLabel(cost: cost)
        default:
            break
        }
    }
    
    func setup(variation: PaywallViewVariation) {
        self.variation = variation
        let size: PaywallSizeVariant = {
            if UIApplication.shared.windowFrame.height >= 896 {
                return .large
            } else if UIApplication.shared.windowFrame.height >= 812 {
                return .normal
            } else {
                return .small
            }
        }()
        switch variation {
        case .first:
            variantView = PaywallFirstVariantView(isSmall: size == .small)
        case .second:
            variantView = PaywallSecondVariantView(size: size)
        case .third:
            variantView = PaywallThirdVariantView(size: size)
        case .fourth:
            variantStatusBarTheme = .dark
            variantView = PaywallFourthVariantView(controller: self, size: size)
        case .fifth:
            variantStatusBarTheme = .dark
            privacyPolicy.titleColor = .init(value: .ds_wh)
            termsOfUse.titleColor = .init(value: .ds_wh)
            variantView = PaywallFifthVariantView(controller: self, size: size)
        case .sixth:
            let view = PaywallSixthVariantView(size: size, model: presenter.getModel())
            view.itemsContainer.allPlans(isShowed: false)
            backButton.onTap = { [weak self] in
                view.changePaywallPage(toSecond: false)
                self?.changePaywallPage(toSecond: false)
            }
            view.allPlansButton.onTap = { [weak self] in 
                view.changePaywallPage(toSecond: true)
                self?.changePaywallPage(toSecond: true)
                self?.presenter.didTapViewAllPlan()
            }
            
            variantView = view
        }
        guard let variantView = variantView else { return }
        switch variation {
        case .fourth:
            view.insertSubview(variantView, at: 0)
        default:
            view.addSubview(variantView)
        }
        
        variantView.alpha = 0
        variantView.pinEdge(.top, to: .bottom, of: restoreButton)
        variantView.pinEdges(toSuperviewEdges: [.left, .right])
        variantView.pinEdge(.bottom, to: .top, of: continueButton)
        view.layoutIfNeeded()
        apply(theme: .current)
        UIView.animate(withDuration: .animations.fastTransitionDuration, animations: {
            variantView.alpha = 1
        })
    }
    func setup(premiumItems: [PremiumItemModel]) {
        variantView?.itemsContainer.setup(premiumItems: premiumItems)
    }
    
    func updateSelected(item: PremiumItemModel) {
        variantView?.itemsContainer.updateSelected(item: item)
    }
    
    func useLightColors() {
        variantView?.itemsContainer.useLightColors()
    }
    
    func update(isLoading: Bool) {
        UIView.animate(withDuration: .animations.fastTransitionDuration) {
            self.continueButton.title = isLoading ? "" : L10n.Paywall.continue
            self.loadingSnipper.isAnimating = isLoading
        }
    }
    
    func changePaywallPage(toSecond: Bool) {
        UIView.animate(withDuration: .animations.fastTransitionDuration, animations: {
            self.backButton.isHidden = !toSecond
            self.closeButton.isHidden = toSecond
            self.variantView?.itemsContainer.allPlans(isShowed: toSecond)
        })
    }
    
    func showInformView(_ model: GenericInformViewModel) {
        variantStatusBarTheme = UITheme.current
        let informView = GenericInformView()
        informView.alpha = 0
        view.addSubview(informView)
        informView.updateView(model: model)
        informView.pin(toTopLayoutGuideOf: self)
        informView.pin(toBottomLayoutGuideOf: self)
        informView.pinEdges(toSuperviewEdges: [.left, .right])
        UIView.animate(withDuration: .animations.defaultDuration) {
            (self.variantView as? PaywallFourthVariantView)?.bgRadialImage.alpha = 0
            (self.variantView as? PaywallFifthVariantView)?.bgView.alpha = 0
            informView.alpha = 1
        }
    }
}
