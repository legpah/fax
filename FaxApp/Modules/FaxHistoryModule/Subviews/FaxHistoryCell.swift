//
//  FaxHistoryCell.swift
//  FaxApp
//
//  Created by Eugene on 29.04.2022.
//

import UIKit

final class FaxHistoryCell: UITableViewCell {
    
    private let previewImage = WebImageView()
    private let statusIcon = StatusIcon()
    private let statusDescriptionLabel = Label(font: .poppins(12))
    private let recepientLabel = Label(font: .poppins(16, .semiBold))
    private let sentTimeLabel = Label(font: .poppins(12), alignment: .right)
    private let filePreviewButton = InvisibleButton()
    private let errorTipButton = InvisibleButton()
    private let errorTipView = ErrorTipView()
    private let bottomBorder = UIView()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepareForReuse() {
        previewImage.cancelLoading()
        super.prepareForReuse()
    }
    
    private func configureSelf() {
        contentView.addSubview(previewImage)
        previewImage.addSubview(filePreviewButton)
        contentView.addSubview(statusIcon)
        contentView.addSubview(statusDescriptionLabel)
        contentView.addSubview(recepientLabel)
        contentView.addSubview(sentTimeLabel)
        contentView.addSubview(errorTipView)
        errorTipView.addSubview(errorTipButton)
        contentView.addSubview(bottomBorder)
        contentView.addSubview(errorTipButton)
        
        previewImage.applyBordersShape(width: 1, radius: 2)
        
        configureConstraints()
        
        themeProvider.register(observer: self)
    }
    
    private func configureConstraints() {
        previewImage.pinEdge(toSuperviewEdge: .left, withInset: 24)
        previewImage.pinEdges(toSuperviewEdges: [.top, .bottom], withInset: 12)
        previewImage.setDimension(.width, toSize: 50)
        
        filePreviewButton.pinEdgesToSuperviewEdges()
        
        statusIcon.pinEdge(.top, to: .top, of: previewImage)
        statusIcon.pinEdge(.left, to: .right, of: previewImage, withOffset: 16)
        
        statusDescriptionLabel.pinEdge(.top, to: .top, of: previewImage)
        statusDescriptionLabel.pinEdge(.left, to: .right, of: statusIcon, withOffset: 6)
        
        sentTimeLabel.pinEdge(.top, to: .top, of: previewImage)
        sentTimeLabel.pinEdge(toSuperviewEdge: .right, withInset: .layout.xInset)
        
        recepientLabel.alignAxis(.y, toSameAxisOf: previewImage)
        recepientLabel.pinEdge(.left, to: .right, of: previewImage, withOffset: 16)
        
        errorTipView.pinEdge(.top, to: .bottom, of: recepientLabel, withOffset: 4)
        errorTipView.pinEdge(.left, to: .right, of: previewImage, withOffset: 16)
        errorTipView.pinEdge(toSuperviewEdge: .right, withInset: .layout.xInset)
        
        errorTipButton.pinEdge(.top, to: .top, of: errorTipView)
        errorTipButton.pinEdge(.left, to: .right, of: previewImage, withOffset: 16)
        errorTipButton.pinEdges(toSuperviewEdges: [.right, .bottom])

        bottomBorder.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        bottomBorder.pinEdge(toSuperviewEdge: .bottom, withInset: 0)
        bottomBorder.setDimension(.height, toSize: 1)
    }
}

extension FaxHistoryCell: SetupableCell {
    func setup(model: FaxHistoryCellModel) {
        previewImage.setImage(url: URL(string: model.previewUrl))
        statusDescriptionLabel.text = model.statusDescription
        statusIcon.status = model.status
        recepientLabel.text = model.recipient
        sentTimeLabel.text = .converter.historyDate(timestamp: model.sentTimeTimestamp)
        switch model.status {
        case .sent:
            statusDescriptionLabel.color = .baboo
        case .delivering:
            statusDescriptionLabel.color = .yelyel
        case .error:
            statusDescriptionLabel.color = .redred
        }
        
        filePreviewButton.onTap = {
            model.onFilePreviewTap(model)
        }
        
        if model.status.isError {
            errorTipView.errorLabel.text = model.errorMessage
            errorTipView.isHidden = false
            errorTipButton.onTap = {
                model.onErrorTipTap(model)
            }
        } else {
            errorTipView.isHidden = true
            errorTipButton.onTap = nil
        }
    }
}

extension FaxHistoryCell: UIThemable {
    func apply(theme: UITheme) {
        bottomBorder.backgroundColor = .themed.xero.value
        backgroundColor = .themed.flerio.value
        previewImage.applyBorders(color: .w1w1)
    }
}
