//
//  FAQPresenter.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import Foundation

protocol FAQPresenter: ViewOutput {
    func didTapDismiss()
}

final class FAQPresenterImpl {
    
    weak var view: FAQViewController?
    
    private weak var router: FAQRouter?
    private let cellModelsProvider: FAQCellModelsProvider
    private var cellModels: [FAQCellModel] = []
    private let analytics: AnalyticsService
    
    init(router: FAQRouter,
         analytics: AnalyticsService = AnalyticsServiceImpl.shared,
         cellModelsProvider: FAQCellModelsProvider = FAQCellModelsProviderImpl()) {
        self.router = router
        self.analytics = analytics
        self.cellModelsProvider = cellModelsProvider
    }
    
    private func toggleExpandState(_ model: FAQCellModel) {
        guard let index = cellModels.firstIndex(where: { $0.question == model.question }) else {
            return
        }
        var mutated = cellModels[index]
        mutated.toogleExpanded()
        cellModels[index] = mutated
        reloadCells()
        
        if mutated.isExpanded {
            analytics.send(
                AnalyticsEventImpl(
                    name: "open_question",
                    parameters: ["name": mutated.question]
                )
            )
        }
    }
    
    private func reloadCells() {
        view?.faqSource.reload(section: .init(), with: cellModels, animated: true)
    }
}

extension FAQPresenterImpl: FAQPresenter {
    func viewDidLoad() {
        self.cellModels = cellModelsProvider.cellModels(onExpandTap: { model in
            return { [weak self] in
                self?.toggleExpandState(model)
            }
        })
        reloadCells()
    }
    
    func didTapDismiss() {
        router?.closeLast(animated: true, completion: nil)
    }
}
