//
//  PaywallThirdVariantView.swift
//  FaxApp
//
//  Created by Eugene on 14.07.2022.
//

import Foundation
import UIKit

final class PaywallThirdVariantView: UIView, PaywallVariantView {
    var itemsContainer: PremiumItemsContainer {
        premiumItemsView
    }
    
    private let faxImage = ImageView(themedImage: .paywallThirdFax)
    private lazy var titleLabel = Label(
        text: L10n.Paywall.Third.title,
        font: .poppins(size.value(36, 24, 24), .semiBold),
        alignment: .center,
        lines: 0
    )
    
    private let checkMarksContainer = UIView()
    private lazy var firstCheckMarkView = CheckMarkItemView(
        size: size,
        title: L10n.Paywall.Third.Check.First.title
    )
    private lazy var secondCheckMarkView = CheckMarkItemView(
        size: size,
        title: L10n.Paywall.Third.Check.Second.title
    )
    private lazy var thirdCheckMarkView = CheckMarkItemView(
        size: size,
        title: L10n.Paywall.Third.Check.Third.title
    )
    
    private let premiumItemsView = PremiumItemsContainerImpl()
    
    private let size: PaywallSizeVariant
    
    init(size: PaywallSizeVariant) {
        self.size = size
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSelf() {
        addSubview(faxImage)
        addSubview(titleLabel)
        addSubview(checkMarksContainer)
        checkMarksContainer.addSubview(firstCheckMarkView)
        checkMarksContainer.addSubview(secondCheckMarkView)
        checkMarksContainer.addSubview(thirdCheckMarkView)
        addSubview(premiumItemsView)
        
        configureConstraints()
    }
    
    private func configureConstraints() {
        faxImage.pinEdge(toSuperviewEdge: .top, withInset: size.value(19, 19, 4))
        faxImage.alignAxis(toSuperviewAxis: .x)
        faxImage.setDimensions(
            to: size.value(
                .symmetric(127),
                .symmetric(103),
                .symmetric(75)
            )
        )
        
        titleLabel.pinEdge(.top, to: .bottom, of: faxImage, withOffset: size.value(32, 24, 24))
        titleLabel.alignAxis(toSuperviewAxis: .x)
        
        checkMarksContainer.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: size.value(40, 48, 32))
        checkMarksContainer.pinEdges(toSuperviewEdges: [.left, .right], withInset: size.value(60, 56, 56))
        
        firstCheckMarkView.pinEdges(toSuperviewEdges: [.top, .left, .right])
        
        secondCheckMarkView.pinEdge(.top, to: .bottom, of: firstCheckMarkView, withOffset: 16)
        secondCheckMarkView.pinEdges(toSuperviewEdges: [.left, .right])
        
        thirdCheckMarkView.pinEdge(.top, to: .bottom, of: secondCheckMarkView, withOffset: 16)
        thirdCheckMarkView.pinEdges(toSuperviewEdges: [.left, .right])
        thirdCheckMarkView.pinEdge(toSuperviewEdge: .bottom)
        
        premiumItemsView.pinEdge(.top, to: .bottom, of: checkMarksContainer, withOffset: size.value(51, 43, 28))
        premiumItemsView.pinEdges(toSuperviewEdges: [.left, .right])
        premiumItemsView.pinEdge(toSuperviewEdge: .bottom, withInset: 10, relation: .greaterThanOrEqual)
    }
}

extension PaywallThirdVariantView {
    fileprivate final class CheckMarkItemView: UIView {
        
        private let size: PaywallSizeVariant
        private let checkmarkImage = ImageView(themedImage: .paywallCheckmark)
        private lazy var titleLabel = Label(
            font: .poppins(size.value(20, 16, 16)),
            lines: 0
        )
        
        init(size: PaywallSizeVariant, title: String) {
            self.size = size
            super.init(frame: .zero)
            titleLabel.text = title
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(checkmarkImage)
            addSubview(titleLabel)
            
            checkmarkImage.pinEdge(toSuperviewEdge: .left)
            checkmarkImage.alignAxis(.y, toSameAxisOf: titleLabel)
            
            titleLabel.pinEdge(.left, to: .right, of: checkmarkImage, withOffset: size.value(12, 11, 11))
            titleLabel.pinEdgesToSuperviewEdges(excludingEdge: .left)
        }
    }
}

