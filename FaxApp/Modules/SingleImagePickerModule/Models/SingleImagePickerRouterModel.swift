//
//  SingleImagePickerRouterModel.swift
//  FaxApp
//
//  Created by Eugene on 19.04.2022.
//

import UIKit

struct SingleImagePickerRouterModel {
    let source: SingleImagePickerSource
    let onPickImage: IClosure<UIImage>
    
    init(source: SingleImagePickerSource,
         onPickImage: @escaping IClosure<UIImage>) {
        self.source = source
        self.onPickImage = onPickImage
    }
}

enum SingleImagePickerSource {
    case photo
    case camera
    case scanner
    
    var asNativeSouce: UIImagePickerController.SourceType {
        switch self {
        case .photo:
            return .photoLibrary
        case .camera, .scanner:
            return .camera
        }
    }
}

