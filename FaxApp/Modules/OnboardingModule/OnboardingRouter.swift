//
//  OnboardingRouter.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import UIKit

final class OnboardingRouter: BaseScreenRouter, ScreenRouter, PaywallRouter {
    
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.onboarding
    let presentationType: ScreenPresentationType = .modal
    
    private lazy var _screen: UIViewController = {
        let paywallScreen = PaywallRouterImpl.factory(router: self)
        let presenter = OnboardingPresenterImpl(paywallScreen: paywallScreen)
        let view = OnboardingViewControllerImpl(presenter: presenter)
        presenter.view = view
        return view
    }()
}
