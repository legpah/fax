import UIKit

class AdjustTitleViewController: UIViewController {
    
    private let titleLabel = Label(font: UIFont.poppins(24, .semiBold))
    
    init() {
        super.init(nibName: nil, bundle: nil)
        setupAdjustLabelToTitleView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var title: String? {
        get { titleLabel.text }
        set { titleLabel.text = newValue }
    }
    
    func setupAdjustLabelToTitleView() {
        titleLabel.adjustsFontSizeToFitWidth = true
        titleLabel.minimumScaleFactor = 0.5
        navigationItem.titleView = titleLabel
    }
    
}
