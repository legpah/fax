// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

#if os(macOS)
  import AppKit
#elseif os(iOS)
  import UIKit
#elseif os(tvOS) || os(watchOS)
  import UIKit
#endif
#if canImport(SwiftUI)
  import SwiftUI
#endif

// Deprecated typealiases
@available(*, deprecated, renamed: "ColorAsset.Color", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetColorTypeAlias = ColorAsset.Color
@available(*, deprecated, renamed: "ImageAsset.Image", message: "This typealias will be removed in SwiftGen 7.0")
internal typealias AssetImageTypeAlias = ImageAsset.Image

// swiftlint:disable superfluous_disable_command file_length implicit_return

// MARK: - Asset Catalogs

// swiftlint:disable identifier_name line_length nesting type_body_length type_name
internal enum Asset {
  internal static let accentColor = ColorAsset(name: "AccentColor")
  internal static let arrowDownDark = ImageAsset(name: "arrow_down_dark")
  internal static let arrowDownLight = ImageAsset(name: "arrow_down_light")
  internal static let arrowRightDark = ImageAsset(name: "arrow_right_dark")
  internal static let arrowRightLight = ImageAsset(name: "arrow_right_light")
  internal static let arrowUpDark = ImageAsset(name: "arrow_up_dark")
  internal static let arrowUpLight = ImageAsset(name: "arrow_up_light")
  internal static let checkmarkGreen = ImageAsset(name: "checkmark_green")
  internal static let contactPersonDark = ImageAsset(name: "contact_person_dark")
  internal static let contactPersonLight = ImageAsset(name: "contact_person_light")
  internal static let contactUsDark = ImageAsset(name: "contact_us_dark")
  internal static let contactUsLight = ImageAsset(name: "contact_us_light")
  internal static let crestInputDark = ImageAsset(name: "crest_input_dark")
  internal static let crestInputLight = ImageAsset(name: "crest_input_light")
  internal static let crestRed = ImageAsset(name: "crest_red")
  internal static let deleteFile = ImageAsset(name: "delete_file")
  internal static let dotsThreeHorizontalDark = ImageAsset(name: "dots_three_horizontal_dark")
  internal static let dotsThreeHorizontalLight = ImageAsset(name: "dots_three_horizontal_light")
  internal static let emptyFaxes = ImageAsset(name: "empty_faxes")
  internal static let exclamationTriangle = ImageAsset(name: "exclamation_triangle")
  internal static let faqQuestionDark = ImageAsset(name: "faq_question_dark")
  internal static let faqQuestionLight = ImageAsset(name: "faq_question_light")
  internal static let faxTabSelectedDark = ImageAsset(name: "fax_tab_selected_dark")
  internal static let faxTabSelectedLight = ImageAsset(name: "fax_tab_selected_light")
  internal static let faxTabUnselectedDark = ImageAsset(name: "fax_tab_unselected_dark")
  internal static let faxTabUnselectedLight = ImageAsset(name: "fax_tab_unselected_light")
  internal static let historyTabSelectedDark = ImageAsset(name: "history_tab_selected_dark")
  internal static let historyTabSelectedLight = ImageAsset(name: "history_tab_selected_light")
  internal static let historyTabUnselectedDark = ImageAsset(name: "history_tab_unselected_dark")
  internal static let historyTabUnselectedLight = ImageAsset(name: "history_tab_unselected_light")
  internal static let imagePlaceholder = ImageAsset(name: "image_placeholder")
  internal static let launchImage = ImageAsset(name: "launch_image")
  internal static let noInternetConnection = ImageAsset(name: "no_internet_connection")
  internal static let notificationsSettingsDark = ImageAsset(name: "notifications_settings_dark")
  internal static let notificationsSettingsLight = ImageAsset(name: "notifications_settings_light")
  internal static let onboarding1 = ImageAsset(name: "onboarding_1")
  internal static let onboarding2 = ImageAsset(name: "onboarding_2")
  internal static let onboarding3 = ImageAsset(name: "onboarding_3")
  internal static let onboarding4 = ImageAsset(name: "onboarding_4")
  internal static let onboarding5 = ImageAsset(name: "onboarding_5")
  internal static let paywallCheckmarkDark = ImageAsset(name: "paywall_checkmark_dark")
  internal static let paywallCheckmarkLight = ImageAsset(name: "paywall_checkmark_light")
  internal static let paywallDeliveryTracking = ImageAsset(name: "paywall_deliveryTracking")
  internal static let paywallFaxSentDark = ImageAsset(name: "paywall_faxSent_dark")
  internal static let paywallFaxSentLight = ImageAsset(name: "paywall_faxSent_light")
  internal static let paywallFifthBg = ImageAsset(name: "paywall_fifth_bg")
  internal static let paywallFourthCancelDark = ImageAsset(name: "paywall_fourth_cancel_dark")
  internal static let paywallFourthCancelLight = ImageAsset(name: "paywall_fourth_cancel_light")
  internal static let paywallFourthFaxDark = ImageAsset(name: "paywall_fourth_fax_dark")
  internal static let paywallFourthFaxLight = ImageAsset(name: "paywall_fourth_fax_light")
  internal static let paywallFourthLockDark = ImageAsset(name: "paywall_fourth_lock_dark")
  internal static let paywallFourthLockLight = ImageAsset(name: "paywall_fourth_lock_light")
  internal static let paywallFourthRadialBgDark = ImageAsset(name: "paywall_fourth_radial_bg_dark")
  internal static let paywallFourthRadialBgLight = ImageAsset(name: "paywall_fourth_radial_bg_light")
  internal static let paywallImageDark = ImageAsset(name: "paywall_image_dark")
  internal static let paywallImageLight = ImageAsset(name: "paywall_image_light")
  internal static let paywallNoLimit = ImageAsset(name: "paywall_noLimit")
  internal static let paywallReverseKeyDark = ImageAsset(name: "paywall_reverse_key_dark")
  internal static let paywallReverseKeyLight = ImageAsset(name: "paywall_reverse_key_light")
  internal static let paywallStarDark = ImageAsset(name: "paywall_star_dark")
  internal static let paywallStarLight = ImageAsset(name: "paywall_star_light")
  internal static let paywallStarWhiteDark = ImageAsset(name: "paywall_star_white_dark")
  internal static let paywallStarWhiteLight = ImageAsset(name: "paywall_star_white_light")
  internal static let paywallThirdFaxDark = ImageAsset(name: "paywall_third_fax_dark")
  internal static let paywallThirdFaxLight = ImageAsset(name: "paywall_third_fax_light")
  internal static let paywallWorldwide = ImageAsset(name: "paywall_worldwide")
  internal static let phoneDark = ImageAsset(name: "phone_dark")
  internal static let phoneLight = ImageAsset(name: "phone_light")
  internal static let plusDark = ImageAsset(name: "plus_dark")
  internal static let plusInactiveDark = ImageAsset(name: "plus_inactive_dark")
  internal static let plusInactiveLight = ImageAsset(name: "plus_inactive_light")
  internal static let plusLight = ImageAsset(name: "plus_light")
  internal static let premiumDark = ImageAsset(name: "premium_dark")
  internal static let premiumLight = ImageAsset(name: "premium_light")
  internal static let privacyPolicyDark = ImageAsset(name: "privacy_policy_dark")
  internal static let privacyPolicyLight = ImageAsset(name: "privacy_policy_light")
  internal static let purchaseSuccessDark = ImageAsset(name: "purchase_success_dark")
  internal static let purchaseSuccessLight = ImageAsset(name: "purchase_success_light")
  internal static let questionErrorDark = ImageAsset(name: "question_error_dark")
  internal static let questionErrorLight = ImageAsset(name: "question_error_light")
  internal static let rateAppDark = ImageAsset(name: "rate_app_dark")
  internal static let rateAppLight = ImageAsset(name: "rate_app_light")
  internal static let readyToGo = ImageAsset(name: "readyToGo")
  internal static let searchDark = ImageAsset(name: "search_dark")
  internal static let searchLight = ImageAsset(name: "search_light")
  internal static let sendFaxDone = ImageAsset(name: "send_fax_done")
  internal static let sendFaxEmptyDocumentsDark = ImageAsset(name: "send_fax_empty_documents_dark")
  internal static let sendFaxEmptyDocumentsLight = ImageAsset(name: "send_fax_empty_documents_light")
  internal static let settingsTabSelectedDark = ImageAsset(name: "settings_tab_selected_dark")
  internal static let settingsTabSelectedLight = ImageAsset(name: "settings_tab_selected_light")
  internal static let settingsTabUnselectedDark = ImageAsset(name: "settings_tab_unselected_dark")
  internal static let settingsTabUnselectedLight = ImageAsset(name: "settings_tab_unselected_light")
  internal static let somethingWentWrong = ImageAsset(name: "something_went_wrong")
  internal static let subscriptionExpiredDark = ImageAsset(name: "subscription_expired_dark")
  internal static let subscriptionExpiredLight = ImageAsset(name: "subscription_expired_light")
  internal static let switchToDarkTheme = ImageAsset(name: "switch_to_dark_theme")
  internal static let switchToLightTheme = ImageAsset(name: "switch_to_light_theme")
  internal static let wrongPhone = ImageAsset(name: "wrong_phone")
}
// swiftlint:enable identifier_name line_length nesting type_body_length type_name

// MARK: - Implementation Details

internal final class ColorAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Color = NSColor
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Color = UIColor
  #endif

  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  internal private(set) lazy var color: Color = {
    guard let color = Color(asset: self) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }()

  #if os(iOS) || os(tvOS)
  @available(iOS 11.0, tvOS 11.0, *)
  internal func color(compatibleWith traitCollection: UITraitCollection) -> Color {
    let bundle = BundleToken.bundle
    guard let color = Color(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load color asset named \(name).")
    }
    return color
  }
  #endif

  #if canImport(SwiftUI)
  @available(iOS 13.0, tvOS 13.0, watchOS 6.0, macOS 10.15, *)
  internal private(set) lazy var swiftUIColor: SwiftUI.Color = {
    SwiftUI.Color(asset: self)
  }()
  #endif

  fileprivate init(name: String) {
    self.name = name
  }
}

internal extension ColorAsset.Color {
  @available(iOS 11.0, tvOS 11.0, watchOS 4.0, macOS 10.13, *)
  convenience init?(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSColor.Name(asset.name), bundle: bundle)
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

#if canImport(SwiftUI)
@available(iOS 13.0, tvOS 13.0, watchOS 6.0, macOS 10.15, *)
internal extension SwiftUI.Color {
  init(asset: ColorAsset) {
    let bundle = BundleToken.bundle
    self.init(asset.name, bundle: bundle)
  }
}
#endif

internal struct ImageAsset {
  internal fileprivate(set) var name: String

  #if os(macOS)
  internal typealias Image = NSImage
  #elseif os(iOS) || os(tvOS) || os(watchOS)
  internal typealias Image = UIImage
  #endif

  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, macOS 10.7, *)
  internal var image: Image {
    let bundle = BundleToken.bundle
    #if os(iOS) || os(tvOS)
    let image = Image(named: name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    let name = NSImage.Name(self.name)
    let image = (bundle == .main) ? NSImage(named: name) : bundle.image(forResource: name)
    #elseif os(watchOS)
    let image = Image(named: name)
    #endif
    guard let result = image else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }

  #if os(iOS) || os(tvOS)
  @available(iOS 8.0, tvOS 9.0, *)
  internal func image(compatibleWith traitCollection: UITraitCollection) -> Image {
    let bundle = BundleToken.bundle
    guard let result = Image(named: name, in: bundle, compatibleWith: traitCollection) else {
      fatalError("Unable to load image asset named \(name).")
    }
    return result
  }
  #endif

  #if canImport(SwiftUI)
  @available(iOS 13.0, tvOS 13.0, watchOS 6.0, macOS 10.15, *)
  internal var swiftUIImage: SwiftUI.Image {
    SwiftUI.Image(asset: self)
  }
  #endif
}

internal extension ImageAsset.Image {
  @available(iOS 8.0, tvOS 9.0, watchOS 2.0, *)
  @available(macOS, deprecated,
    message: "This initializer is unsafe on macOS, please use the ImageAsset.image property")
  convenience init?(asset: ImageAsset) {
    #if os(iOS) || os(tvOS)
    let bundle = BundleToken.bundle
    self.init(named: asset.name, in: bundle, compatibleWith: nil)
    #elseif os(macOS)
    self.init(named: NSImage.Name(asset.name))
    #elseif os(watchOS)
    self.init(named: asset.name)
    #endif
  }
}

#if canImport(SwiftUI)
@available(iOS 13.0, tvOS 13.0, watchOS 6.0, macOS 10.15, *)
internal extension SwiftUI.Image {
  init(asset: ImageAsset) {
    let bundle = BundleToken.bundle
    self.init(asset.name, bundle: bundle)
  }

  init(asset: ImageAsset, label: Text) {
    let bundle = BundleToken.bundle
    self.init(asset.name, bundle: bundle, label: label)
  }

  init(decorative asset: ImageAsset) {
    let bundle = BundleToken.bundle
    self.init(decorative: asset.name, bundle: bundle)
  }
}
#endif

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
