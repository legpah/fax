//
//  CountryPhonesListRouter.swift
//  FaxApp
//
//  Created by Eugene on 27.04.2022.
//

import Foundation

import UIKit

protocol CountryPhonesListRouter: AnyObject {
    func closeScreen(selectedCounty: CountryPhone?)
}

final class CountryPhonesListRouterImpl: BaseScreenRouter, ScreenRouter {
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.countryPhonesList
    let presentationType: ScreenPresentationType = .modal
    
    private let onSelectCountry: IClosure<CountryPhone>
    
    private lazy var _screen: UIViewController = {
        let presenter = CountryPhonesListPresenterImpl(
            router: self
        )
        let view = CountryPhonesListViewControllerImpl(
            presenter: presenter
        )
        presenter.view = view
        return NavigationController(rootViewController: view)
    }()
    
    init(onSelectCountry: @escaping IClosure<CountryPhone>) {
        self.onSelectCountry = onSelectCountry
    }
}

extension CountryPhonesListRouterImpl: CountryPhonesListRouter {
    func closeScreen(selectedCounty: CountryPhone?) {
        closeLast(animated: true) { [onSelectCountry] in
            if let selectedCounty = selectedCounty {
                onSelectCountry(selectedCounty)
            }
        }
    }
}
