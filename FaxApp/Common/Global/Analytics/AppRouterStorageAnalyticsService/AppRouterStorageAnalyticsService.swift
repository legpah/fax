//
//  AppRouterStorageAnalyticsService.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation
import Combine

final class AppRouterStorageAnalyticsService: AppRoutersStorageListener {
    let push: PassthroughSubject<ScreenRouter, Never> = .init()
    let dismiss: PassthroughSubject<ScreenRouter, Never> = .init()
    
    private let analytics: AnalyticsService
    private var cancelBag = Set<AnyCancellable>()
    
    init(analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.analytics = analytics
        dismiss.sink { [weak self] router in
            self?.sendCloseEvent(router: router)
        }.store(in: &cancelBag)
    }
    
    private func sendCloseEvent(router: ScreenRouter) {
//        analytics.send(AnalyticsEventImpl(name: "screen_closed", parameters: [
//            "screen_type": router.routerIdentifiable.identifier
//        ]))
    }
}
