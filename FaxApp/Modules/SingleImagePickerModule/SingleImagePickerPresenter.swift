//
//  SingleImagePickerPresenter.swift
//  FaxApp
//
//  Created by Eugene on 19.04.2022.
//

import Foundation
import UIKit

protocol SingleImagePickerPresenter: ViewOutput {
    func imagePicked(image: UIImage?)
    
    func dismissTapped()
}

final class SingleImagePickerPresenterImpl {
    
    weak var view: SingleImagePickerViewController?
    
    private weak var router: SingleImagePickerRouter?
    
    init(router: SingleImagePickerRouter) {
        self.router = router
    }
}

extension SingleImagePickerPresenterImpl: SingleImagePickerPresenter {
    func imagePicked(image: UIImage?) {
        guard let image = image else {
            router?.closeScreen(image: nil)
            return
        }
        
        
        let compressedData = image
            .resizeImage(targetSize: PDFConstants.pageSize)?
            .jpeg(.lowest)
        
        let compressedImage = UIImage(data: compressedData ?? Data())
        
        router?.closeScreen(image: compressedImage)
    }
    
    func dismissTapped() {
        router?.closeScreen(image: nil)
    }
}
