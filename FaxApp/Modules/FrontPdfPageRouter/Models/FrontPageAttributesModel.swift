//
//  FrontPageAttributesModel.swift
//  FaxApp
//
//  Created by Eugene on 18.04.2022.
//

import Foundation

struct FrontPageAttributesModel: Equatable {
    let title: String
    let from: String
    let contact: String
    let message: String
    let theme: UITheme
}
