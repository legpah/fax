//
//  AssertDebug.swift
//  FaxApp
//
//  Created by Eugene on 19.04.2022.
//

import Foundation

func assertionFailureDebug(_ message: @autoclosure () -> String = String(),
                           file: StaticString = #file,
                           line: UInt = #line) {
    #if DEBUG
    assertionFailure(message(), file: file, line: line)
    #endif
}
