//
//  ContactsPickerViewController.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import ContactsUI

final class ContactsPickerViewControllerImpl: CNContactPickerViewController {
    
    private weak var router: CloseRouterCoordinator?
    private weak var moduleOuput: ContactsPickerModuleOuput?
    private var selectedPhone: String?
    
    init(router: CloseRouterCoordinator,
         moduleOuput: ContactsPickerModuleOuput?) {
        self.router = router
        self.moduleOuput = moduleOuput
        super.init(nibName: nil, bundle: nil)
        displayedPropertyKeys = [CNContactPhoneNumbersKey]
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { .fullScreen }
        set {}
    }
    
    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        let innnerCompletion = { [router, moduleOuput, selectedPhone] in
            router?.lastClosed()
            if let selectedPhone = selectedPhone {
                moduleOuput?.didPickContact(phoneNumber: selectedPhone)
            }
            completion?()
        }
        
        super.dismiss(animated: flag, completion: innnerCompletion)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        confugureSelf()
    }
    
    private func confugureSelf() {
        self.delegate = self
        themeProvider.register(observer: self)
    }
}

extension ContactsPickerViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        overrideUserInterfaceStyle = theme.asSystemTheme
    }
}

extension ContactsPickerViewControllerImpl: CNContactPickerDelegate {
    func contactPicker(_ picker: CNContactPickerViewController,
                       didSelect contactProperty: CNContactProperty) {
        guard let phone = contactProperty.value as? CNPhoneNumber else {
            return
        }
        selectedPhone = phone.stringValue
    }
}
