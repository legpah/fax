//
//  Firebase.swift
//  FaxApp
//
//  Created by Eugene on 17.05.2022.
//

import Firebase
import FirebaseAnalytics
import FirebaseCrashlytics
import Adapty

final class Fir {
    
    static let shared = Fir()
    private let uidProvider: UserUIDProvider = UserUIDProviderImpl.shared
    
    func configure() {
        FirebaseApp.configure()
        Crashlytics.crashlytics().setUserID(uidProvider.uuid)
        Crashlytics.crashlytics().sendUnsentReports()
        
        guard let id = Analytics.appInstanceID() else {
            return
        }
        Adapty.updateAttribution(["fir_id": id], source: .custom)
    }
}
