//
//  Logger.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import Foundation

protocol Logger {
    func log(_ text: @autoclosure () -> String)
}
