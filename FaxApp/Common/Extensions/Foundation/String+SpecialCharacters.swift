//
//  String+SpecialCharacters.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import Foundation

public extension String {
    
    /// Неразрывный пробел.
    static var nbsp: String { "\u{00A0}" }
    
    /// Соединитель слов
    static var wjnr = { "\u{2060}" }
    
    /// Дефис
    static var hyphen: String { "\u{2010}" }
    
    /// Тире.
    static var emDash: String { "\u{2014}" }
    
    /// Неразрывный дефис.
    static var noBreakHyphen: String { "\u{2011}" }
    
    /// <<
    static var leftAngleQuotes: String { "\u{00AB}" }
    
    /// >>
    static var rightAngleQuotes: String { "\u{00BB}" }
    
    /// `<<String>>`
    var quoted: String {
        return "\(String.leftAngleQuotes)\(self)\(String.rightAngleQuotes)"
    }
}
