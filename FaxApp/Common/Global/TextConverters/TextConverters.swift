//
//  TextConverters.swift
//  FaxApp
//
//  Created by Eugene on 29.04.2022.
//

import Foundation

enum TextConvertersTag {}

extension String {
    static let converter = TextConvertersTag.self
}
