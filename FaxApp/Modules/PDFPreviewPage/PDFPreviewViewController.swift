//
//  PDFPreviewViewController.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit
import PDFKit

protocol PDFPreviewViewController: AnyObject {
    func updatePdf(pdfObject: PDFObject)
}

final class PDFPreviewViewControllerImpl: AdjustTitleViewController {
    
    private let presenter: PDFPreviewPresenter
    
    private let backBarButton = TextBarButton(text: L10n.Sending.back, position: .left)
    private let pageNumbersBarView = PageNumbersView()
    private let pdfView = PDFUIView()
    private let editFontPageView = EditFrontPageView()
    private let showEditFrontPage: Bool
    
    
    init(showEditFrontPage: Bool,
         presenter: PDFPreviewPresenter) {
        self.presenter = presenter
        self.showEditFrontPage = showEditFrontPage
        super.init()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { .overFullScreen }
        set {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apply(theme: .current)
        presenter.viewWillAppear()
    }
    
    private func configureUI() {
        view.addSubview(pdfView)
        if showEditFrontPage {
            view.addSubview(editFontPageView)
        }
        
        configureBackBarButton()
        configureRightBarText()
        configurePdfView()
        configureEditFrontPageBottomView()
        
        themeProvider.register(observer: self)
    }
    
    private func configureRightBarText() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: pageNumbersBarView)
    }
    
    private func configureBackBarButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backBarButton)
        backBarButton.onTap = { [weak self] in
            self?.presenter.backButtonTapped()
        }
    }
    
    private func configurePdfView() {
        pdfView.pin(toTopLayoutGuideOf: self)
        pdfView.pinEdges(toSuperviewEdges: [.left, .right])
        if showEditFrontPage {
            pdfView.pinEdge(.bottom, to: .top, of: editFontPageView)
        } else {
            pdfView.pin(toBottomLayoutGuideOf: self)
        }
        pdfView.onPageNumberChange = { [weak self] number in
            guard let self = self else { return }
            let text = "\(number) \(L10n.Main.of) \(self.pdfView.document?.pageCount ?? 0)"
            self.pageNumbersBarView.text = text
        }
        pdfView.onPdfLoad = { [weak self] in
            guard let self = self else { return }
            self.fixPdfView()
        }
    }
    
    private func configureEditFrontPageBottomView() {
        guard showEditFrontPage else { return }
        editFontPageView.pin(toBottomLayoutGuideOf: self)
        editFontPageView.pinEdges(toSuperviewEdges: [.left, .right])
        editFontPageView.onEditTap = { [weak self] in
            self?.presenter.didTapEditFrontPage()
        }
    }
    
    private func fixPdfView() {
        pdfView.scrollView?.showsVerticalScrollIndicator = false
        pdfView.scrollView?.alwaysBounceVertical = false
        pdfView.scrollToTop(animated: false)
        if (pdfView.document?.pageCount ?? 0) == 1 {
            pdfView.scrollView?.isUserInteractionEnabled = false
        }
    }
}

extension PDFPreviewViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        pdfView.iterateRecursiceSubviews(action: {
            $0.backgroundColor = .themed.flerio.value
        })
        view.backgroundColor = .themed.flerio.value
        applyNavigationBarColor(.flerio)
    }
}

extension PDFPreviewViewControllerImpl: PDFPreviewViewController {
    func updatePdf(pdfObject: PDFObject) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) { [weak self] in
            self?.pdfView.document = pdfObject.pdfDocument
        }
    }
}

extension PDFPreviewViewControllerImpl {
    final class PageNumbersView: UIView {
        var text: String? {
            get { label.text }
            set { label.text = newValue }
        }
        
        private let label = Label(font: .poppins(14, .semiBold), color: .gr16)
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            setDimensions(to: CGSize(width: 50, height: 20))
            addSubview(label)
            label.centerInSuperview()
        }
    }
}


extension UIView {
    func iterateRecursiceSubviews(action: @escaping IClosure<UIView>) {
        subviews.forEach {
            action($0)
            $0.iterateRecursiceSubviews(action: action)
        }
    }
}
