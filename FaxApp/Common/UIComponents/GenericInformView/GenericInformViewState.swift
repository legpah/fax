//
//  GenericInformViewState.swift
//  FaxApp
//
//  Created by Eugene on 24.05.2022.
//

import UIKit

struct GenericInformViewModel {
    
    let style: Style
    let onTap: VoidClosure
    
    init(style: Style,
         onTap: @escaping VoidClosure) {
        self.style = style
        self.onTap = onTap
    }
    
    enum Style {
        case tiny(imageLight: UIImage,
                  imageDark: UIImage?,
                  title: String,
                  subtitleButton: String)
        
        case girry(imageLight: UIImage,
                   imageDark: UIImage?,
                   title: String,
                   subtitle: String,
                   button: String)
    }
    
    func extendTap(action: @escaping VoidClosure,
                   actionBeforeCurrent: Bool = true) -> GenericInformViewModel {
        let tapAction = {
            if actionBeforeCurrent {
                action()
            }
            onTap()
            if !actionBeforeCurrent {
                action()
            }
        }
        return GenericInformViewModel(style: style, onTap: tapAction)
    }
}

extension GenericInformViewModel {
    static func networkError(
        _ error: NetworkError,
        buttonText: String,
        onTap: @escaping VoidClosure
    ) -> GenericInformViewModel {
        switch error {
        case .notInternetConnection:
            return .init(
                style: .tiny(
                    imageLight: Asset.noInternetConnection.image,
                    imageDark: nil,
                    title: L10n.GenericInform.NoInternet.title,
                    subtitleButton: L10n.GenericInform.NoInternet.subTitle
                ),
                onTap: onTap
            )
        case .apiError(_) where error.isWrongNumber:
            return .init(
                style: .girry(
                    imageLight: Asset.wrongPhone.image,
                    imageDark: nil,
                    title: L10n.GenericInform.Error.Number.title,
                    subtitle: L10n.GenericInform.Error.Number.subTitle,
                    button: buttonText
                ),
                onTap: onTap
            )
            
        case .unknownError, .decodingError, .decodingInternalError, .wrongRequest, .httpError:
            return .init(
                style: .girry(
                    imageLight: Asset.somethingWentWrong.image,
                    imageDark: nil,
                    title: L10n.GenericInform.Error.Wrong.title,
                    subtitle: "",
                    button: buttonText
                ),
                onTap: onTap
            )
        
        case let .apiError(internalError) where error.isSubscriptionExpired:
            let imageLight = UIImage(named: internalError.image)
            ?? UIImage(named: "\(internalError.image)_light")
            let imageDark = UIImage(named: internalError.image)
            ?? UIImage(named: "\(internalError.image)_dark")
            return .init(
                style: .girry(
                    imageLight: imageLight ?? Asset.somethingWentWrong.image,
                    imageDark: imageDark ?? Asset.somethingWentWrong.image,
                    title: L10n.GenericInform.Error.Expired.title,
                    subtitle: L10n.GenericInform.Error.Expired.subTitle,
                    button: buttonText
                ),
                onTap: onTap
            )
        case let .apiError(error):
            let imageLight = UIImage(named: error.image)
            ?? UIImage(named: "\(error.image)_light")
            let imageDark = UIImage(named: error.image)
            ?? UIImage(named: "\(error.image)_dark")
            return .init(
                style: .girry(
                    imageLight: imageLight ?? Asset.somethingWentWrong.image,
                    imageDark: imageDark ?? Asset.somethingWentWrong.image,
                    title: error.localizationTitle,
                    subtitle: error.localizationSubTitle,
                    button: buttonText
                ),
                onTap: onTap
            )
        }
    }
}

extension GenericInformViewModel {
    static func emptyFaxes(onTap: @escaping VoidClosure) -> GenericInformViewModel {
        return .init(
            style: .tiny(
                imageLight: Asset.emptyFaxes.image,
                imageDark: nil,
                title: L10n.GenericInform.History.empty,
                subtitleButton: L10n.GenericInform.History.Empty.button
            ),
            onTap: onTap
        )
    }
}

extension GenericInformViewModel {
    static func sendFaxDone(
        estimatedMinutes: String,
        onTap: @escaping VoidClosure
    ) -> GenericInformViewModel {
        return .init(
            style: .girry(
                imageLight: Asset.sendFaxDone.image,
                imageDark: nil,
                title: L10n.GenericInform.Done.title,
                subtitle: L10n.GenericInform.Done.subTitle(estimatedMinutes, L10n.History.title.quoted),
                button: "OK"
            ),
            onTap: onTap
        )
    }
}

extension GenericInformViewModel {
    static func purchaseSucceed(onTap: @escaping VoidClosure) -> GenericInformViewModel {
        return .init(
            style: .girry(
                imageLight: Asset.purchaseSuccessLight.image,
                imageDark: Asset.purchaseSuccessDark.image,
                title: L10n.GenericInform.Purchase.title,
                subtitle: L10n.GenericInform.Purchase.subTitle,
                button: L10n.GenericInform.Purchase.button
            ),
            onTap: onTap
        )
    }
}

 
