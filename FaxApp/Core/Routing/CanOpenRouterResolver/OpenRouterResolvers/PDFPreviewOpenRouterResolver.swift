//
//  PDFPreviewOpenRouterResolver.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import Foundation

extension CanOpenRouterResolverToken {
    static var pdfPreviewResolver: CanOpenScreenRouterResolver {
        PDFPreviewResolver()
    }
}

fileprivate struct PDFPreviewResolver: CanOpenScreenRouterResolver {
    func canOpen(router: ScreenRouter, from: ScreenRouter) -> Bool {
        switch RouterType(indentifiable: router.routerIdentifiable) {
        case .rootTab,
                .onboarding,
                .sendingFax,
                .contactsPicker,
                .pdfPreview,
                .singleImagePicker,
                .documentPicker,
                .countryPhonesList,
                .alertOptions,
                .paywall,
                .sendMail,
                .faq:
            return false
        case .frontPdfPage:
            return true
        }
    }
}
