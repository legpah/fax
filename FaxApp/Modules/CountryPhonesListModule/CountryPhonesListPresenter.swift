//
//  CountryPhonesListPresenter.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import Foundation

protocol CountryPhonesListPresenter: ViewOutput {
    func backButtonTapped()
    func didChangeSearch(text: String)
}

final class CountryPhonesListPresenterImpl {
    
    weak var view: CountryPhonesListViewController?
    
    private let storage: ContryPhonesProvider
    private weak var router: CountryPhonesListRouter?
    private let analytics: AnalyticsService
    
    init(router: CountryPhonesListRouter,
         storage: ContryPhonesProvider = ContryPhonesStorage.shared,
         analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.storage = storage
        self.router = router
        self.analytics = analytics
    }
    
    private func updateCountriesTable(phones: [CountryPhone]) {
        let cells = phones
            .sorted(by: { $0.name < $1.name })
            .map {
                CountyPhonesCellModel(
                    id: $0.id,
                    name: $0.name,
                    flagCode: $0.flagUnicode,
                    countryCode: $0.code,
                    onTap: { model in
                        return { [weak self] in
                            self?.handleTap(model)
                        }
                    })
            }
        let isEmpty = view?.countryPhonesSource.items(in: .first).isEmpty ?? true
        view?.countryPhonesSource.reloadData(section: .first,
                                             with: cells,
                                             animated: !isEmpty)
    }
    
    private func handleTap(_ model: CountyPhonesCellModel) {
        guard let selectedCountry = storage.phones.first(where: {
            $0.name == model.name
        }) else {
            return
        }
        router?.closeScreen(selectedCounty: selectedCountry)
    }
}

extension CountryPhonesListPresenterImpl: CountryPhonesListPresenter {
    func viewDidLoad() {
        updateCountriesTable(phones: storage.phones)
    }
    
    func backButtonTapped() {
        router?.closeScreen(selectedCounty: nil)
        analytics.send(AnalyticsEventImpl(name: "tab1_country_screen_back_clicked"))
    }
    
    func didChangeSearch(text: String) {
        guard text.isNotEmpty else {
            updateCountriesTable(phones: storage.phones)
            return
        }
        
        let numbers = text.onlyDidgits()
        let texts = text.replaceDidgits("")
        
        let byCode = numbers.isEmpty ? [] : storage.phones.filter {
            $0.code.contains(numbers)
        }
        let byName = texts.isEmpty ? [] : storage.phones.filter {
            $0.name.lowercased().contains(texts.lowercased())
        }
        
        updateCountriesTable(phones: Array(Set(byCode + byName)))
    }
}
