//
//  Product+CustomPayload.swift
//  FaxApp
//
//  Created by Eugene on 16.06.2022.
//

import Adapty

extension AdaptyPaywall {
    var typedCustomPayload: CustomPaywallModelPayload? {
        guard let data = remoteConfigString?.data(using: .utf8) else {
            return nil
        }

        return try? JSONDecoder().decode(CustomPaywallModelPayload.self, from: data)
    }
}

struct CustomPaywallModelPayload: Decodable {
    
    let initialProductId: String
    let paywallVariant: PaywallViewVariation?
    let productsUI: [ProductUI]
    
    enum CodingKeys: String, CodingKey {
        case initialProductId = "initial_product_id"
        case paywallVariant = "paywall_variant"
        case productsUI = "products_ui"
    }
}

extension CustomPaywallModelPayload {
    struct ProductUI: Decodable {
        
        let productId: String
        let topTip: TopTip?
        
        enum CodingKeys: String, CodingKey {
            case productId = "product_id"
            case topTip = "top_tip"
        }
    }
}

extension CustomPaywallModelPayload.ProductUI {
    struct TopTip: Decodable {
        let style: String
        let title: String
    }
}
