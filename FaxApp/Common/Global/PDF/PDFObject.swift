//
//  PDFObject.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit
import PDFKit
import Combine

protocol PDFObject {
    var unerlyingSource: PDFSource { get }
    var pdfDocument: PDFDocument? { get }
    var pdfData: Data? { get }
    var pdfPreview: AnyPublisher<UIImage?, Never> { get }
}

extension PDFObject {
    var pageCount: Int? {
        pdfDocument?.pageCount
    }
    
    var title: String {
        switch self.unerlyingSource {
        case .frontPage(let attributesModel):
            return attributesModel.title
        default:
            return L10n.Main.document
        }
    }
    
    var analyticsDocumentType: String {
        unerlyingSource.analyticsDocumentType
    }
    
    func generatePdfPreview(completion: @escaping IClosure<UIImage?>) {
        var times = 10
        let pdfView = PDFView(frame: PDFConstants.pageFrame)
        pdfView.autoScales = true
        pdfView.pageShadowsEnabled = false
        pdfView.document = pdfDocument
        (pdfView.subviews.first as? UIScrollView)?.showsVerticalScrollIndicator = false
        (pdfView.subviews.first as? UIScrollView)?.showsHorizontalScrollIndicator = false
        
        Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { timer in
            guard times > 0 else {
                timer.invalidate()
                completion(nil)
                return
            }
            guard pdfView.visiblePages.isNotEmpty else {
                times -= 1
                return
            }
            
            let image = pdfView.renderImage(withBlackFilter: true)?
                .resizeImage(targetSize: PDFConstants.pageSize)?
                .scaleImage(x: 0.5, y: 0.5)
            timer.invalidate()
            completion(image)
        })
    }
}
