//
//  FaxHistoryRouter.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit
import Combine
 
protocol FaxHistoryRouter: AlertOptionsRoutable, TabOpenerRouter, PDFPreviewRoutable {}

final class FaxHistoryRouterImpl: BaseScreenRouter, ScreenRouter, FaxHistoryRouter {
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.rootTab(.faxHistory)
    let presentationType: ScreenPresentationType = .plain
    
    private lazy var _screen: UIViewController = {
        let presenter = FaxHistoryPresenterImpl(
            router: self,
            faxSendedPublisher: SendFaxNotification.sendPublisher
        )
        let view = FaxHistoryViewControllerImpl(presenter: presenter)
        presenter.view = view
        return NavigationController(rootViewController: view, bgColor: .bw)
    }()
}
