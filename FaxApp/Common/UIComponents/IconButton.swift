//
//  IconButton.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

final class IconButton: UIView {
    
    var onTap: VoidClosure? {
        didSet {
            onTapChanged()
        }
    }
    
    var isSelected: Bool = false {
        didSet {
            apply(theme: UITheme.current)
        }
    }
    
    private let image: UIThemed<UIImage>
    private let selectedImage: UIThemed<UIImage>?
    private let imageXAlignment: LXAlignment
    private let imageYAlignment: LYAlignment
    private let imageSizeRule: ImageSizeRule
    
    private let imageView = UIImageView()
    
    init(
        image: UIThemed<UIImage>,
        selectedImage: UIThemed<UIImage>? = nil,
        imageXAlignment: LXAlignment = .center(offset: 0),
        imageYAlignment: LYAlignment = .center(offset: 0),
        imageSizeRule: ImageSizeRule = .unspecified
    ) {
        self.image = image
        self.selectedImage = selectedImage
        self.imageXAlignment = imageXAlignment
        self.imageYAlignment = imageYAlignment
        self.imageSizeRule = imageSizeRule
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSelf() {
        addSubview(imageView)
        imageView.isUserInteractionEnabled = true
        
        imageView.alignView(x: imageXAlignment)
        imageView.alignView(y: imageYAlignment)
        
        switch imageSizeRule {
        case .unspecified:
            break
        case let .scale(x, y):
            imageView.match(.height, to: .height, of: self, withMultiplier: y)
            imageView.match(.width, to: .width, of: self, withMultiplier: x)
        case let .fixed(size):
            imageView.setDimensions(to: size)
        }
        
        themeProvider.register(observer: self)
    }
    
    private func onTapChanged() {
        if onTap == nil {
            self.gestureRecognizers?.removeAll()
        } else {
            self.addGestureRecognizer(
                UITapGestureRecognizer(target: self, action: #selector(handleTap))
            )
        }
    }
    
    @objc
    private func handleTap(){
        onTap?()
    }
}

extension IconButton: UIThemable {
    func apply(theme: UITheme) {
        let image: UIThemed<UIImage> = isSelected
        ? (selectedImage ?? image)
        : image
        
        imageView.image = image.value
    }
}

extension IconButton {
    enum ImageSizeRule {
        case unspecified
        case scale(x: CGFloat, y: CGFloat)
        case fixed(CGSize)
    }
}
