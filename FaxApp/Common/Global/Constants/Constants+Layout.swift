//
//  Constants+Layout.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import CoreGraphics

extension ConstantsToken.LayoutToken {
    static let tabHeight: CGFloat = 50.0
    static let xInset = 24.0
}

extension CGFloat {
    static let layout = ConstantsToken.LayoutToken.self
}
