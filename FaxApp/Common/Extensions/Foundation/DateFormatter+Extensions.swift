//
//  DateFormatter+Extensions.swift
//  FaxApp
//
//  Created by Eugene on 18.04.2022.
//

import Foundation

extension DateFormatter {
    static let fronPageDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = .current
        //formatter.dateFormat = Locale.current.isJapan ? "yyyy年MM月dd日 HH:mm" : "dd.MM.yyyy HH:mm"
        formatter.dateFormat = "dd.MM.yyyy HH:mm"
        return formatter
    }()
    
    static let historyDateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = .current
        //formatter.dateFormat = Locale.current.isJapan ? "yyyy年MM月dd日 HH:mm" : "dd.MM.yyyy HH:mm"
        formatter.dateFormat = "dd.MM.yyyy HH:mm"
        return formatter
    }()
    
    static let hhmmFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = .current
        formatter.dateFormat = "HH:mm"
        return formatter
    }()
    
    static let uiSubscriptionFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.timeZone = .current
        //formatter.dateFormat = Locale.current.isJapan ? "yyyy年MM月dd日" : "dd.MM.yyyy"
        formatter.dateFormat = "dd.MM.yyyy"
        return formatter
    }()
}

extension Locale {
    var isJapan: Bool {
        Locale.current.languageCode?.lowercased() == "ja"
    }
    
    var isDeutsch: Bool {
        Locale.current.languageCode?.lowercased() == "de"
    }
}
