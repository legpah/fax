//
//  SendPaywallViewController.swift
//  FaxApp
//
//  Created by 123 on 26.06.2023.
//

import UIKit

final class PaywallSixthVariantView: UIView, PaywallVariantView {
    var itemsContainer: PremiumItemsContainer {
        premiumItemsView
    }
    private let premiumItemsView = PremiumItemsContainerImpl()
    private let size: PaywallSizeVariant
    private var model: PaywallModel?
    
    private var viewFirst: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.distribution = .equalSpacing
        stackView.isLayoutMarginsRelativeArrangement = true
        return stackView
    }()
    
    private var viewSecond: UIStackView = {
        let stackView = UIStackView()
        stackView.axis = .vertical
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 24, left: 0, bottom: 16, right: 0)
        stackView.distribution = .equalSpacing
        stackView.alpha = 0
        return stackView
    }()
    
    private let stackViewForFeatures: UIStackView = {
        let stackView = UIStackView()
        stackView.spacing = 30
        stackView.distribution = .fillEqually
        stackView.isLayoutMarginsRelativeArrangement = true
        stackView.layoutMargins = UIEdgeInsets(top: 0, left: 35, bottom: 0, right: 35)
        return stackView
    }()
    
    private let viewWithInfo = ViewInfo()
    var allPlansButton = TextButton(text: L10n.Paywall.Sixth.allPlans, font: .poppins(14, .regular))
    private let readyToGoImage = UIImageView(image: UIThemed.readyToGo.value)
    private let infoAboutFaxLabel = Label(font: .poppins(16, .semiBold), color: .ibw, alignment: .left, lines: 3)
    private let infoAboutFaxTitleLabel = Label(text: "Information", font: .poppins(24, .semiBold), color: .ibw, alignment: .left, lines: 1)
    private let namesOfParams = Label(text: L10n.Paywall.Sixth.time + ":\n" + L10n.Paywall.Sixth.pages + ":\n" + L10n.Paywall.Sixth.contact + ":", font: .poppins(16), color: .ibw, alignment: .left, lines: 3)
    private let underline = CALayer(backgroundColor: .tukesh)
    private let subtitle = Label(font: .poppins(14), color: .ibw50, alignment: .center, lines: 1)
    private let price = Label(font: .poppins(14), color: .ibw50, alignment: .center, lines: 1)
    private let imageViewOfSendetFax = UIImageView(image: UIThemed.paywallFaxSent.value.withAlignmentRectInsets(UIEdgeInsets(top: 0, left: -43, bottom: 0, right: -43)))
    init(
        size: PaywallSizeVariant,
        model: PaywallModel?
    ) {
        self.size = size
        self.model = model
        super.init(frame: .zero)
        configureFirstPage()
        configureSecondPage()
        configureConstraints()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        underline.removeFromSuperlayer()
        underline.frame = CGRect(x: 2, y: allPlansButton.bounds.height - 4, width: allPlansButton.bounds.width - 2, height: 1)
        allPlansButton.layer.addSublayer(underline)
        readyToGoImage.pinEdge(.top, to: .bottom, of: infoAboutFaxLabel, withOffset: size.value(
            -infoAboutFaxLabel.bounds.width * 0.19,
            -infoAboutFaxLabel.bounds.width * 0.16,
            -infoAboutFaxLabel.bounds.width * 0.16)
        )
        readyToGoImage.pinEdge(.left, to: .right, of: infoAboutFaxLabel, withOffset: -infoAboutFaxLabel.bounds.width * 0.52)
    }
    
    private func configureFirstPage() {
        let title = Label(font: .poppins(size.value(30, Locale.current.isDeutsch ? 28 : 30, 28)), color: .ibw, alignment: .center, lines: 3)
        title.minimumScaleFactor = 0.7
        let attrStr = NSMutableAttributedString(string: L10n.Paywall.Sixth.press + " ")
        attrStr.append(NSAttributedString(string: L10n.Paywall.continue, attributes: [NSAttributedString.Key.font: UIFont.poppins(size.value(30, 29, 28), .bold)]))
        attrStr.append(NSAttributedString(string: L10n.Paywall.Sixth.sendFax))
        title.attributedText = attrStr
        title.minimumScaleFactor = 0.6
        
        let stackViewForSubtitle = UIStackView(arrangedSubviews: [subtitle, allPlansButton])
        stackViewForSubtitle.axis = .vertical
        stackViewForSubtitle.alignment = .center
        
        let stackViewForTitle = UIStackView(arrangedSubviews: [title, stackViewForSubtitle])
        stackViewForTitle.axis = .vertical
        stackViewForTitle.spacing = 24
        
        let firstFeatures = ViewForImageAndTitleOfFeatures(text: L10n.Paywall.Sixth.limit, image: .paywallNoLimit)
        let secondFeatures = ViewForImageAndTitleOfFeatures(text: L10n.Paywall.Sixth.worldwide, image: .paywallWorldWide)
        let thirdFeatures = ViewForImageAndTitleOfFeatures(text: L10n.Paywall.Sixth.delivery, image: .paywalldeliveryTracking)
        stackViewForFeatures.addArrangedSubviews([firstFeatures,
                                                  secondFeatures,
                                                  thirdFeatures])
        
        viewFirst.layoutMargins = UIEdgeInsets(top: 24, left: 0, bottom: size.value(64, 48, 16), right: 0)
        viewFirst.addArrangedSubviews([stackViewForTitle, viewWithInfo, stackViewForFeatures])
        
        addSubview(viewFirst)
        viewWithInfo.addSubview(infoAboutFaxTitleLabel)
        viewWithInfo.addSubview(namesOfParams)
        viewWithInfo.addSubview(infoAboutFaxLabel)
        viewWithInfo.addSubview(infoAboutFaxTitleLabel)
        viewWithInfo.addSubview(readyToGoImage)
        
        setInfoAboutFax()
    }
    
    private func setInfoAboutFax() {
        if let model = model {
            infoAboutFaxLabel.text = model.time + "\n" + String(model.pages) + "\n" + model.number
        }
    }
    
    private func configureSecondPage() {
        let title = PaddingLabel(font: .poppins(30), color: .ibw, alignment: .center, lines: 2)
        title.paddingLeft = 16
        title.paddingRight = 16
        
        let attrStr = NSMutableAttributedString(string: L10n.Paywall.Sixth.unlim + " ", attributes: [NSAttributedString.Key.font: UIFont.poppins(30, .bold)])
        attrStr.append(NSAttributedString(string: L10n.Paywall.Sixth.access + " "))
        attrStr.append(NSAttributedString(string: L10n.Paywall.Sixth.features  + " ", attributes: [NSAttributedString.Key.font: UIFont.poppins(32, .bold)]))
        title.attributedText = attrStr
        
        let subtitle = PaddingLabel(text: L10n.Paywall.Sixth.subtitle, font: .poppins(14), color: .ibw50, alignment: .center, lines: 2)
        subtitle.paddingLeft = 16
        subtitle.paddingRight = 16
        
        viewSecond.addArrangedSubviews([title, subtitle, imageViewOfSendetFax, premiumItemsView, price])
        addSubview(viewSecond)
    }
    
    private func configureConstraints() {
        viewWithInfo.match(.height, to: .width, of: viewWithInfo, withMultiplier: 0.65)
        
        viewFirst.pinEdge(.top, to: .top, of: self)
        viewFirst.pinEdge(.bottom, to: .bottom, of: self)
        viewFirst.pinEdges(toSuperviewEdges: [.left, .right], withInset: ConstantsToken.LayoutToken.xInset)
        
        imageViewOfSendetFax.match(.height, to: .width, of: imageViewOfSendetFax, withMultiplier: 0.3)
        
        stackViewForFeatures.match(.height, to: .width, of: stackViewForFeatures, withMultiplier: 0.22)
        
        infoAboutFaxTitleLabel.pinEdges(toSuperviewEdges: [.top, .left], withInset: [40, 24])
        
        infoAboutFaxLabel.pinEdge(.left, to: .right, of: namesOfParams, withOffset: 26)
        infoAboutFaxLabel.pinEdge(.top, to: .top, of: namesOfParams, withOffset: 0)
        
        namesOfParams.pinEdge(toSuperviewEdge: .left, withInset: 24)
        namesOfParams.pinEdge(.top, to: .bottom, of: infoAboutFaxTitleLabel, withOffset: 16)
        
        readyToGoImage.match(.width, to: .width, of: viewWithInfo, withMultiplier: 0.41)
        readyToGoImage.match(.height, to: .width, of: readyToGoImage, withMultiplier: 0.5)
        
        viewSecond.pinEdge(.top, to: .top, of: self)
        viewSecond.pinEdge(.bottom, to: .bottom, of: self)
        viewSecond.pinEdges(toSuperviewEdges: [.left, .right])
    }
    
    func changePaywallPage(toSecond: Bool) {
        UIView.animate(withDuration: .animations.fastTransitionDuration, animations: {
            self.viewFirst.alpha = toSecond ? 0 : 1
            self.viewSecond.alpha = toSecond ? 1 : 0
        })
    }
    
    func changeCostLabel(cost: String) {
        if subtitle.text?.isEmpty ?? true {
            subtitle.text = cost
        }
        price.text = cost
    }
    
    final class ViewForImageAndTitleOfFeatures: UIStackView {
        private var size: PaywallSizeVariant = .normal
        let imageView: UIImageView = {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFit
            return imageView
        }()
        
        let title = Label(font: .poppins(11, .regular), color: .ibw, alignment: .center, lines: 2)
        
        convenience init(text: String, image: UIThemed<UIImage>) {
            self.init()
            imageView.image = image.value
            title.text = text
            addArrangedSubviews([imageView, title])
            configure()
        }
        
        private func configure() {
            axis = .vertical
            distribution = .fillProportionally
            spacing = 4
        }
    }
    
    final class ViewInfo: UIView {
        override func layoutSubviews() {
            super.layoutSubviews()
            let shadowLayerFirst = CALayer(frame: CGRect(x: 32, y: 0, width: layer.bounds.width - 64, height: layer.bounds.height), backgroundColor: .gr15d6, cornerRadius: 12, zPosition: -3)
            layer.addSublayer(shadowLayerFirst)
            
            let shadowLayerSecond = CALayer(frame: CGRect(x: 16, y: 8, width: layer.bounds.width - 32, height: layer.bounds.height - 8), backgroundColor: .gr14d5, cornerRadius: 12, zPosition: -2)
            layer.addSublayer(shadowLayerSecond)
            
            let documentLayer = CALayer(frame: CGRect(x: 0, y: 16, width: layer.bounds.width, height: layer.bounds.height - 16), backgroundColor: .whd1, cornerRadius: 12, zPosition: -1, borderColor: .amber, borderWidth: 1)
            layer.addSublayer(documentLayer)
        }
    }
}
