//
//  UIStackView + Extensions.swift
//  FaxApp
//
//  Created by 123 on 28.06.2023.
//

import Foundation
import UIKit

extension UIStackView {
    func addArrangedSubviews(_ subviews: [UIView]) {
        for subview in subviews {
            addArrangedSubview(subview)
        }
    }
}
