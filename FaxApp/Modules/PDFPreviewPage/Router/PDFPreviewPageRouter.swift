//
//  PDFPreviewPageRouter.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit

protocol PDFPreviewPageRouter: CloseRouterCoordinator {
    func openFrontPage(attributesModel: FrontPageAttributesModel,
                       onCreate: @escaping IClosure<PDFObject>)
}

final class PDFPreviewPageRouterImpl: BaseScreenRouter, ScreenRouter {
    
    var screen: UIViewController { _screen }
    
    let routerIdentifiable: RouterIdentifiable = RouterType.pdfPreview
    var presentationType: ScreenPresentationType { .modal }
    
    private let model: PDFPreviewPageRouterModel
    
    private lazy var _screen: UIViewController = {
        let presenter = PDFPreviewPresenterImpl(pdfObject: model.pdfObject,
                                                onDocumentEdited: model.onDocumentEdited,
                                                router: self)
        let view = PDFPreviewViewControllerImpl(showEditFrontPage: model.isFontPageEditable,
                                                presenter: presenter)
        presenter.view = view
        view.title = model.viewTitle
        
        return NavigationController(rootViewController: view)
    }()
    
    init(model: PDFPreviewPageRouterModel) {
        self.model = model
    }
}

extension PDFPreviewPageRouterImpl: PDFPreviewPageRouter {
    func openFrontPage(attributesModel: FrontPageAttributesModel,
                       onCreate: @escaping IClosure<PDFObject>) {
        let router = FrontPdfPageRouterImpl(attributes: attributesModel,
                                            onCreate: onCreate)
        open(router: router)
    }
}
