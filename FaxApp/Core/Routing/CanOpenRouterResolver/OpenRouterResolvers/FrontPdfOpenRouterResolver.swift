//
//  FrontPdfOpenRouterResolver.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import Foundation

extension CanOpenRouterResolverToken {
    static var frontPdfResolver: CanOpenScreenRouterResolver {
        FrontPdfResolver()
    }
}

fileprivate struct FrontPdfResolver: CanOpenScreenRouterResolver {
    func canOpen(router: ScreenRouter, from: ScreenRouter) -> Bool {
        switch RouterType(indentifiable: router.routerIdentifiable) {
        case .rootTab,
                .sendingFax,
                .onboarding,
                .contactsPicker,
                .frontPdfPage,
                .alertOptions,
                .singleImagePicker,
                .documentPicker,
                .countryPhonesList,
                .paywall,
                .sendMail,
                .faq:
            return false
        case .pdfPreview:
            return true
        }
    }
}
