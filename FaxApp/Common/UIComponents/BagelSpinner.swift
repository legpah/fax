//
//  BagelSpinner.swift
//  FaxApp
//
//  Created by Eugene on 3.05.2022.
//

import UIKit

final class BagelSpinner: UIView {
    
    private let bagelLayer = CAShapeLayer()
    
    var color: UIThemed<UIColor> = .yelyel {
        didSet { apply(theme: .current) }
    }
    
    var width: CGFloat = 1 {
        didSet { bagelLayer.lineWidth = width }
    }
    
    var duration: TimeInterval = 2.5 {
        didSet { rotateAnimation?.duration = duration }
    }
    
    var isAnimating: Bool = true {
        didSet { animateSnipper() }
    }
    
    private let rotateKey = "spinner_animation"
    private var rotateAnimation: CABasicAnimation? {
        bagelLayer.animation(forKey: rotateKey) as? CABasicAnimation
    }
    
    init() {
        super.init(frame: .zero)
        apply(theme: .current)
        clipsToBounds = true
        drawSpinner()
        animateSnipper()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        drawSpinner()
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        if window != nil {
            animateSnipper()
        }
    }
    
    private func drawSpinner() {
        bagelLayer.removeFromSuperlayer()
        let edge = min(frame.width, frame.height)
        let radius = (edge - width) / 2
        bagelLayer.frame.size = CGSize(width: edge, height: edge)
        bagelLayer.path = UIBezierPath(arcCenter: center,
                                       radius: radius,
                                       startAngle: 0,
                                       endAngle: .pi * 1.7,
                                       clockwise: true).cgPath
        bagelLayer.lineWidth = width
        
        layer.addSublayer(bagelLayer)
    }
    
    private func animateSnipper() {
        if !isAnimating {
            bagelLayer.removeAllAnimations()
            return
        }
        
        let animation = CABasicAnimation(keyPath: "transform.rotation.z")
        animation.fromValue = 0.0
        animation.toValue = Double.pi * 2.0
        animation.duration = duration
        animation.repeatCount = .infinity
        bagelLayer.add(animation, forKey: rotateKey)
    }
}

extension BagelSpinner: UIThemable {
    func apply(theme: UITheme) {
        bagelLayer.fillColor = nil
        bagelLayer.strokeColor = color.value.cgColor
    }
}
