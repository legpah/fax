//
//  CanOpenRouterResolverImpl.swift
//  FaxApp
//
//  Created by Eugene on 19.04.2022.
//

import Foundation

struct CanOpenRouterResolverImpl: CanOpenRouterResolver {
    
    func canOpen(router: ScreenRouter, from: ScreenRouter) -> Bool {    
        guard !isOpeningRootScreen(router) else {
            assertionFailureDebug("unable to open root screen")
            return false
        }
        
        let token = CanOpenRouterResolverToken.self
        let resolver: CanOpenScreenRouterResolver = {
            switch RouterType(indentifiable: from.routerIdentifiable) {
            case let .rootTab(tabScreenType):
                return tabScreenType.canOpenRouterResolver
            case .frontPdfPage:
                return token.frontPdfResolver
            case .pdfPreview:
                return token.pdfPreviewResolver
            case .paywall, .onboarding:
                return token.paywallResolver
            case .sendingFax:
                return token.sendingFaxResolver
            case .alertOptions,
                    .contactsPicker,
                    .singleImagePicker,
                    .documentPicker,
                    .countryPhonesList,
                    .sendMail,
                    .faq:
                return token.falseAlwaysResolver
            }
        }()
        
        return resolver.canOpen(router: router, from: from)
    }
    
    func isOpeningRootScreen(_ router: ScreenRouter) -> Bool {
        switch RouterType(indentifiable: router.routerIdentifiable) {
        case .rootTab:
            return true
        default:
            return false
        }
    }
}

extension TabBarType {
    fileprivate var canOpenRouterResolver: CanOpenScreenRouterResolver {
        let token = CanOpenRouterResolverToken.self
        switch self {
        case .fax:
            return token.faxTabResolver
        case .faxHistory:
            return token.historyTabResolver
        case .settings:
            return token.settingsTabResolver
        }
    }
}
