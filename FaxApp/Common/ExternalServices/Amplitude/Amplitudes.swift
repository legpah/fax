//
//  Amplitudes.swift
//  FaxApp
//
//  Created by Eugene on 18.05.2022.
//

import Amplitude_iOS
import Combine

final class Amplitudes {
    
    static let shared = Amplitudes()
    private init() {}
    
    private var cancelBag = Set<AnyCancellable>()
    
    private var _instance: Amplitude?
    private var instance: Amplitude {
        if let instance = _instance {
            return instance
        }
        return Amplitude.instance()
    }
    
    func configure() {
        instance.trackingSessionEvents = true
        instance.setUserId(UserUIDProviderImpl.shared.uuid)
        instance.initializeApiKey("5a08f4e38d189946d19d52b18bff0619")
        instance.logEvent("custom_event")
        
        subscribeOnFaxNotifications()
        subscribeOnSubscriptionState()
        
        IDFATrackingManager.shared.idfaStatusPublisher.sink { [weak self] status in
            if status {
                self?.instance.useAdvertisingIdForDeviceId()
            }
        }.store(in: &cancelBag)
    }
    
    private func subscribeOnFaxNotifications() {
        SendFaxNotification.sendPublisher.sinkSuccess { [weak self] count in
            self?.upFaxAndPagesSentAnalytics(pagesCount: count)
        }.store(in: &cancelBag)
        
        SendFaxErrorNotification.sendPublisher.sinkSuccess { [weak self] _ in
            self?.upFaxErrorSentAnalytics()
        }.store(in: &cancelBag)
    }
    
    private func subscribeOnSubscriptionState() {
        PurchasesService.shared.subscriptionState.sinkSuccess { [weak self] state in
            self?.updateSubscribeStateAnalytics(state: state)
        }.store(in: &cancelBag)
    }
    
    private func updateSubscribeStateAnalytics(state: AppleSubscriptionState) {
        if case .loading = state { return }
        
        let identify = AMPIdentify().set("subscribed", value: state.analyticValue as NSString)
        instance.identify(identify)
    }
    
    private func upFaxAndPagesSentAnalytics(pagesCount: Int) {
        let identify = AMPIdentify().add("fax_sent", value: NSNumber(value: 1))
                                    .add("pages_sent", value: NSNumber(value: pagesCount))
        instance.identify(identify)
    }
    
    private func upFaxErrorSentAnalytics() {
        let identify = AMPIdentify().add("fax_sending_failed", value: NSNumber(value: 1))
        instance.identify(identify)
    }
    
}

extension Amplitudes: AnalyticsService {
    func send(_ event: AnalyticsEvent) {
        instance.logEvent(event.name, withEventProperties: event.parameters)
    }
}
