//
//  ContactsPickerRouter.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import UIKit

typealias ContactsPhoneNumber = String

final class ContactsPickerRouterImpl: BaseScreenRouter, ScreenRouter {
    
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.contactsPicker
    let presentationType: ScreenPresentationType = .modal
    
    private lazy var _screen: UIViewController = {
        let view = ContactsPickerViewControllerImpl(router: self,
                                                    moduleOuput: output)
        return view
    }()
    
    private let output: ContactsPickerModuleOuput?
    
    init(output: ContactsPickerModuleOuput?) {
        self.output = output
    }
}
