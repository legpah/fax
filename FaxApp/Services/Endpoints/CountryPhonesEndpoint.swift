//
//  CountryPhonesEndpoint.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation

struct CountryPhonesEndpoint: NetworkRequest {
    typealias ReturnType = [CountryPhone]
    
    let path: String = "/country"
    let method: HTTPMethod = .get
}

struct CountryPhone: Codable, Equatable, Hashable {
    let id: String
    let name: String
    let code: String
    let mask: String
    let flagUnicode: String
    let canSendFax: Bool
    
    init(id: String, name: String, code: String, mask: String, flagUnicode: String, canSendFax: Bool) {
        self.id = id
        self.name = name
        self.code = code
        self.mask = mask
        self.flagUnicode = flagUnicode
        self.canSendFax = canSendFax
    }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        id = try container.decode(String.self, forKey: .id)
        name = try container.decode(String.self, forKey: .name)
        code = try container.decode(String.self, forKey: .code)
        mask = try container.decode(String.self, forKey: .mask)
        let flagStr = try container.decode(String.self, forKey: .flagUnicode)
        let rawScalars = flagStr.split(separator: "\\").compactMap { value -> UnicodeScalar? in
            var value = String(value)
            value = value.replacingOccurrences(of: "u{", with: "")
            value = value.replacingOccurrences(of: "}", with: "")
            guard let hex = UInt32(value, radix: 16) else {
                return nil
            }
            let scalar = UnicodeScalar(hex)
            return scalar
        }
        
        flagUnicode = rawScalars.reduce("", { $0 + String($1) })
        canSendFax = try container.decode(Bool.self, forKey: .canSendFax)
    }
    
    private enum CodingKeys: String, CodingKey {
        case id
        case name
        case code
        case mask
        case flagUnicode
        case canSendFax
    }
}
