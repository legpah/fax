//
//  UIEdgeInsets+Extensions.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

extension UIEdgeInsets {
    static func symmetric(value: CGFloat) -> UIEdgeInsets {
        UIEdgeInsets(top: value, left: value, bottom: value, right: value)
    }
    
    static func symmetric(x: CGFloat, y: CGFloat) -> UIEdgeInsets {
        UIEdgeInsets(top: y, left: x, bottom: y, right: x)
    }
}
