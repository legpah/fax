//
//  UIThemable.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit

protocol UIThemable: AnyObject {
    func apply(theme: UITheme)
}

extension UIViewController {
    var themeProvider: UIThemeProvider {
        UIThemeProviderImpl.shared
    }
}

extension UIView {
    var themeProvider: UIThemeProvider {
        UIThemeProviderImpl.shared
    }
}
