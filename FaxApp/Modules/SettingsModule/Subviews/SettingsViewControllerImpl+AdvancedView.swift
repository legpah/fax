//
//  SettingsViewControllerImpl+AdvancedView.swift
//  FaxApp
//
//  Created by Eugene on 30.05.2022.
//

import UIKit

extension SettingsViewControllerImpl {
    final class AdvancedView: UIView {
        
        var isNotificationSwitcherOn: Bool = false {
            didSet {
                DispatchQueue.main.async { [self] in
                    notificationSwitcher.switcher.isOn = isNotificationSwitcherOn
                    notificationSwitcher.updateAppearance()
                }
            }
        }
        
        var onNotificationsSwitch: IClosure<Bool>?
        
        private let titleLabel = Label(text: L10n.Settings.advanced,
                                       font: .poppins(12, .semiBold))
        
        private lazy var notificationSwitcher = SwitcherItemView(
            text: L10n.Settings.notifications,
            image: .notificationsSettings,
            onSwitch: {
                [weak self] flag in self?.onNotificationsSwitch?(flag)
            }
        )
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(titleLabel)
            addSubview(notificationSwitcher)
            
            notificationSwitcher.applyBordersShape(width: 1, radius: 8)
            
            configureConstaints()
            
            themeProvider.register(observer: self)
        }
        
        private func configureConstaints() {
            titleLabel.pinEdges(toSuperviewEdges: [.left, .top])
            
            notificationSwitcher.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 12)
            notificationSwitcher.pinEdges(toSuperviewEdges: [.left, .right])
            
            pinEdge(.top, to: .top, of: titleLabel)
            pinEdge(.bottom, to: .bottom, of: notificationSwitcher)
        }
    }
}

extension SettingsViewControllerImpl.AdvancedView: UIThemable {
    func apply(theme: UITheme) {
        notificationSwitcher.applyBorders(color: .xero)
    }
}

fileprivate extension SettingsViewControllerImpl.AdvancedView {
    final class SwitcherItemView: UIView {
        private let label: Label
        private let image: ImageView
        let switcher = UISwitch()
        private let onSwitch: IClosure<Bool>
        
        init(text: String,
             image: UIThemed<UIImage>,
             onSwitch: @escaping IClosure<Bool>) {
            self.label = Label(text: text, font: .poppins(14))
            self.image = ImageView(themedImage: image)
            self.onSwitch = onSwitch
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        @objc
        private func didSwitch() {
            updateAppearance()
            onSwitch(switcher.isOn)
        }
        
        private func configureSelf() {
            addSubview(image)
            addSubview(label)
            addSubview(switcher)
            
            switcher.addTarget(self, action: #selector(didSwitch), for: .valueChanged)
            themeProvider.register(observer: self)
            
            setDimension(.height, toSize: 48)
            
            image.pinEdge(toSuperviewEdge: .left, withInset: 15)
            image.setDimensions(to: CGSize(width: 18, height: 17))
            label.pinEdge(.left, to: .right, of: image, withOffset: 12)
            switcher.pinEdge(toSuperviewEdge: .right, withInset: 16)
         
            switcher.alignAxis(toSuperviewAxis: .y)
            [image, label].forEach {
                $0.alignAxis(toSuperviewAxis: .y)
            }
        }
        
        func updateAppearance() {
            apply(theme: .current)
        }
    }
}

extension SettingsViewControllerImpl.AdvancedView.SwitcherItemView: UIThemable {
    func apply(theme: UITheme) {
        backgroundColor = .themed.goeridio.value
        switcher.tintColor = .themed.cucumber.value
        switcher.onTintColor = .themed.tukesh.value
        switcher.applyBorders(color: .igram)
        
        if switcher.isOn {
            switcher.thumbTintColor = .themed.bw.value
        } else {
            switcher.thumbTintColor = .themed.gr77.value
        }
    }
}
 
