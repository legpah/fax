//
//  PDFPreviewViewControllerImpl+EditFrontPageView.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import UIKit

extension PDFPreviewViewControllerImpl {
    final class EditFrontPageView: UIView {
        
        var onEditTap: VoidClosure? {
            get { button.onTap }
            set { button.onTap = newValue }
        }
        
        private let button = Button(title: L10n.Settings.edit,
                                    titleColor: .bw,
                                    bgColor: .tukesh)
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(button)
            button.applyBordersShape(width: 0, radius: 8)
            button.textAlignment = .center
            
            setDimension(.height, toSize: 80)
            
            button.centerInSuperview()
            button.setDimension(.height, toSize: 48)
            button.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
            
            themeProvider.register(observer: self)
        }
    }
}

extension PDFPreviewViewControllerImpl.EditFrontPageView: UIThemable {
    func apply(theme: UITheme) {
        backgroundColor = .themed.flerio.value
    }
}
