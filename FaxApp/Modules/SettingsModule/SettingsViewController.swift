//
//  SettingsPageViewController.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit

protocol SettingsViewController: AnyObject {
    func setSubscriptionViewState(_ state: SubscriptionViewSate)
    func updateName(_ text: String)
    func updatePhone(_ text: String)
    func updateNotificationSwitcher(isOn: Bool)
}

final class SettingsViewControllerImpl: UIViewController {
    
    private let presenter: SettingsPresenter
    private let scrollContainer = ScrollView(direction: .vertical)
    private let subscriptionView = SubscriptionView()
    private let helpAndSupportView = HelpAndSupportView()
    private let senderInformationView = SenderInformationView()
    private let advancedView = AdvancedView()
    private lazy var keyboardResigner = KeyboardOutsideTouchesResignerView(controller: self)
    private lazy var responderVisibilityManger = ResponderVisibilityManager(
        viewsToTrack: [senderInformationView.phoneInput, senderInformationView.nameInput],
        relativeToView: scrollContainer,
        movingType: .bounds
    )
    
    init(presenter: SettingsPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSelf()
        responderVisibilityManger.start()
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        presenter.viewWillAppear()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    private func configureSelf() {
        title = L10n.Settings.title
        view.addSubview(scrollContainer)
        scrollContainer.addSubview(subscriptionView)
        scrollContainer.addSubview(helpAndSupportView)
        scrollContainer.addSubview(senderInformationView)
        scrollContainer.addSubview(advancedView)
        
        configureScrollContainer()
        configureSwitchThemeBarButton()
        configureSubscriptionView()
        configureHelpAndSupportView()
        configureSenderInformationView()
        configureAdvancedView()
        
        keyboardResigner.trackKeyboard(of: [
            senderInformationView.nameInput,
            senderInformationView.phoneInput
        ])
        themeProvider.register(observer: self)
    }
    
    private func configureSwitchThemeBarButton() {
        navigationItem.rightBarButtonItem = UIBarButtonItem(
            image: navIcon(),
            style: .plain,
            target: self,
            action: #selector(switchThemeTapped)
        )
    }
    
    private func configureScrollContainer() {
        scrollContainer.contentInset = .init(top: 10, left: 0, bottom: 10, right: 0)
        scrollContainer.pin(toTopLayoutGuideOf: self)
        scrollContainer.pinEdges(toSuperviewEdges: [.left, .right])
        scrollContainer.pin(toBottomLayoutGuideOf: self)
        
        subscriptionView.pinEdge(.top, to: .top, of: scrollContainer)
        advancedView.pinEdge(.bottom, to: .bottom, of: scrollContainer)
    }
    
    private func configureSubscriptionView() {
        subscriptionView.pinEdges(toSuperviewEdges: [.left, .right])
    }
    
    private func configureHelpAndSupportView() {
        helpAndSupportView.pinEdge(.top, to: .bottom, of: subscriptionView, withOffset: 24)
        helpAndSupportView.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        helpAndSupportView.onOptionTap = { [weak self] option in
            self?.presenter.helpAndSupportTapped(option)
        }
    }
    
    private func configureSenderInformationView() {
        senderInformationView.pinEdge(.top, to: .bottom, of: helpAndSupportView, withOffset: 24)
        senderInformationView.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        senderInformationView.nameInput.onTextChange = { [weak self] change in
            self?.presenter.nameDidChange(change.text)
        }
        senderInformationView.phoneInput.onTextChange = { [weak self] change in
            self?.presenter.phoneDidChange(change.text)
        }
    }
    
    private func configureAdvancedView() {
        advancedView.pinEdge(.top, to: .bottom, of: senderInformationView, withOffset: 24)
        advancedView.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        advancedView.onNotificationsSwitch = { [weak self] isOn in
            self?.presenter.didTapNotifications(isOn)
        }
    }
    
    @objc private func switchThemeTapped() {
        presenter.themeChangeIconTapped()
    }
}

extension SettingsViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        view.backgroundColor = .themed.flerio.value
        navigationItem.rightBarButtonItem?.image = navIcon()
    }
    
    private func navIcon() -> UIImage {
        return .themed.switchThemeIcon.value.withRenderingMode(.alwaysOriginal)
    }
}

extension SettingsViewControllerImpl: SettingsViewController {
    func setSubscriptionViewState(_ state: SubscriptionViewSate) {
        subscriptionView.updateState(state)
    }
    
    func updateName(_ text: String) {
        senderInformationView.nameInput.text = text
    }
    
    func updatePhone(_ text: String) {
        senderInformationView.phoneInput.text = text
    }
    
    func updateNotificationSwitcher(isOn: Bool) {
        advancedView.isNotificationSwitcherOn = isOn
    }
}
