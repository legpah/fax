//
//  ToOnboardingRouterInterceptor.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation

extension RoutingInterceptorToken {
    static func toOnboardingInterceptor() -> RoutingInterceptor {
        ToOnboardingRouterInterceptor()
    }
}

struct ToOnboardingRouterInterceptor: RoutingInterceptor {
    
    static var isOnboardingShowed: Bool {
        UserDefaults.standard.bool(forKey: .userDefaults.isOnboardingShowed)
    }
    
    func interceptRoute(from: ScreenRouter, to: ScreenRouter) -> ScreenRouter? {
        if ToOnboardingRouterInterceptor.isOnboardingShowed {
            return nil
        } else {
            return to
        }
    }
}
