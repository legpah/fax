//
//  UIColor+Themed.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit

extension UIColor {
    static let themed = UIThemed<UIColor>.self
}

extension UIThemed where T == UIColor {
    /// FFFFFF; FFFFFF
    static let ww = UIThemed<UIColor>(light: .ds_wh, dark: .ds_wh)
    
    /// 000000; 000000
    static let dd = UIThemed<UIColor>(light: .ds_d, dark: .ds_d)
    
    /// E5E7EB; E5E7EB
    static let w1w1 = UIThemed<UIColor>(light: .ds_wh1, dark: .ds_wh1)
    
    /// 9CA3AF; 3A3A3A
    static let gr23 = UIThemed<UIColor>(light: .ds_gr2, dark: .ds_gr3)
    
    /// F9FAFB ; 000000
    static let flerio = UIThemed<UIColor>(light: .ds_wh3, dark: .ds_d)
    
    /// FFFFFF; 000000
    static let bw = UIThemed<UIColor>(light: .ds_wh, dark: .ds_d)
    
    /// 000000;  FFFFFF
    static let ibw = UIThemed<UIColor>(light: .ds_d, dark: .ds_wh)
    
    /// F9FAFB ; 2D2E2E
    static let gojo = UIThemed<UIColor>(light: .ds_wh3, dark: .ds_d2)
    
    /// E3E3E3 ; 2D2E2E
    static let mavenpik = UIThemed<UIColor>(light: .ds_wh5, dark: .ds_d2)
    
    /// F9FAFC; 000000
    static let paida = UIThemed<UIColor>(light: .ds_wh2, dark: .ds_d)
    
    /// E5E7EB; 000000
    static let girbesh = UIThemed<UIColor>(light: .ds_wh1, dark: .ds_d)
    
    /// E5E7EB; 0C0C0C
    static let lanka = UIThemed<UIColor>(light: .ds_wh1, dark: .ds_d1)
    
    /// F9FAFB; 0C0C0C
    static let tikruk = UIThemed<UIColor>(light: .ds_wh3, dark: .ds_d1)
    
    /// E5E7EB; 2D2E2E
    static let xero = UIThemed<UIColor>(light: .ds_wh1, dark: .ds_d2)
    
    /// 6366F1; 6366F1
    static let tukesh = UIThemed<UIColor>(light: .ds_pur, dark: .ds_pur)
    
    /// FFFFFF; 0C0C0C
    static let goeridio = UIThemed<UIColor>(light: .ds_wh, dark: .ds_d1)
    
    /// 9CA3AF; 000000
    static let hetu = UIThemed<UIColor>(light: .ds_gr2, dark: .ds_d)
    
    /// B3B3B3; 555555
    static let wibgok = UIThemed<UIColor>(light: .ds_gr1, dark: .ds_gr)
    
    /// B3B3B3; 555555
    static let ameiloo = UIThemed<UIColor>(light: .ds_wh3, dark: .ds_d3)
    
    /// B3B3B3; 555555
    static let voiero = UIThemed<UIColor>(light: .ds_gr2, dark: .ds_gr3)
    
    /// 9CA3AF; 737373
    static let baboo = UIThemed<UIColor>(light: .ds_gr2, dark: .ds_gr4)
    
    /// 191919; B3B3B3
    static let equil = UIThemed<UIColor>(light: .ds_d3, dark: .ds_gr1)
    
    /// F9FAFB; 191919
    static let w3d3 = UIThemed<UIColor>(light: .ds_wh3, dark: .ds_d3)
    
    /// 4C4C4C; 959697
    static let gr65 = UIThemed<UIColor>(light: .ds_gr6, dark: .ds_gr5)
    
    /// B3B3B3; 4C4C4C
    static let gr16 = UIThemed<UIColor>(light: .ds_gr1, dark: .ds_gr6)
    
    /// 6B7280; 6B7280
    static let gr77 = UIThemed<UIColor>(light: .ds_gr7, dark: .ds_gr7)
    
    /// E5E7EB; 242424
    static let amber = UIThemed<UIColor>(light: .ds_wh1, dark: .ds_d4)
    
    /// F59E0B; F59E0B
    static let yelyel = UIThemed<UIColor>(light: .ds_yel, dark: .ds_yel)
    
    /// EF4444; EF4444
    static let redred = UIThemed<UIColor>(light: .ds_red, dark: .ds_red)
    
    /// 10B981; 10B981
    static let greengreen = UIThemed<UIColor>(light: .ds_green, dark: .ds_green)
    
    /// E0E0FC; 2D2E2E
    static let purity = UIThemed<UIColor>(light: .ds_pur1, dark: .ds_d2)
    
    /// 808080; 808080
    static let gr9gr9 = UIThemed<UIColor>(light: .ds_gr9, dark: .ds_gr9)
    
    /// 565D69; 9CA3AF
    static let gundeshtalt = UIThemed<UIColor>(light: .ds_gr10, dark: .ds_gr2)
    
    /// 565D69; 9CA3AF
    static let quejek = UIThemed<UIColor>(light: .ds_gr12, dark: .ds_gr11)
    
    /// FFFFFF; 242424
    static let cucumber = UIThemed<UIColor>(light: .ds_wh, dark: .ds_d4)
    
    /// E5E7EB; 373738
    static let igram = UIThemed<UIColor>(light: .ds_wh1, dark: .ds_gr_13)
    
    /// FFFFFF; 0C0C0C
    static let whd1 = UIThemed<UIColor>(light: .ds_wh, dark: .ds_d1)
    
    /// C4C4C5; 161616
    static let gr14d5 = UIThemed<UIColor>(light: .ds_gr_14, dark: .ds_d5)
    
    /// 000000;  FFFFFF alpha = 0.5
    static let ibw50 = UIThemed<UIColor>(light: .ds_d.withAlphaComponent(0.5), dark: .ds_wh.withAlphaComponent(0.5))
    
    /// BABABA; 202020
    static let gr15d6 = UIThemed<UIColor>(light: .ds_gr_15, dark: .ds_d6)
 
    /// 9CA3AF; FFFFFF
    static let grawh = UIThemed<UIColor>(light: .ds_gr2, dark: .ds_wh)
}

extension UIColor {
    /// FFFFFF
    static let ds_wh = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    /// E5E7EB
    static let ds_wh1 = #colorLiteral(red: 0.8980392157, green: 0.9058823529, blue: 0.9215686275, alpha: 1)
    /// F9FAFC
    static let ds_wh2 = #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 0.9882352941, alpha: 1)
    /// F9FAFB
    static let ds_wh3 = #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 0.9843137255, alpha: 1)
    /// E5E7EA
    static let ds_wh4 = #colorLiteral(red: 0.8980392157, green: 0.9058823529, blue: 0.9176470588, alpha: 1)
    /// E3E3E3
    static let ds_wh5 = #colorLiteral(red: 0.8980392157, green: 0.9058823529, blue: 0.9176470588, alpha: 1)
    
    /// 555555
    static let ds_gr = #colorLiteral(red: 0.3333333333, green: 0.3333333333, blue: 0.3333333333, alpha: 1)
    /// B3B3B3
    static let ds_gr1 = #colorLiteral(red: 0.7019607843, green: 0.7019607843, blue: 0.7019607843, alpha: 1)
    /// 9CA3AF
    static let ds_gr2 = #colorLiteral(red: 0.6117647059, green: 0.6392156863, blue: 0.6862745098, alpha: 1)
    /// 3A3A3A
    static let ds_gr3 = #colorLiteral(red: 0.2274509804, green: 0.2274509804, blue: 0.2274509804, alpha: 1)
    /// 737373
    static let ds_gr4 = #colorLiteral(red: 0.4509803922, green: 0.4509803922, blue: 0.4509803922, alpha: 1)
    /// 4C4C4C
    static let ds_gr5 = #colorLiteral(red: 0.2980392157, green: 0.2980392157, blue: 0.2980392157, alpha: 1)
    /// 959697
    static let ds_gr6 = #colorLiteral(red: 0.5843137255, green: 0.5882352941, blue: 0.5921568627, alpha: 1)
    /// 6B7280
    static let ds_gr7 = #colorLiteral(red: 0.4196078431, green: 0.4470588235, blue: 0.5019607843, alpha: 1)
    /// C4C4C5
    static let ds_gr8 = #colorLiteral(red: 0.768627451, green: 0.768627451, blue: 0.768627451, alpha: 1)
    /// 808080
    static let ds_gr9 = #colorLiteral(red: 0.5019607843, green: 0.5019607843, blue: 0.5019607843, alpha: 1)
    /// 565D69
    static let ds_gr10 = #colorLiteral(red: 0.337254902, green: 0.3647058824, blue: 0.4117647059, alpha: 1)
    /// 414141
    static let ds_gr11 = #colorLiteral(red: 0.2549019608, green: 0.2549019608, blue: 0.2549019608, alpha: 1)
    /// AFAFAF
    static let ds_gr12 = #colorLiteral(red: 0.6862745098, green: 0.6862745098, blue: 0.6862745098, alpha: 1)
    /// 373738
    static let ds_gr_13 = #colorLiteral(red: 0.2156862745, green: 0.2156862745, blue: 0.2196078431, alpha: 1)
    /// D8D8D8
    static let ds_gr_14 = #colorLiteral(red: 0.8470588235, green: 0.8470588235, blue: 0.8470588235, alpha: 1)
    /// BABABA
    static let ds_gr_15 = #colorLiteral(red: 0.7294117647, green: 0.7294117647, blue: 0.7294117647, alpha: 1)
    
    
    /// 6366F1
    static let ds_pur = #colorLiteral(red: 0.3882352941, green: 0.4, blue: 0.9450980392, alpha: 1)
    /// E0E0FC
    static let ds_pur1 = #colorLiteral(red: 0.8784313725, green: 0.8784313725, blue: 0.9882352941, alpha: 1)
    /// 7C7FE5
    static let ds_pur2 = #colorLiteral(red: 0.4862745098, green: 0.4980392157, blue: 0.8980392157, alpha: 1)
    
    /// 000000
    static let ds_d = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    /// 0C0C0C
    static let ds_d1 = #colorLiteral(red: 0.04705882353, green: 0.04705882353, blue: 0.04705882353, alpha: 1)
    /// 2D2E2E
    static let ds_d2 = #colorLiteral(red: 0.1764705882, green: 0.1803921569, blue: 0.1803921569, alpha: 1)
    /// 191919
    static let ds_d3 = #colorLiteral(red: 0.09803921569, green: 0.09803921569, blue: 0.09803921569, alpha: 1)
    /// 242424
    static let ds_d4 = #colorLiteral(red: 0.1411764706, green: 0.1411764706, blue: 0.1411764706, alpha: 1)
    /// 161616
    static let ds_d5 = #colorLiteral(red: 0.0862745098, green: 0.0862745098, blue: 0.0862745098, alpha: 1)
    /// 202020
    static let ds_d6 = #colorLiteral(red: 0.1254901961, green: 0.1254901961, blue: 0.1254901961, alpha: 1)
    
    /// EF4444
    static let ds_red = #colorLiteral(red: 0.937254902, green: 0.2666666667, blue: 0.2666666667, alpha: 1)
    
    /// F59E0B
    static let ds_yel = #colorLiteral(red: 0.9607843137, green: 0.6196078431, blue: 0.0431372549, alpha: 1)
    
    /// 10B981
    static let ds_green = #colorLiteral(red: 0.06274509804, green: 0.7254901961, blue: 0.5058823529, alpha: 1)
}
