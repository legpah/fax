//
//  FaxHistoryCellModel.swift
//  FaxApp
//
//  Created by Eugene on 29.04.2022.
//

import Foundation

struct FaxHistoryCellModel: Hashable {
    let id: String
    let status: FaxHistorySentStatus
    let errorMessage: String
    let statusDescription: String
    let previewUrl: String
    let pdfUrl: String
    let recipient: String
    let sentTimeTimestamp: Int
    let onFilePreviewTap: IClosure<FaxHistoryCellModel>
    let onErrorTipTap: IClosure<FaxHistoryCellModel>
    
    static func == (lhs: FaxHistoryCellModel, rhs: FaxHistoryCellModel) -> Bool {
        lhs.id == rhs.id
        && lhs.status == rhs.status
        && lhs.errorMessage == rhs.errorMessage
        && lhs.previewUrl == rhs.previewUrl
        && lhs.pdfUrl == rhs.pdfUrl
        && lhs.statusDescription == rhs.statusDescription
        && lhs.recipient == rhs.recipient
        && lhs.sentTimeTimestamp == rhs.sentTimeTimestamp
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}

extension FaxHistoryCellModel {
    init(remoteItem: FaxHistoryRemoteItem,
         onFilePreviewTap: @escaping IClosure<FaxHistoryCellModel>,
         onErrorTipTap: @escaping IClosure<FaxHistoryCellModel>) {
        self.id = remoteItem.id
        self.status = remoteItem.status
        self.previewUrl = remoteItem.previewUrl
        self.pdfUrl = remoteItem.fileUrl
        self.statusDescription = FaxApp.statusDescription(remoteItem)
        self.recipient = remoteItem.recipient
        self.sentTimeTimestamp = remoteItem.sentAt
        self.errorMessage = remoteItem.errorDescription?.localizationTitle ?? ""
        self.onFilePreviewTap = onFilePreviewTap
        self.onErrorTipTap = onErrorTipTap
    }
}

fileprivate func statusDescription(_ item: FaxHistoryRemoteItem) -> String {
    switch item.status {
    case .sent:
        if item.isOnePage {
            return L10n.History.sentCount1
        }
        return L10n.History.sentCount(item.pagesCount)
    case .delivering:
        if item.isOnePage {
            return L10n.History.deliveringCount1
        }
        return L10n.History.deliveringCount(item.pagesCount)
    case .error:
        return L10n.History.notDelivered
    }
}
