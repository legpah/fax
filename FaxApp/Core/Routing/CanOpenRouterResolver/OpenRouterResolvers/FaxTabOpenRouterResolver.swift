//
//  FaxTabOpenRouterResolver.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import Foundation

extension CanOpenRouterResolverToken {
    static var faxTabResolver: CanOpenScreenRouterResolver {
        FaxTabOpenRouterResolver()
    }
}

fileprivate struct FaxTabOpenRouterResolver: CanOpenScreenRouterResolver {
    func canOpen(router: ScreenRouter, from: ScreenRouter) -> Bool {
        switch RouterType(indentifiable: router.routerIdentifiable) {
        case .rootTab,
                .paywall,
                .sendMail,
                .faq:
            return false
        case .alertOptions,
                .sendingFax,
                .onboarding,
                .contactsPicker,
                .frontPdfPage,
                .singleImagePicker,
                .pdfPreview,
                .documentPicker,
                .countryPhonesList:
            return true
        }
    }
}
