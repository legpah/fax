//
//  ViewOutput.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import Foundation

protocol ViewOutput: AnyObject {
    func viewDidLoad()
    
    func viewWillAppear()
    
    func viewDidAppear()
    
    func viewWillDisappear()
    
    func viewWillDissmiss()
    
    func viewWillDeinit()
}

extension ViewOutput {
    func viewDidLoad() {}
    
    func viewWillAppear() {}
    
    func viewDidAppear() {}
    
    func viewWillDisappear() {}
    
    func viewWillDissmiss() {}
    
    func viewWillDeinit() {}
}
