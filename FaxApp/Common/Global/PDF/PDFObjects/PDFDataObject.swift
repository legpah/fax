//
//  PDFDataObject.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import Foundation
import PDFKit
import Combine

final class PDFDataObject: PDFObject {
    let unerlyingSource: PDFSource
    let pdfDocument: PDFDocument?
    private(set) lazy var pdfData: Data? = pdfDocument?.dataRepresentation()
    var pdfPreview: AnyPublisher<UIImage?, Never> {
        imageSubject.eraseToAnyPublisher()
    }
    
    private let imageSubject = CurrentValueSubject<UIImage?, Never>(nil)
    
    init(data: Data) {
        self.unerlyingSource = .pdf(data)
        self.pdfDocument = PDFDocument(data: data)
        generatePdfPreview { [weak self] image in
            self?.imageSubject.send(image)
        }
    }
}
