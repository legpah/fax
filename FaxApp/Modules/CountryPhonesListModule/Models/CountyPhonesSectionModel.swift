//
//  CountyPhonesSectionModel.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import UIKit

enum CountyPhonesSectionModel: Int, DiffableTableSection {
    
    case first
    
    var sectionIndex: Int { rawValue }
    
    func createHeader() -> UIView? {
        nil
    }
}
