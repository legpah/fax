//
//  SingleImagePickerRouter.swift
//  FaxApp
//
//  Created by Eugene on 19.04.2022.
//

import UIKit

protocol SingleImagePickerRouter: AnyObject {
    func closeScreen(image pickedImage: UIImage?)
}

final class SingleImagePickerRouterImpl: BaseScreenRouter, ScreenRouter {
    
    var screen: UIViewController { _screen }
    var routerIdentifiable: RouterIdentifiable {
        RouterType.singleImagePicker(model.source)
    }
    let presentationType: ScreenPresentationType = .modal
    
    private let model: SingleImagePickerRouterModel
    
    private lazy var _screen: UIViewController = {
        let presenter = SingleImagePickerPresenterImpl(router: self)
        let view = SingleImagePickerViewControllerImpl()
        presenter.view = view
        view.configure(presenter: presenter, source: model.source)
        return view
    }()
    
    init(model: SingleImagePickerRouterModel) {
        self.model = model
    }
}

extension SingleImagePickerRouterImpl: SingleImagePickerRouter {
    func closeScreen(image pickedImage: UIImage?) {
        closeLast { [model] in
            if let pickedImage = pickedImage {
                model.onPickImage(pickedImage)
            }
        }
        lastClosed()
    }
}
