//
//  UIViewController+AddSubController.swift
//  FaxApp
//
//  Created by Eugene on 27.04.2022.
//

import UIKit

extension UIViewController {
    func addSubController(_ controller: UIViewController) {
        view.addSubview(controller.view)
        addChild(controller)
        controller.didMove(toParent: self)
    }
}
