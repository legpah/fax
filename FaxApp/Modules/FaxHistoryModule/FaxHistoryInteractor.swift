//
//  FaxHistoryInteractor.swift
//  FaxApp
//
//  Created by Eugene on 29.04.2022.
//

import Combine
import Foundation

protocol FaxHistoryInteractor {
    var faxHistory: AnyPublisher<[FaxHistoryRemoteItem], NetworkError> { get }
    func deleteAllHistory() -> AnyPublisher<EmptyResponse, NetworkError>
    func delete(id: String) -> AnyPublisher<EmptyResponse, NetworkError>
}

final class FaxHistoryInteractorImpl {
    
    private let historyService: FaxHistoryService
    private let deleteService: FaxDeleteService
    
    init(historyService: FaxHistoryService = FaxHistoryServiceImpl(),
         deleteService: FaxDeleteService = FaxDeleteServiceImpl()) {
        self.historyService = historyService
        self.deleteService = deleteService
    }
}

extension FaxHistoryInteractorImpl: FaxHistoryInteractor {
    var faxHistory: AnyPublisher<[FaxHistoryRemoteItem], NetworkError> {
        historyService.faxHistory
    }
    
    func deleteAllHistory() -> AnyPublisher<EmptyResponse, NetworkError> {
        deleteService.deleteAll()
    }
    
    func delete(id: String) -> AnyPublisher<EmptyResponse, NetworkError> {
        deleteService.delete(id: id)
    }
}
