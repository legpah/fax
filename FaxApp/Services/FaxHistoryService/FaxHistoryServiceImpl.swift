//
//  FaxHistoryServiceImpl.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Combine
import Foundation

final class FaxHistoryServiceImpl {
    private let api = ApiClient()
}

extension FaxHistoryServiceImpl: FaxHistoryService {
    var faxHistory: AnyPublisher<[FaxHistoryRemoteItem], NetworkError> {
        let request = FaxHistoryEndpoint()
        return api
            .request(request: request)
            .eraseToAnyPublisher()
    }
}
