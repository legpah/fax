//
//  TextConverters+SubscriptionDate.swift
//  FaxApp
//
//  Created by Eugene on 27.05.2022.
//

import Foundation

extension TextConvertersTag {
    static func subscriptionDate(date: Date?) -> String {
        if let date = date {
            return DateFormatter.uiSubscriptionFormatter.string(from: date)
        }
        return "unrecognized"
    }
}
