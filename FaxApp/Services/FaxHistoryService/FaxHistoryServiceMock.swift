//
//  FaxHistoryServiceMock.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Combine
import Foundation

final class FaxHistoryServiceMock {
    
    private func time() -> Int {
        let rnd = (500...300000).randomElement() ?? 0
        return Int(Date().timeIntervalSince1970) - rnd
    }
    
    private func recipient() -> String {
        ["+7 777 77 77", "+90 000 00 00", "+1 000 00 00 "].randomElement() ?? ""
    }
    
    private func status() -> FaxHistorySentStatus {
        [FaxHistorySentStatus.sent, FaxHistorySentStatus.delivering, FaxHistorySentStatus.error].randomElement() ?? .sent
    }
    
    func mockItem() -> FaxHistoryRemoteItem {
        let stat = status()
        let isWrongPhone = Bool.random()
        let error: ApiInternalError? = stat == .error ? .init(
            id: "1343",
            image: isWrongPhone ? "wrong_phone" : "something_went_wrong",
            title: isWrongPhone ? L10n.Error.WrongNumber.title : L10n.GenericInform.Error.Wrong.title,
            subtitle: "lorem lorem",
            analyticsCode: isWrongPhone ? "0" : "1",
            analyticsMessage: isWrongPhone ? "wrong_phone" : "something_went_wrong"
        ) : nil

        return FaxHistoryRemoteItem(
            id: UUID().uuidString,
            status: stat,
            fileUrl: "",
            previewUrl: "",
            pagesCount: (1...5).randomElement() ?? 3,
            recipient: recipient(),
            sentAt: time(),
            errorDescription: error
        )
    }
    
    func mock() -> [FaxHistoryRemoteItem] {
        (0..<20).map { _ in mockItem() }
    }
}

extension FaxHistoryServiceMock: FaxHistoryService {
    var faxHistory: AnyPublisher<[FaxHistoryRemoteItem], NetworkError> {
//        let value = (0...3).randomElement() ?? 0
        let value = 0
        if value == 0 {
            return Just(mock())
                .setFailureType(to: NetworkError.self)
                .delay(for: 1, scheduler: DispatchQueue.main)
                .eraseToAnyPublisher()
        } else if value == 1 {
            return Fail(error: NetworkError.wrongRequest)
                .delay(for: 1, scheduler: DispatchQueue.main)
                .eraseToAnyPublisher()
        } else if value == 2 {
            return Fail(error: NetworkError.notInternetConnection)
                .delay(for: 1, scheduler: DispatchQueue.main)
                .eraseToAnyPublisher()
        } else {
            return Fail(error: NetworkError.httpError(code: 012))
                .delay(for: 1, scheduler: DispatchQueue.main)
                .eraseToAnyPublisher()
        }
    }
}
