//
//  FaxMainPageViewController+FaxDocumentsHeaderBlockView.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

extension FaxMainPageViewControllerImpl {
    final class FaxDocumentsHeaderBlockView: UIView {
        
        private let titleLabel = Label(text: L10n.Main.documents,
                                       font: .poppins(12, .semiBold))
        
        init() {
            super.init(frame: .zero)
            configureUI()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureUI() {
            addSubview(titleLabel)
            
            configureConstraints()
        }
        
        private func configureConstraints() {
            setDimension(.height, toSize: 20)
            
            titleLabel.pinEdges(toSuperviewEdges: [.left])
            titleLabel.alignAxis(toSuperviewAxis: .y)
        }
    }
}
