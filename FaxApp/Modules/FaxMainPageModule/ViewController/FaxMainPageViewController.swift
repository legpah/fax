//
//  FaxMainPageViewController.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit
import Combine

typealias FaxDiffableSource = DiffableTableDataSource<
    SendFaxDocumentSectionModel,
    SendFaxDocumentCellModel
>

protocol FaxMainPageViewController: AnyObject {
    var documentsTable: FaxDiffableSource { get }
    func update(state: FaxMainPageViewState, animated: Bool)
    func updateTableViewEditing(isEditing: Bool)
    func updateSendButton(isActive: Bool)
    func setShouldScrollToBottom(flag: Bool)
    func setLoadDocumentState(_ value: Bool)
}

final class FaxMainPageViewControllerImpl: UIViewController {
    
    private let presenter: FaxMainPagePresenter
    
    private let clearBarButton = TextBarButton(text: L10n.Main.clear, position: .right)
    private let previewBarButton = TextBarButton(text: L10n.Main.preview, position: .left)
    private let recipientLabel = Label(text: L10n.Main.recipient, font: .poppins(12, .semiBold))
    private let countryPhoneInputViewController: CountryPhoneInputController
    private let documentsHeaderBlock = FaxDocumentsHeaderBlockView()
    private let documentsBlock = FaxDocumentsBlockView()
    private let bottomButtonBlock = BottomButtonsBlockView()
    private lazy var keyboardResigner = KeyboardOutsideTouchesResignerView(controller: self)
    private let activityIndicatorView = UIActivityIndicatorView(style: UIActivityIndicatorView.Style.medium)
    
    private lazy var documentsHeightConstraint =
    documentsBlock.setDimension(.height, toSize: 0)
    private let bottomButtonsOffset: CGFloat = 32
    
    private var cancelBag = Set<AnyCancellable>()
    private var isFirstLayoutSubviews = true
    private var shouldScrollToBottom = false
    
    init(presenter: FaxMainPagePresenter,
         countryPhoneInputViewController: CountryPhoneInputController) {
        self.presenter = presenter
        self.countryPhoneInputViewController = countryPhoneInputViewController
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apply(theme: .current)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if isFirstLayoutSubviews {
            updateDocumentsHeight()
        }
        isFirstLayoutSubviews = false
    }
    
    private func configureUI() {
        title = L10n.Main.fax
        
        view.addSubview(recipientLabel)
        addSubController(countryPhoneInputViewController)
        view.addSubview(documentsHeaderBlock)
        view.addSubview(documentsBlock)
        view.addSubview(bottomButtonBlock)
        view.addSubview(activityIndicatorView)
        
        keyboardResigner.trackKeyboard(of: countryPhoneInputViewController.input)
        
        configureActivityIndicator()
        configurePreviewBarButton()
        configureClearBarButton()
        configureRecipientLabel()
        configureCountryPhoneInputView()
        configureDocumentsHeaderBlock()
        configureDocumentsBlockBlock()
        configureBottomButtonsBlock()
        
        themeProvider.register(observer: self)
        
        presenter.editDocumentsStateSet()
    }
    
    private func configureActivityIndicator() {
        activityIndicatorView.color = .themed.ibw50.value
        activityIndicatorView.centerInSuperview()
    }
    
    private func configurePreviewBarButton() {
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: previewBarButton)
        previewBarButton.onTap = { [weak self] in
            self?.presenter.previewAllTapped()
        }
    }
    
    private func configureClearBarButton() {
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(customView: clearBarButton)
        clearBarButton.onTap = { [weak self] in
            self?.presenter.clearAllTapped()
        }
    }
    
    private func configureRecipientLabel() {
        recipientLabel.pin(toTopLayoutGuideOf: self, withInset: 24)
        recipientLabel.pinEdge(toSuperviewEdge: .left, withInset: .layout.xInset)
    }
    
    private func configureCountryPhoneInputView() {
        countryPhoneInputViewController.view.pinEdge(.top, to: .bottom, of: recipientLabel, withOffset: 12)
        countryPhoneInputViewController.view.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
    }
    
    private func configureDocumentsHeaderBlock() {
        documentsHeaderBlock.pinEdge(.top, to: .bottom, of: countryPhoneInputViewController.view, withOffset: 24)
        documentsHeaderBlock.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
    }
    
    private func configureDocumentsBlockBlock() {
        documentsBlock.pinEdge(.top, to: .bottom, of: documentsHeaderBlock, withOffset: 13)
        documentsBlock.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        _ = documentsHeightConstraint
        documentsBlock
            .dataSource
            .sourceDidChange
            .sink(receiveValue: { [weak self] in
                self?.updateDocumentsHeight()
            }).store(in: &cancelBag)
    }
    
    private func configureBottomButtonsBlock() {
        bottomButtonBlock.pin(toBottomLayoutGuideOf: self, withInset: 32)
        bottomButtonBlock.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        bottomButtonBlock.onSendTap = { [weak self] in
            self?.presenter.sendFileTapped()
        }
        bottomButtonBlock.onAddFileTap = { [weak self] in
            self?.presenter.addFileTapped()
        }
    }
    
    private func updateDocumentsHeight() {
        let animated = !isFirstLayoutSubviews
        let height: CGFloat
        let cellsCount = CGFloat(documentsBlock.dataSource.itemsCount(section: .first))
        let cellHeight = documentsBlock.cellHeight
        defer {
            if documentsHeightConstraint.constant != height {
                documentsHeightConstraint.constant = height
                UIView.animate(withDuration: animated ? .animations.defaultDuration : 0) {
                    self.view.layoutIfNeeded()
                }
            }
        }
        
        guard cellsCount > 2 else {
            height = cellsCount == 0
            ? cellHeight * 2
            : cellsCount * cellHeight
            return
        }

        let topBound = documentsBlock.frame.minY
        let bottomBound = bottomButtonBlock.frame.minY - bottomButtonsOffset
        let availableHeight = bottomBound - topBound
        let totalHeight = cellsCount * documentsBlock.cellHeight

        height = min(totalHeight, availableHeight)
        
        if cellsCount > 0 && totalHeight > availableHeight && shouldScrollToBottom {
            let ip = IndexPath(item: Int(cellsCount - 1),
                               section: 0)
            documentsBlock.tableView.scrollToRow(at: ip,
                                                 at: .bottom,
                                                 animated: true)
            documentsBlock.reloadTableView()
        }
        setShouldScrollToBottom(flag: false)
    }
}

extension FaxMainPageViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        view.backgroundColor = .themed.bw.value
        activityIndicatorView.color = .themed.ibw50.value
        applyNavigationBarColor(.bw)
    }
}

extension FaxMainPageViewControllerImpl: FaxMainPageViewController {
    func setLoadDocumentState(_ value: Bool) {
        tabBarController?.view.isUserInteractionEnabled = value
        view.isUserInteractionEnabled = value
        activityIndicatorView.isHidden = value
        value ? activityIndicatorView.stopAnimating() : activityIndicatorView.startAnimating()
    }
    
    var documentsTable: FaxDiffableSource {
        documentsBlock.dataSource
    }
    
    func updateTableViewEditing(isEditing: Bool) {
        documentsBlock.tableView.setEditing(isEditing, animated: true)
    }
    
    func updateSendButton(isActive: Bool) {
        bottomButtonBlock.isSendFileButtonActive = isActive
    }
    
    func setShouldScrollToBottom(flag: Bool) {
        shouldScrollToBottom = flag
    }
    
    func update(state: FaxMainPageViewState, animated: Bool) {
        switch state {
        case .emptyDocuments:
            clearBarButton.alpha = 0
            previewBarButton.alpha = 0
            documentsBlock.togglePlaceholder(isVisible: true, animated: animated)
            bottomButtonBlock.toggleSendButton(isVisible: false, animated: animated)
        case .haveDocuments:
            clearBarButton.alpha = 1
            previewBarButton.alpha = 1
            documentsBlock.togglePlaceholder(isVisible: false, animated: animated)
            bottomButtonBlock.toggleSendButton(isVisible: true, animated: animated)
        }
    }
}
