//
//  DocumentPickerRouter.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import UIKit

protocol DocumentPickerRouter: AnyObject {
    func screenClosed()
    func closeScreen(documentResult: Result<DocumentPickerOutput, Error>)
}

final class DocumentPickerRouterImpl: BaseScreenRouter, ScreenRouter {
    
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.documentPicker
    let presentationType: ScreenPresentationType = .modal
    
    private let model: DocumentPickerRoutingModel
    
    private lazy var _screen: UIViewController = {
        let presenter = DocumentPickerPresenterImpl(router: self)
        let view = DocumentPickerViewControllerImpl(
            presenter: presenter,
            documentTypes: model.documentTypes,
            mode: .import
        )
        return view
    }()
    
    init(model: DocumentPickerRoutingModel) {
        self.model = model
    }
}

extension DocumentPickerRouterImpl: DocumentPickerRouter {
    func screenClosed() {
        lastClosed()
        model.onDismissAction()
    }
    
    func closeScreen(documentResult: Result<DocumentPickerOutput, Error>) {
        closeLast { [weak self, model] in
            self?.lastClosed()
            model.onDocumentsPicked(documentResult)
            model.onDismissAction()
        }
    }
}
