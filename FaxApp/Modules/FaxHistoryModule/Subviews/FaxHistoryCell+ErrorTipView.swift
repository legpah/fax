//
//  FaxHistoryCell+ErrorTipView.swift
//  FaxApp
//
//  Created by Eugene on 24.05.2022.
//

import Foundation
import UIKit

extension FaxHistoryCell {
    final class ErrorTipView: UIView {
        
        private let questionIcon = ImageView(themedImage: .questionError)
        let errorLabel = Label(font: .poppins(12), color: .quejek)
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(questionIcon)
            addSubview(errorLabel)
            
            questionIcon.pinEdge(toSuperviewEdge: .left)
            questionIcon.alignAxis(toSuperviewAxis: .y)
            questionIcon.setDimensions(to: .symmetric(14))
            
            errorLabel.pinEdge(.left, to: .right, of: questionIcon, withOffset: 6)
            errorLabel.pinEdge(toSuperviewEdge: .right)
            errorLabel.alignAxis(toSuperviewAxis: .y)
            
            pinEdge(.top, to: .top, of: errorLabel)
            pinEdge(.bottom, to: .bottom, of: errorLabel)
        }
    }
}
