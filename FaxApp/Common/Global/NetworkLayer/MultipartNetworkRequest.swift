//
//  MultipartNetworkRequest.swift
//  FaxApp
//
//  Created by Eugene on 27.05.2022.
//

import Foundation

struct MultipartNetworkRequestFile {
    
    let name: String
    let fileData: Data
    let fileType: FileType
    
    enum FileType {
        case jpeg
        case pdf
    }
}

protocol MultipartNetworkRequest: NetworkRequest {
    var files: [MultipartNetworkRequestFile] { get }
}

extension MultipartNetworkRequest {
    var headers: [String : String]? { nil }
    
    func asURLRequest(baseURL: String) -> URLRequest? {
        guard var urlComponents = URLComponents(string: baseURL) else {
            return nil
        }
        
        urlComponents.path = "\(urlComponents.path)\(path)"
        urlComponents.queryItems = queryParams?.map{ k, v in
            URLQueryItem(name: k, value: v)
        }
        
        guard let finalURL = urlComponents.url else {
            return nil
        }
        
        var request = URLRequest(url: finalURL)
        request.httpMethod = method.rawValue
        
        let boundary = "\(UUID().uuidString)"
        let bodyData = mulipartBody(boundary: boundary)
        
        request.addValue(
            .httpHeaders.multipartData + "; boundary=\(boundary)",
            forHTTPHeaderField: .httpHeaders.contentType
        )
        if let bodyData = bodyData {
            request.addValue(
                "\(bodyData.count)",
                forHTTPHeaderField: .httpHeaders.contentLength
            )
        }
        
        headers?.forEach { k, v in
            request.addValue(v, forHTTPHeaderField: k)
        }
        
        request.httpBody = bodyData
        
        return request
    }
    
    private func mulipartBody(boundary: String) -> Data? {
        var resultData = Data()
        
        let filesItems = files.map {
            multiPartFileData(boundary: boundary,
                              name: $0.name,
                              fileType: $0.fileType,
                              data: $0.fileData)
        }
        
        let valuesItems = body?.map {
            multiPartValueData(boundary: boundary, name: $0.key, value: $0.value)
        } ?? []
        
        let allItems = filesItems + valuesItems
        
        allItems.enumerated().forEach { idx, value in
            resultData.append(value)
            if idx < allItems.count - 1 {
                resultData.append("\r\n".data(using: .utf8)!)
            } else {
                resultData.append("\r\n--\(boundary)--".data(using: .utf8)!)
            }
        }
        return resultData
    }
}

fileprivate func multiPartFileData(boundary: String,
                                   name: String,
                                   fileType: MultipartNetworkRequestFile.FileType,
                                   data: Data) -> Data {
    
    let contentType = fileType.contentType
    let fileExtension = fileType.fileExtension
    
    var result = Data()
    result.append("--\(boundary)\r\n".data(using: .utf8)!)
    result.append("Content-Disposition: form-data; name=\"\(name)\"; filename=\"\(name).\(fileExtension)\"\r\n".data(using: .utf8)!)
    result.append("Content-Type: \(contentType)\r\n\r\n".data(using: .utf8)!)
    result.append(data)
    return result
}

fileprivate func multiPartValueData(boundary: String,
                                    name: String,
                                    value: Any) -> Data {
    
    var result = Data()
    result.append("\r\n--\(boundary)\r\n".data(using: .utf8)!)
    result.append("Content-Disposition: form-data; name=\"\(name)\"\r\n\r\n".data(using: .utf8)!)
    result.append(String(describing: value).data(using: .utf8)!)
    
    return result
}

extension Data {
    fileprivate mutating func addBoundary(boundary: String) {
        self.append("--\(boundary)".data(using: .utf8)!)
    }
    
    fileprivate mutating func addReturnNewLine() {
        self.append("\r\n".data(using: .utf8)!)
    }
}

extension MultipartNetworkRequestFile.FileType {
    fileprivate var contentType: String {
        switch self {
        case .jpeg:
            return "image/jpeg"
        case .pdf:
            return "application/pdf"
        }
    }
    
    fileprivate var fileExtension: String {
        switch self {
        case .jpeg:
            return "jpeg"
        case .pdf:
            return "pdf"
        }
    }
}
