//
//  CountryPhoneInputFormatter.swift
//  FaxApp
//
//  Created by Eugene on 3.05.2022.
//

import Foundation

final class CountryPhoneInputFormatter {
    struct FormatResult {
        let fomattedPhone: String
        let isValid: Bool
    }
    
    func format(_ phone: String, country: CountryPhone) -> FormatResult {
        let formattedPhone = _format(phone, mask: country.mask)
        let isValid = isValid(phone: phone, formattedPhone: formattedPhone, country: country)
        return FormatResult(
            fomattedPhone: formattedPhone,
            isValid: isValid
        )
    }
    
    private func _format(_ phone: String, mask: String) -> String {
        let numbers = phone.replacingOccurrences(of: "[^0-9]",
                                                 with: "",
                                                 options: .regularExpression)
        var result = ""
        var numbersIndex = numbers.startIndex
        var maskIndex = mask.startIndex
        
        while maskIndex < mask.endIndex || numbersIndex < numbers.endIndex {
            if let maskCh = mask[safe: maskIndex], let numbersCh = numbers[safe: numbersIndex] {
                if maskCh == "X" {
                    result.append(numbersCh)
                    numbersIndex = numbers.index(after: numbersIndex)
                    maskIndex = mask.index(after: maskIndex)
                } else {
                    result.append(maskCh)
                    maskIndex = mask.index(after: maskIndex)
                }
                
                continue
            }
            
            if let numbersCh = numbers[safe: numbersIndex] {
                result.append(numbersCh)
                numbersIndex = numbers.index(after: numbersIndex)
                continue
            }
            
            break
        }
        
        return result
    }
    
    private func isValid(phone: String,
                         formattedPhone: String,
                         country: CountryPhone) -> Bool {
        if country.code == .countries.usaCanadaCode {
            return formattedPhone.replaceDidgits("X") == country.mask
        } else {
            let clearPhone = phone.onlyDidgits()
            return clearPhone.count >= 6 && clearPhone.count <= 15
        }
    }
}


extension String {
    subscript(safe index: Index) -> Element? {
        if index >= self.startIndex && index < self.endIndex {
            return self[index]
        }
        return nil
    }
}
