//
//  PDFConstants.swift
//  FaxApp
//
//  Created by Eugene on 15.04.2022.
//

import UIKit

enum PDFConstants {
    static let pagesLimit = 30
    static let megabytesLimit: Float = 20.0
    static let megabytesLimitInternal: Float = 19.8
    static let pageWidth: CGFloat = 595
    static let pageHeight: CGFloat = 842
    static let pageSize = CGSize(width: pageWidth, height: pageHeight)
    static let pageFrame = CGRect(origin: .zero, size: pageSize)   
}
