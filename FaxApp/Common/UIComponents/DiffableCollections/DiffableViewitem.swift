//
//  DiffableViewitem.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import Foundation

protocol DiffableViewItem: Hashable {}
