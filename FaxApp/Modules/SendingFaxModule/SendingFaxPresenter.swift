//
//  SendingFaxPresenter.swift
//  FaxApp
//
//  Created by Eugene on 20.05.2022.
//

import Foundation
import Combine
import UIKit

protocol SendingFaxPresenter: ViewOutput {
    var recipientPhone: String { get }
    var minPerPage: Int { get }
    var pagesNumber: String { get }
    var deliveryTime: String { get }
    
    func backButtonTapped()
    func previewButtonTapped()
    func sendButtonTapped()
}

final class SendingFaxPresenterImpl {
    
    enum State {
        case preview
        case sending
        case done
        case error
    }
    
    weak var view: SendingFaxViewController?
    
    private weak var router: SendingFaxRouter?
    private let recipientPhoneInj: String
    private let recipientCountryInj: String
    private let pdfObject: PDFMultiplePagesObject
    private let interactor: SendingFaxInteractor
    private let analytics: AnalyticsService
    private let purchaseService: PurchasesService
    private var cancelBag = Set<AnyCancellable>()
    private var subscriptionBag: Cancellable?
    private var state: State = .preview
    private let notificationsRegisterer: NotificationsRegisterer
    
    init(recipientPhone: String,
         recipientCountry: String,
         pdfObject: PDFMultiplePagesObject,
         router: SendingFaxRouter,
         interactor: SendingFaxInteractor = SendingFaxInteractorImpl(),
         analytics: AnalyticsService = AnalyticsServiceImpl.shared,
         purchaseService: PurchasesService = PurchasesService.shared,
         notificationsRegisterer: NotificationsRegisterer = NotificationsRegistererImpl()
    ) {
        self.recipientPhoneInj = recipientPhone
        self.recipientCountryInj = recipientCountry
        self.pdfObject = pdfObject
        self.router = router
        self.interactor = interactor
        self.analytics = analytics
        self.purchaseService = purchaseService
        self.notificationsRegisterer = notificationsRegisterer
    }
    
    private var pageCount: Int {
        pdfObject.pageCount ?? 0
    }
    
    private func requestSendFax() {
        #if !DEBUG
        guard purchaseService.currentState.subscription != nil else {
            router?.openPaywallScreen(model: PaywallModel(pages: pageCount, number: recipientPhoneInj))
            analytics.send(AnalyticsEventImpl(name: "paywall_shown", parameters: ["source": "send"]))
            return
        }
        #endif
        state = .sending
        view?.update(state: .sending)
        interactor.sendFax(
            pdfObject: pdfObject,
            recipient: recipientPhone
        )
        .sinkResult { [weak self] result in
            self?.handleSendFaxResponse(result)
        }.store(in: &cancelBag)
    }
    
    private func handleSendFaxResponse(_ result: Result<Void, NetworkError>) {
        switch result {
        case .success:
            state = .done
            view?.updateControllerTitle("Done")
            view?.update(
                state: .infromView(
                    .sendFaxDone(
                        estimatedMinutes: self.deliveryTime,
                        onTap: { [weak self] in
                            self?.router?.closeLast()
                            self?.router?.lastClosed()
                            self?.analytics.send(AnalyticsEventImpl(name: "done_screen_ok_clicked"))
                        }
                    ), .done
                )
            )
            
            SendFaxNotification.notifyFaxSended(pagesCount: pdfObject.pageCount ?? 0)
            
            analytics.send(
                AnalyticsEventImpl(
                    name: "done_screen_shown",
                    parameters: [
                        "files_number": pdfObject.documentsCount,
                        "page_quantity": pdfObject.pageCount ?? 0,
                        "recepient_country": recipientCountryInj,
                        "has_frontpage": pdfObject.hasFrontPage.analyticValue
                    ]
                
                )
            )
        case let .failure(error):

            SendFaxErrorNotification.notifyErrorSended(error: error)
            
            state = .error
            
            if error.isSubscriptionExpired {
                view?.update(
                    state: .infromView(
                        .networkError(error, buttonText: L10n.Sending.goToPaywall, onTap: { [weak self] in
                            guard let self = self else { return }
                            self.router?.openPaywallScreen(model: PaywallModel(pages: self.pageCount, number: self.recipientPhoneInj))
                        }), .subscriptionExpired
                    )
                )
                
                subscribeOnSubscriptionState()
            }
            else if error.isWrongNumber {
                view?.update(
                    state: .infromView(
                        .networkError(error, buttonText: "OK", onTap: { [weak self] in
                            self?.router?.closeLast(animated: true, completion: nil)
                            self?.router?.lastClosed()
                        }), .generic
                    )
                )
                
                analytics.send(AnalyticsEventImpl(name: "wrong_number_screen_shown",
                                                  parameters: ["error_id": error.id ?? "",
                                                               "error_description": error.errorDescription]))
            }
            else {
                view?.update(
                    state: .infromView(
                        .networkError(error, buttonText: L10n.Sending.retry, onTap: { [weak self] in
                            self?.requestSendFax()
                            self?.sendAnalytics(isRetry: true)
                        }), .generic
                    )
                )
                
                if case .notInternetConnection = error {
                    analytics.send(AnalyticsEventImpl(name: "no_internet_screen_shown"))
                }
                else {
                    analytics.send(AnalyticsEventImpl(name: "smth_went_wrong_screen_shown"))
                }
                
                analytics.send(AnalyticsEventImpl(name: "sending_error_screen_shown"))
            }
        }
    }
    
    private func sendAnalytics(isRetry: Bool) {
        analytics.send(
            AnalyticsEventImpl(
                name: isRetry ? "sending_error_screen_retry_clicked" : "ready_to_send_screen_send_clicked",
                parameters: [
                    "files_number": pdfObject.documentsCount,
                    "page_quantity": pdfObject.pageCount ?? 0,
                    "recepient_country": recipientCountryInj,
                    "has_frontpage": pdfObject.hasFrontPage.analyticValue
                ]
            )
        )
    }
    
    private func sendBackAnalytics() {
        switch state {
        case .preview, .sending:
            analytics.send(AnalyticsEventImpl(name: "ready_to_send_screen_back_clicked"))
        case .done:
            analytics.send(AnalyticsEventImpl(name: "done_screen_back_clicked"))
        case .error:
            analytics.send(AnalyticsEventImpl(name: "sending_error_screen_back_clicked"))
        }
    }
    
    private func askForPushNotificationIfNeeded() {
        notificationsRegisterer.checkAuthorizationStatus(.notDetermined) { [weak self] in
            self?.notificationsRegisterer.requestAuthorization()
            return
        }
        notificationsRegisterer.checkAuthorizationStatus(.denied) { [weak self] in
            self?.showPushNotification()
        }
    }
    
    private func showPushNotification() {
        if !(UserDefaults.standard.value(forKey: .userDefaults.isAlertForNotificationsShowed) as? Bool ?? false) {
            router?.showPushNotificationAlert(onSelect: { [weak self] options in
                switch options {
                case .yes:
                    self?.router?.openSetings()
                    self?.analytics.send(AnalyticsEventImpl(name: "notif_perm", parameters: ["answer": "yes", "variant": "custom"]))
                case .no:
                    self?.router?.showOkPushAlert()
                    self?.analytics.send(AnalyticsEventImpl(name: "notif_perm", parameters: ["answer": "no", "variant": "custom"]))
                }
            })
        }
    }
    
    private func subscribeOnSubscriptionState() {
        subscriptionBag = purchaseService.purchasePublisher.sinkSuccess { [weak self] _ in
            guard let self else { return }
            
            if case .subscribed(_) = self.purchaseService.currentState {
                self.view?.update(state: .preview)
                self.subscriptionBag?.cancel()
            }
        }
    }
    
}

extension SendingFaxPresenterImpl: SendingFaxPresenter {
    func viewDidLoad() {
        view?.update(state: .preview)
        analytics.send(AnalyticsEventImpl(name: "ready_to_send_screen_shown"))
        askForPushNotificationIfNeeded()
    }
    
    var minPerPage: Int {
        2
    }
    
    var recipientPhone: String {
        recipientPhoneInj
    }
    
    var pagesNumber: String {
        "\(pageCount) " + .converter.pageWord(count: pageCount)
    }
    
    var deliveryTime: String {
        "\(pageCount * minPerPage) \(L10n.Sending.minutes)"
    }
    
    func sendButtonTapped() {
        requestSendFax()
        sendAnalytics(isRetry: false)
    }
    
    func previewButtonTapped() {
        router?.showPdfPreview(
            model: PDFPreviewPageRouterModel(
                pdfObject: pdfObject,
                canEditFrontPage: false,
                onDocumentEdited: nil
            )
        )
    }
    
    func backButtonTapped() {
        router?.closeLast()
        router?.lastClosed()
        sendBackAnalytics()
    }
}
