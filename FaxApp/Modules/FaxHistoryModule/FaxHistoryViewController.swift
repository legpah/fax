//
//  FaxHistoryViewController.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit
import Combine

typealias FaxHistoryDiffableSource = DiffableTableDataSource<
    FaxHistorySectionModel,
    FaxHistoryTableItemModel
>

enum FaxHistoryAppearance {
    case loading
    case history
    case inform(GenericInformViewModel)
}

protocol FaxHistoryViewController: AnyObject {
    func update(appearance: FaxHistoryAppearance)
    var tableSource: FaxHistoryDiffableSource { get }
}

final class FaxHistoryViewControllerImpl: UIViewController {
    
    private let presenter: FaxHistoryPresenter
    private lazy var dataSource = FaxHistoryDiffableSource(
        tableView: tableView,
        canEditItem: { params in params.item.historyCellModel != nil }
    )
    private var appearance: FaxHistoryAppearance?
    private var cancelBag = Set<AnyCancellable>()
    
    private let clearAllButton = TextBarButton(text: L10n.History.clear, position: .right)
    private let loadingSpinner = ActivityIndicator(style: .medium)
    private let pullToRefreshControl = UIRefreshControl()
    private let tableView = TableView(registerCells: [
        FaxHistoryCell.self,
        FaxHistoryInfoCell.self,
    ])
    private let informView = GenericInformView()
    
    init(presenter: FaxHistoryPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSelf()
        presenter.viewDidLoad()
        
        #warning("REMOVE")
        //view.addGestureRecognizer(UILongPressGestureRecognizer(target: self, action: #selector(tap)))
    }
    
    @objc func tap() {
        let id = UserUIDProviderImpl.shared.uuid
        
        let alertController = UIAlertController(title: "My id", message: id, preferredStyle: .alert)

        // Create a copy action
        let copyAction = UIAlertAction(title: "Copy", style: .default) { (action:UIAlertAction!) in
            // Handle copy action
            UIPasteboard.general.string = id
        }

        // Add the copy action to the alert controller
        alertController.addAction(copyAction)

        // Create a cancel action
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (action:UIAlertAction!) in
            // Handle cancel action
        }

        // Add the cancel action to the alert controller
        alertController.addAction(cancelAction)

        // Present the alert controller
        self.present(alertController, animated: true, completion:nil)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }
    
    private func configureSelf() {
        title = L10n.History.title
     
        dataSource.defaultRowAnimation = .fade
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(customView: clearAllButton)
        view.addSubview(tableView)
        view.addSubview(loadingSpinner)
        view.addSubview(informView)
        
        configureLoadingSpinner()
        configureTableView()
        configurePullToRefreshSpipper()
        configreErrorView()
        
        tableView.separatorStyle = .none
        
        clearAllButton.isHidden = true
        clearAllButton.onTap = { [weak self] in
            self?.presenter.clearAllTapped()
        }
        dataSource.sourceDidChange.sink(receiveValue: { [weak self] in
            self?.pullToRefreshControl.endRefreshing()
            self?.clearAllButton.isHidden = self?.dataSource.items(in: .first).isEmpty ?? true
        }).store(in: &cancelBag)
        
        themeProvider.register(observer: self)
    }
    
    private func configureLoadingSpinner() {
        loadingSpinner.centerInSuperview()
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.contentInset = .init(top: 24, left: 0, bottom: 0, right: 0)
        tableView.pin(toTopLayoutGuideOf: self)
        tableView.pin(toBottomLayoutGuideOf: self)
        tableView.pinEdges(toSuperviewEdges: [.left, .right])
    }
    
    private func configurePullToRefreshSpipper() {
        tableView.refreshControl = pullToRefreshControl
    }
    
    private func configreErrorView() {
        informView.pin(toTopLayoutGuideOf: self)
        informView.pin(toBottomLayoutGuideOf: self)
        informView.pinEdges(toSuperviewEdges: [.left, .right])
    }
}

extension FaxHistoryViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        view.backgroundColor = .themed.bw.value
        tableView.backgroundColor = .themed.flerio.value
        pullToRefreshControl.overrideUserInterfaceStyle = theme.asSystemTheme
    }
}

extension FaxHistoryViewControllerImpl: FaxHistoryViewController {
    var tableSource: FaxHistoryDiffableSource {
        dataSource
    }
    
    func update(appearance: FaxHistoryAppearance) {
        func update() {
            switch appearance {
            case .loading:
                clearAllButton.alpha = 0
                tableView.isUserInteractionEnabled = false
                informView.alpha = 0
                loadingSpinner.isAnimating = true
            case .history:
                clearAllButton.alpha = 1
                tableView.isUserInteractionEnabled = true
                informView.alpha = 0
                loadingSpinner.isAnimating = false
                pullToRefreshControl.endRefreshing()
            case let .inform(model):
                clearAllButton.alpha = 0
                tableView.isUserInteractionEnabled = false
                informView.alpha = 1
                loadingSpinner.isAnimating = false
                pullToRefreshControl.endRefreshing()
                informView.updateView(model: model)
            }
        }
        
        UIView.animate(
            withDuration: self.appearance == nil ? 0 : .animations.defaultDuration
        ) {
            update()
        }
        
        self.appearance = appearance
    }
}

extension FaxHistoryViewControllerImpl: UITableViewDelegate {
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        if pullToRefreshControl.isRefreshing {
            presenter.pullToRefreshTriggered()
        }
    }
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        93
    }
}
