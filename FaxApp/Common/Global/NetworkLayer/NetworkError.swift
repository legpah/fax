//
//  NetworkError.swift
//  FaxApp
//
//  Created by Eugene on 11.05.2022.
//

import Foundation

enum NetworkError: Error {
    case decodingError
    case decodingInternalError
    case unknownError
    case wrongRequest
    case notInternetConnection
    case apiError(ApiInternalError)
    case httpError(code: Int)
}

struct ApiInternalError: Decodable {
    let id: String
    let image: String
    let title: String
    let subtitle: String
    let analyticsCode: String
    let analyticsMessage: String
    
    enum CodingKeys: String, CodingKey {
        case id
        case image
        case title
        case subtitle
        case analyticsCode
        case analyticsMessage = "analyticMessage"
    }
    
    var localizationTitle: String {
        if analyticsCode == "3912" {
            return L10n.Error.WrongNumber.title
        }
        else if analyticsCode == "12007" {
            return L10n.Error.SmtWrong.title
        }
        return title
    }
    
    var localizationSubTitle: String {
        if analyticsCode == "3912" {
            return L10n.GenericInform.Error.Number.subTitle
        }
        else if analyticsCode == "12007" {
            return L10n.Error.SmtWrong.subtitle
        }
        
        return subtitle
    }
}

extension NetworkError {
    var id: String? {
        switch self {
        case let .apiError(err):
            return err.id
        default:
            return nil
        }
    }
    var errorCode: String {
        switch self {
        case .decodingError:
            return "1000"
        case .unknownError:
            return "1001"
        case .wrongRequest:
            return "1002"
        case .notInternetConnection:
            return "1003"
        case .decodingInternalError:
            return "1005"
        case .apiError(let apiInternalError):
            return apiInternalError.analyticsCode
        case .httpError(let code):
            return "\(code)"
        }
    }
    
    var errorDescription: String {
        switch self {
        case .decodingError:
            return "Serizalization error"
        case .decodingInternalError:
            return "Serizalization internal error"
        case .unknownError:
            return "Unknown error"
        case .wrongRequest:
            return "Wrong client request"
        case .notInternetConnection:
            return "No internet connection"
        case .apiError(let apiInternalError):
            return apiInternalError.analyticsMessage
        case .httpError:
            return "HTTP Error"
        }
    }
    
    var isSubscriptionExpired: Bool {
        switch self {
        case .apiError(let apiInternalError):
            return apiInternalError.analyticsCode == "1006"
        default:
            return false
        }
    }
    
    //TODO: Need right analyticsCode for invalid number on server, now it -1 for all error
    var isWrongNumber: Bool {
        switch self {
        case .apiError(let apiInternalError):
            return  apiInternalError.analyticsCode == "3912" ||
                    apiInternalError.analyticsMessage.lowercased() == "Invalid faxNumber".lowercased()
        default:
            return false
        }
    }
}
