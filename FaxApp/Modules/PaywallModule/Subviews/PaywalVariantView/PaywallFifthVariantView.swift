//
//  PaywallFifthVariantView.swift
//  FaxApp
//
//  Created by Eugene on 15.07.2022.
//

import UIKit

final class PaywallFifthVariantView: UIView, PaywallVariantView {
    var itemsContainer: PremiumItemsContainer {
        premiumItemsView
    }
    
    let bgView = UIImageView()
    private let faxImage = ImageView(themedImage: .paywallFourthFax)
    private lazy var titleLabel = Label(
        text: size.value(
            L10n.Paywall.Fifth.title,
            L10n.Paywall.Fifth.title2,
            L10n.Paywall.Fifth.title3
        ),
        font: .poppins(size.value(42, 32, 24), .semiBold),
        color: .init(value: .ds_wh),
        lines: 0
    )
    private lazy var firstStarView = StarItemView(
        size: size,
        title: L10n.Paywall.Fifth.Star.First.title,
        subtitle: L10n.Paywall.Fifth.Star.First.subtitle
    )
    private lazy var secondStarView = StarItemView(
        size: size,
        title: L10n.Paywall.Fifth.Star.Second.title,
        subtitle: L10n.Paywall.Fifth.Star.Second.subtitle
    )
    
    private let premiumItemsView = PremiumItemsContainerImpl()
    
    private weak var controller: UIViewController?
    private let size: PaywallSizeVariant
    
    init(
        controller: UIViewController,
        size: PaywallSizeVariant
    ) {
        self.size = size
        self.controller = controller
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSelf() {
        controller?.view.insertSubview(bgView, at: 0)
        bgView.image = Asset.paywallFifthBg.image
        addSubview(faxImage)
        addSubview(titleLabel)
        addSubview(firstStarView)
        addSubview(secondStarView)
        addSubview(premiumItemsView)
        
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        
        configureConstraints()
    }
    
    private func configureConstraints() {
        bgView.pinEdgesToSuperviewEdges()
        
        titleLabel.pinEdge(toSuperviewEdge: .top, withInset: size.value(24, 24, 16))
        titleLabel.pinEdge(toSuperviewEdge: .left, withInset: 24)
        
        faxImage.pinEdge(.top, to: .top, of: titleLabel, withOffset: size.value(35, 20, 10))
        faxImage.pinEdge(toSuperviewEdge: .right, withInset: -38)
        faxImage.setDimensions(
            to: size.value(
                .init(width: 232, height: 271),
                .init(width: 208, height: 243),
                .init(width: 148, height: 183)
            )
        )
        faxImage.transform = .identity.rotated(by: -(30 * Double.pi / 180))
        
        firstStarView.pinEdge(toSuperviewEdge: .left, withInset: size.value(24, 24, 24))
        firstStarView.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: size.value(24, 24, 32))
        firstStarView.pinEdge(.right, to: .left, of: faxImage, withOffset: size.value(40, 40, 20))
        
        secondStarView.pinEdge(toSuperviewEdge: .left, withInset: 24)
        secondStarView.pinEdge(.top, to: .bottom, of: firstStarView, withOffset: 17)
        secondStarView.pinEdge(.right, to: .left, of: faxImage, withOffset: size.value(150, 150, 100))
        
        
        premiumItemsView.pinEdge(.top, to: .bottom, of: secondStarView, withOffset: size.value(30, 25, 42))
        premiumItemsView.pinEdges(toSuperviewEdges: [.left, .right])
        premiumItemsView.pinEdge(toSuperviewEdge: .bottom, withInset: 5, relation: .greaterThanOrEqual)
    }
}

extension PaywallFifthVariantView {
    fileprivate final class StarItemView: UIView {
        
        private let size: PaywallSizeVariant
        private let starImage = ImageView(themedImage: .paywallStarWhite)
        private lazy var titleLabel = Label(
            font: .poppins(size.value(20, 18, 18), .semiBold),
            color: .init(value: .ds_wh),
            lines: 0
        )
        private let subtitleLabel = Label(
            font: .poppins(12),
            color: .init(value: .ds_wh.withAlphaComponent(0.7)),
            lines: 0
        )
        
        init(size: PaywallSizeVariant, title: String, subtitle: String) {
            self.size = size
            super.init(frame: .zero)
            titleLabel.text = title
            subtitleLabel.text = subtitle
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(starImage)
            addSubview(titleLabel)
            addSubview(subtitleLabel)
            
            starImage.setDimensions(to: CGSize(width: 27, height: 24))
            starImage.pinEdges(toSuperviewEdges: [.left, .top])
            
            titleLabel.pinEdge(toSuperviewEdge: .top)
            titleLabel.pinEdge(.left, to: .right, of: starImage, withOffset: 16)
            titleLabel.pinEdge(toSuperviewEdge: .right)
            
            subtitleLabel.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 4)
            subtitleLabel.pinEdge(.left, to: .left, of: titleLabel)
            subtitleLabel.pinEdge(toSuperviewEdge: .right)
            
            self.pinEdge(.bottom, to: .bottom, of: subtitleLabel)
        }
    }
}
