//
//  StringReuseIdentifiable.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import Foundation
import UIKit

protocol StringReuseIdentifiable {
    var reuseIdentifier: StringIdentifier { get }
}
