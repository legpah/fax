//
//  TabBarController.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import UIKit
import Combine

final class TabBarController: UITabBarController {
    private(set) var currentTab: TabBarType = .fax {
        didSet { sendAnalytics() }
    }
    
    var onItemTap: IClosure<TabBarType>?
    
    var statusBarTheme: UITheme? {
        didSet {
            statusBarTracker.statusBarTheme = statusBarTheme
            DispatchQueue.main.async { [weak self] in
                self?.setNeedsStatusBarAppearanceUpdate()
            }
        }
    }
    
    private let tabBarTopBorder = UIView()
    private let tabBarBackgroundArea = UIView()
    private let tabBarMenu: TabBarMenu
    private let statusBarTracker = StatusbarTracker()
    private let analytics: AnalyticsService
    
    init(items: [TabBarType],
         analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.tabBarMenu = TabBarMenu(items: items)
        self.analytics = analytics
        super.init(nibName: nil, bundle: nil)
        self.tabBar.isHidden = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSelf()
    }
    
    override var childForStatusBarStyle: UIViewController? {
        statusBarTracker
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.viewControllers?.forEach { controller in
            controller.additionalSafeAreaInsets =  UIEdgeInsets(
                top: tabBar.safeAreaInsets.top,
                left: tabBar.safeAreaInsets.left,
                bottom: .layout.tabHeight,
                right: tabBar.safeAreaInsets.right
            )
        }
    }
    
    func switchToTab(tab: TabBarType) {
        tabBarMenu.setSelected(item: tab)
        self.selectedIndex = tab.rawValue
        self.currentTab = tab
    }
    
    // MARK: - Private
    
    private func configureSelf() {
        view.addSubview(tabBarTopBorder)
        view.addSubview(tabBarBackgroundArea)
        view.addSubview(tabBarMenu)
        
        tabBarMenu.onItemTap = { [weak self] item in
            guard let self = self else { return }
            self.selectedIndex = item.rawValue
            self.currentTab = item
            self.onItemTap?(item)
        }
        
        configureConstraints()
        
        themeProvider.register(observer: self)
    }
    
    private func configureConstraints() {
        tabBarTopBorder.pinEdges(toSuperviewEdges: [.left, .right])
        tabBarTopBorder.pinEdge(.top, to: .top, of: tabBarMenu, withOffset: -1)
        tabBarTopBorder.setDimension(.height, toSize: 1)
        
        tabBarBackgroundArea.pinEdge(.top, to: .top, of: tabBarMenu)
        tabBarBackgroundArea.pinEdges(toSuperviewEdges: [.left, .right, .bottom])
        
        tabBarMenu.pinEdges(toSuperviewEdges: [.left, .right], withInset: 5)
        tabBarMenu.pin(toBottomLayoutGuideOf: self)
        tabBarMenu.setDimension(.height, toSize: .layout.tabHeight - 1)
    }
    
    private func sendAnalytics() {
        analytics.send(
            AnalyticsEventImpl(
                name: "tab\(currentTab.analyticsIndex)_clicked"
            )
        )
    }
}

extension TabBarController: UIThemable {
    func apply(theme: UITheme) {
        self.tabBarBackgroundArea.backgroundColor = .themed.flerio.value
        self.tabBarTopBorder.backgroundColor = .themed.mavenpik.value
        setNeedsStatusBarAppearanceUpdate()
    }
}

fileprivate final class StatusbarTracker: UIViewController {
    
    var statusBarTheme: UITheme?
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        if let theme = statusBarTheme {
            return theme.asStatusBarStyle
        }
        return UITheme.current.asStatusBarStyle
    }
}
