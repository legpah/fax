//
//  CountryPhonesListRoutable.swift
//  FaxApp
//
//  Created by Eugene on 27.04.2022.
//

import Foundation

protocol CountryPhonesListRoutable: AnyObject {
    func openCountyPhonesList(onSelectCountry: @escaping IClosure<CountryPhone>)
}

extension CountryPhonesListRoutable where Self: ScreenRouter {
    func openCountyPhonesList(onSelectCountry: @escaping IClosure<CountryPhone>) {
        open(router: CountryPhonesListRouterImpl(onSelectCountry: onSelectCountry))
    }
}
