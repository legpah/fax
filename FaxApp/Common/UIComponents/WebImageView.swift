//
//  WebImageView.swift
//  FaxApp
//
//  Created by Eugene on 10.05.2022.
//

import UIKit
import Nuke

final class WebImageView: UIView {
    
    // MARK: - Internal properties
    
    var bgColor: UIThemed<UIColor> = .bw {
        didSet { apply(theme: .current) }
    }
    
    var placeholderImageContentMode: UIView.ContentMode = .center {
        didSet {
            placeholderImageView.contentMode = placeholderImageContentMode
        }
    }
    
    var placeholderEdgesScale: CGFloat = 0.9 {
        didSet { updatePlaceholderImageEdgesScale() }
    }
    
    // MARK: - Private properties
    
    private enum State {
        case empty
        case fetching
        case webImage
        case placeholder
        
        var isFetching: Bool { self == .fetching }
    }
    
    private let webImageView = UIImageView()
    private let placeholderImageView = UIImageView()
    
    private var imageUrl: URL?
    private var fetchedImageUrl: URL?
    
    private var state: State = .empty {
        didSet {
            switch state {
            case .empty:
                imageUrl = nil
                fetchedImageUrl = nil
                webImageView.image = nil
                webImageView.isHidden = false
                placeholderImageView.isHidden = true
            case .webImage:
                webImageView.isHidden = false
                placeholderImageView.isHidden = true
            case .placeholder, .fetching:
                webImageView.isHidden = true
                placeholderImageView.isHidden = false
            }
        }
    }
    
    // MARK: - Init
    
    init() {
        super.init(frame: .zero)
        configureUI()
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    
    override func layoutSubviews() {
        super.layoutSubviews()
        resizeWebImage()
    }
    
    // MARK: - Internal methods
    
    func setImage(url: URL?) {
        imageUrl = url
        fetchImage()
    }
    
    func cancelLoading() {
        cancelRequest(for: webImageView)
        state = .empty
    }
    
    // MARK: - Private methods
    
    private func fetchImage() {
        guard let imageUrl = imageUrl else {
            state = .placeholder
            return
        }
        
        if fetchedImageUrl == imageUrl {
            return
        }
        
        guard !state.isFetching else {
            return
        }
        
        state = .fetching
        
        loadImage(with: imageUrl, into: webImageView, completion: { [weak self] result in
            guard let self = self else {
                return
            }
            switch result {
            case .success:
                self.fetchedImageUrl = imageUrl
                self.resizeWebImage()
                self.state = .webImage
            case .failure:
                self.state = .placeholder
            }
        })
    }
    
    // MARK: - UI Configuration
    
    private func configureUI() {
        configureWebImageView()
        configurePlaceholderImageView()
        themeProvider.register(observer: self)
    }
    
    private func configureWebImageView() {
        webImageView.layer.masksToBounds = false
        addSubview(webImageView)
        webImageView.pinEdgesToSuperviewEdges()
    }
    
    private func configurePlaceholderImageView() {
        placeholderImageView.isHidden = true
        updatePlaceholderImageEdgesScale()
        placeholderImageView.contentMode = placeholderImageContentMode
        addSubview(placeholderImageView)
        placeholderImageView.image = Asset.imagePlaceholder.image
        placeholderImageView.pinEdgesToSuperviewEdges()
    }
    
    private func updatePlaceholderImageEdgesScale() {
        placeholderImageView.transform = .init(scaleX: placeholderEdgesScale,
                                                    y: placeholderEdgesScale)
    }
    
    private func resizeWebImage() {
        guard self.frame != .zero else { return }
        webImageView.image = webImageView.image?.resizeImage(targetSize: frame.size)
    }
}

extension WebImageView: UIThemable {
    func apply(theme: UITheme) {
        backgroundColor = bgColor.value
    }
}
