//
//  NotificationCenterRegisterer.swift
//  FaxApp
//
//  Created by Eugene on 30.05.2022.
//

import UserNotifications
import UIKit

protocol NotificationsRegisterer {
    var isNotificationsEnabled: Bool { get }
    
    func requestAuthorization()
    func unregister()
    func updateFirebase(token: String)
    func checkAuthorizationStatus(_ status: UNAuthorizationStatus, onStatus: @escaping VoidClosure)
}

final class NotificationsRegistererImpl: NotificationsRegisterer {
    
    private let updateNotificationService: UpdateNotificationService
    private let analytics: AnalyticsService
    private let udToken = UDAccess<Bool>(key: .userDefaults.notificationToken)
    
    init(updateNotificationService: UpdateNotificationService = UpdateNotificationServiceImpl(),
         analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.updateNotificationService = updateNotificationService
        self.analytics = analytics
    }
    
    var isNotificationsEnabled: Bool {
        udToken.value ?? false
    }
    
    func requestAuthorization() {
        requestUserPermissions { [weak self] in
            guard let self = self else { return }
            self.udToken.value = true
            DispatchQueue.main.async {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
    
    func unregister() {
        udToken.value = false
        UIApplication.shared.unregisterForRemoteNotifications()
    }
    
    func updateFirebase(token: String) {
        guard let deviceId = UIDevice.current.identifierForVendor else {
            //analytics.send(AnalyticsEventImpl(name: "obtain_device_id_failed"))
            return
        }
        
        updateNotificationService.updateToken(token: token, deviceId: deviceId.uuidString)
    }
    
    func checkAuthorizationStatus(_ status: UNAuthorizationStatus, onStatus: @escaping VoidClosure) {
        UNUserNotificationCenter.current().getNotificationSettings { settings in
            if case status = settings.authorizationStatus {
                onStatus()
            }
        }
    }
    
    private func requestUserPermissions(then completion: @escaping VoidClosure) {
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options: [.sound, .alert, .badge]) { [weak self] (granted, error) in
            guard error == nil else { return }
            guard granted else {
                self?.analytics.send(AnalyticsEventImpl(name: "notif_perm", parameters: ["answer": "no", "variant": "nativ"]))
                return
            }
            self?.analytics.send(AnalyticsEventImpl(name: "notif_perm", parameters: ["answer": "yes", "variant": "nativ"]))
            completion()
        }
    }
}
