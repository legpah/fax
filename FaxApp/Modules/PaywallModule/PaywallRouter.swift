//
//  PaywallRouter.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import UIKit

protocol PaywallRouter: AlertOptionsRoutable, TabOpenerRouter, PDFPreviewRoutable {
}

final class PaywallRouterImpl: BaseScreenRouter, ScreenRouter, PaywallRouter {
    
    private let type: PaywallType
    
    init(type: PaywallType = .standart) {
        self.type = type
    }
    
    static func factory(router: PaywallRouter, type: PaywallType = .standart) -> UIViewController {
        let presenter =
        PaywallPresenterImpl(router: router, type: type)
        let view = PaywallViewControllerImpl(presenter: presenter)
        presenter.view = view
        return view
    }
    
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.paywall
    let presentationType: ScreenPresentationType = .modal
    
    private lazy var _screen: UIViewController = {
        return PaywallRouterImpl.factory(router: self, type: self.type)
    }()
}
