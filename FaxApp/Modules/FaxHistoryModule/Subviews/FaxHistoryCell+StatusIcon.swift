//
//  FaxHistoryCell+StatusIcon.swift
//  FaxApp
//
//  Created by Eugene on 3.05.2022.
//

import UIKit

extension FaxHistoryCell {
    final class StatusIcon: UIView {
        
        var status: FaxHistorySentStatus? {
            didSet { updateStatusAppearance() }
        }
        
        private let iconImage = ImageView(themedImage: nil)
        private let spinner = BagelSpinner()
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(iconImage)
            addSubview(spinner)
            
            configureConstraints()
        }
        
        private func configureConstraints() {
            setDimensions(to: CGSize(width: 14, height: 14))
            iconImage.pinEdgesToSuperviewEdges(with: .symmetric(value: 0.8))
            spinner.pinEdgesToSuperviewEdges()
        }
        
        private func updateStatusAppearance() {
            guard let status = status else {
                return
            }
            switch status {
            case .sent:
                spinner.isAnimating = false
                spinner.alpha = 0
                iconImage.alpha = 1
                iconImage.themedImage = .checkmarkGreen
            case .error:
                spinner.isAnimating = false
                spinner.alpha = 0
                iconImage.alpha = 1
                iconImage.themedImage = .crestRed
            case .delivering:
                spinner.isAnimating = true
                spinner.alpha = 1
                iconImage.alpha = 0
            }
        }
    }
}
