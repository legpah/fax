//
//  FaxHistoryInfoCell.swift
//  FaxApp
//
//  Created by Eugene on 1.06.2022.
//

import UIKit

final class FaxHistoryInfoCell: UITableViewCell {
    
    private let infoLabel = Label(font: .poppins(12), alignment: .center, lines: 0)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        self.selectionStyle = .none
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSelf() {
        infoLabel.alpha = 0.3
        contentView.addSubview(infoLabel)
        infoLabel.pinEdgesToSuperviewEdges(
            with: .symmetric(x: .layout.xInset, y: 10)
        )
        themeProvider.register(observer: self)
    }
}

extension FaxHistoryInfoCell: UIThemable {
    func apply(theme: UITheme) {
        backgroundColor = .themed.flerio.value
    }
}

extension FaxHistoryInfoCell: SetupableCell {
    typealias Model = FaxHistoryInfoCellModel
    
    func setup(model: Model) {
        infoLabel.text = model.title
    }
}
