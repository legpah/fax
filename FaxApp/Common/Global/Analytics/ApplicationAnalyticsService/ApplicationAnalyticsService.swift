//
//  ApplicationAnalyticsService.swift
//  FaxApp
//
//  Created by Eugene on 31.05.2022.
//

import Foundation
import Combine

final class ApplicationAnalyticsService {
    
    private let analytics = AnalyticsServiceImpl.shared
    private let isFirstLaunch = UDAccess<Bool>(key: .userDefaults.isFirstLaunch)
    private let purchaseService = PurchasesService.shared
    private var cancelBag = Set<AnyCancellable>()
    
    func applicationLaunched() {
        sendFirstLaunchIfNeeded()
        sendRenewedSubscription()
    }
    
    private func sendRenewedSubscription() {
        purchaseService
            .subscriptionState
            .compactMap { $0.subscription }
            .first()
            .sinkSuccess({ [weak self] subscription in
                guard let self = self,
                      subscription.isRenewed
                else {
                    return
                }
            }).store(in: &cancelBag)
    }
    
    private func sendFirstLaunchIfNeeded() {
        guard isFirstLaunch.value == nil else {
            return
        }
        analytics.send(AnalyticsEventImpl(name: "first_launch"))
        isFirstLaunch.value = false
    }
}
