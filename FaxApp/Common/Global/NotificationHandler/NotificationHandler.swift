//
//  NotificationHandler.swift
//  FaxApp
//
//  Created by Eugene on 9.06.2022.
//

import Foundation
import UIKit

final class NotificationHandler {
    static let shared = NotificationHandler()
    
    private init() {}
    
    func handle(_ notification: UNNotification) {
        sendEvent(notification)
        
        if let deeplink = notification.userInfo["deeplink"] as? String,
           let deeplinkUrl = URL(string: deeplink) {
            handleDeeplink(deeplinkUrl)
        }
    }
    
    private func handleDeeplink(_ url: URL) {
        switch url.path {
        case "/history":
            UIApplication.shared.sceneDelegate?.appCoordinator.openTab(.faxHistory)
        default:
            break
        }
    }
    
    private func sendEvent(_ notification: UNNotification) {
        guard let type = notification.userInfo["push_type"] as? String else {
            return
        }
        AnalyticsServiceImpl.shared.send(
            AnalyticsEventImpl(
                name: "push_clicked",
                parameters: [
                    "push_type": type
                ]
            )
        )
    }
}

extension UNNotification {
    var userInfo: [AnyHashable: Any] {
        self.request.content.userInfo
    }
}
