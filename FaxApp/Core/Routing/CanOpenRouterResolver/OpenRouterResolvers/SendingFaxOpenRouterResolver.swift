//
//  SendingFaxOpenRouterResolver.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation

extension CanOpenRouterResolverToken {
    static var sendingFaxResolver: CanOpenScreenRouterResolver {
        SendingFaxOpenRouterResolver()
    }
}

fileprivate struct SendingFaxOpenRouterResolver: CanOpenScreenRouterResolver {
    func canOpen(router: ScreenRouter, from: ScreenRouter) -> Bool {
        switch RouterType(indentifiable: router.routerIdentifiable) {
        case .pdfPreview, .paywall, .alertOptions:
            return true
        default:
            return false
        }
    }
}
