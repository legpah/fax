//
//  StringIdentifiable.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

typealias StringIdentifier = String

protocol StaticStringIdentifiable {
    static var identifier: StringIdentifier { get }
}

extension UIView: StaticStringIdentifiable {
    static var identifier: StringIdentifier {
        String(describing: self)
    }
}
