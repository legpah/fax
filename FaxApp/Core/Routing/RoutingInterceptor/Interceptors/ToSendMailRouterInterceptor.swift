//
//  ToSendMailRouterInterceptor.swift
//  FaxApp
//
//  Created by Eugene on 24.05.2022.
//

import MessageUI

extension RoutingInterceptorToken {
    static func toSendMail() -> RoutingInterceptor {
        ToSendMailRouterInterceptor()
    }
}

fileprivate struct ToSendMailRouterInterceptor: RoutingInterceptor {
    func interceptRoute(from: ScreenRouter, to: ScreenRouter) -> ScreenRouter? {
        if MFMailComposeViewController.canSendMail() {
            return to
        } else {
            return AlertOptionsRouter(model: .unableToOpenMail())
        }
    }
}
