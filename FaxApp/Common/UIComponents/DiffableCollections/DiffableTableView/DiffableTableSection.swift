//
//  DiffableTableSection.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

protocol DiffableTableSection: DiffableViewItem {
    var sectionIndex: Int { get }
    func createHeader() -> UIView?
}
