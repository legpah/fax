//
//  PrintDebug.swift
//  FaxApp
//
//  Created by Eugene on 26.05.2022.
//

import Foundation

func printDebug(_ message: @autoclosure () -> String = String()) {
    #if DEBUG
    print(message())
    #endif
}
