//
//  DotsView.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import UIKit

extension OnboardingViewControllerImpl {
    final class DotsView: UIView {
        
        private let dotsSize: CGFloat
        private let dotsGap: CGFloat
        private let activeColor: UIColor
        private let inactiveColor: UIColor
        private let dotsViews: [UIView]
        private var isInitialSetuped = false
        private var activeIndex = 0
        
        init(dotsSize: CGFloat = 7,
             dotsGap: CGFloat = 12,
             activeColor: UIColor = .ds_d,
             inactiveColor: UIColor = .ds_d.withAlphaComponent(0.2)) {
            self.dotsSize = dotsSize
            self.dotsGap = dotsGap
            self.activeColor = activeColor
            self.inactiveColor = inactiveColor
            dotsViews = (0..<5).map { idx in
                let dotView = UIView()
                dotView.frame.size = .symmetric(dotsSize)
                dotView.backgroundColor = idx == 0 ? activeColor : inactiveColor
                dotView.applyBordersShape(width: 0, radius: dotsSize / 2)
                return dotView
            }
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            guard frame != .zero, !isInitialSetuped else {
                return
            }
            
            let dotsCount = CGFloat(dotsViews.count)
            let gapsWidth = dotsGap * (dotsCount - 1)
            let fullWidth = (dotsSize * dotsCount) + gapsWidth
            let dotY = frame.size.height / 2 - dotsSize / 2
            
            var dotX = (frame.size.width - fullWidth) / 2
            dotsViews.enumerated().forEach { idx, dot in
                dot.frame.origin = CGPoint(x: dotX, y: dotY)
                dotX += dotsSize + dotsGap
            }
            
            isInitialSetuped = true
        }
        
        private func configureSelf() {
            backgroundColor = .ds_wh3
            
            dotsViews.forEach { addSubview($0) }
        }
        
        func showNext() {
            guard let nextDot = dotsViews[safe: activeIndex + 1] else {
                return
            }
            
            let currentDot = dotsViews[0]
            let currentFrame = currentDot.frame
            let nextFrame = nextDot.frame
            
            activeIndex += 1
            UIView.animate(withDuration: .animations.onboardingCarousel, animations: {
                currentDot.frame = nextFrame
                nextDot.frame = currentFrame
            })
        }
    }
}
