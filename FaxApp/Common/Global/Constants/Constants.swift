//
//  Constants.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import Foundation

enum ConstantsToken {
    enum LayoutToken {}
    enum AnimationsToken {}
    enum Countries {}
}
