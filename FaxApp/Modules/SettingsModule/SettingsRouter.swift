//
//  SettingsRouter.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit
import StoreKit

protocol SettingsRouter: AnyObject, SettingsOpenerRouter {
    func openFaqScreen()
    func openSendMailScreen()
    func openPaywallScreen()
    func openPrivacyPolicy()
    func openRateApp()
}

final class SettingsRouterImpl: BaseScreenRouter, ScreenRouter, PDFPreviewRoutable {
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.rootTab(.settings)
    let presentationType: ScreenPresentationType = .plain
    
    private lazy var _screen: UIViewController = {
        let presenter = SettingsPresenterImpl(router: self)
        let view = SettingsViewControllerImpl(presenter: presenter)
        presenter.view = view
        
        return NavigationController(rootViewController: view)
    }()
}

extension SettingsRouterImpl: SettingsRouter {
    func openFaqScreen() {
        open(router: FAQRouterImpl())
    }
    
    func openPaywallScreen() {
        open(router: PaywallRouterImpl())
    }
    
    func openSendMailScreen() {
        open(router: SendMailRouter())
    }
    
    func openPrivacyPolicy() {
        openPdf(name: "privacy_policy", title: L10n.Settings.privacy)
    }
    
    func openRateApp() {
        guard let scene = UIApplication.shared.activeWindow?.windowScene else {
            return
        }
        
        if #available(iOS 14.0, *) {
            SKStoreReviewController.requestReview(in: scene)
        } else {
            SKStoreReviewController.requestReview()
        }
    }
}
