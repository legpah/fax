//
//  PaywallViewVariation.swift
//  FaxApp
//
//  Created by Eugene on 14.07.2022.
//

import Foundation

enum PaywallViewVariation: String, Decodable {
    case first
    case second
    case third
    case fourth
    case fifth
    case sixth
}

enum PaywallType {
    case sending(PaywallModel)
    case standart
}
