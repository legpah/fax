//
//  CountryPhonesListViewController.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import UIKit

typealias CountryPhonesListDiffableSource = DiffableTableDataSource<
    CountyPhonesSectionModel,
    CountyPhonesCellModel
>

protocol CountryPhonesListViewController: AnyObject {
    var countryPhonesSource: CountryPhonesListDiffableSource { get }
}

final class CountryPhonesListViewControllerImpl: UIViewController {
    
    private let presenter: CountryPhonesListPresenter
    private lazy var dataSource = CountryPhonesListDiffableSource(tableView: tableView)
    
    private let backBarButton = TextBarButton(text: L10n.Sending.back, position: .left)
    
    private let searchContainer = UIView()
    private let searchIcon = ImageView(themedImage: .searchIcon)
    private let searchInput = Input(
        font: .poppins(14),
        placeholder: .init(
            text: L10n.Country.search,
            color: .baboo
        ),
        bgColor: .flerio
    )
    private let tableView = TableView(registerCells: [
        CountyPhoneCell.self
    ])
    
    init(presenter: CountryPhonesListPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSelf()
        presenter.viewDidLoad()
    }
    
    private func configureSelf() {
        title = L10n.Country.title
        
        view.addSubview(searchContainer)
        searchContainer.addSubview(searchInput)
        searchInput.addSubview(searchIcon)
        
        view.addSubview(tableView)
        
        configureSearch()
        configureBackBarButton()
        configureTableView()
        
        themeProvider.register(observer: self)
    }
    
    private func configureBackBarButton() {
        navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backBarButton)
        backBarButton.onTap = { [weak self] in
            self?.presenter.backButtonTapped()
        }
    }
    
    private func configureSearch() {
        searchContainer.pin(toTopLayoutGuideOf: self)
        searchContainer.pinEdges(toSuperviewEdges: [.left, .right])
        searchContainer.setDimension(.height, toSize: 69)
        
        searchInput.addDoneToolBar(action: nil)
        searchInput.textInsets = .init(top: 0, left: 32, bottom: 0, right: 0)
        searchInput.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        searchInput.pinEdge(toSuperviewEdge: .top, withInset: 5)
        
        searchIcon.alignAxis(toSuperviewAxis: .y)
        searchIcon.pinEdge(toSuperviewEdge: .left, withInset: 12)
        searchIcon.setDimensions(to: .symmetric(12))
        
        searchInput.borderColor = .xero
        searchInput.onTextChange = { [weak self] change in
            self?.presenter.didChangeSearch(text: change.text)
        }
    }
    
    private func configureTableView() {
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
        tableView.separatorStyle = .none
        tableView.contentInset = .init(top: 0, left: 0, bottom: 10, right: 0)
        tableView.pinEdge(.top, to: .bottom, of: searchContainer)
        tableView.pin(toBottomLayoutGuideOf: self)
        tableView.pinEdges(toSuperviewEdges: [.left, .right])
    }
}

extension CountryPhonesListViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        applyNavigationBarColor(.bw)
        view.backgroundColor = .themed.bw.value
        searchContainer.backgroundColor = .themed.bw.value
        tableView.backgroundColor = .themed.bw.value
    }
}

extension CountryPhonesListViewControllerImpl: CountryPhonesListViewController {
    var countryPhonesSource: CountryPhonesListDiffableSource {
        dataSource
    }
}

extension CountryPhonesListViewControllerImpl: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40
    }
}
