//
//  PDFSource.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import UIKit

enum PDFSource {
    case doc(Data)
    case docx(Data)
    case pdf(Data)
    case frontPage(FrontPageAttributesModel)
    case image(UIImage)
}

extension PDFSource {
    func pdfObject() -> PDFObject {
        switch self {
        case let .doc(data):
            return PDFDataObject(data: data)
        case let .docx(data):
            return PDFDataObject(data: data)
        case let .pdf(data):
            return PDFDataObject(data: data)
        case let .frontPage(attributes):
            return PDFFrontPageObject(attributes: attributes)
        case let .image(image):
            return PDFImageObject(image: image)
        }
    }
    
    var isFrontPage: Bool {
        switch self {
        case .frontPage: return true
        default: return false
        }
    }
    
    var analyticsDocumentType: String {
        switch self {
        case .doc:
            return "doc"
        case .docx:
            return "docx"
        case .pdf:
            return "pdf"
        case .frontPage:
            return "front_page"
        case .image:
            return "img"
        }
    }
}

extension PDFSource: Equatable {
    static func == (lhs: PDFSource, rhs: PDFSource) -> Bool {
        switch (lhs, rhs) {
        case let (.doc(lhsData), .doc(rhsData)):
            return lhsData == rhsData
        case let (.docx(lhsData), .docx(rhsData)):
            return lhsData == rhsData
        case let (.pdf(lhsData), .pdf(rhsData)):
            return lhsData == rhsData
        case let (.frontPage(lhsAttributes), .frontPage(rhsAttributes)):
            return lhsAttributes == rhsAttributes
        case let (.image(lhsImage), .image(rhsImage)):
            return lhsImage.pngData() == rhsImage.pngData()
        default:
            return false
        }
    }
}
