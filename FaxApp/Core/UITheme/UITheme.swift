//
//  UITheme.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit

enum UITheme: String, CaseIterable {
    case light
    case dark
    
    static var current: UITheme {
        UIThemeProviderImpl.shared.current
    }
    
    var asSystemTheme: UIUserInterfaceStyle {
        switch self {
        case .light:
            return .light
        case .dark:
            return .dark
        }
    }
    
    var asKeyboardAppearance: UIKeyboardAppearance {
        switch self {
        case .light:
            return .light
        case .dark:
            return .dark
        }
    }
    
    var asStatusBarStyle: UIStatusBarStyle {
        switch self {
        case .light:
            return .darkContent
        case .dark:
            return .lightContent
        }
    }
}
