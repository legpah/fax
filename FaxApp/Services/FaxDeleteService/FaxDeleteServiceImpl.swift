//
//  DeleteFaxServiceImpl.swift
//  FaxApp
//
//  Created by Eugene on 24.05.2022.
//

import Foundation
import Combine

final class FaxDeleteServiceImpl {
    private let api = ApiClient()
}

extension FaxDeleteServiceImpl: FaxDeleteService {
    func deleteAll() -> AnyPublisher<EmptyResponse, NetworkError> {
        let endpoint = DeleteFaxEndpoint.deleteAll
        return api.request(request: endpoint)
    }
    
    func delete(id: String) -> AnyPublisher<EmptyResponse, NetworkError> {
        let endpoint = DeleteFaxEndpoint.delete(id: id)
        return api.request(request: endpoint)
    }
}
