//
//  CountryPhoneCell.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import UIKit

final class CountyPhoneCell: UITableViewCell {
    
    private let button = InvisibleButton()
    private let countyIconLabel = Label(font: .poppins(14))
    private let descriptionLabel = Label(font: .poppins(14, .light),
                                         color: .equil)
    private let bottomBorder = UIView()
    
    private var model: Model?
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        themeProvider.unregister(observer: self)
    }
    
    private func configureSelf() {
        contentView.addSubview(button)
        contentView.addSubview(countyIconLabel)
        contentView.addSubview(descriptionLabel)
        contentView.addSubview(bottomBorder)
        
        button.onTap = { [weak self] in
            guard let self = self, let model = self.model else {
                return
            }
            model.onTap(model)()
        }
        
        configureConstraints()
        
        themeProvider.register(observer: self)
    }
    
    private func configureConstraints() {
        button.pinEdgesToSuperviewEdges()
        
        countyIconLabel.pinEdge(toSuperviewEdge: .left, withInset: .layout.xInset)
        countyIconLabel.alignAxis(toSuperviewAxis: .y)
        countyIconLabel.setContentHuggingPriority(.required, for: .horizontal)
        
        descriptionLabel.pinEdge(.left, to: .right, of: countyIconLabel, withOffset: 16)
        descriptionLabel.pinEdge(toSuperviewEdge: .right, withInset: .layout.xInset)
        descriptionLabel.alignAxis(toSuperviewAxis: .y)
        
        bottomBorder.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        bottomBorder.pinEdge(toSuperviewEdge: .bottom, withInset: 0)
        bottomBorder.setDimension(.height, toSize: 1)
    }
}

extension CountyPhoneCell: UIThemable {
    func apply(theme: UITheme) {
        bottomBorder.backgroundColor = .themed.xero.value
        backgroundColor = .themed.bw.value
    }
}

extension CountyPhoneCell: SetupableCell {
    func setup(model: CountyPhonesCellModel) {
        self.model = model
        countyIconLabel.text = model.flagCode
        descriptionLabel.text = "(\(model.countryCode)) \(model.name)"
    }
}
