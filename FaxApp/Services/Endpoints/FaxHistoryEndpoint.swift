//
//  FaxHistoryEndpoint.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation

struct FaxHistoryEndpoint: NetworkRequest {
    typealias ReturnType = [FaxHistoryRemoteItem]
    
    let path: String = "/fax"
    let method: HTTPMethod = .get
    
    var decode: (Data) throws -> [FaxHistoryRemoteItem] {
        return {
            return try JSONDecoder().decodeArraySafely(data: $0)
        }
    }
}

struct FaxHistoryRemoteItem: Decodable {
    let id: String
    let status: FaxHistorySentStatus
    let fileUrl: String
    let previewUrl: String
    let pagesCount: Int
    let recipient: String
    let sentAt: Int
    let errorDescription: ApiInternalError?
    
    var isOnePage: Bool {
        pagesCount == 1
    }
}

enum FaxHistorySentStatus: String, Decodable {
    case sent = "delivered"
    case delivering = "sending"
    case error = "failed"
    
    var isError: Bool {
        switch self {
        case .error:
            return true
        case .delivering, .sent:
            return false
        }
    }
}
