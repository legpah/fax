//
//  OnboardingPresenter.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import Foundation
import UIKit

protocol OnboardingPresenter: ViewOutput {
    func continueTapped()
}

final class OnboardingPresenterImpl {
    
    weak var view: OnboardingViewController?
    
    private let models: [(title: String, subtitle: String)] = [
        (L10n.Boarding.sendFaxes, L10n.Boarding.fromIphone),
        (L10n.Boarding.uploadAnyDocuments, L10n.Boarding.andImagesToSend),
        (L10n.Boarding.previewYourFax, L10n.Boarding.beforeSending),
        (L10n.Boarding.trackAll, L10n.Boarding.yourSendings),
        (L10n.Boarding.chooseBetween, L10n.Boarding.lightAndDarkMode),
    ]
    private let paywallScreen: UIViewController
    private let analytics: AnalyticsService
    private var currentIdx = 0
    
    init(paywallScreen: UIViewController,
         analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.paywallScreen = paywallScreen
        self.analytics = analytics
    }
    
    private func showNextOnboardingView() {
        let model = models[currentIdx]
        view?.showNext(title: model.title, subtitle: model.subtitle, moveImages: currentIdx != 0)
    }
}

extension OnboardingPresenterImpl: OnboardingPresenter {
    func viewDidLoad() {
        showNextOnboardingView()
    }
    
    func continueTapped() {
        currentIdx += 1
        analytics.send(AnalyticsEventImpl(name: "onboarding_screen\(currentIdx)_continue_click"))
        
        guard models[safe: currentIdx] != nil else {
            view?.showPaywall(viewController: paywallScreen)
            UserDefaults.standard.setValue(true, forKey: .userDefaults.isOnboardingShowed)
            analytics.send(AnalyticsEventImpl(name: "paywall_shown", parameters: ["source": "first_launch"]))
            return
        }
        
        showNextOnboardingView()
    }
}
