//
//  SendFaxService.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation
import Combine

protocol SendFaxService {
    func sendFax(
        pdfData: Data,
        previewData: Data,
        recipient: String,
        pagesCount: Int
    ) -> AnyPublisher<SendFaxResponse, NetworkError>
}
