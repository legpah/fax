//
//  CountyPhonesListServiceMock.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation
import Combine

final class CountyPhonesListServiceMock: CountyPhonesListService {
    private func mock() -> [CountryPhone] {
        return [
            CountryPhone(id: "1",
                         name: "Turkey",
                         code: "+90",
                         mask: "XXX XX XX",
                         flagUnicode: "\u{1F1F9}\u{1F1F7}",
                         canSendFax: true),
            CountryPhone(id: "2",
                         name: "Russia",
                         code: "+7",
                         mask: "XXX XX XX",
                         flagUnicode: "\u{1F1F7}\u{1F1FA}",
                         canSendFax: false),
            CountryPhone(id: "3",
                         name: "United States",
                         code: "+1",
                         mask: "(XXX) XXX-XXXX",
                         flagUnicode: "\u{1F1FA}\u{1F1F8}",
                         canSendFax: true),
            CountryPhone(id: "4",
                         name: "Austria",
                         code: "+43",
                         mask: "(XXX) XXX-XXXX",
                         flagUnicode: "\u{1F1E6}\u{1F1F9}",
                         canSendFax: true),
        ]
    }
    
    var countryPhones: AnyPublisher<[CountryPhone], NetworkError> {
        Just(mock()).setFailureType(to: NetworkError.self).eraseToAnyPublisher()
    }
}
