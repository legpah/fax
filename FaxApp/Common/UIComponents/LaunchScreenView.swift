//
//  LaunchScreenView.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import UIKit

@IBDesignable
final class LaunchScreenView: UIView {
    
    private let gradientView = GradientView(direction: .fromTopToBottom,
                                            colors: [#colorLiteral(red: 0.3882352941, green: 0.4, blue: 0.9450980392, alpha: 1), #colorLiteral(red: 0.9764705882, green: 0.9803921569, blue: 0.9843137255, alpha: 1)])
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(gradientView)
        gradientView.pinEdgesToSuperviewEdges()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
