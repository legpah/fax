//
//  CountyPhoneInputInteractor.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import Foundation
import Combine

protocol CountryPhoneInputInteractor {
    var countryPhones: AnyPublisher<[CountryPhone], NetworkError> { get }
    var storedSelectedCountry: CountryPhone? { get }
    func storeSelectedCountry(_ country: CountryPhone?)
}

final class CountryPhoneInputInteractorImpl {
    
    private let storage: ContryPhonesStorage
    private let countryPhonesListService: CountyPhonesListService
    
    init(storage: ContryPhonesStorage = ContryPhonesStorage.shared,
         countryPhonesListService: CountyPhonesListService = CountyPhonesListServiceImpl()) {
        self.storage = storage
        self.countryPhonesListService = countryPhonesListService
    }
}

extension CountryPhoneInputInteractorImpl: CountryPhoneInputInteractor {
    var countryPhones: AnyPublisher<[CountryPhone], NetworkError> {
        guard storage.phones.isEmpty else {
            return Just(storage.phones)
                .setFailureType(to: NetworkError.self)
                .eraseToAnyPublisher()
        }
        
        return countryPhonesListService
            .countryPhones
            .map { [weak self] phones in
                self?.storage.setPhones(phones)
                return phones
            }
            .eraseToAnyPublisher()
    }
    
    var storedSelectedCountry: CountryPhone? {
        guard let data = UserDefaults.standard.value(
            forKey: .userDefaults.selectedCountry
        ) as? Data else {
            return nil
        }
        return try? JSONDecoder().decode(CountryPhone.self, from: data)
    }
    
    func storeSelectedCountry(_ country: CountryPhone?) {
        guard let country = country else {
            UserDefaults.standard.removeObject(forKey: .userDefaults.selectedCountry)
            return
        }
        guard let data = try? JSONEncoder().encode(country) else {
            return
        }
        UserDefaults.standard.setValue(data, forKey: .userDefaults.selectedCountry)
    }
}
