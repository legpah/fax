//
//  PDFPreviewRoutable.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import Foundation

protocol PDFPreviewRoutable: AnyObject {
    func showPdfPreview(model: PDFPreviewPageRouterModel)
    func openPdf(name: String, title: String)
}

extension PDFPreviewRoutable where Self: ScreenRouter {
    func showPdfPreview(model: PDFPreviewPageRouterModel) {
        let router = PDFPreviewPageRouterImpl(model: model)
        open(router: router)
    }
    
    func openPdf(name: String, title: String) {
        guard let file = Bundle.main.path(forResource: name, ofType: "pdf"),
              let data = try? Data(contentsOf: URL(fileURLWithPath: file))
        else {
            return
        }
        
        open(
            router: PDFPreviewPageRouterImpl(
                model: PDFPreviewPageRouterModel(
                    viewTitle: title,
                    pdfObject: PDFSource.pdf(data).pdfObject(),
                    canEditFrontPage: false,
                    onDocumentEdited: nil
                )
            )
        )
    }
}
