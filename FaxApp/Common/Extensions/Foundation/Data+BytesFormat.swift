//
//  Data+BytesFormat.swift
//  FaxApp
//
//  Created by Eugene on 1.06.2022.
//

import Foundation

extension Data {
    var megabytes: Float {
        return Float(self.count) / 1024.0 / 1024.0
    }
}

extension Int {
    var megabytes: Float {
        return Float(self) / 1024.0 / 1024.0
    }
}
