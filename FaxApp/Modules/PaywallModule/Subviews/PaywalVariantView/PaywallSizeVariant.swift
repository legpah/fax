//
//  PaywallSizeVariant.swift
//  FaxApp
//
//  Created by Eugene on 15.07.2022.
//

import Foundation

enum PaywallSizeVariant {
    case large
    case normal
    case small
    
    func value<T>(
        _ large: @autoclosure () -> T,
        _ normal: @autoclosure () -> T,
        _ small: @autoclosure () -> T
    ) -> T {
        switch self {
        case .large:
            return large()
        case .normal:
            return normal()
        case .small:
            return small()
        }
    }
}
