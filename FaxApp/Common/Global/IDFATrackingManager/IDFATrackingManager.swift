//
//  IDFATrackingManager.swift
//  FaxApp
//
//  Created by Eugene on 18.05.2022.
//

import AppTrackingTransparency
import Combine

final class IDFATrackingManager {
    
    static let shared = IDFATrackingManager()
    
    var idfaStatusPublisher: AnyPublisher<Bool, Never> {
        idfaEnabledSubject.eraseToAnyPublisher()
    }
    
    private let idfaEnabledSubject = CurrentValueSubject<Bool, Never>(false)
    private let analytics: AnalyticsService
    private let isIDFARequested = UDAccess<Bool>(key: .userDefaults.isIDFARequested)
    
    private init(analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.analytics = analytics
    }
    
    func requestIdfa() {
        if #available(iOS 14, *) {
            ATTrackingManager.requestTrackingAuthorization { [weak self] status in
                switch status {
                case .authorized:
                    self?.idfaEnabledSubject.send(true)
                    self?.sendAnalyticsIfNeeded(isAllow: true)
                case .denied:
                    self?.idfaEnabledSubject.send(false)
                    self?.sendAnalyticsIfNeeded(isAllow: false)
                default:
                    self?.idfaEnabledSubject.send(false)
                }
            }
        } else {
            idfaEnabledSubject.send(true)
            sendAnalyticsIfNeeded(isAllow: true)
        }
    }
    
    private func sendAnalyticsIfNeeded(isAllow: Bool) {
        guard isIDFARequested.value == nil else {
            return
        }
        analytics.send(AnalyticsEventImpl(name: "att_perm", parameters: ["answer": isAllow.analyticValue]))
        isIDFARequested.value = false
    }
}
