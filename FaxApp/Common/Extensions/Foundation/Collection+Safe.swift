//
//  Collection+Safe.swift
//  FaxApp
//
//  Created by Eugene on 14.05.2022.
//

import Foundation

extension RandomAccessCollection where Index == Int {
    subscript(safe index: Index) -> Element? {
        guard index >= 0 && index < self.count else {
            return nil
        }
        return self[index]
    }
}
