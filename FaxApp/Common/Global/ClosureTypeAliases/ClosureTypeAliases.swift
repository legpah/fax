//
//  ClosureTypeAliases.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import Foundation

typealias IClosure<T> = (T) -> Void

typealias OClosure<T> = () -> T

typealias IOClosure<I, O> = (I) -> O

typealias VoidClosure = () -> Void
