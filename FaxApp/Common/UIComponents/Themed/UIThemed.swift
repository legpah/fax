//
//  UIThemed.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

struct UIThemed<T> {
    let light: T
    let dark: T
    
    init(light: T, dark: T) {
        self.light = light
        self.dark = dark
    }
    
    init(value: T) {
        self.light = value
        self.dark = value
    }
    
    var value: T {
        themed(.current)
    }
    
    func themed(_ theme: UITheme) -> T {
        switch theme {
        case .light:
            return light
        case .dark:
            return dark
        }
    }
}
