//
//  AnyEquatable.swift
//  FaxApp
//
//  Created by Eugene on 2.06.2022.
//

import Foundation

enum SwiftDataType {
    case String
    case Int
    case Int64
    case Double
    case Bool
    case Undefined
}

private func getType(of: Any) -> SwiftDataType {
    if let _ = of as? String {
        return SwiftDataType.String
    } else if let _ = of as? Int {
        return SwiftDataType.Int
    } else if let _ = of as? Int64 {
        return SwiftDataType.Int64
    } else if let _ = of as? Double {
        return SwiftDataType.Double
    } else if let _ = of as? Bool {
        return SwiftDataType.Bool
    } else {
        return SwiftDataType.Undefined
    }
}

func isAnyEqual(_ a: Any, b: Any) -> Bool {
    let aType : SwiftDataType = getType( of : a )
    let bType : SwiftDataType = getType( of : b )
    if aType != bType {
        return false
    } else {
        switch aType  {
        case SwiftDataType.String :
            guard let aValue = a as? String, let bValue = b as? String else {
                return false
            }
            return aValue == bValue
            
        case SwiftDataType.Int :
            guard let aValue = a as? Int, let bValue = b as? Int else {
                return false
            }
            return aValue == bValue
            
        case SwiftDataType.Int64 :
            guard let aValue = a as? Int64, let bValue = b as? Int64 else {
                return false
            }
            return aValue == bValue
            
        case SwiftDataType.Double :
            guard let aValue = a as? Double, let bValue = b as? Double else {
                return false
            }
            return aValue == bValue
            
        case SwiftDataType.Bool :
            guard let aValue = a as?  Bool, let bValue = b as? Bool else {
                return false
            }
            return aValue == bValue
            
        default:
            return false
        }
    }
}
