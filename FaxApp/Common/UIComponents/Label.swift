//
//  Label.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit

class Label: UILabel {
    
    // MARK: - Internal properties
    
    var textInsets: UIEdgeInsets = .zero
    
    var color: UIThemed<UIColor>? {
        didSet { apply(theme: .current) }
    }
    
    var linesHeight: CGFloat {
        if numberOfLines == 0 {
            return .greatestFiniteMagnitude
        } else {
            return font.lineHeight * CGFloat(numberOfLines)
        }
    }
    
    // MARK: - Init
    
    init(text: String? = nil,
         font: UIFont,
         color: UIThemed<UIColor> = .ibw,
         alignment: NSTextAlignment = .left,
         lines: Int = 1) {
        self.color = color
        super.init(frame: .zero)
        self.numberOfLines = lines
        self.text = text
        self.textAlignment = alignment
        self.font = font
        themeProvider.register(observer: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var intrinsicContentSize: CGSize {
        let height = super.intrinsicContentSize.height + textInsets.top + textInsets.bottom
        let width = super.intrinsicContentSize.width + textInsets.left + textInsets.right
        return CGSize(width: width, height: height)
    }
}

extension Label: UIThemable {
    func apply(theme: UITheme) {
        textColor = color?.value
    }
}
