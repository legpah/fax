//
//  TrunButton.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

final class TrunButton: UIView {
    
    var title: String {
        didSet {
            titleLabel.text = title
        }
    }
    
    var onTap: VoidClosure? {
        didSet {
            onTapChanged()
        }
    }
    
    var borderRadius: CGFloat = 8 {
        didSet { updateBordersShape() }
    }
    
    var bordersWidth: CGFloat = 1 {
        didSet { updateBordersShape() }
    }
    
    var isActive: Bool = true {
        didSet {
            UIView.transition(with: self, duration: .animations.fastTransitionDuration) {
                self.apply(theme: .current)
            }
        }
    }
    
    var isSelected: Bool = true {
        didSet {
            UIView.transition(with: self, duration: .animations.fastTransitionDuration) {
                self.apply(theme: .current)
            }
        }
    }
    
    var titleColor: UIThemed<UIColor> {
        didSet {
            apply(theme: .current)
        }
    }
    
    var backroundThemedColor: UIThemed<UIColor>? {
        didSet { apply(theme: .current) }
    }
    
    private let container = UIView()
    private let titleLabel = UILabel()
    
    private let contentXAlignment: LXAlignment
    private let contentYAlignment: LYAlignment
    private let titleInactiveColor: UIThemed<UIColor>?
    private let backroundInactiveThemedColor: UIThemed<UIColor>?
    private let borderColor: UIThemed<UIColor>?
    private let borderInactiveColor: UIThemed<UIColor>?
    private let iconModel: IconModel?
    private let iconImageView: UIImageView?
    private let height: CGFloat
    
    init(
        title: String,
        titleColor: UIThemed<UIColor> = .flerio,
        backroundColor: UIThemed<UIColor>? = .tukesh,
        borderColor: UIThemed<UIColor>? = .lanka,
        titleInactiveColor: UIThemed<UIColor>? = nil,
        backroundInactiveThemedColor: UIThemed<UIColor>? = nil,
        borderInactiveColor: UIThemed<UIColor>? = nil,
        font: UIFont = .poppins(16, .semiBold),
        iconModel: IconModel? = nil,
        contentXAlignment: LXAlignment = .center(offset: 0),
        contentYAlignment: LYAlignment = .center(offset: 0),
        height: CGFloat = 48
    ) {
        self.title = title
        self.contentXAlignment = contentXAlignment
        self.contentYAlignment = contentYAlignment
        self.titleColor = titleColor
        self.titleInactiveColor = titleInactiveColor
        self.backroundThemedColor = backroundColor
        self.backroundInactiveThemedColor = backroundInactiveThemedColor
        self.borderColor = borderColor
        self.borderInactiveColor = borderInactiveColor
        self.iconModel = iconModel
        iconImageView = iconModel?.image == nil ? nil : UIImageView()
        self.height = height
        super.init(frame: .zero)
        self.titleLabel.font = font
        titleLabel.text = title
        configureUI()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func configureUI() {
        addSubview(container)
        container.addSubview(titleLabel)
        if let iconImageView = iconImageView {
            container.addSubview(iconImageView)
        }
        
        updateBordersShape()
        configureConstraints()
        
        themeProvider.register(observer: self)
    }
    
    private func configureConstraints() {
        setDimension(.height, toSize: height)
        
        configureContentContainerConstrains()
        configureContentConstraints()
    }
    
    private func configureContentContainerConstrains() {
        container.alignView(y: contentYAlignment)
        container.alignView(x: contentXAlignment)
    }
    
    private func configureContentConstraints() {
        guard let iconModel = iconModel, let iconView = iconImageView else {
            titleLabel.centerInSuperview()
            container.pinEdge(.top, to: .top, of: titleLabel)
            container.pinEdge(.bottom, to: .bottom, of: titleLabel)
            return
        }
        
        let offset = iconModel.offset
        
        switch iconModel.position {
        case .left:
            container.centerInSuperview()
            
            iconView.alignAxis(toSuperviewAxis: .y)
            iconView.pinEdge(toSuperviewEdge: .left)
            
            titleLabel.alignAxis(toSuperviewAxis: .y)
            titleLabel.pinEdge(.left, to: .right, of: iconView, withOffset: offset)
            titleLabel.pinEdge(toSuperviewEdge: .right)
            
        case .right:
            container.centerInSuperview()
            
            titleLabel.pinEdge(toSuperviewEdge: .left)
            titleLabel.alignAxis(toSuperviewAxis: .y)
            
            iconView.alignAxis(toSuperviewAxis: .y)
            iconView.pinEdge(.left, to: .right, of: iconView, withOffset: offset)
            iconView.pinEdge(toSuperviewEdge: .right)
            
        case .top:
            container.alignAxis(toSuperviewAxis: .x)
            container.pinEdge(.top, to: .top, of: iconView)
            container.pinEdge(.bottom, to: .bottom, of: titleLabel)
            
            iconView.alignAxis(toSuperviewAxis: .x)
            iconView.pinEdge(toSuperviewEdge: .top)
            
            titleLabel.alignAxis(toSuperviewAxis: .x)
            titleLabel.pinEdge(.top, to: .bottom, of: iconView, withOffset: offset)
        }
        
        switch iconModel.sizelayout {
        case .empty:
            break
        case let .fixed(size):
            iconView.setDimensions(to: size)
        case let .scaled(x, y):
            iconView.match(.height, to: .height, of: container, withMultiplier: y)
            iconView.match(.width, to: .width, of: container, withMultiplier: x)
        }
    }
    
    private func onTapChanged() {
        if onTap == nil {
            self.gestureRecognizers?.removeAll()
        } else {
            self.addGestureRecognizer(
                UITapGestureRecognizer(target: self, action: #selector(handleTap))
            )
        }
    }
    
    private func updateBordersShape() {
        applyBordersShape(width: bordersWidth, radius: borderRadius)
    }
    
    @objc
    private func handleTap() {
        guard isActive else { return }
        onTap?()
    }
}

extension TrunButton: UIThemable {
    func apply(theme: UITheme) {
        if isActive && isSelected {
            applyBorders(color: borderColor)
            titleLabel.textColor = titleColor.value
            self.backgroundColor = backroundThemedColor?.value
            iconImageView?.image = iconModel?.image?.value
        } else {
            applyBorders(color: borderInactiveColor)
            titleLabel.textColor =  titleInactiveColor?.value
            self.backgroundColor = backroundInactiveThemedColor?.value
            iconImageView?.image = iconModel?.inactiveImage?.value ?? iconModel?.image?.value
        }
    }
}

extension TrunButton {
    struct IconModel {
        enum Position {
            case left
            case right
            case top
        }
        
        enum SizeLayout {
            case empty
            case fixed(size: CGSize)
            case scaled(x: CGFloat, y: CGFloat)
        }
        
        let position: Position
        let offset: CGFloat
        let sizelayout: SizeLayout
        let image: UIThemed<UIImage>?
        let inactiveImage: UIThemed<UIImage>?
        
        init(position: Position,
             offset: CGFloat,
             sizelayout: SizeLayout,
             image: UIThemed<UIImage>? = nil,
             inactiveImage: UIThemed<UIImage>? = nil) {
            self.position = position
            self.offset = offset
            self.sizelayout = sizelayout
            self.image = image
            self.inactiveImage = inactiveImage
        }
    }
}
