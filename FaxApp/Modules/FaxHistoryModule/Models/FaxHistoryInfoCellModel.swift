//
//  FaxHistoryInfoCellModel.swift
//  FaxApp
//
//  Created by Eugene on 1.06.2022.
//

import Foundation

struct FaxHistoryInfoCellModel: Hashable {
    let title: String
}
