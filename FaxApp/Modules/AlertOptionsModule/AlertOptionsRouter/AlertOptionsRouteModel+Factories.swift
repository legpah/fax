//
//  AlterOptionsRouteModel+Factories.swift
//  FaxApp
//
//  Created by Eugene on 5.05.2022.
//

import Foundation
import VisionKit

extension AlertOptionsRouteModel {
    static func documentSource(onSelect: @escaping IClosure<String>) -> AlertOptionsRouteModel {
        var options: [String] = []
        if VNDocumentCameraViewController.isSupported {
            options = FaxDocumentSourceType.allCases.filter { $0 != .camera }.map { $0.localize }
        } else {
            options = FaxDocumentSourceType.allCases.filter { $0 != .scanner }.map { $0.localize }
        }
        return .optionsAlert(
            style: .actionSheet,
            title: L10n.Alerts.Documents.title,
            subtitle: nil,
            options: options,
            onSelect: onSelect,
            onCancel: {}
        )
    }
    
    static func failExtractCountyCode() -> AlertOptionsRouteModel {
        .okAlert(
            title: L10n.Alerts.Country.Code.title,
            subtitle: L10n.Alerts.Country.Code.subtitle,
            onOk: {}
        )
    }
    
    static func unableToSendFax(onCheckOut: @escaping VoidClosure) -> AlertOptionsRouteModel {
        optionsAlert(
            style: .alert,
            title: L10n.Alerts.Country.Unable.title,
            subtitle: L10n.Alerts.Country.Unable.subTitle,
            options: [L10n.Alerts.Country.button],
            onSelect: { _ in onCheckOut() },
            onCancel: {}
        )
    }
    
    static func failedToParseDocument() -> AlertOptionsRouteModel {
        .okAlert(
            title: L10n.Alerts.Main.Unexpected.title,
            subtitle: nil,
            onOk: {}
        )
    }
    
    static func pagesLimitExceed(count: Int = PDFConstants.pagesLimit) -> AlertOptionsRouteModel {
        .okAlert(
            title: L10n.Alerts.Main.Limit.title,
            subtitle: L10n.Alerts.Main.Limit.subtitle(PDFConstants.pagesLimit),
            onOk: {}
        )
    }
    
    static func bytesLimitExceeded() -> AlertOptionsRouteModel {
        .okAlert(
            title: L10n.Alerts.Main.SizeLimit.title,
            subtitle: L10n.Alerts.Main.SizeLimit.subTitle(PDFConstants.megabytesLimit),
            onOk: {}
        )
    }
    
    static func deleteAllFaxesFiles(onConfirm: @escaping VoidClosure) -> AlertOptionsRouteModel {
        .optionsAlert(
            style: .alert,
            title: L10n.Alerts.Main.Delete.title,
            subtitle: "",
            options: [L10n.Alerts.Main.Delete.confirm],
            onSelect: { _ in onConfirm() },
            onCancel: {}
        )
    }
    
    static func failedToPurchaseProduct(onRetry: @escaping VoidClosure) -> AlertOptionsRouteModel {
        .optionsAlert(
            style: .alert,
            title: L10n.Alerts.Paywall.Purchase.title,
            subtitle: "",
            options: [L10n.Alerts.Paywall.Purchase.retry],
            onSelect: { _ in onRetry() },
            onCancel: {}
        )
    }
    
    static func failedToRestorePurchases() -> AlertOptionsRouteModel {
        .okAlert(title: L10n.Alerts.Paywall.Restore.title, subtitle: nil, onOk: {})
    }
    
    static func purchasesRestored(onOk: @escaping VoidClosure) -> AlertOptionsRouteModel {
        .okAlert(title: L10n.Alerts.Paywall.Restore.successfull, subtitle: nil, onOk: onOk)
    }
    
    static func confirmClearAll(onOk: @escaping VoidClosure) -> AlertOptionsRouteModel {
        .okCancelAlert(title: L10n.Alerts.History.Clear.title,
                       subtitle: L10n.Alerts.History.Clear.subTitle,
                       onOk: onOk,
                       onCancel: {})
    }
    
    static func unableToOpenCamera() -> AlertOptionsRouteModel {
        AlertOptionsRouteModel.okAlert(
            title: L10n.Alerts.Picker.title,
            subtitle: nil,
            onOk: {}
        )
    }
    
    static func unableToOpenMail() -> AlertOptionsRouteModel {
       AlertOptionsRouteModel.okAlert(
        title: L10n.Alerts.MailPicker.title ,
        subtitle: L10n.Alerts.MailPicker.subTitle,
            onOk: {}
        )
    }
    
    static func unableToOpenGallery(onSettings: @escaping VoidClosure) -> AlertOptionsRouteModel {
        AlertOptionsRouteModel.optionsAlert(style: .alert,
                                            title: L10n.Alerts.Main.Gallery.title,
                                            subtitle: L10n.Alerts.Main.Gallery.subTitle,
                                            options: [L10n.Alerts.Main.Gallery.settings],
                                            onSelect: { _ in onSettings() },
                                            onCancel: {}
        )
    }
    
    static func askForPushNotification(onSelect: @escaping IClosure<String>) -> AlertOptionsRouteModel {
        AlertOptionsRouteModel.optionsAlert(style: .alert,
                                            title: L10n.Alerts.Sending.Push.title,
                                            subtitle: nil,
                                            options: [L10n.Alerts.Sending.Push.no, L10n.Alerts.Sending.Push.yes],
                                            onSelect: onSelect,
                                            onCancel: nil
        )
    }
    
    static func pushOkAlert() -> AlertOptionsRouteModel {
       AlertOptionsRouteModel.okAlert(
        title: L10n.Alerts.Sending.Push.nextTitle,
            subtitle: nil,
            onOk: {}
        )
    }
    
    static func errorAlert(error: Error) -> AlertOptionsRouteModel {
       AlertOptionsRouteModel.okAlert(
        title: L10n.Error.SmtWrong.title,
            subtitle: error.localizedDescription,
            onOk: {}
        )
    }
}
