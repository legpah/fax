//
//  UIApplication+ActiveWindow.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit

extension UIApplication {
    var activeWindow: UIWindow? {
        return connectedScenes
            .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
            .first { $0.isKeyWindow }
    }
    
    var windowFrame: CGRect {
        activeWindow?.frame ?? .zero
    }
    
    var sceneDelegate: SceneDelegate? {
        activeWindow?.windowScene?.delegate as? SceneDelegate
    }
    
    var hasHomeButton: Bool {
        (activeWindow?.safeAreaInsets.bottom ?? 0) == 0
    }
    
    func setStatusBarTheme(_ theme: UITheme?) {
        sceneDelegate?.appCoordinator.tabBar.statusBarTheme = theme
    }
}


