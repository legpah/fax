//
//  FAQItemCell.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import UIKit

final class FAQItemCell: UITableViewCell {
    
    private let button = InvisibleButton()
    private let questionLabel = Label(font: .poppins(16, .semiBold), lines: 0)
    private let expandIcon = ImageView(themedImage: .arrowDown)
    private let answerLabel = Label(font: .poppins(14, .light), lines: 0)
    private let bottomBorderView = UIView()
    
    private var isAbleToTap = true
    private var model: FAQCellModel?
    private var storedConstraints: [NSLayoutConstraint] = []
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        selectionStyle = .none
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    deinit {
        themeProvider.unregister(observer: self)
    }
    
    private func configureSelf() {
        contentView.addSubview(questionLabel)
        contentView.addSubview(expandIcon)
        contentView.addSubview(answerLabel)
        contentView.addSubview(bottomBorderView)
        contentView.addSubview(button)
        
        button.onTap = { [weak self] in
            guard let self = self,
                  let model = self.model
            else { return }
            guard self.isAbleToTap else {
                return
            }
            self.isAbleToTap = false
            model.onExpandTap(model)()
        }
        
        configureConstraints()
        updateAppearance(isAnswerExpanded: false)
        
        themeProvider.register(observer: self)
    }
    
    private func configureConstraints() {
        button.pinEdgesToSuperviewEdges()
        
        questionLabel.pinEdge(toSuperviewEdge: .top, withInset: 12)
        questionLabel.pinEdges(toSuperviewEdges: [.left], withInset: .layout.xInset)
        questionLabel.pinEdge(.right, to: .left, of: expandIcon, withOffset: -12, relation: .lessThanOrEqual)
        
        expandIcon.alignAxis(.y, toSameAxisOf: questionLabel)
        expandIcon.pinEdge(toSuperviewEdge: .right, withInset: .layout.xInset)
        
        bottomBorderView.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        bottomBorderView.setDimension(.height, toSize: 1)
        bottomBorderView.pinEdge(toSuperviewEdge: .bottom, withInset: 1)
    }
    
    private func updateAppearance(isAnswerExpanded: Bool) {
        NSLayoutConstraint.deactivate(self.storedConstraints)
        self.storedConstraints = []
        
        if isAnswerExpanded {
            self.storedConstraints = [
                bottomBorderView.pinEdge(.top, to: .bottom, of: answerLabel, withOffset: 12),
                answerLabel.pinEdge(.top, to: .bottom, of: questionLabel, withOffset: 12),
                answerLabel.setDimension(.height, toSize: 0, relation: .greaterThanOrEqual),
            ]
            self.storedConstraints += answerLabel.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        } else {
            self.storedConstraints = [
                bottomBorderView.pinEdge(.top, to: .bottom, of: questionLabel, withOffset: 12)
            ]
        }
    }
}

extension FAQItemCell: UIThemable {
    func apply(theme: UITheme) {
        backgroundColor = .themed.bw.value
        bottomBorderView.backgroundColor = .themed.xero.value
    }
}

extension FAQItemCell: SetupableCell {
    func setup(model: FAQCellModel) {
        self.model = model
        questionLabel.text = model.question
        answerLabel.text = model.isExpanded ? model.answer : nil
        expandIcon.themedImage = model.isExpanded ? .arrowUp : .arrowDown
        updateAppearance(isAnswerExpanded: model.isExpanded)
        isAbleToTap = true
    }
}
