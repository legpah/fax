//
//  SendingFaxRouter.swift
//  FaxApp
//
//  Created by Eugene on 20.05.2022.
//

import UIKit

protocol SendingFaxRouter: CloseRouterCoordinator, PDFPreviewRoutable, AlertOptionsRoutable, SettingsOpenerRouter {
    func openPaywallScreen(model: PaywallModel)
    func showPushNotificationAlert(onSelect: @escaping IClosure<AlertOptions>)
    func showOkPushAlert()
    func showErrorPush(error: String)
}

enum AlertOptions: String, CaseIterable {
    case yes
    case no
    
    var localization: String {
        switch self {
        case .yes:
            return L10n.Alerts.Sending.Push.yes
        case .no:
            return L10n.Alerts.Sending.Push.no
        }
    }
    
    init?(option: String) {
        guard let alert = AlertOptions.allCases.first(where: { $0.localization.lowercased() == option.lowercased() }) else {
            return nil
        }
        
        self = alert
    }
}

final class SendingFaxRouterImpl: BaseScreenRouter, ScreenRouter, SendingFaxRouter {
    
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.sendingFax
    let presentationType: ScreenPresentationType = .modal
    
    private lazy var _screen: UIViewController = {
        let presenter = SendingFaxPresenterImpl(recipientPhone: model.recipientPhone,
                                                recipientCountry: model.recepientCountry,
                                                pdfObject: model.pdfObject,
                                                router: self)
        let view = SendingFaxViewControllerImpl(presenter: presenter)
        presenter.view = view
        //let nav = NavigationController(rootViewController: view)
        view.modalPresentationStyle = .fullScreen
        return view
    }()
    
    private let model: SendingFaxRouterModel
    
    init(model: SendingFaxRouterModel) {
        self.model = model
    }
    
    fileprivate func extractedFunc(model: PaywallModel) -> PaywallRouterImpl {
        return PaywallRouterImpl(type: .sending(model))
    }
    
    func openPaywallScreen(model: PaywallModel) {
        open(router: extractedFunc(model: model))
    }

    func showPushNotificationAlert(onSelect: @escaping IClosure<AlertOptions>) {
        let onSelect: IClosure<String> = { rawItem in
            guard let sourceType = AlertOptions(option: rawItem) else {
                return
            }
            onSelect(sourceType)
        }
        
        UserDefaults.standard.setValue(true, forKey: .userDefaults.isAlertForNotificationsShowed)
        
        DispatchQueue.main.async {
            self.openAlertOptions(model: .askForPushNotification(onSelect: onSelect), animated: true)
        }
    }
    
    func showOkPushAlert() {
        openAlertOptions(model: .pushOkAlert(), animated: true)
    }
    
    func showErrorPush(error: String) {
        openAlertOptions(model: .okAlert(title: "error", subtitle: error, onOk: {
            UIPasteboard.general.string = error
        }), animated: true)
    }
}
