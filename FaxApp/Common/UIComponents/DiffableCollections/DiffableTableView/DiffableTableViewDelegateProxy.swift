//
//  DiffableTableViewDelegateProxy.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

final class DiffableTableViewDelegateProxy: NSObject, UITableViewDelegate {
    
    var viewForHeaderInSection: IOClosure<Int, UIView?>?
    
    /// If set, proxy use it for all methods except `viewForheaderInSection`
    weak var mainDelegate: UITableViewDelegate?
    
    func tableView(
        _ tableView: UITableView,
        viewForHeaderInSection section: Int
    ) -> UIView? {
        viewForHeaderInSection?(section)
    }
    
    func tableView(
        _ tableView: UITableView,
        heightForHeaderInSection section: Int
    ) -> CGFloat {
        return mainDelegate?.tableView?(
            tableView,
            heightForHeaderInSection: section
        ) ?? 0.0
    }
    
    func tableView(
        _ tableView: UITableView,
        heightForRowAt indexPath: IndexPath
    ) -> CGFloat {
        return mainDelegate?.tableView?(
            tableView,
            heightForRowAt: indexPath
        ) ?? 0.0
    }
}
