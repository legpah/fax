//
//  SendFaxAccessEndpoint.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation

struct SendFaxAccessEndpoint: NetworkRequest {
    typealias ReturnType = EmptyResponse
    
    let path: String = "/user/check-access-sent-fax"
    let method: HTTPMethod = .post
    let body: [String : Any]?
    
    init(pagesCount: Int) {
        body = [
            "userProperty": pagesCount
        ]
    }
}
