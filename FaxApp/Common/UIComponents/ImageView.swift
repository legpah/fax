//
//  ImageView.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import UIKit

final class ImageView: UIImageView {
    
    var themedImage: UIThemed<UIImage>? {
        didSet { apply(theme: .current) }
    }
    
    init(themedImage: UIThemed<UIImage>?) {
        self.themedImage = themedImage
        super.init(frame: .zero)
        themeProvider.register(observer: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension ImageView: UIThemable {
    func apply(theme: UITheme) {
        self.image = themedImage?.value
    }
}
