//
//  KeyboardOutsideTouchesResignerView.swift
//  FaxApp
//
//  Created by Eugene on 6.05.2022.
//

import UIKit

final class KeyboardOutsideTouchesResignerView: UIView {
    
    private var trackingResponders: [UIView] = []
    
    init() {
        super.init(frame: .zero)
        isUserInteractionEnabled = true
    }
    
    convenience init(controller: UIViewController) {
        self.init()
        controller.view.addSubview(self)
        self.pin(toTopLayoutGuideOf: controller)
        self.pin(toBottomLayoutGuideOf: controller)
        self.pinEdges(toSuperviewEdges: [.left, .right])
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func trackKeyboard(of responder: UIView) {
        trackingResponders.append(responder)
    }
    
    func trackKeyboard(of responders: [UIView]) {
        trackingResponders.append(contentsOf: responders)
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        let hitView = super.hitTest(point, with: event)
        guard hitView == self else {
            return hitView
        }
        
        var resignView: UIView?
        var pickedOverResponder = false
        
        for trackingView in trackingResponders {
            let rect = convert(trackingView.bounds, to: self)
            let point = convert(point, to: trackingView)
            if trackingView.isFirstResponder {
                if !rect.contains(point) {
                    resignView = trackingView
                }
            } else {
                if rect.contains(point) {
                    pickedOverResponder = true
                }
            }
        }
        
        
        if !pickedOverResponder {
            resignView?.resignFirstResponder()
        }
        
        return nil
    }
}


extension UIView {
    var firstResponder: UIView? {
        guard !isFirstResponder else { return self }

        for subview in subviews {
            if let firstResponder = subview.firstResponder {
                return firstResponder
            }
        }

        return nil
    }
}
