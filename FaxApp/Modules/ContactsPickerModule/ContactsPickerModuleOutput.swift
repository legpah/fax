//
//  ContactsPickerModuleOutput.swift
//  FaxApp
//
//  Created by Eugene on 5.05.2022.
//

import Foundation

protocol ContactsPickerModuleOuput: AnyObject {
    func didPickContact(phoneNumber: String)
}
