//
//  WKWebView+RenderPdf.swift
//  FaxApp
//
//  Created by Eugene on 10.05.2022.
//

import WebKit

extension WKWebView {
    func renderPdfImage(frame: CGRect = PDFConstants.pageFrame,
                        insets: UIEdgeInsets = .symmetric(x: 10, y: 10)) -> Data {
        let renderer = UIPrintPageRenderer()
        renderer.addPrintFormatter(self.viewPrintFormatter(), startingAtPageAt: 0)
        
        renderer.setValue(frame, forKeyPath: "paperRect")
        renderer.setValue(frame.inset(by:insets), forKeyPath: "printableRect")
        
        
        let out = NSMutableData()
        UIGraphicsBeginPDFContextToData(out, frame, nil)
        
        renderer.prepare(forDrawingPages: NSRange(location: 0, length: renderer.numberOfPages))
        
        let bounds = UIGraphicsGetPDFContextBounds()
        
        for page in (0..<renderer.numberOfPages) {
            UIGraphicsBeginPDFPage()
            renderer.drawPage(at: page, in: bounds)
        }
        UIGraphicsEndPDFContext()
        
        return out as Data
    }
}
