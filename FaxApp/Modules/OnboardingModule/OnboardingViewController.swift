//
//  OnboardingViewController.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import UIKit

protocol OnboardingViewController: AnyObject {
    func showNext(title: String, subtitle: String, moveImages: Bool)
    func showPaywall(viewController: UIViewController)
}

final class OnboardingViewControllerImpl: UIViewController {
    
    private let presenter: OnboardingPresenter
    
    private let contentView = UIView()
    private let titleView = TitleView()
    private let flashImagesView = FlashImagesView()
    private let bottomContentContainer = UIView()
    private let continueButton = TrunButton(title: L10n.Paywall.continue,
                                            titleColor: .bw,
                                            backroundColor: .tukesh,
                                            borderColor: nil)
    private let dotsView = DotsView()
    
    private var isMovingToNextAnimating = false
    
    init(presenter: OnboardingPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override var modalPresentationStyle: UIModalPresentationStyle {
        get { .overFullScreen }
        set {}
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSelf()
        presenter.viewDidLoad()
    }
    
    private func configureSelf() {
        view.backgroundColor = .ds_wh3
        
        view.addSubview(contentView)
        contentView.addSubview(titleView)
        contentView.addSubview(flashImagesView)
        contentView.addSubview(bottomContentContainer)
        bottomContentContainer.addSubview(continueButton)
        bottomContentContainer.addSubview(dotsView)
        
        continueButton.bordersWidth = 0
        continueButton.onTap = { [weak self] in
            guard let self = self else { return }
            guard !self.isMovingToNextAnimating else { return }
            self.isMovingToNextAnimating = true
            self.presenter.continueTapped()
        }
        
        configureConstraints()
    }
    
    private func configureConstraints() {
        contentView.pin(toTopLayoutGuideOf: self)
        contentView.pin(toBottomLayoutGuideOf: self)
        contentView.pinEdges(toSuperviewEdges: [.left, .right])
        
        titleView.pinEdge(toSuperviewEdge: .top, withInset: 30)
        titleView.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        
        flashImagesView.pinEdge(.top, to: .bottom, of: titleView, withOffset: UIApplication.shared.hasHomeButton ? 0 : 20)
        flashImagesView.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        flashImagesView.pinEdge(.bottom, to: .top, of: bottomContentContainer, withOffset: UIApplication.shared.hasHomeButton ? -20 : -40)
        
        bottomContentContainer.pinEdge(.top, to: .top, of: continueButton)
        bottomContentContainer.pinEdgesToSuperviewEdges(excludingEdge: .top)
        
        dotsView.setDimension(.height, toSize: 7)
        dotsView.pinEdge(toSuperviewEdge: .bottom, withInset: UIApplication.shared.hasHomeButton ? 15 : 5)
        dotsView.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        
        continueButton.pinEdge(.bottom, to: .top, of: dotsView, withOffset: -27)
        continueButton.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
    }
}

extension OnboardingViewControllerImpl: OnboardingViewController {
    func showNext(title: String, subtitle: String, moveImages: Bool) {
        titleView.set(title: title, subtitle: subtitle)
        
        guard moveImages else {
            return
        }
        dotsView.showNext()
        self.flashImagesView.showNext(completion: { [weak self] in
            self?.isMovingToNextAnimating = false
        })
    }
    
    func showPaywall(viewController: UIViewController) {
        addSubController(viewController)
        viewController.view.frame = CGRect(x: view.frame.width,
                                           y: 0,
                                           width: view.frame.width,
                                           height: view.frame.height)
        
        UIView.animate(withDuration: .animations.onboardingCarousel, animations: {
            viewController.view.frame.origin.x = 0
            self.contentView.transform = .init(translationX: -self.view.frame.width, y: 0)
        })
    }
}
