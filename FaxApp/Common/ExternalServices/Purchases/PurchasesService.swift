//
//  PurchasesService.swift
//  FaxApp
//
//  Created by Eugene on 12.05.2022.
//

import Adapty
import Combine
import Foundation

final class PurchasesService {
    
    static let shared = PurchasesService()
    
    private(set) var currentState: AppleSubscriptionState = .loading {
        didSet { subscriptionSubject.send(currentState) }
    }
    
    var subscriptionState: AnyPublisher<AppleSubscriptionState, Never> {
        subscriptionSubject.eraseToAnyPublisher()
    }
    
    var purchasePublisher: AnyPublisher<AppleSubscription, Never> {
        purchaseSubject.eraseToAnyPublisher()
    }
    
    private let useruidProvider = UserUIDProviderImpl.shared
    private let subscriptionSubject = CurrentValueSubject<AppleSubscriptionState, Never>(.loading)
    private let purchaseSubject = PassthroughSubject<AppleSubscription, Never>()
    private var paywallCompletions: [IClosure<Result<AdaptyPaywall, Error>>] = []
    
    private init() {
        Adapty.activate("public_live_OHFIeqlN.wzTOTqWRiUgWstqbAcDF",
                        customerUserId: useruidProvider.uuid)
        purchaserInfo(completion: { [weak self] isSubscribed in
            if !isSubscribed {
                self?.getPaywall(completion: { _ in })
            }
        })
    }
    
    func getPaywallAndProducts(completion: @escaping IClosure<Result<(AdaptyPaywall, [AdaptyPaywallProduct]), Error>>) {
        getPaywall { result in
            switch result {
            case let .success(paywall):
                self.getProducts(paywall: paywall) { result in
                    switch result {
                        case let .success(products):
                            completion(.success((paywall, products)))
                        case let .failure(error):
                            completion(.failure(error))
                    }
                }
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    func getPaywall(completion: @escaping IClosure<Result<AdaptyPaywall, Error>>) {
        if paywallCompletions.isNotEmpty {
            paywallCompletions.append(completion)
            return
        }
        
        paywallCompletions.append(completion)
        
        Adapty.getPaywall("main_subscription_ab", locale: Locale.current.languageCode) { result in
            switch result {
            case let .success(paywall):
                self.completePaywall(.success(paywall))
            case let .failure(error):
                self.completePaywall(.failure(error))
            }
        }
    }
    
    func getProducts(paywall: AdaptyPaywall, completion: @escaping IClosure<Result<[AdaptyPaywallProduct], Error>>) {
        Adapty.getPaywallProducts(paywall: paywall) { result in
            switch result {
            case let .success(products):
                completion(.success(products))
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
    
    func purchase(product: AdaptyPaywallProduct, completion: @escaping IClosure<Bool>) {
        Adapty.makePurchase(product: product) { [weak self] result in
            guard let self else { return }
            
            switch result {
            case let .success(profile):
                let hasSubscription = self.updateState(profile: profile)
                completion(hasSubscription)
            case .failure(_):
                completion(false)
            }
        }
    }
    
    func restorePurchases(completion: @escaping IClosure<Bool>) {
        Adapty.restorePurchases { result in
            switch result {
            case let .success(profile):
                let hasSubscription = self.updateState(profile: profile)
                completion(hasSubscription)
            case .failure(_):
                completion(false)
            }
        }
    }
    
    private func completePaywall(_ result: Result<AdaptyPaywall, Error>) {
        let temp = paywallCompletions
        paywallCompletions.removeAll()
        temp.forEach {
            $0(result)
        }
    }
    
    private func purchaserInfo(completion: IClosure<Bool>?) {
        Adapty.getProfile { [weak self] result in
            guard let self else { return }
            
            switch result {
            case let .success(profile):
                let hasSubscription = self.updateState(profile: profile)
                completion?(hasSubscription)
            case .failure(_):
                //TODO: - Refactor repeater
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) { [weak self] in
                    self?.purchaserInfo(completion: nil)
                }
            }
        }
    }
    
    private typealias HasActiveSubscription = Bool
    private func updateState(profile: AdaptyProfile?) -> HasActiveSubscription {
        if let profile = profile,
           let activeSubscription = profile.subscriptions.first(where: {
               $0.value.isActive
           }) {
            let subscription = activeSubscription.value
            let appleSubscription = AppleSubscription(
                name: subscritionName(subscription),
                identifier: subscription.vendorProductId,
                expiresAt: .converter.subscriptionDate(date: subscription.expiresAt),
                renewable: subscription.willRenew,
                isRenewed: subscription.renewedAt != nil
            )
            
            currentState = .subscribed(appleSubscription)
            purchaseSubject.send(appleSubscription)
            
            return true
        } else {
            currentState = .notSubscribed
            return false
        }
    }
    
    func makeFakePurchase() {
        #if DEBUG
               
        let nextWeek = Calendar.current.date(byAdding: .day, value: 7, to: Date())
        
        let fakeSubscription = AppleSubscription(name: L10n.Purchase.week,
                                                identifier: "weekly_subscription",
                                                expiresAt: .converter.subscriptionDate(date: nextWeek),
                                                renewable: true,
                                                isRenewed: false)
        
        currentState = .subscribed(fakeSubscription)
        purchaseSubject.send(fakeSubscription)
                
        #endif
    }
    
    
    private func subscritionName(_ model: AdaptyProfile.Subscription) -> String {
        switch model.vendorProductId {
        case "weekly_subscription":
            return L10n.Purchase.week
        case "monthly_subscription":
            return L10n.Purchase.month
        case "yearly_subscription":
            return L10n.Purchase.year
        default:
            return model
                .vendorProductId
                .replacingOccurrences(of: "subscription", with: "")
                .replacingOccurrences(of: "_", with: " ")
                .capitalized
                .trimmingCharacters(in: .whitespacesAndNewlines)
        }
    }
}

fileprivate enum Err: Error {
    case unknown
}
