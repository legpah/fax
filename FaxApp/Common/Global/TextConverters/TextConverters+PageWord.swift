//
//  TextConverters+PageWord.swift
//  FaxApp
//
//  Created by Eugene on 27.05.2022.
//

import Foundation


extension TextConvertersTag {
    static func pageWord(count: Int) -> String {
        if count == 1 {
            return L10n.Main.page
        } else {
            return L10n.Main.pages
        }
    }
}
