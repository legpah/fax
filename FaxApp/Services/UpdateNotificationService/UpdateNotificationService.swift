//
//  UpdateNotificationService.swift
//  FaxApp
//
//  Created by Eugene on 30.05.2022.
//

import Combine
import Foundation
import CoreData

protocol UpdateNotificationService {
    func updateToken(
        token: String,
        deviceId: String
    )
}

final class UpdateNotificationServiceImpl: UpdateNotificationService {
    
    private let api = ApiClient()
    private var cancelBag = Set<AnyCancellable>()
    
    func updateToken(
        token: String,
        deviceId: String
    ) {
        let request = UpdateNotificationTokenEndpoint(token: token, deviceId: deviceId)
        api
            .request(request: request)
            .retry(interval: 0.5, retries: .max)
            .eraseToAnyPublisher()
            .sinkResult({ _ in })
            .store(in: &cancelBag)
    }
}
