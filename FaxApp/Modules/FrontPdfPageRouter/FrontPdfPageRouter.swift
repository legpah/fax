//
//  FrontPdfPageRouter.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit

protocol FrontPdfPageRouter: PDFPreviewRoutable {
    func didCreate(pdfObject: PDFObject)
}

final class FrontPdfPageRouterImpl: BaseScreenRouter, ScreenRouter {
    
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.frontPdfPage
    let presentationType: ScreenPresentationType = .plain
    
    private lazy var _screen: UIViewController = {
        let presenter = FrontPdfPagePresenterImpl(attributes: attributes,
                                                  router: self,
                                                  onCreate: onCreate)
        let view = FrontPdfPageViewControllerImpl(presenter: presenter)
        presenter.view = view
        return view
    }()
    
    private let attributes: FrontPageAttributesModel?
    private let onCreate: IClosure<PDFObject>
    
    init(attributes: FrontPageAttributesModel?,
         onCreate: @escaping IClosure<PDFObject>) {
        self.attributes = attributes
        self.onCreate = onCreate
    }
}

extension FrontPdfPageRouterImpl: FrontPdfPageRouter {
    func didCreate(pdfObject: PDFObject) {
        closeLast(completion: { [onCreate] in
            onCreate(pdfObject)
        })
    }
}
