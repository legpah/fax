//
//  GenericInformView.swift
//  FaxApp
//
//  Created by Eugene on 24.05.2022.
//

import UIKit

final class GenericInformView: UIView {
    
    private let container = UIView()
    private let contentContainer = UIView()
    private let imageView = UIImageView()
    private let titleViewController = Label(font: .poppins(24, .semiBold), alignment: .center)
    private let titleLabel = Label(font: .poppins(24, .semiBold),
                                   alignment: .center,
                                   lines: 2)
    private let subtitleLabel = Label(font: .poppins(14),
                                      alignment: .center,
                                      lines: 0)
    private let subtitleButton = TextButton(insets: .symmetric(x: 0, y: 5))
    private let bottomButton = TrunButton(title: "",
                                          titleColor: .bw,
                                          backroundColor: .tukesh,
                                          borderColor: nil)
    private let backBarButton = TextBarButton(text: L10n.Sending.back, position: .right)
    
    private var model: GenericInformViewModel?
    
    var backButtonTap: VoidClosure? {
        didSet {
            backBarButton.onTap = backButtonTap
        }
    }
    
    init() {
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateView(model: GenericInformViewModel) {
        self.model = model
        switch model.style {
        case let .tiny(_, _, title, subtitleButton):
            switchView(isBottomButtonVisible: false)
            titleLabel.text = title
            self.subtitleButton.title = subtitleButton
            self.subtitleButton.onTap = model.onTap
            
        case let .girry(_, _, title, subtitle, button):
            switchView(isBottomButtonVisible: true)
            titleLabel.text = title
            subtitleLabel.text = subtitle
            bottomButton.title = button
            bottomButton.onTap = model.onTap
        }
        
        apply(theme: .current)
    }
    
    private func configureSelf() {
        addSubview(container)
        addSubview(backBarButton)
        addSubview(titleViewController)
        container.addSubview(contentContainer)
        contentContainer.addSubview(imageView)
        contentContainer.addSubview(titleLabel)
        contentContainer.addSubview(subtitleLabel)
        contentContainer.addSubview(subtitleButton)
        addSubview(bottomButton)
        
        bottomButton.bordersWidth = 0
        
        configureConstraints()
        
        themeProvider.register(observer: self)
        
        backBarButton.isHidden = true
        titleViewController.isHidden = true
    }
    
    private func configureConstraints() {
        backBarButton.pinEdge(toSuperviewEdge: .top, withInset: 17)
        backBarButton.pinEdge(toSuperviewEdge: .left, withInset: 24)
        
        titleViewController.alignAxis(toSuperviewAxis: .x)
        titleViewController.alignAxis(.y, toSameAxisOf: backBarButton)
        titleViewController.pinEdge(.left, to: .right, of: backBarButton, withOffset: 10)
        
        container.pinEdgesToSuperviewEdges(excludingEdge: .bottom)
        container.pinEdge(.bottom, to: .top, of: bottomButton)
        
        contentContainer.centerInSuperview()
        contentContainer.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        subtitleLabel.pinEdge(.bottom, to: .bottom, of: contentContainer, withOffset: 0, relation: .lessThanOrEqual)
        subtitleButton.pinEdge(.bottom, to: .bottom, of: contentContainer, withOffset: 0, relation: .lessThanOrEqual)
        
        imageView.pinEdge(toSuperviewEdge: .top)
        imageView.alignView(x: .center(offset: 0))
        
        titleLabel.pinEdge(.top, to: .bottom, of: imageView, withOffset: 17)
        titleLabel.pinEdges(toSuperviewEdges: [.left, .right])
        
        subtitleLabel.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 7)
        subtitleLabel.pinEdges(toSuperviewEdges: [.left, .right])
        
        subtitleButton.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 12)
        subtitleButton.pinEdges(toSuperviewEdges: [.left, .right])
        
        bottomButton.pinEdge(toSuperviewEdge: .bottom, withInset: 48)
        bottomButton.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
    }
    
    func setupTitle(text: String) {
        titleViewController.text = text
    }
    
    func hideNavigation(buttonIsHidden: Bool, titleIsHidden: Bool) {
        backBarButton.isHidden = buttonIsHidden
        titleViewController.isHidden = titleIsHidden
    }
    
    private func switchView(isBottomButtonVisible: Bool) {
        subtitleLabel.isHidden = !isBottomButtonVisible
        subtitleButton.isHidden = isBottomButtonVisible
        bottomButton.isHidden = !isBottomButtonVisible
    }
}

extension GenericInformView: UIThemable {
    func apply(theme: UITheme) {
        backgroundColor = .themed.flerio.value
        
        guard let style = model?.style else {
            return
        }
        switch theme {
        case .light:
            self.imageView.image = style.lightImage
        case .dark:
            self.imageView.image = style.darkImage ?? style.lightImage
        }
    }
}

fileprivate extension GenericInformViewModel.Style {
    var lightImage: UIImage {
        switch self {
        case let .tiny(imageLight, _, _, _):
            return imageLight
        case let .girry(imageLight, _, _, _, _):
            return imageLight
        }
    }
    
    var darkImage: UIImage? {
        switch self {
        case let .tiny(_, imageDark, _, _):
            return imageDark
        case let .girry(_, imageDark, _, _, _):
            return imageDark
        }
    }
}
