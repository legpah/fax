//
//  UDAccess.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation

final class UDAccess<T> {
    
    var value: T? {
        get { retrieve() }
        set { store(newValue) }
    }
    
    private let key: String
    private let retrieveMap: IOClosure<Any, T?>
    private var storedValue: T?
    
    init(key: String,
         retrieveMap: IOClosure<Any, T?>? = nil) {
        self.key = key
        if let retrieveMap = retrieveMap {
            self.retrieveMap = retrieveMap
        } else {
            self.retrieveMap = { anyValue in
                return anyValue as? T
            }
        }
        
        _ = retrieve()
    }
    
    private func store(_ value: T?) {
        storedValue = value
        if let value = value {
            UserDefaults.standard.setValue(value, forKey: key)
        } else {
            UserDefaults.standard.removeObject(forKey: key)
        }
    }
    
    private func retrieve() -> T? {
        if let storedValue = storedValue {
            return storedValue
        }
        
        guard let udValue = UserDefaults.standard.value(forKey: key) else {
            return nil
        }
        
        let value = retrieveMap(udValue)
        storedValue = value
        return value
    }
}
