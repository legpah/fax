//
//  PhoneInputViewControllerImpl.swift
//  FaxApp
//
//  Created by Eugene on 27.04.2022.
//

import UIKit

enum CountryInputValidAppearance {
    case unspecified
    case valid
    case invalid
}

protocol CountryPhoneInputViewController: AnyObject {
    func updateToLoadingState(isLoading: Bool)
    func updateCountyFlag(_ countryCode: String?)
    func updatePhoneCode(_ phoneCode: String?)
    func updatePhonePlaceholder(_ text: String?)
    func updatePhoneText(_ text: String?)
    func updateInvalidInput(_ appearance: CountryInputValidAppearance)
    func updateClearButton(isVisible: Bool)
}

protocol CountryPhoneInputController where Self: UIViewController {
    var input: UIView { get }
}

final class CountryPhoneInputViewControllerImpl: UIViewController, CountryPhoneInputController {
    
    var input: UIView {
        phoneInput
    }
    
    private let showContactsButton: Bool
    private let presenter: CountryPhoneInputPresenter
    private var invalidInputState: CountryInputValidAppearance?
    
    private let container = UIView()
    private let countryButton = CountryButton()
    private let separatorView = UIView()
    private let phoneImage = ImageView(themedImage: .phoneIcon)
    private let phoneCodeLabel = Label(font: .poppins(14))
    private let phoneInput = Input(
        font: .poppins(14),
        placeholder: Input.Placeholder(
            text: "",
            color: .baboo,
            font: .poppins(14)
        ),
        bgColor: .goeridio,
        keyboard: .numberPad
    )
    private let clearButton = IconButton(
        image: .crestInput,
        imageSizeRule: .fixed(.symmetric(15))
    )
    private let contactsButton = IconButton(
        image: .contactPersonIcon,
        imageSizeRule: .fixed(CGSize(width: 15, height: 15))
    )
    private let invalidTipIcon = ImageView(themedImage: .exclamationTriangle)
    private let invalidTipLabel = Label(
        text: L10n.Country.validNumber,
        font: .poppins(10, .light),
        color: .gr77
    )
    
    private lazy var clearButtonWidth = clearButton.setDimension(.width, toSize: 35)
    
    init(presenter: CountryPhoneInputPresenter,
         showContactsButton: Bool) {
        self.presenter = presenter
        self.showContactsButton = showContactsButton
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSelf()
        presenter.viewDidLoad()
    }
    
    private func configureSelf() {
        view.setDimension(.height, toSize: 60)
        view.addSubview(container)
        container.addSubview(countryButton)
        container.addSubview(separatorView)
        container.addSubview(phoneImage)
        container.addSubview(phoneCodeLabel)
        container.addSubview(phoneInput)
        container.addSubview(clearButton)
        if showContactsButton {
            view.addSubview(contactsButton)
        }
        view.addSubview(invalidTipIcon)
        view.addSubview(invalidTipLabel)
        
        configureContainer()
        configureContactsButton()
        configureCountryButton()
        configureSeparatorView()
        configurePhoneImage()
        configurePhoneCodeLabel()
        configurePhoneInput()
        configureClearButton()
        configureInvalidTip()
        
        themeProvider.register(observer: self)
    }
    
    private func configureContainer() {
        container.applyBordersShape(width: 1, radius: 8)
        if showContactsButton {
            container.pinEdges(toSuperviewEdges: [.left, .top])
            container.pinEdge(.right, to: .left, of: contactsButton, withOffset: -12)
        } else {
            container.pinEdgesToSuperviewEdges(excludingEdge: .bottom)
        }
        container.setDimension(.height, toSize: 40)
    }
    
    private func configureContactsButton() {
        guard showContactsButton else { return }
        contactsButton.applyBordersShape(width: 1, radius: 8)
        contactsButton.pinEdges(toSuperviewEdges: [.top, .right])
        contactsButton.match(.height, to: .height, of: container)
        contactsButton.match(.width, to: .height, of: contactsButton)
        contactsButton.onTap = { [weak self] in
            self?.presenter.didTapContacts()
        }
    }
    
    private func configureCountryButton() {
        countryButton.pinEdgesToSuperviewEdges(excludingEdge: .right)
        countryButton.setDimension(.width, toSize: 64)
        countryButton.onTap = { [weak self] in
            self?.presenter.didTapCountryButton()
        }
    }
    
    private func configureSeparatorView() {
        separatorView.pinEdge(.left, to: .right, of: countryButton)
        separatorView.pinEdges(toSuperviewEdges: [.top, .bottom], withInset: 1)
        separatorView.setDimension(.width, toSize: 1)
    }
    
    private func configurePhoneImage() {
        phoneImage.pinEdge(.left, to: .left, of: separatorView, withOffset: 11)
        phoneImage.alignAxis(.y, toSameAxisOf: separatorView)
        phoneImage.setContentHuggingPriority(.required, for: .horizontal)
    }
    
    private func configurePhoneCodeLabel() {
        phoneCodeLabel.pinEdge(.left, to: .right, of: phoneImage, withOffset: 6)
        phoneCodeLabel.alignAxis(toSuperviewAxis: .y)
        phoneCodeLabel.setDimension(.width, toSize: 10, relation: .greaterThanOrEqual)
        phoneCodeLabel.setContentHuggingPriority(.required, for: .horizontal)
        phoneCodeLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
    }
    
    private func configurePhoneInput() {
        phoneInput.bordersWidth = 0
        phoneInput.borderColor = nil
        phoneInput.textInsets = .symmetric(x: 6, y: 0)
        phoneInput.pinEdge(.left, to: .right, of: phoneCodeLabel)
        phoneInput.pinEdges(toSuperviewEdges: [.top, .bottom])
        phoneInput.pinEdge(.right, to: .left, of: clearButton)
        phoneInput.onTextChange = { [weak self] change in
            self?.presenter.didChangePhone(change.text)
            self?.updateClearButtonWidth()
        }
        phoneInput.onFirstResponderChange = { [weak self] change in
            self?.presenter.didChangePhoneInput(change.isActive, text: change.text)
        }
        phoneInput.addDoneToolBar(action: nil)
    }
    
    private func configureClearButton() {
        clearButton.pinEdgesToSuperviewEdges(excludingEdge: .left)
        _ = clearButtonWidth
        updateClearButtonWidth()
        clearButton.onTap = { [weak self] in
            self?.presenter.didTapClear()
        }
    }
    
    private func configureInvalidTip() {
        invalidTipIcon.pinEdge(toSuperviewEdge: .left, withInset: 4)
        invalidTipIcon.alignAxis(.y, toSameAxisOf: invalidTipLabel)
        
        invalidTipLabel.pinEdge(.left, to: .right, of: invalidTipIcon, withOffset: 4)
        invalidTipLabel.pinEdge(.top, to: .bottom, of: container, withOffset: 4)
    }
    
    private func updateClearButtonWidth() {
        clearButtonWidth.constant = (phoneInput.text ?? "").isEmpty ? 0 : 35
        UIView.animate(withDuration: .animations.fastTransitionDuration, animations: {
            self.phoneInput.layoutIfNeeded()
        })
    }
}

extension CountryPhoneInputViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        func innerApply() {
            view.backgroundColor = .themed.bw.value
            container.backgroundColor = .themed.goeridio.value
            contactsButton.applyBorders(color: .xero)
            contactsButton.backgroundColor = .themed.w3d3.value
            switch (invalidInputState ?? .unspecified) {
            case .unspecified:
                container.applyBorders(color: .xero)
                separatorView.backgroundColor = .themed.xero.value
            case .invalid:
                container.applyBorders(color: .redred)
                separatorView.backgroundColor = .themed.redred.value
            case .valid:
                container.applyBorders(color: .greengreen)
                separatorView.backgroundColor = .themed.greengreen.value
            }
        }
        UIView.animate(withDuration: .animations.fastTransitionDuration) {
            innerApply()
        }
    }
}

extension CountryPhoneInputViewControllerImpl: CountryPhoneInputViewController {
    func updateCountyFlag(_ countryCode: String?) {
        countryButton.countyCode = countryCode
    }
    
    func updatePhoneCode(_ phoneCode: String?) {
        phoneCodeLabel.text = phoneCode
    }
    
    func updatePhoneText(_ text: String?) {
        phoneInput.text = text
        updateClearButtonWidth()
    }
    
    func updatePhonePlaceholder(_ text: String?) {
        phoneInput.placeholderText = text
    }
    
    func updateToLoadingState(isLoading: Bool) {
        self.view.isUserInteractionEnabled = !isLoading
        countryButton.isLoading = isLoading
        contactsButton.isUserInteractionEnabled = !isLoading
    }
    
    func updateInvalidInput(_ appearance: CountryInputValidAppearance) {
        self.invalidInputState = appearance
        apply(theme: .current)
        func updateInvalidTip() {
            switch appearance {
            case .unspecified, .valid:
                invalidTipIcon.alpha = 0
                invalidTipLabel.alpha = 0
            case .invalid:
                invalidTipIcon.alpha = 1
                invalidTipLabel.alpha = 1
            }
        }
        UIView.animate(withDuration: .animations.fastTransitionDuration) {
            updateInvalidTip()
        }
    }
    
    func updateClearButton(isVisible: Bool) {
        UIView.animate(withDuration: .animations.fastTransitionDuration) {
            self.clearButton.alpha = isVisible ? 1 : 0
        }
    }
}
