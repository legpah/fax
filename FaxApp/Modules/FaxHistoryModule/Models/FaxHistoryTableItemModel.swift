//
//  FaxHistoryTableItemModel.swift
//  FaxApp
//
//  Created by Eugene on 1.06.2022.
//

import UIKit

enum FaxHistoryTableItemModel: DiffableTableItem {
    case historyItem(FaxHistoryCellModel)
    case info(FaxHistoryInfoCellModel)
    
    var reuseIdentifier: StringIdentifier {
        switch self {
        case .historyItem:
            return FaxHistoryCell.identifier
        case .info:
            return FaxHistoryInfoCell.identifier
        }
    }
    
    var historyCellModel: FaxHistoryCellModel? {
        switch self {
        case let .historyItem(model):
            return model
        case .info:
            return nil
        }
    }
    
    func configure(cell: UITableViewCell) {
        switch self {
        case let .historyItem(model):
            cell.setup(FaxHistoryCell.self, model: model)
        case let .info(model):
            cell.setup(FaxHistoryInfoCell.self, model: model)
        }
    }
}
