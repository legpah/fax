//
//  DocumentPickerRoutingModel.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import Foundation

typealias DocumentPickerOutput = (content: Data, fileType: DocumentPickerFileType)

struct DocumentPickerRoutingModel {
    
    let documentTypes: [DocumentPickerFileType]
    let onDocumentsPicked: IClosure<Result<DocumentPickerOutput, Error>>
    let onDismissAction: VoidClosure
    
    init(documentTypes: [DocumentPickerFileType] = DocumentPickerFileType.allTypes,
         onDocumentsPicked: @escaping IClosure<Result<DocumentPickerOutput, Error>>, onDismissAction: @escaping VoidClosure) {
        self.documentTypes = documentTypes
        self.onDocumentsPicked = onDocumentsPicked
        self.onDismissAction = onDismissAction
    }
}
