//
//  FrontPdfPagePresenter.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import Foundation

protocol FrontPdfPagePresenter: ViewOutput {
    var messageMaxCharactersCount: Int { get }
    
    func didChangeTitle(_ text: String)
    
    func didChangeFrom(_ text: String)
    
    func didChangeContact(_ text: String)
    
    func didChangeMessage(_ text: String)
    
    func didTapShowPreview()
    
    func didTapCreate()
    
    func didTapBackButton()
}

final class FrontPdfPagePresenterImpl {
    
    weak var view: FrontPdfPageViewController?
    
    let messageMaxCharactersCount = 650
    
    private var title = ""
    private var fromText = ""
    private var contactText = ""
    private var messageText = ""
    
    private var initialAttributes: FrontPageAttributesModel?
    private weak var router: FrontPdfPageRouter?
    private let frontPagePdfGenerator: FrontPagePDFGenerator
    private let analytics: AnalyticsService
    private let onCreate: IClosure<PDFObject>
    
    init(attributes: FrontPageAttributesModel?,
         router: FrontPdfPageRouter,
         analytics: AnalyticsService = AnalyticsServiceImpl.shared,
         frontPagePdfGenerator: FrontPagePDFGenerator = FrontPagePDFGeneratorImpl(),
         onCreate: @escaping IClosure<PDFObject>) {
        let name = UDAccess<String>(key: .userDefaults.senderName)
        let phone = UDAccess<String>(key: .userDefaults.senderPhone)
        self.initialAttributes = attributes
        self.title = attributes?.title ?? ""
        self.fromText = initialAttributes?.from ?? name.value ?? ""
        self.contactText = initialAttributes?.contact ?? phone.value ?? ""
        self.messageText = initialAttributes?.message ?? ""
        self.router = router
        self.analytics = analytics
        self.frontPagePdfGenerator = frontPagePdfGenerator
        self.onCreate = onCreate
    }
    
    private func attributesModel(theme: UITheme) -> FrontPageAttributesModel {
        FrontPageAttributesModel(title: title,
                                 from: fromText,
                                 contact: contactText,
                                 message: messageText,
                                 theme: theme)
    }
    
    private func setMessageCharactersLeftText() {
        let left = messageMaxCharactersCount - messageText.count
        view?.setMessageCharactersLeft(text: L10n.FrontPage.charactersLeft(left))
    }
}

extension FrontPdfPagePresenterImpl: FrontPdfPagePresenter {
    func viewDidLoad() {
        setMessageCharactersLeftText()
        view?.apply(
            atttibutesModel: .init(
                title: title,
                from: fromText,
                contact: contactText,
                message: messageText,
                theme: initialAttributes?.theme ?? .current
            )
        )
        
        analytics.send(AnalyticsEventImpl(name: "create_page_screen_shown"))
    }
    
    func didTapBackButton() {
        analytics.send(AnalyticsEventImpl(name: "create_page_back_clicked"))
    }
    
    func didChangeTitle(_ text: String) {
        title = text
    }
    
    func didChangeFrom(_ text: String) {
        fromText = text
    }
    
    func didChangeContact(_ text: String) {
        contactText = text
    }
    
    func didChangeMessage(_ text: String) {
        messageText = text
        setMessageCharactersLeftText()
    }
    
    func didTapShowPreview() {
        let source = PDFSource.frontPage(attributesModel(theme: .current))
        router?.showPdfPreview(model: .init(viewTitle: L10n.FrontPage.preview,
                                            pdfObject: source.pdfObject(),
                                            canEditFrontPage: false,
                                            onDocumentEdited: nil))
        
        analytics.send(AnalyticsEventImpl(name: "create_page_preview_clicked"))
    }
    
    func didTapCreate() {
        analytics.send(
            AnalyticsEventImpl(
                name: "create_page_create_clicked",
                parameters: [
                    "title": title.isNotEmpty.analyticValue,
                    "from": fromText.isNotEmpty.analyticValue,
                    "contact": contactText.isNotEmpty.analyticValue,
                    "message": messageText.isNotEmpty.analyticValue
                ]
            )
        )
        router?.didCreate(
            pdfObject: PDFSource.frontPage(attributesModel(theme: .light)).pdfObject()
        )
    }
}
