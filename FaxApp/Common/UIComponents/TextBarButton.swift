//
//  TextBarButton.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import UIKit

final class TextBarButton: UIView {
    
    enum BarPosition {
        case left
        case right
    }
    
    var onTap: VoidClosure?
    
    private let label: Label
    
    init(text: String, position: BarPosition) {
        self.label = Label(text: text,
                           font: .poppins(14, .semiBold),
                           color: .tukesh)
        super.init(frame: .zero)
        addSubview(label)
        switch position {
        case .left:
            label.pinEdgesToSuperviewEdges(excludingEdge: .left)
            label.pinEdge(toSuperviewEdge: .left, withInset: 9)
        case .right:
            label.pinEdgesToSuperviewEdges(excludingEdge: .right)
            label.pinEdge(toSuperviewEdge: .right, withInset: 9)
        }

        addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(didTap))
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func didTap() {
        onTap?()
    }
}
