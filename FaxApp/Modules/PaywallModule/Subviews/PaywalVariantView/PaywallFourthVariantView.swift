//
//  PaywallFourthVariantView.swift
//  FaxApp
//
//  Created by Eugene on 14.07.2022.
//

import UIKit

final class PaywallFourthVariantView: UIView, PaywallVariantView {
    var itemsContainer: PremiumItemsContainer {
        premiumItemsView
    }
    
    let bgRadialImage = ImageView(themedImage: .paywallFourthRadialBg)
    private let faxImage = ImageView(themedImage: .paywallFourthFax)
    private lazy var titleLabel = Label(
        text: L10n.Paywall.Fourth.title,
        font: .poppins(size.value(35, 28, 24), .semiBold),
        color: .init(value: .ds_wh),
        alignment: .center,
        lines: 0
    )
    private lazy var subtitleLabel = Label(
        text: L10n.Paywall.Fourth.subtitle,
        font: .poppins(size.value(16, 12, 12)),
        color: .init(value: .ds_wh),
        alignment: .center,
        lines: 0
    )
    
    let bottomContainer = UIView()
    private let lockImage = ImageView(themedImage: .paywallFourthLock)
    private let lockTitle = Label(
        text: L10n.Paywall.Fourth.lockTitle,
        font: .poppins(16),
        lines: 2
    )
    private let cancelImage = ImageView(themedImage: .paywallFourthCancel)
    private let cancelTitle = Label(
        text: L10n.Paywall.Fourth.cancelTitle,
        font: .poppins(16),
        lines: 2
    )
    private let premiumItemsView = PremiumItemsContainerImpl()
    
    private weak var controller: UIViewController?
    private let size: PaywallSizeVariant
    
    init(
        controller: UIViewController,
        size: PaywallSizeVariant
    ) {
        self.size = size
        self.controller = controller
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSelf() {
        addSubview(bgRadialImage)
        addSubview(faxImage)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(premiumItemsView)
        addSubview(bottomContainer)
        bottomContainer.addSubview(lockImage)
        bottomContainer.addSubview(lockTitle)
        bottomContainer.addSubview(cancelImage)
        bottomContainer.addSubview(cancelTitle)
        
        titleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        subtitleLabel.setContentCompressionResistancePriority(.required, for: .vertical)
        lockTitle.setContentCompressionResistancePriority(.required, for: .horizontal)
        cancelTitle.setContentCompressionResistancePriority(.required, for: .horizontal)
        
        bottomContainer.layer.masksToBounds = false
        
        configureConstraints()
    }
    
    private func configureConstraints() {
        faxImage.pinEdge(toSuperviewEdge: .top, withInset: 4)
        faxImage.alignAxis(toSuperviewAxis: .x)
        faxImage.setDimensions(
            to: size.value(
                .init(width: 105, height: 125),
                .init(width: 83, height: 97),
                .init(width: 61, height: 71)
            )
        )
        
        titleLabel.pinEdge(.top, to: .bottom, of: faxImage, withOffset: size.value(14, 14, 8))
        titleLabel.alignAxis(toSuperviewAxis: .x)
        
        subtitleLabel.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: size.value(16, 16, 8))
        subtitleLabel.pinEdges(toSuperviewEdges: [.left, .right], withInset: size.value(35, 50, 50))
        
        bgRadialImage.pinEdge(toSuperviewEdge: .top, withInset: -200)
        bgRadialImage.pinEdge(.bottom, to: .bottom, of: subtitleLabel, withOffset: size.value(64, 40, 24))
        bgRadialImage.pinEdges(toSuperviewEdges: [.left, .right])
        
        premiumItemsView.pinEdge(.top, to: .bottom, of: bgRadialImage, withOffset: size.value(40, 39, 32))
        premiumItemsView.pinEdges(toSuperviewEdges: [.left, .right])
        
        bottomContainer.pinEdge(.top, to: .bottom, of: premiumItemsView, withOffset: size.value(25, 25, 25))
        bottomContainer.pinEdge(toSuperviewEdge: .bottom, withInset: 5, relation: .greaterThanOrEqual)
        bottomContainer.setDimension(.height, toSize: 45)
        bottomContainer.alignAxis(toSuperviewAxis: .x)
        
        lockImage.pinEdge(toSuperviewEdge: .left)
        lockImage.alignAxis(.y, toSameAxisOf: lockTitle)
        lockImage.setDimensions(to: CGSize(width: 39, height: 35))
        
        lockTitle.pinEdge(.left, to: .right, of: lockImage, withOffset: 7)
        lockTitle.pinEdges(toSuperviewEdges: [.top, .bottom])
        
        cancelImage.pinEdge(.left, to: .right, of: lockTitle, withOffset: 25)
        cancelImage.setDimensions(to: CGSize(width: 37, height: 36))
        cancelImage.alignAxis(.y, toSameAxisOf: cancelTitle)
        
        cancelTitle.pinEdge(.left, to: .right, of: cancelImage, withOffset: 7)
        cancelTitle.pinEdges(toSuperviewEdges: [.top, .bottom, .right])
    }
}

extension PaywallThirdVariantView {
    fileprivate final class CheckMarkItemView: UIView {
        
        private let checkmarkImage = ImageView(themedImage: .paywallCheckmark)
        private let titleLabel = Label(
            font: .poppins(16),
            lines: 0
        )
        
        init(title: String) {
            super.init(frame: .zero)
            titleLabel.text = title
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(checkmarkImage)
            addSubview(titleLabel)
            
            checkmarkImage.pinEdge(toSuperviewEdge: .left)
            checkmarkImage.alignAxis(.y, toSameAxisOf: titleLabel)
            
            titleLabel.pinEdge(.left, to: .right, of: checkmarkImage, withOffset: 9)
            titleLabel.pinEdgesToSuperviewEdges(excludingEdge: .left)
        }
    }
}

