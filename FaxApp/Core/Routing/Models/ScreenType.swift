//
//  ScreenType.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import Foundation

enum RouterType {
    case rootTab(TabBarType)
    case onboarding
    case sendingFax
    case contactsPicker
    case alertOptions
    case frontPdfPage
    case pdfPreview
    case singleImagePicker(SingleImagePickerSource)
    case documentPicker
    case countryPhonesList
    case paywall
    case faq
    case sendMail
}

extension RouterType: RouterIdentifiable {
    var identifier: String {
        switch self {
        case .rootTab(let tabBarType):
            switch tabBarType {
            case .fax:
                return .tabFax
            case .faxHistory:
                return .tabHistory
            case .settings:
                return .tabSettings
            }
        case .onboarding:
            return .onboarding
        case .sendingFax:
            return .sendingFax
        case .contactsPicker:
            return .contactsPicker
        case .alertOptions:
            return .alertOptions
        case .frontPdfPage:
            return .frontPdfPage
        case .pdfPreview:
            return .pdfPreview
        case .singleImagePicker(let singleImagePickerSource):
            switch singleImagePickerSource {
            case .photo:
                return .photoPicker
            case .camera:
                return .cameraPicker
            case .scanner:
                return .documentScaner
            }
        case .documentPicker:
            return .documentPicker
        case .countryPhonesList:
            return .countryPhonesList
        case .paywall:
            return .paywall
        case .sendMail:
            return .sendMail
        case .faq:
            return .faq
        }
    }
    
    init(identifier: RouterIdentifier) {
        switch identifier {
        case .tabFax:
            self = .rootTab(.fax)
        case .tabHistory:
            self = .rootTab(.faxHistory)
        case .tabSettings:
            self = .rootTab(.settings)
        case .onboarding:
            self = .onboarding
        case .sendingFax:
            self = .sendingFax
        case .contactsPicker:
            self = .contactsPicker
        case .alertOptions:
            self = .alertOptions
        case .frontPdfPage:
            self = .frontPdfPage
        case .pdfPreview:
            self = .pdfPreview
        case .photoPicker:
            self = .singleImagePicker(.photo)
        case .cameraPicker:
            self = .singleImagePicker(.camera)
        case .documentScaner:
            self = .singleImagePicker(.scanner)
        case .documentPicker:
            self = .documentPicker
        case .countryPhonesList:
            self = .countryPhonesList
        case .paywall:
            self = .paywall
        case .sendMail:
            self = .sendMail
        case .faq:
            self = .faq
        default:
            fatalError()
        }
    }
}

extension String {
    fileprivate static let tabFax = "tab_fax"
    fileprivate static let tabHistory = "tab_history"
    fileprivate static let tabSettings = "tab_settings"
    fileprivate static let onboarding = "onboarding"
    fileprivate static let sendingFax = "sending_fax"
    fileprivate static let contactsPicker = "contacts_picker"
    fileprivate static let alertOptions = "alert_options"
    fileprivate static let frontPdfPage = "front_pdf_page"
    fileprivate static let pdfPreview = "pdf_preview"
    fileprivate static let photoPicker = "photo_picker"
    fileprivate static let documentScaner = "document_scaner"
    fileprivate static let cameraPicker = "camera_picker"
    fileprivate static let documentPicker = "document_picker"
    fileprivate static let countryPhonesList = "country_phones_list"
    fileprivate static let paywall = "paywall"
    fileprivate static let sendMail = "send_mail"
    fileprivate static let faq = "faq"
}
