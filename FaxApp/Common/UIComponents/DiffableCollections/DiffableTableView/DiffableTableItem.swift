//
//  DiffableTableItem.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

protocol DiffableTableItem: DiffableViewItem, StringReuseIdentifiable {
    func configure(cell: UITableViewCell)
}
