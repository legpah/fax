//
//  SendFaxDocumentSectionModel.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

enum SendFaxDocumentSectionModel: Int, DiffableTableSection {
    var sectionIndex: Int {
        self.rawValue
    }
    
    case first
    
    func createHeader() -> UIView? {
        nil
    }
}
