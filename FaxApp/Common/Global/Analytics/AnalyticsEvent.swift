//
//  AnalyticsEvent.swift
//  FaxApp
//
//  Created by Eugene on 17.05.2022.
//

import Foundation

protocol AnalyticsEvent {
    var name: String { get }
    var parameters: [String: Any]? { get }
}

struct AnalyticsEventImpl: AnalyticsEvent {
    let name: String
    let parameters: [String : Any]?
    
    init(name: String,
         parameters: [String : Any]? = nil) {
        self.name = name
        self.parameters = parameters
    }
}
