//
//  FaxMainPageViewController+FaxDocumentsBlockView.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import Combine
import UIKit

extension FaxMainPageViewControllerImpl {
    final class FaxDocumentsBlockView: UIView {
        
        // MARK: - Private properties
        
        let cellHeight: CGFloat = 93
        let tableView = TableView(registerCells: [
            SendFaxDocumentsCell.self
        ])
        private(set) lazy var dataSource = FaxDiffableSource(
            tableView: tableView,
            canEditItem: { _ in true },
            canReorderItem: { _ in true }
        )
        
        private let emptyAppPlaceholderText = Label(text: L10n.Main.placeholder, font: .poppins(12), color: .quejek, alignment: .center)
        
        init() {
            super.init(frame: .zero)
            configureUI()
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            tableView.frame = self.bounds
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func togglePlaceholder(isVisible: Bool, animated: Bool) {
            UIView.animate(withDuration: .animations.defaultDuration) {
                if isVisible {
                    self.tableView.alpha = 0
                    self.emptyAppPlaceholderText.alpha = 1
                } else {
                    self.tableView.alpha = 1
                    self.emptyAppPlaceholderText.alpha = 0
                }
            }
        }
        
        private func configureUI() {
            addSubview(tableView)
            addSubview(emptyAppPlaceholderText)
            tableView.alpha = 0
            emptyAppPlaceholderText.alpha = 0
            
            tableView.rowHeight = cellHeight
            tableView.estimatedSectionHeaderHeight = 0
            
            applyBordersShape(width: 1, radius: 8)
            tableView.delegate = self

//            tableView.dragDelegate = self
//            tableView.dropDelegate = self
            
            configureConstraints()
            
            themeProvider.register(observer: self)
        }
        
        func reloadTableView() {
            DispatchQueue.main.async { [weak self] in
                self?.tableView.reloadData()
            }
        }
        
        private func configureConstraints() {
            emptyAppPlaceholderText.centerInSuperview()
        }
    }
}

extension FaxMainPageViewControllerImpl.FaxDocumentsBlockView: UIThemable {
    func apply(theme: UITheme) {
        applyBorders(color: .xero)
        backgroundColor = .themed.tikruk.value
        tableView.backgroundColor = .themed.tikruk.value
    }
}


extension FaxMainPageViewControllerImpl.FaxDocumentsBlockView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        cellHeight
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        cellHeight
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.applyReorderingImageControl(color: .themed.grawh.value)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        .none
    }
}

// TODO: - remove
//extension FaxMainPageViewControllerImpl.FaxDocumentsBlockView: UITableViewDragDelegate {
//    func tableView(_ tableView: UITableView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
//        guard let item = dataSource.itemIdentifier(for: indexPath) else {
//            return []
//        }
//        let itemProvider = NSItemProvider(object: item.documentTitle as NSString)
//        let dragItem = UIDragItem(itemProvider: itemProvider)
//        dragItem.localObject = item
//        return [dragItem]
//    }
//}
//
/*extension FaxMainPageViewControllerImpl.FaxDocumentsBlockView: UITableViewDropDelegate {
    func tableView(_ tableView: UITableView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UITableViewDropProposal {
        
       return UITableViewDropProposal(operation: .move, intent: .insertAtDestinationIndexPath)
   }
//
    func tableView(_ tableView: UITableView, performDropWith coordinator: UITableViewDropCoordinator) {}
}*/
