//
//  PaywallViewController+DocumentsButton.swift
//  FaxApp
//
//  Created by Eugene on 31.05.2022.
//

import UIKit

final class DocumentsButton: UIView, UIThemable {
        
        var titleColor: UIThemed<UIColor> = .ibw {
            didSet { apply(theme: .current) }
        }
        
        var onTap: VoidClosure?
        
        private let text: String
        private let label = UILabel()
        
        init(text: String) {
            self.text = text
            super.init(frame: .zero)
            
            self.alpha = 0.3
            addSubview(label)
            label.pinEdgesToSuperviewEdges()
            setDimension(.height, toSize: 18)
            addGestureRecognizer(
                UITapGestureRecognizer(target: self, action: #selector(didTap))
            )
            themeProvider.register(observer: self)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func apply(theme: UITheme) {
            switch theme {
            case .light:
                label.attributedText = NSAttributedString(
                    string: text,
                    attributes: [
                        .foregroundColor: self.titleColor.value,
                        .underlineStyle: NSUnderlineStyle.single.rawValue,
                        .font: UIFont.poppins(12),
                    ]
                )
            case .dark:
                label.attributedText = NSAttributedString(
                    string: text,
                    attributes: [
                        .foregroundColor: self.titleColor.value,
                        .underlineStyle: NSUnderlineStyle.single.rawValue,
                        .font: UIFont.poppins(12),
                    ]
                )
            }
        }
        
        @objc
        private func didTap() {
            onTap?()
        }
}

