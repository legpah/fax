//
//  SettingsViewControllerImpl+SubscriptionView.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import Foundation
import UIKit

enum SubscriptionViewSate {
    case loading
    case notSubscribed(onTap: VoidClosure)
    case subscribed(name: String, expire: String, renewable: Bool)
}

extension SettingsViewControllerImpl {
    final class SubscriptionView: UIView, UIThemable {
        
        private var state: SubscriptionViewSate?
        private let loadingView = LoadingSubscriptionView()
        private let activeSubcriptionView = ActiveSubscriptionView()
        private let purchaseButton = TrunButton(
            title: L10n.Settings.buy,
            titleColor: .bw,
            backroundColor: .tukesh,
            borderColor: nil,
            font: .poppins(16, .semiBold),
            iconModel: TrunButton.IconModel(
                position: .left,
                offset: 8,
                sizelayout: .empty,
                image: .premium
            ),
            height: 64
        )
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func updateState(_ state: SubscriptionViewSate) {
            func update() {
                switch state {
                case .loading:
                    purchaseButton.alpha = 0
                    activeSubcriptionView.alpha = 0
                    loadingView.alpha = 1
                case let .notSubscribed(onTap):
                    purchaseButton.alpha = 1
                    activeSubcriptionView.alpha = 0
                    loadingView.alpha = 0
                    purchaseButton.onTap = onTap
                case let .subscribed(name, expire, renewable):
                    purchaseButton.alpha = 0
                    activeSubcriptionView.alpha = 1
                    loadingView.alpha = 0
                    activeSubcriptionView.typeLabel.text = name
                    activeSubcriptionView.expireLabel.text = expire
                    activeSubcriptionView.isRenewable = renewable
                }
            }
            
            let duration = self.state == nil ? 0 : .animations.defaultDuration
            self.state = state
            
            UIView.animate(withDuration: duration) {
                update()
            }
        }
        
        func apply(theme: UITheme) {
            backgroundColor = .themed.flerio.value
        }
        
        private func configureSelf() {
            setDimension(.height, toSize: 64)
            addSubview(purchaseButton)
            addSubview(activeSubcriptionView)
            addSubview(loadingView)
            
            purchaseButton.bordersWidth = 0
            
            [purchaseButton, activeSubcriptionView, loadingView].forEach {
                $0.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
                $0.pinEdges(toSuperviewEdges: [.top, .bottom])
            }
        }
    }
}


fileprivate class ActiveSubscriptionView: UIView, UIThemable {
    
    var isRenewable: Bool = false {
        didSet { updateRenewableText() }
    }
    
    let typeLabel = Label(font: .poppins(14, .semiBold))
    let expireLabel = Label(font: .poppins(14),
                            color: .redred,
                            alignment: .right)
    private let asteriksLabel = Label(text: "*",
                                      font: .poppins(10),
                                      color: .wibgok,
                                      alignment: .left)
    private let renewLabel = Label(font: .poppins(10),
                                   color: .wibgok,
                                   alignment: .left)
    
    init() {
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func apply(theme: UITheme) {
        backgroundColor = .themed.goeridio.value
        applyBorders(color: .xero)
    }
    
    private func configureSelf() {
        applyBordersShape(width: 1, radius: 8)
        
        addSubview(typeLabel)
        addSubview(expireLabel)
        addSubview(asteriksLabel)
        addSubview(renewLabel)
        
        typeLabel.pinEdge(toSuperviewEdge: .top, withInset: 12)
        typeLabel.pinEdge(toSuperviewEdge: .left, withInset: 16)
        
        expireLabel.pinEdge(toSuperviewEdge: .right, withInset: 16)
        expireLabel.alignAxis(.y, toSameAxisOf: typeLabel)
        
        asteriksLabel.pinEdge(.top, to: .bottom, of: typeLabel, withOffset: 2)
        asteriksLabel.pinEdge(toSuperviewEdge: .left, withInset: 16)
        
        renewLabel.pinEdge(.top, to: .bottom, of: typeLabel, withOffset: 4)
        renewLabel.pinEdge(.left, to: .right, of: asteriksLabel)
        
        updateRenewableText()
        themeProvider.register(observer: self)
    }
    
    private func updateRenewableText() {
        renewLabel.text = isRenewable
        ? L10n.Settings.renews
        : L10n.Settings.change
    }
}


fileprivate class LoadingSubscriptionView: UIView, UIThemable {
    
    private let spinner = ActivityIndicator()
    
    init() {
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func apply(theme: UITheme) {
        applyBorders(color: .xero)
    }
    
    private func configureSelf() {
        spinner.isAnimating = true
        applyBordersShape(width: 1, radius: 8)
        
        addSubview(spinner)
        spinner.centerInSuperview()
        
        themeProvider.register(observer: self)
    }
}
