//
//  FAQSectionModel.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import UIKit

struct FAQSectionModel: DiffableTableSection {
    
    let sectionIndex: Int = 0
    
    func createHeader() -> UIView? {
        nil
    }
}
