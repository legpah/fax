//
//  FaxMainPageRouter.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit

protocol FaxMainPageRouter:
    PDFPreviewRoutable,
    DocumentPickerRoutable,
    CountryPhonesListRoutable,
    ContactsPickerRoutable,
    AlertOptionsRoutable
{
    func showDocumentsSourceAlert(onSelect: @escaping IClosure<FaxDocumentSourceType>)
    func showFrontPage(onCreate: @escaping IClosure<PDFObject>)
    func showSinglePhotoPicker(model: SingleImagePickerRouterModel)
    func showSendingFaxScreen(model: SendingFaxRouterModel)
    func showGalleryErrorAlert(onSettings: @escaping VoidClosure)
}

final class FaxMainPageRouterImpl: BaseScreenRouter, ScreenRouter {
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.rootTab(.fax)
    let presentationType: ScreenPresentationType = .plain
    
    private(set) lazy var presenter = FaxMainPagePresenterImpl(router: self)
    
    private lazy var _screen: UIViewController = {
        
        let countryPhoneInputPresenter = CountryPhoneInputPresenterImpl(
            router: self,
            moduleOutput: presenter
        )
        let countryPhoneInputViewController = CountryPhoneInputViewControllerImpl(
            presenter: countryPhoneInputPresenter,
            showContactsButton: true
        )
        countryPhoneInputPresenter.view = countryPhoneInputViewController
        
        let view = FaxMainPageViewControllerImpl(
            presenter: presenter,
            countryPhoneInputViewController: countryPhoneInputViewController
        )
        
        presenter.view = view
        presenter.phoneModuleInput = countryPhoneInputPresenter
        
        return NavigationController(rootViewController: view, bgColor: .bw)
    }()
}

extension FaxMainPageRouterImpl: FaxMainPageRouter {
    func showDocumentsSourceAlert(onSelect: @escaping IClosure<FaxDocumentSourceType>) {
        let onSelect: IClosure<String> = { rawItem in
            guard let sourceType = FaxDocumentSourceType(string: rawItem) else {
                return
            }
            onSelect(sourceType)
        }
        openAlertOptions(model: .documentSource(onSelect: onSelect), animated: true)
    }
    
    func showFrontPage(onCreate: @escaping IClosure<PDFObject>) {
        let router = FrontPdfPageRouterImpl(attributes: nil, onCreate: onCreate)
        open(router: router)
    }
    
    func showSinglePhotoPicker(model: SingleImagePickerRouterModel) {
        switch model.source {
        case .scanner:
            let router = DocumentScannerRouterImpl(model: model)
            open(router: router)
        case .camera, .photo:
            let router = SingleImagePickerRouterImpl(model: model)
            open(router: router)
        }
    }
    
    func showSendingFaxScreen(model: SendingFaxRouterModel) {
        let router = SendingFaxRouterImpl(model: model)
        open(router: router)
    }
    
    func showGalleryErrorAlert(onSettings: @escaping VoidClosure) {
        openAlertOptions(model: .unableToOpenGallery(onSettings: onSettings), animated: true)
    }
}
