//
//  MultilineInput.swift
//  FaxApp
//
//  Created by Eugene on 18.04.2022.
//

import UIKit

final class MultilineInput: UITextView {
    
    typealias IsKeyboardVisible = Bool
    
    // MARK: - Public properties
    
    var onKeyboardChangeVisibility: IClosure<IsKeyboardVisible>?
    
    var onTextChange: IClosure<TextChange>?
    
    var maxCharactersCount: Int = .max - 1
    
    var borderRadius: CGFloat = 8 {
        didSet { updateBordersShape() }
    }
    
    var bordersWidth: CGFloat = 1 {
        didSet { updateBordersShape() }
    }
    
    var borderColor: UIThemed<UIColor> = .lanka {
        didSet { applyBorders(color: borderColor) }
    }
    
    var insets: UIEdgeInsets = .symmetric(x: 12, y: 11) {
        didSet { textContainerInset = insets }
    }
    
    // MARK: - Private properties
    
    private let toolBar = UIToolbar()
    private let placeholder: Placeholder?
    private let textThemedColor: UIThemed<UIColor>
    private let textFont: UIFont
    private let bgColor: UIThemed<UIColor>
    private var onToolbarDoneTap: VoidClosure?
    
    
    // MARK: Init
    
    init(placeholder: Placeholder?,
         textColor: UIThemed<UIColor> = .ibw,
         bgColor: UIThemed<UIColor> = .lanka,
         textFont: UIFont = .poppins(12)) {
        self.placeholder = placeholder
        self.textThemedColor = textColor
        self.textFont = textFont
        self.bgColor = bgColor
        super.init(frame: .zero, textContainer: nil)
        delegate = self
        self.textContainerInset = insets
        applyAttributes(isShowingPlaceholder: true)
        self.setPlaceholderText()
        updateBordersShape()
        themeProvider.register(observer: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setText(_ text: String) {
        self.text = text
        applyAttributes(isShowingPlaceholder: text.isEmpty)
        if text.isEmpty {
            setPlaceholderText()
        }
    }
    
    func addDoneToolBar(title: String = "Done", action: VoidClosure?) {
        
        onToolbarDoneTap = { [weak self] in
            self?.resignFirstResponder()
            action?()
        }
        
        guard self.inputAccessoryView == nil else { return }
        
        toolBar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: title, style: .done, target: self, action: #selector(handleDoneTap))
        ]
        
        toolBar.sizeToFit()
        self.inputAccessoryView = toolBar
    }
    
    // MARK: - Private
    
    private var isShowingPlaceholder: Bool {
        self.text == placeholder?.text
    }
    
    private func updateBordersShape() {
        applyBordersShape(width: bordersWidth, radius: borderRadius)
    }
    
    private func setPlaceholderText() {
        guard let placeholder = placeholder else {
            return
        }
        self.text = placeholder.text
    }
    
    private func applyAttributes(isShowingPlaceholder: Bool) {
        func applyDefaultTextAttributes() {
            self.textColor = textThemedColor.value
            self.font = textFont
        }
        
        if isShowingPlaceholder {
            guard let placeholder = placeholder else {
                applyDefaultTextAttributes()
                return
            }
            self.textColor = placeholder.color.value
            self.font = placeholder.font
        } else {
            applyDefaultTextAttributes()
        }
    }
    
    @objc private func handleDoneTap() {
        onToolbarDoneTap?()
    }
}

extension MultilineInput: UIThemable {
    func apply(theme: UITheme) {
        applyAttributes(isShowingPlaceholder: text.isEmpty || isShowingPlaceholder)
        backgroundColor = bgColor.value
        applyBorders(color: borderColor)
        
        keyboardAppearance = theme.asKeyboardAppearance
        
        switch theme {
        case .light:
            toolBar.barStyle = .default
        case .dark:
            toolBar.barStyle = .black
        }
    }
}

extension MultilineInput: UITextViewDelegate {
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        onKeyboardChangeVisibility?(true)
        applyAttributes(isShowingPlaceholder: false)
        if isShowingPlaceholder {
            self.text = ""
        }
        return true
    }
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        onKeyboardChangeVisibility?(false)
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            applyAttributes(isShowingPlaceholder: true)
            setPlaceholderText()
        }
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        guard var viewText = textView.text else { return true }
        let newLength = viewText.count + text.count - range.length
        
        guard newLength <= maxCharactersCount else { return false }
        
        if newLength > viewText.count {
            viewText.append(contentsOf: text)
            DispatchQueue.main.async {
                self.onTextChange?(TextChange(text: viewText, editAction: .addition))
            }
        } else if newLength < viewText.count {
            viewText = String(viewText.prefix(newLength))
            DispatchQueue.main.async {
                self.onTextChange?(TextChange(text: viewText, editAction: .deletion))
            }
        }
        
        return true
    }
}

extension MultilineInput {
    struct Placeholder {
        let text: String
        let color: UIThemed<UIColor>
        let font: UIFont
        
        init(text: String,
             color: UIThemed<UIColor> = .baboo,
             font: UIFont = .poppins(12, .light)) {
            self.text = text
            self.color = color
            self.font = font
        }
    }
}

extension MultilineInput {
    struct TextChange {
        let text: String
        let editAction: EditAction
    }
    
    enum EditAction {
        case addition
        case deletion
    }
}
