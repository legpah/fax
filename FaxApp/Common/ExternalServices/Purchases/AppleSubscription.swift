//
//  AppleSusbctiption.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation

enum AppleSubscriptionState {
    case loading
    case subscribed(AppleSubscription)
    case notSubscribed
}

struct AppleSubscription {
    let name: String
    let identifier: String
    let expiresAt: String
    let renewable: Bool
    let isRenewed: Bool
}

extension AppleSubscriptionState {
    var subscription: AppleSubscription? {
        switch self {
        case let .subscribed(subscription):
            return subscription
        default:
            return nil
        }
    }
}

extension AppleSubscriptionState {
    var analyticValue: String {
        switch self {
        case .subscribed(let appleSubscription):
            return appleSubscription.identifier
        case .notSubscribed, .loading:
            return "no"
        }
    }
}
