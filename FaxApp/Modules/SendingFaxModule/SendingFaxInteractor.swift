//
//  SendingFaxInteractor.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation
import Combine

protocol SendingFaxInteractor {
    func sendFax(
        pdfObject: PDFObject,
        recipient: String
    ) -> AnyPublisher<Void, NetworkError>
}

final class SendingFaxInteractorImpl {
    
    private let checkAccessService: SendFaxAccessService
    private let sendFaxService: SendFaxService
    
    init(checkAccessService: SendFaxAccessService = SendFaxAccessServiceImpl(),
         sendFaxService: SendFaxService = SendFaxServiceImpl()) {
        self.checkAccessService = checkAccessService
        self.sendFaxService = sendFaxService
    }
}

extension SendingFaxInteractorImpl: SendingFaxInteractor {
    func sendFax(
        pdfObject: PDFObject,
        recipient: String
    ) -> AnyPublisher<Void, NetworkError> {
        return pdfObject
            .pdfPreview.setFailureType(to: NetworkError.self)
            .flatMap { img in
                self
                    .checkAccessService
                    .sendFaxAccess(pagesCount: pdfObject.pageCount ?? 0)
                    .map { _ -> Data in
                        return img?.jpeg(.lowest) ?? Data()
                    }
            }
            .flatMap { [weak self] imgData -> AnyPublisher<Void, NetworkError> in
                guard let self = self else {
                    return Fail(error: NetworkError.unknownError).eraseToAnyPublisher()
                }
                return self.sendFaxService.sendFax(
                    pdfData: pdfObject.pdfData ?? Data(),
                    previewData: imgData,
                    recipient: recipient,
                    pagesCount: pdfObject.pageCount ?? 0
                )
                .map { _ in () }
                .eraseToAnyPublisher()
            }
            .eraseToAnyPublisher()
    }
}
