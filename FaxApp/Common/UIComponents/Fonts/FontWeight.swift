//
//  FontWeight.swift
//  FaxApp
//
//  Created by Eugene on 18.04.2022.
//

import Foundation

enum FontWeight: String {
    case light = "Light"
    case regular = "Regular"
    case semiBold = "SemiBold"
    case bold = "Bold"
}
