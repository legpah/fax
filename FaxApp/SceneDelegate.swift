//
//  SceneDelegate.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import UIKit

final class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    
    let appCoordinator = AppCoordinator(
        routersStorage: AppRoutersStorageImpl(
            listener: AppRouterStorageAnalyticsService()
        )
    )
    
    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else {
            return
        }
        
        window = UIWindow(windowScene: windowScene)
        appCoordinator.configure(routers: [
            FaxMainPageRouterImpl(),
            FaxHistoryRouterImpl(),
            SettingsRouterImpl(),
        ])
        
        window?.rootViewController = appCoordinator.tabBar
        window?.makeKeyAndVisible()
        
        appCoordinator.open(router: OnboardingRouter(), animated: true)
    }
    
    func scene(_ scene: UIScene, openURLContexts URLContexts: Set<UIOpenURLContext>) {
        guard let url = URLContexts.first?.url else {
            return
        }
        
        Facebook.shared.applicationOpen(url: url)
    }
    
    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not necessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        Facebook.shared.appDidBecomeActive()
        Appsflyer.shared.appDidBecomeActive()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            IDFATrackingManager.shared.requestIdfa()
        }
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
    }
}
