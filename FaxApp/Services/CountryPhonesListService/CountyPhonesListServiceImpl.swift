//
//  CountyPhonesListServiceImpl.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation
import Combine

final class CountyPhonesListServiceImpl {
    private let api = ApiClient()
}

extension CountyPhonesListServiceImpl: CountyPhonesListService {
    var countryPhones: AnyPublisher<[CountryPhone], NetworkError> {
        let request = CountryPhonesEndpoint()
        return api
            .request(request: request)
            .retry(interval: 0.5, retries: .max)
            .eraseToAnyPublisher()
    }
}
