//
//  SendFaxDocumentCellModel.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

struct SendFaxDocumentCellModel {
    let id: String
    let pdfObject: PDFObject
    let pagesCountTitle: String
    let onTap: IOClosure<SendFaxDocumentCellModel, VoidClosure>
    let onDeleteButtonTap: IOClosure<SendFaxDocumentCellModel, VoidClosure>
    
    init(id: String,
         pdfObject: PDFObject,
         pagesCountTitle: String,
         onTap: @escaping IOClosure<SendFaxDocumentCellModel, VoidClosure>,
         onDeleteButtonTap: @escaping IOClosure<SendFaxDocumentCellModel, VoidClosure>) {
        self.id = id
        self.pdfObject = pdfObject
        self.pagesCountTitle = pagesCountTitle
        self.onTap = onTap
        self.onDeleteButtonTap = onDeleteButtonTap
    }
    
    init(model: SendFaxDocumentCellModel,
         pdfObject: PDFObject) {
        self.id = model.id
        self.pdfObject = pdfObject
        self.pagesCountTitle = model.pagesCountTitle
        self.onTap = model.onTap
        self.onDeleteButtonTap = model.onDeleteButtonTap
    }
}

extension SendFaxDocumentCellModel: DiffableTableItem {
    var reuseIdentifier: StringIdentifier {
        SendFaxDocumentsCell.identifier
    }
    
    func configure(cell: UITableViewCell) {
        cell.setup(SendFaxDocumentsCell.self, model: self)
    }
    
    static func == (lhs: SendFaxDocumentCellModel, rhs: SendFaxDocumentCellModel) -> Bool {
        lhs.id == rhs.id
        && lhs.pdfObject.pdfData == rhs.pdfObject.pdfData
        && lhs.pagesCountTitle == rhs.pagesCountTitle
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
