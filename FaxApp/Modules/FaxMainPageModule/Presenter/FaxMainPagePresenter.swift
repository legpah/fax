//
//  FaxMainPagePresenter.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import Foundation
import Combine
import PhotosUI

protocol FaxMainPagePresenter: ViewOutput {
    var isFirstViewUpdate: Bool { get }
    func addFileTapped()
    func sendFileTapped()
    func editDocumentsStateSet()
    func previewAllTapped()
    func clearAllTapped()
}

final class FaxMainPagePresenterImpl {
    
    weak var view: FaxMainPageViewController?
    weak var phoneModuleInput: CountryPhoneInputModuleInput?
    
    private weak var router: FaxMainPageRouter?
    
    private let analytics: AnalyticsService
    private var isInputPhoneValid = false
    private var isCountriesLoaded = false
    private(set) var isFirstViewUpdate = true
    private var isDocumentsTableEditing = false
    private var cancelBag = Set<AnyCancellable>()
    
    init(router: FaxMainPageRouter,
         analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.router = router
        self.analytics = analytics
    }
    
    // MARK: - Routing
    
    private func openFronPage() {
        router?.showFrontPage(onCreate: { [weak self] pdfObject in
            self?.appendCell(pdfObject)
        })
        analytics.send(AnalyticsEventImpl(name: "tab1_document_create_front_page_clicked"))
    }
    
    private func openSinglePhotoPicker(source: SingleImagePickerSource) {
        router?.showSinglePhotoPicker(
            model: .init(
                source: source,
                onPickImage: { [weak self] image in
                    self?.appendCell(PDFSource.image(image).pdfObject())
                }
            )
        )
        
        analytics.send(AnalyticsEventImpl(name: source == .camera ?
                                          "tab1_document_camera_clicked" :
                                          "tab1_document_gallery_clicked"))
    }
    
    private func openPdfPreview(cellModel: SendFaxDocumentCellModel) {
        let onDocumentEdit: IClosure<PDFObject> = { [weak self] pdfObject in
            guard let self = self,
                  var items = self.view?.documentsTable.items(in: .first)
            else {
                return
            }
            if let index = items.firstIndex(where: { $0.id == cellModel.id }) {
                items[index] = SendFaxDocumentCellModel(model: cellModel, pdfObject: pdfObject)
                self.view?.documentsTable.reloadData(section: .first,
                                                     with: items,
                                                     animated: false)
            }
        }
        router?.showPdfPreview(
            model: PDFPreviewPageRouterModel(
                viewTitle: L10n.Main.documentPreview,
                pdfObject: cellModel.pdfObject,
                canEditFrontPage: true,
                onDocumentEdited: onDocumentEdit
            )
        )
    }
    
    private func openDocumentPicker() {
        view?.setLoadDocumentState(false)
        router?.openDocumentPicker(
            model: .init(
                onDocumentsPicked: { [weak self] result in
                    switch result {
                    case .success(let model):
                        let pdfSource: PDFSource
                        switch model.fileType {
                        case .doc:
                            pdfSource = .doc(model.content)
                        case .docx:
                            pdfSource = .docx(model.content)
                        case .pdf:
                            pdfSource = .pdf(model.content)
                        }
                        self?.appendCell(pdfSource.pdfObject())
                    case .failure:
                        self?.router?.openAlertOptions(
                            model: .failedToParseDocument(),
                            animated: true
                        )
                    }
                }, onDismissAction: { [weak self] in
                    self?.view?.setLoadDocumentState(true)
                }
            )
        )
        
        analytics.send(AnalyticsEventImpl(name: "tab1_document_icloud_clicked"))
    }
    
    private func openDocumentSourceAlert() {
        router?.showDocumentsSourceAlert(onSelect: { [weak self] source in
            self?.handleAlertSelect(source: source)
        })
    }
    
    private func handleSourcePermission(source: SingleImagePickerSource, isGranted: Bool) {
        DispatchQueue.main.async { [weak self] in
            if isGranted {
                self?.openSinglePhotoPicker(source: source)
            }
            else {
                self?.router?.showGalleryErrorAlert {
                    if let url = URL(string: UIApplication.openSettingsURLString) {
                        UIApplication.shared.open(url, options: [:], completionHandler: { _ in })
                    }
                }
            }
        }
    }
    
    // MARK: - Private methods
    
    private func checkPermission(source: SingleImagePickerSource) {
        if source == .photo {
            if #available(iOS 14, *) {
                switch PHPhotoLibrary.authorizationStatus(for: .readWrite) {
                case .notDetermined:
                    requestPermission(source: source)
                case .restricted , .denied:
                    handleSourcePermission(source: source, isGranted: false)
                case .authorized, .limited:
                    handleSourcePermission(source: source, isGranted: true)
                @unknown default:
                    break
                }
            }
            else {
                handleSourcePermission(source: source, isGranted: true)
            }
        }
        else {
            switch AVCaptureDevice.authorizationStatus(for: .video) {
            case .notDetermined:
                requestPermission(source: source)
            case .restricted, .denied:
                handleSourcePermission(source: source, isGranted: false)
            case .authorized:
                handleSourcePermission(source: source, isGranted: true)
            @unknown default:
                break
            }
        }
    }
    
    private func requestPermission(source: SingleImagePickerSource) {
        if source == .photo {
            if #available(iOS 14, *) {
                PHPhotoLibrary.requestAuthorization(for: .readWrite) { status in
                    switch status {
                    case .notDetermined:
                        break
                    case .denied, .restricted:
                        self.checkPermission(source: source)
                        self.analytics.send(AnalyticsEventImpl(name: "gallery_perm", parameters: ["answer": false.analyticValue]))
                    case .authorized, .limited:
                        self.checkPermission(source: source)
                        self.analytics.send(AnalyticsEventImpl(name: "gallery_perm", parameters: ["answer": true.analyticValue]))
                    @unknown default:
                        break
                    }
                }
            }
        }
        else if source == .camera || source == .scanner {
            AVCaptureDevice.requestAccess(for: .video) { granted in
                self.checkPermission(source: source)
                self.analytics.send(AnalyticsEventImpl(name: "camera_perm", parameters: ["answer": granted.analyticValue]))
            }
        }
    }
    
    private func handleAlertSelect(source: FaxDocumentSourceType) {
        switch source {
        case .frontPage:
            openFronPage()
        case .photos:
            checkPermission(source: .photo)
        case .camera:
            checkPermission(source: .camera)
        case .files:
            openDocumentPicker()
        case .scanner:
            checkPermission(source: .scanner)
        }
    }
    
    private func appendCell(_ pdfObject: PDFObject) {
        guard canAppendPdfObject(pdfObject) else {
            return
        }
        let items = tableItems
        _ = items.reduce(0, { $0 + ($1.pdfObject.pageCount ?? 0) })
        
        let id = UUID().uuidString
        let pageCount = pdfObject.pageCount ?? 0
        let model = SendFaxDocumentCellModel(id: id, pdfObject: pdfObject, pagesCountTitle: "\(pageCount) " + .converter.pageWord(count: pageCount)) { [weak self] model in
            return { self?.openPdfPreview(cellModel: model) }
        } onDeleteButtonTap: { [weak self] model in
            return {self?.view?.documentsTable.delete(items: [model], animated: true)}
        }
        
        _ = items + [model]
        
        view?.setShouldScrollToBottom(flag: true)
        view?.documentsTable.append(items: [model], to: .first, animated: true)
        view?.documentsTable.reloadSameData(animated: true)
        
//        analytics.send(
//            AnalyticsEventImpl(
//                name: "fax_file_added",
//                parameters: [
//                    "file_pages_number": pdfObject.pdfDocument?.pageCount ?? 0,
//                    "current_pages_number": pagesNumber,
//                    "current_files_number": items.count,
//                    "total_pages_number": pagesNumber + (pdfObject.pageCount ?? 0),
//                    "total_files_number": totalItems.count,
//                    "type": pdfObject.unerlyingSource.analyticsDocumentType,
//                    "total_types": totalItems.map { $0.pdfObject.analyticsDocumentType }
//                ]
//            )
//        )
    }
    
    private func updateViewState() {
        let animated = !isFirstViewUpdate
        if pagesCount == 0 {
            view?.update(state: .emptyDocuments, animated: animated)
        } else {
            view?.update(state: .haveDocuments, animated: animated)
        }
        isFirstViewUpdate = false
    }
    
    private func updateSendButton() {
        view?.updateSendButton(isActive: isCountriesLoaded && isInputPhoneValid)
    }
    
    private func canAppendPdfObject(_ pdfObject: PDFObject) -> Bool {
        let totalPages = pagesCount + (pdfObject.pageCount ?? 0)
        if totalPages > PDFConstants.pagesLimit {
            router?.openAlertOptions(model: .pagesLimitExceed(), animated: true)
            return false
        }
        
        let currentBytes = self.tableItems.reduce(0, {
            res, next in (next.pdfObject.pdfData?.count ?? 0) + res
        })
        let totalBytes = (pdfObject.pdfData?.count ?? 0) + currentBytes
        
        if totalBytes.megabytes > PDFConstants.megabytesLimitInternal {
            router?.openAlertOptions(model: .bytesLimitExceeded(), animated: true)
            return false
        }
        
        return true
    }
    
    private func configureReactionOnDocumentsChanges() {
        view?.documentsTable.sourceDidChange.sink(receiveValue: { [weak self] _ in
            self?.updateViewState()
        }).store(in: &cancelBag)
    }
    
    private func removeAllDocuments(animated: Bool) {
        view?.documentsTable.deleteAll(animated: animated)
    }
    
    private var pagesCount: Int {
        view?.documentsTable.items(in: .first).reduce(0, {
            $0 + ($1.pdfObject.pdfDocument?.pageCount ?? 0)
        }) ?? 0
    }
    
    private var tableItems: [SendFaxDocumentCellModel] {
        view?.documentsTable.items(in: .first) ?? []
    }
    
    private var previewAllPdfObject: PDFMultiplePagesObject {
        return PDFMultiplePagesObject(sources: tableItems.map { $0.pdfObject.unerlyingSource })
    }
}

extension FaxMainPagePresenterImpl: FaxMainPagePresenter {
    func viewDidLoad() {
        configureReactionOnDocumentsChanges()
        updateViewState()
        updateSendButton()
//        addMockCells() // TODO: - remove
        
        view?.documentsTable
            .didDeleteItem
            .sink(receiveValue: { [weak self] item in
                guard let self = self else { return }
                self.analytics.send(AnalyticsEventImpl(name: "tab1_added_file_deleted"))
            })
            .store(in: &cancelBag)
        
        SendFaxNotification.sendPublisher.sinkSuccess { [weak self] _ in
            self?.removeAllDocuments(animated: false)
        }.store(in: &cancelBag)
    }
    
    func viewDidAppear() {
        analytics.send(AnalyticsEventImpl(name: "tab1_screen_shown"))
    }
    
    func addFileTapped() {
        guard pagesCount < PDFConstants.pagesLimit else {
            router?.openAlertOptions(model: .pagesLimitExceed(), animated: true)
            return
        }
        openDocumentSourceAlert()
        analytics.send(AnalyticsEventImpl(name: "tab1_add_file_clicked"))
    }
    
    func editDocumentsStateSet() {
        isDocumentsTableEditing.toggle()
        view?.updateTableViewEditing(isEditing: isDocumentsTableEditing)
        analytics.send(AnalyticsEventImpl(name: "tab1_edit_clicked"))
    }
    
    func previewAllTapped() {
        router?.showPdfPreview(
            model: PDFPreviewPageRouterModel(pdfObject: previewAllPdfObject,
                                             canEditFrontPage: false,
                                             onDocumentEdited: nil)
        )
        analytics.send(AnalyticsEventImpl(name: "tab1_preview_clicked"))
    }
    
    func sendFileTapped() {
        let pdfObject = previewAllPdfObject
        router?.showSendingFaxScreen(
            model: SendingFaxRouterModel(
                recipientPhone: phoneModuleInput?.maskedPhoneRecipient ?? "",
                recepientCountry: phoneModuleInput?.selectedCountry?.name ?? "",
                pdfObject: pdfObject
            )
        )
        analytics.send(AnalyticsEventImpl(name: "tab1_send_clicked"))
    }
    
    func clearAllTapped() {
        router?.openAlertOptions(model: .deleteAllFaxesFiles(onConfirm: { [weak self] in
            self?.view?.documentsTable.deleteAll(animated: true)
        }), animated: true)
        analytics.send(AnalyticsEventImpl(name: "tab1_clear_clicked"))
    }
    
//    private func addMockCells() { // TODO: -Remove
//        self.view?.documentsTable.reload([
//            .init(section: SendFaxDocumentSectionModel.first,
//                  items: [
//                    mockOject(pdfObject: mockPdfObject(title: "Mock title 1")),
//                  ]),
//        ], animated: false)
//    }
//
//    private func mockOject(id: String = UUID().uuidString,
//                           pdfObject: PDFObject) -> SendFaxDocumentCellModel {
//        let pageCount = pdfObject.pageCount ?? 0
//        return SendFaxDocumentCellModel(
//            id: id,
//            pdfObject: pdfObject,
//            pagesCountTitle: "\(pageCount) " + .converter.pageWord(count: pageCount),
//            onTap: { [weak self] model in
//                return { self?.openPdfPreview(cellModel: model) }
//            }
//        )
//    }
//
//    private func mockPdfObject(title: String = "") -> PDFObject {
//        PDFSource.frontPage(FrontPageAttributesModel(title: title, from: "", contact: "", message: "", theme: .light)).pdfObject()
//    }
}

extension FaxMainPagePresenterImpl: CountryPhoneInputModuleOuput {
    func didChangeValidState(_ state: Bool) {
        isInputPhoneValid = state
        updateSendButton()
    }
    
    func countriesLoaded() {
        isCountriesLoaded = true
        updateSendButton()
    }
}
