//
//  AnalyticsService.swift
//  FaxApp
//
//  Created by Eugene on 17.05.2022.
//

import Foundation

protocol AnalyticsService {
    func send(_ event: AnalyticsEvent)
}
