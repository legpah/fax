//
//  CountryPhoneInputViewControllerImpl+CountryButton.swift
//  FaxApp
//
//  Created by Eugene on 27.04.2022.
//

import UIKit

extension CountryPhoneInputViewControllerImpl {
    final class CountryButton: UIView {
        var countyCode: String? {
            didSet { countryIcon.text = countyCode }
        }
        
        var isArrowDown: Bool = true {
            didSet { toggleArrowImage() }
        }
        
        var onTap: VoidClosure? {
            didSet {
                onTapChanged()
            }
        }
        
        var isLoading: Bool = false {
            didSet { updateLoadingState() }
        }
        
        private let container = UIView()
        private let loadingIndicator = ActivityIndicator()
        private let countryIcon = UILabel()
        private let arrowIcon = ImageView(themedImage: nil)
        
        init() {
            super.init(frame: .zero)
            toggleArrowImage()
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func toggleArrowImage() {
            arrowIcon.themedImage = isArrowDown ? .arrowDown : .arrowUp
        }
        
        private func configureSelf() {
            addSubview(loadingIndicator)
            addSubview(container)
            container.addSubview(countryIcon)
            container.addSubview(arrowIcon)
            
            confiureConstraints()
        }
        
        private func confiureConstraints() {
            loadingIndicator.centerInSuperview()
            
            container.centerInSuperview()
            container.setDimension(.height, toSize: 20)
            container.pinEdge(.left, to: .left, of: countryIcon)
            container.pinEdge(.right, to: .right, of: arrowIcon)
            
            countryIcon.pinEdge(toSuperviewEdge: .left)
            countryIcon.alignAxis(toSuperviewAxis: .y)
            
            arrowIcon.pinEdge(.left, to: .right, of: countryIcon, withOffset: 7)
            arrowIcon.pinEdge(.right, to: .right, of: container)
            arrowIcon.alignAxis(.y, toSameAxisOf: countryIcon)
        }
        
        private func updateLoadingState() {
            container.alpha = isLoading ? 0 : 1
            loadingIndicator.isAnimating = isLoading
        }
        
        private func onTapChanged() {
            if onTap == nil {
                self.gestureRecognizers?.removeAll()
            } else {
                self.addGestureRecognizer(
                    UITapGestureRecognizer(target: self, action: #selector(handleTap))
                )
            }
        }
        
        @objc
        private func handleTap() {
            onTap?()
        }
    }
}
