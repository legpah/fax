//
//  PremiumItemView.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import UIKit

final class PremiumItemView: UIView {
    
    var useLightColors: Bool = false {
        didSet {
            titleLabel.color = .init(value: .ds_wh)
            subtitleLabel.color = .init(value: .ds_wh.withAlphaComponent(0.7))
            priceLabel.color = .init(value: .ds_wh)
            apply(theme: .current)
        }
    }
    
    var isSelected: Bool = false {
        didSet {
            guard isSelected != oldValue else { return }
            UIView.animate(withDuration: .animations.fastTransitionDuration) {
                self.apply(theme: .current)
            }
        }
    }
    
    private let btn = InvisibleButton()
    private let titleLabel = Label(font: .poppins(14, .semiBold))
    private let subtitleLabel = Label(font: .poppins(12))
    private let priceLabel = Label(font: .poppins(14, .semiBold),
                                   color: .tukesh)
    private var topTipBgMock: UIView?
    private var topTipLabel: Label?
    
    private(set) var model: PremiumItemModel
    
    init(model: PremiumItemModel) {
        self.model = model
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        guard topTipBgMock == nil else { return }
        configureTopTipLabel()
    }
    
    private func configureSelf() {
        addSubview(btn)
        addSubview(titleLabel)
        addSubview(subtitleLabel)
        addSubview(priceLabel)
        
        titleLabel.text = model.title
        subtitleLabel.text = model.subtitle
        priceLabel.text = model.price
        
        btn.onTap = { [weak self] in
            guard let self = self else { return }
            self.model.onTap(self.model)
        }
        
        applyBordersShape(width: 1, radius: 8, masksToBounds: false)
        
        configureConstraints()
        
        themeProvider.register(observer: self)
    }
    
    private func configureConstraints() {
        setDimension(.height, toSize: 48)
        
        btn.pinEdgesToSuperviewEdges()
        
        titleLabel.pinEdge(toSuperviewEdge: .left, withInset: 16)
        titleLabel.pinEdge(toSuperviewEdge: .top, withInset: 6)
        
        subtitleLabel.pinEdge(toSuperviewEdge: .left, withInset: 16)
        subtitleLabel.pinEdge(.top, to: .bottom, of: titleLabel)
        
        [titleLabel, subtitleLabel].forEach {
            $0.pinEdge(.right, to: .left, of: priceLabel, withOffset: 10, relation: .lessThanOrEqual)
        }
        
        priceLabel.pinEdge(.right, to: .right, of: self, withOffset: -16)
        priceLabel.alignAxis(toSuperviewAxis: .y)
    }
    
    private func configureTopTipLabel() {
        guard let tipModel = model.topTipModel else {
            return
        }
        
        let bgMock = UIView()
        let label = Label(
            text: tipModel.title,
            font: .poppins(10, .semiBold),
            color: tipModel.titleColor(useLightColors: useLightColors),
            alignment: .center
        )
        
        topTipBgMock = bgMock
        topTipLabel = label
        bgMock.applyBordersShape(width: 0, radius: 3.5)
        
        superview?.addSubview(bgMock)
        bgMock.addSubview(label)
        
        bgMock.alignAxis(toSuperviewAxis: .x)
        bgMock.pinEdge(.top, to: .top, of: self, withOffset: -7)
        label.pinEdgesToSuperviewEdges(with: .init(top: 0, left: 8, bottom: 0, right: 8))
        
        apply(theme: .current)
    }
}

extension PremiumItemView: UIThemable {
    func apply(theme: UITheme) {
        if useLightColors {
            backgroundColor = .clear
          //  topTipBgMock?.backgroundColor = .clear
        } else {
            backgroundColor = .themed.goeridio.value
          //  topTipBgMock?.backgroundColor = .themed.goeridio.value
        }
        
        
        topTipLabel?.color = model.topTipModel?.titleColor(useLightColors: useLightColors)
        topTipBgMock?.backgroundColor = model.topTipModel?.bgColor(useLightColors: useLightColors).value
        
        if isSelected {
            if useLightColors {
                applyBorders(color: .init(value: .ds_wh))
            } else {
                applyBorders(color: .tukesh)
            }
        } else {
            if useLightColors {
                applyBorders(color: .init(value: .ds_wh.withAlphaComponent(0.5)))
            } else {
                applyBorders(color: .purity)
            }
        }
    }
}

extension PremiumItemModel.TopTipModel {
    fileprivate func titleColor(useLightColors: Bool) -> UIThemed<UIColor> {
        switch self.style {
        case .genstro:
            if useLightColors {
                return .init(value: .ds_wh)
            }
            return UIThemed(
                light: .ds_pur,
                dark: .ds_wh
            )
        case .lebry:
            if useLightColors {
                return .init(value: .ds_pur)
            }
            return .bw
        }
    }
    
    fileprivate func bgColor(useLightColors: Bool) -> UIThemed<UIColor> {
        switch self.style {
        case .genstro:
            if useLightColors {
                return .init(value: .ds_pur2)
            }
            return UIThemed(
                light: .ds_pur1,
                dark: .ds_d2
            )
        case .lebry:
            if useLightColors {
                return .init(value: .ds_wh)
            } else {
                return .tukesh
            }
        }
    }
}
