//
//  WeakBox.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import Foundation

struct WeakBox<T> {
    private weak var object: AnyObject?
    
    var value: T? {
        return object as? T
    }
    
    init(_ object: T?) {
        guard let object = object else {
            return
        }
        self.object = object as AnyObject
    }
}
