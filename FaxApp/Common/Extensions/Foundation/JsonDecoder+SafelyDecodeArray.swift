//
//  JsonDecoder+SafelyDecodeArray.swift
//  FaxApp
//
//  Created by Eugene on 25.05.2022.
//

import Foundation

private struct OptionalObject<Base: Decodable>: Decodable {
    let value: Base?
    
    init(from decoder: Decoder) throws {
        do {
            let container = try decoder.singleValueContainer()
            self.value = try container.decode(Base.self)
        } catch {
            self.value = nil
        }
    }
}

extension JSONDecoder {
    func decodeArraySafely<Element: Decodable>(
        data: Data,
        analytics: AnalyticsService = AnalyticsServiceImpl.shared
    ) throws -> [Element] {
        return try self
            .decode([OptionalObject<Element>].self, from: data)
            .compactMap { obj in
                if obj.value == nil {
                    analytics.send(
                        AnalyticsEventImpl(
                            name: "safe_serialization_array_element",
                            parameters: ["object_type": "\(Element.self)"]
                        )
                    )
                }
                return obj.value
            }
    }
}
