//
//  FAQCellModel.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import UIKit

struct FAQCellModel: DiffableTableItem {
    
    let reuseIdentifier = FAQItemCell.identifier
    
    let question: String
    let answer: String
    var isExpanded: Bool
    let onExpandTap: IOClosure<FAQCellModel, VoidClosure>
    
    mutating func toogleExpanded() {
        self.isExpanded.toggle()
    }
    
    func configure(cell: UITableViewCell) {
        cell.setup(FAQItemCell.self, model: self)
    }
    
    static func == (lhs: FAQCellModel, rhs: FAQCellModel) -> Bool {
        lhs.question == rhs.question && lhs.isExpanded == rhs.isExpanded
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(question)
    }
}
