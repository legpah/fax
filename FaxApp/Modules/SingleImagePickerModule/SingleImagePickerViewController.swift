//
//  SingleImagePickerViewController.swift
//  FaxApp
//
//  Created by Eugene on 19.04.2022.
//

import UIKit
import MobileCoreServices
import UniformTypeIdentifiers

protocol SingleImagePickerViewController: AnyObject {
    
}

final class SingleImagePickerViewControllerImpl: UIImagePickerController {
    
    private var presenter: SingleImagePickerPresenter?
    
    func configure(presenter: SingleImagePickerPresenter,
                   source: SingleImagePickerSource) {
        self.presenter = presenter
        self.sourceType = source.asNativeSouce
        if #available(iOS 14, *) {
            self.mediaTypes = [UTType.image.identifier]
        } else {
            self.mediaTypes = [kUTTypeImage as String]
        }
        self.allowsEditing = false
        self.overrideUserInterfaceStyle = UITheme.current.asSystemTheme
        self.modalPresentationStyle = .fullScreen
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }
}

extension SingleImagePickerViewControllerImpl: SingleImagePickerViewController {}
extension SingleImagePickerViewControllerImpl: UINavigationControllerDelegate {}

extension SingleImagePickerViewControllerImpl: UIImagePickerControllerDelegate {
    func imagePickerController(
        _ picker: UIImagePickerController,
        didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]
    ) {
        presenter?.imagePicked(image: info[.originalImage] as? UIImage)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        presenter?.dismissTapped()
    }
}


