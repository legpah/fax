//
//  FaxMainPageViewController+SendToRecipientBlockView.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

extension FaxMainPageViewControllerImpl {
    final class SendToRecipientBlockView: UIView {
        
        var onContactsTap: VoidClosure?
        
        private let titleLabel = UILabel()
        
        private let container = UIView()
        private let contactsButton = IconButton(image: .contactPersonIcon)
        
        init() {
            super.init(frame: .zero)
            configureUI()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureUI() {
            addSubview(titleLabel)
            addSubview(container)
            addSubview(contactsButton)
            
            titleLabel.text = L10n.Main.recipient
            
            container.applyBordersShape(width: 1, radius: 8)
            contactsButton.applyBordersShape(width: 1, radius: 8)
            
            configureContraints()
            
            themeProvider.register(observer: self)
        }
        
        private func configureContraints() {
            titleLabel.pinEdges(toSuperviewEdges: [.left, .top])
            
            container.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 12)
            container.pinEdge(.right, to: .left, of: contactsButton, withOffset: -12)
            container.pinEdge(toSuperviewEdge: .left)
            container.setDimension(.height, toSize: 40)
            
            contactsButton.match(.width, to: .height, of: container)
            contactsButton.match(.height, to: .height, of: container)
            contactsButton.pinEdge(toSuperviewEdge: .right)
            contactsButton.alignAxis(.y, toSameAxisOf: container)
            
            pinEdge(.bottom, to: .bottom, of: container)
        }
    }
}

extension FaxMainPageViewControllerImpl.SendToRecipientBlockView: UIThemable {
    func apply(theme: UITheme) {
        titleLabel.textColor = .themed.ibw.value
        container.applyBorders(color: .gojo)
        contactsButton.applyBorders(color: .gojo)
        contactsButton.backgroundColor = .themed.paida.value
    }
}
