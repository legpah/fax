//
//  PremiumItemModel.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import Adapty

struct PremiumItemModel {
    let topTipModel: TopTipModel?
    let premiumId: String
    let title: String
    let subtitle: String
    let price: String
    let onTap: IClosure<PremiumItemModel>
}

extension PremiumItemModel {
    struct TopTipModel {
        enum Style: String {
            case genstro
            case lebry
        }
        
        let style: Style
        let title: String
    }
}


extension PremiumItemModel {
    static func from(
        paywall: AdaptyPaywall,
        product: AdaptyPaywallProduct,
        onTap: @escaping IClosure<PremiumItemModel>
    ) -> PremiumItemModel {
        
        let topTipRemoteModel = paywall.typedCustomPayload?.productsUI.first(where: {
            $0.productId == product.vendorProductId
        })?.topTip
        
        let topTipModel = topTipRemoteModel.flatMap {
            PremiumItemModel.TopTipModel(
                style: .init(rawValue: $0.style) ?? .genstro,
                title: $0.title
            )
        }
        
        return PremiumItemModel(
            topTipModel: topTipModel,
            premiumId: product.vendorProductId,
            title: product.localizedTitle,
            subtitle: getSubtitleForModel(from: product.localizedDescription),
            price: product.localizedPrice ?? "",
            onTap: onTap
        )
        
        func getSubtitleForModel(from productSubtitle: String) -> String {
            if productSubtitle.contains("$") {
                var str = productSubtitle.replacingOccurrences(of: "$", with: "")
                str = "$" + str
                return str
            } else {
                return productSubtitle
            }
        }
    }
}
