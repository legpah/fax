//
//  UDAccessFactory.swift
//  FaxApp
//
//  Created by Eugene on 19.05.2022.
//

import Foundation

enum UDAccessFactory {
    static var startupDate: UDAccess<Int> {
        UDAccess<Int>(key: .userDefaults.startupDate)
    }
}
