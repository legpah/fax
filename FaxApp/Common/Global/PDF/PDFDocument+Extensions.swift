//
//  PDFDocument+Extensions.swift
//  FaxApp
//
//  Created by Eugene on 29.04.2022.
//

import PDFKit

extension PDFDocument {
    convenience init(image: UIImage) {
        self.init()
        if let page = PDFPage(image: image) {
            insert(page, at: 0)
        }
    }
}



