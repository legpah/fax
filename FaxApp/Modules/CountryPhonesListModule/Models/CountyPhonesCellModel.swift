//
//  CountyPhonesCellModel.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import UIKit

struct CountyPhonesCellModel: DiffableTableItem {
    
    var reuseIdentifier: StringIdentifier {
        CountyPhoneCell.identifier
    }
    
    let id: String
    let name: String
    let flagCode: String
    let countryCode: String
    let onTap: IOClosure<CountyPhonesCellModel, VoidClosure>
    
    
    func configure(cell: UITableViewCell) {
        cell.setup(CountyPhoneCell.self, model: self)
    }
    
    static func == (lhs: CountyPhonesCellModel, rhs: CountyPhonesCellModel) -> Bool {
        lhs.id == rhs.id
        && lhs.name == rhs.name
        && lhs.flagCode == rhs.flagCode
        && lhs.countryCode == rhs.countryCode
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
