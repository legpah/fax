//
//  UIImage+Jpeg.swift
//  FaxApp
//
//  Created by Eugene on 1.06.2022.
//

import UIKit

extension UIImage {
    enum JPEGQuality: CGFloat {
        /// 0.0
        case lowest  = 0
        /// 0.25
        case low     = 0.25
        /// 0.5
        case medium  = 0.5
        /// 0.75
        case high    = 0.75
        /// 1.0
        case highest = 1
    }

    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ quality: JPEGQuality) -> Data? {
        return self.jpegData(compressionQuality: quality.rawValue)
    }
}
