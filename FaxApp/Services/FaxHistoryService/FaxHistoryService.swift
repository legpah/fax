//
//  FaxHistoryService.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Combine

protocol FaxHistoryService {
    var faxHistory: AnyPublisher<[FaxHistoryRemoteItem], NetworkError> { get }
}
