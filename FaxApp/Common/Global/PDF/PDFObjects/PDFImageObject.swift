//
//  PDFImageObject.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import Foundation
import PDFKit
import Combine

final class PDFImageObject: PDFObject {
    let unerlyingSource: PDFSource
    let pdfDocument: PDFDocument?
    private(set) lazy var pdfData: Data? = pdfDocument?.dataRepresentation()
    var pdfPreview: AnyPublisher<UIImage?, Never> {
        imageSubject.eraseToAnyPublisher()
    }
    
    private let imageSubject = CurrentValueSubject<UIImage?, Never>(nil)
    
    init(image: UIImage) {
        let pdfImage = pdfImage(image: image) ?? image
        self.unerlyingSource = .image(pdfImage)
        self.pdfDocument = PDFDocument(image: pdfImage)
        generatePdfPreview { [weak self] image in
            self?.imageSubject.send(image)
        }
    }
}

fileprivate func pdfImage(image: UIImage) -> UIImage? {
    let pageSize = PDFConstants.pageSize
    guard let image = image.resizeImage(targetSize: pageSize) else {
        return nil
    }
    guard image.size.height < pageSize.height else {
        return image
    }
    
    let imageView = UIImageView(image: image)
    let background = UIView(frame: CGRect(origin: .zero, size: pageSize))
    background.backgroundColor = .white
    background.addSubview(imageView)
    imageView.center = background.center
    
    return background.renderImage(withBlackFilter: true)
}
