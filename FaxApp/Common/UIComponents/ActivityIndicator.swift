//
//  ActivityIndicator.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import UIKit

final class ActivityIndicator: UIView {
    
    var isAnimating: Bool = false {
        didSet { updateAnimatingState() }
    }
    
    private let color: UIThemed<UIColor>
    private let indicator: UIActivityIndicatorView
    
    init(color: UIThemed<UIColor> = .ibw,
         style: UIActivityIndicatorView.Style = .medium) {
        indicator = UIActivityIndicatorView(style: style)
        self.color = color
        super.init(frame: .zero)
        let size: CGFloat = {
            switch style {
            case .large:
                return 20
            case .medium:
                return 30
            default:
                return 40
            }
        }()
        configureSelf(size: size)
        themeProvider.register(observer: self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSelf(size: CGFloat) {
        indicator.hidesWhenStopped = true
        addSubview(indicator)
        indicator.centerInSuperview()
        setDimensions(to: .symmetric(size))
    }
    
    private func updateAnimatingState() {
        if isAnimating {
            indicator.startAnimating()
        } else {
            indicator.stopAnimating()
        }
    }
}


extension ActivityIndicator: UIThemable {
    func apply(theme: UITheme) {
        indicator.color = color.value
    }
}
