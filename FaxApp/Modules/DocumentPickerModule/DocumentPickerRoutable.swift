//
//  DocumentPickerRoutable.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import Foundation

protocol DocumentPickerRoutable: AnyObject {
    func openDocumentPicker(model: DocumentPickerRoutingModel)
}

extension DocumentPickerRoutable where Self: ScreenRouter {
    func openDocumentPicker(model: DocumentPickerRoutingModel) {
        open(router: DocumentPickerRouterImpl(model: model))
    }
}
