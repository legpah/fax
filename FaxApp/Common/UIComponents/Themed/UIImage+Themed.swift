//
//  UIImage+Themed.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit

extension UIImage {
    static let themed = UIThemed<UIImage>.self
}

extension UIThemed where T == UIImage {
    static var emptyDocumentsPlaceholder: UIThemed<UIImage> {
        .init(light: Asset.sendFaxEmptyDocumentsLight, dark: Asset.sendFaxEmptyDocumentsDark)
    }
    static var plusIcon: UIThemed<UIImage> {
        .init(light: Asset.plusLight, dark: Asset.plusDark)
    }
    
    static var plusInactiveIcon: UIThemed<UIImage> {
        .init(light: Asset.plusInactiveLight, dark: Asset.plusInactiveDark)
    }
    
    static var contactPersonIcon: UIThemed<UIImage> {
        .init(light: Asset.contactPersonLight, dark: Asset.contactPersonDark)
    }
    
    static var switchThemeIcon: UIThemed<UIImage> {
        .init(light: Asset.switchToDarkTheme, dark: Asset.switchToLightTheme)
    }
    
    static var arrowRight: UIThemed<UIImage> {
        .init(light: Asset.arrowRightLight, dark: Asset.arrowRightDark)
    }
    
    static var arrowUp: UIThemed<UIImage> {
        .init(light: Asset.arrowUpLight, dark: Asset.arrowUpDark)
    }
    
    static var arrowDown: UIThemed<UIImage> {
        .init(light: Asset.arrowDownLight, dark: Asset.arrowDownDark)
    }
    
    static var dotsThreeHorizontal: UIThemed<UIImage> {
        .init(light: Asset.dotsThreeHorizontalLight, dark: Asset.dotsThreeHorizontalDark)
    }
    
    static var phoneIcon: UIThemed<UIImage> {
        .init(light: Asset.phoneLight, dark: Asset.phoneDark)
    }
    
    static var premium: UIThemed<UIImage> {
        .init(light: Asset.premiumLight, dark: Asset.premiumDark)
    }
    
    static var paywallImage: UIThemed<UIImage> {
        .init(light: Asset.paywallImageLight, dark: Asset.paywallImageDark)
    }
    
    static var questionError: UIThemed<UIImage> {
        .init(light: Asset.questionErrorLight, dark: Asset.questionErrorDark)
    }
    
    static var searchIcon: UIThemed<UIImage> {
        .init(light: Asset.searchLight, dark: Asset.searchDark)
    }
    
    // Settings
    
    static var faq: UIThemed<UIImage> {
        .init(light: Asset.faqQuestionLight, dark: Asset.faqQuestionDark)
    }
    
    static var contactUs: UIThemed<UIImage> {
        .init(light: Asset.contactUsLight, dark: Asset.contactUsDark)
    }
    
    static var privacyPolicy: UIThemed<UIImage> {
        .init(light: Asset.privacyPolicyLight, dark: Asset.privacyPolicyDark)
    }
    
    static var rate: UIThemed<UIImage> {
        .init(light: Asset.rateAppLight, dark: Asset.rateAppDark)
    }
    
    static var crestRed: UIThemed<UIImage> {
        .init(light: Asset.crestRed, dark: Asset.crestRed)
    }
    
    static var checkmarkGreen: UIThemed<UIImage> {
        .init(light: Asset.checkmarkGreen, dark: Asset.checkmarkGreen)
    }
    
    static var exclamationTriangle: UIThemed<UIImage> {
        .init(light: Asset.exclamationTriangle, dark: Asset.exclamationTriangle)
    }
    
    static var crestInput: UIThemed<UIImage> {
        .init(light: Asset.crestInputLight, dark: Asset.crestInputDark)
    }
    
    static var notificationsSettings: UIThemed<UIImage> {
        .init(light: Asset.notificationsSettingsLight, dark: Asset.notificationsSettingsDark)
    }
    
    // Tab bar icons
    
    static var faxTabSelectedIcon: UIThemed<UIImage> {
        .init(light: Asset.faxTabSelectedLight, dark: Asset.faxTabSelectedLight)
    }
    
    static var faxTabUnselectedIcon: UIThemed<UIImage> {
        .init(light: Asset.faxTabUnselectedLight, dark: Asset.faxTabUnselectedDark)
    }
    
    static var historySelectedTabIcon: UIThemed<UIImage> {
        .init(light: Asset.historyTabSelectedLight, dark: Asset.historyTabSelectedDark)
    }
    
    static var historyUnselectedTabIcon: UIThemed<UIImage> {
        .init(light: Asset.historyTabUnselectedLight, dark: Asset.historyTabUnselectedDark)
    }
    
    static var settingsSelectedTabIcon: UIThemed<UIImage> {
        .init(light: Asset.settingsTabSelectedLight, dark: Asset.settingsTabSelectedDark)
    }
    
    static var settingsUnselectedTabIcon: UIThemed<UIImage> {
        .init(light: Asset.settingsTabUnselectedLight, dark: Asset.settingsTabUnselectedDark)
    }
    
    static var paywallStar: UIThemed<UIImage> {
        .init(light: Asset.paywallStarLight, dark: Asset.paywallStarDark)
    }
    
    static var paywallStarWhite: UIThemed<UIImage> {
        .init(light: Asset.paywallStarWhiteLight, dark: Asset.paywallStarWhiteDark)
    }
    
    static var paywallReverseKey: UIThemed<UIImage> {
        .init(light: Asset.paywallReverseKeyLight, dark: Asset.paywallReverseKeyDark)
    }
    
    static var paywallCheckmark: UIThemed<UIImage> {
        .init(light: Asset.paywallCheckmarkLight, dark: Asset.paywallCheckmarkDark)
    }
    
    static var paywallThirdFax: UIThemed<UIImage> {
        .init(light: Asset.paywallThirdFaxLight, dark: Asset.paywallThirdFaxDark)
    }
    
    static var paywallFourthFax: UIThemed<UIImage> {
        .init(light: Asset.paywallFourthFaxLight, dark: Asset.paywallFourthFaxDark)
    }
    
    static var paywallFourthLock: UIThemed<UIImage> {
        .init(light: Asset.paywallFourthLockLight, dark: Asset.paywallFourthLockDark)
    }
    
    static var paywallNoLimit: UIThemed<UIImage> {
        .init(light: Asset.paywallNoLimit, dark: Asset.paywallNoLimit)
    }
    
    static var paywalldeliveryTracking: UIThemed<UIImage> {
        .init(light: Asset.paywallDeliveryTracking, dark: Asset.paywallDeliveryTracking)
    }
    
    static var paywallWorldWide: UIThemed<UIImage> {
        .init(light: Asset.paywallWorldwide, dark: Asset.paywallWorldwide)
    }
    
    static var paywallFaxSent: UIThemed<UIImage> {
        .init(light: Asset.paywallFaxSentLight, dark: Asset.paywallFaxSentDark)
    }
    
    static var readyToGo: UIThemed<UIImage> {
        .init(light: Asset.readyToGo, dark: Asset.readyToGo)
    }
    
    static var paywallFourthCancel: UIThemed<UIImage> {
        .init(light: Asset.paywallFourthCancelLight, dark: Asset.paywallFourthCancelDark)
    }
    
    static var paywallFourthRadialBg: UIThemed<UIImage> {
        .init(light: Asset.paywallFourthRadialBgLight, dark: Asset.paywallFourthRadialBgDark)
    }
    
    static var deleteFile: UIThemed<UIImage> {
        .init(light: Asset.deleteFile, dark: Asset.deleteFile)
    }
    
    fileprivate init(light: ImageAsset, dark: ImageAsset) {
        self.light = light.image
        self.dark = dark.image
    }
}
