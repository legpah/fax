//
//  FaxHistoryPresenter.swift
//  FaxApp
//
//  Created by Eugene on 29.04.2022.
//

import Foundation
import Combine

protocol FaxHistoryPresenter: ViewOutput {
    func clearAllTapped()
    func pullToRefreshTriggered()
}

final class FaxHistoryPresenterImpl {
    
    weak var view: FaxHistoryViewController?
    
    private weak var router: FaxHistoryRouter?
    private let faxSendedPublisher: AnyPublisher<PagesCount, Never>
    private let interactor: FaxHistoryInteractor
    private let analytics: AnalyticsService
    private var cancelBag = Set<AnyCancellable>()
    
    init(router: FaxHistoryRouter,
         faxSendedPublisher: AnyPublisher<PagesCount, Never>,
         analytics: AnalyticsService = AnalyticsServiceImpl.shared,
         interactor: FaxHistoryInteractor = FaxHistoryInteractorImpl()) {
        self.analytics = analytics
        self.faxSendedPublisher = faxSendedPublisher
        self.interactor = interactor
        self.router = router
    }
    
    // show pdf
    
    private func showPdf(url: String) {
        guard let url = URL(string: url) else { return }
        view?.update(appearance: .loading)
        
        DispatchQueue.global().async { [weak self] in
            guard let data = try? Data(contentsOf: url) else {
                DispatchQueue.main.async {
                    self?.justShowErrorInform(.unknownError)
                }
                return
            }
            DispatchQueue.main.async {
                self?.view?.update(appearance: .history)
                self?.router?.showPdfPreview(
                    model: PDFPreviewPageRouterModel(
                        viewTitle: L10n.History.sended,
                        pdfObject: PDFSource.pdf(data).pdfObject()
                    )
                )
            }
        }
    }
    
    // fetch history
    
    private func fetchHistory(showLoading: Bool = true) {
        if showLoading {
            view?.update(appearance: .loading)
        }
        interactor
            .faxHistory
            .sinkResult { [weak self] result in
                switch result {
                case .success(let items):
                    self?.handleHistoryResponse(items)
                case .failure(let error):
                    self?.handleHistoryError(error)
                }
            }
            .store(in: &cancelBag)
    }
    
    private func handleHistoryResponse(_ remoteItems: [FaxHistoryRemoteItem]) {
        guard remoteItems.isNotEmpty else {
            showEmptyFaxInform()
            return
        }
        
        let historyCells = remoteItems
            .map { remoteItem in
                return FaxHistoryCellModel(
                    remoteItem: remoteItem,
                    onFilePreviewTap: { [weak self] model in
                        self?.showPdf(url: model.pdfUrl)
                    },
                    onErrorTipTap: { [weak self] model in
                        self?.showHistoryErrorTipInform(model: remoteItem)
                    })
            }.map {
                FaxHistoryTableItemModel.historyItem($0)
            }
        
        let cells = historyCells + [
            .info(
                FaxHistoryInfoCellModel(
                    title: L10n.History.desc
                )
            )
        ]
        
        view?.update(appearance: .history)
        view?.tableSource.reloadData(section: .first,
                                     with: cells,
                                     animated: true)
    }
    
    private func handleHistoryError(_ error: NetworkError) {
        view?.update(
            appearance: .inform(
                .networkError(
                    error,
                    buttonText: L10n.History.tryAgain,
                    onTap: { [weak self] in
                        self?.fetchHistory()
                    }
                )
            )
        )
    }
    
    private func showHistoryErrorTipInform(model: FaxHistoryRemoteItem) {
        guard let errorDescription = model.errorDescription else {
            return
        }
        analytics.send(
            AnalyticsEventImpl(
                name: "failed_delivery_reason_click",
                parameters: [
                    "sent_at": model.sentAt,
                    "pages": model.pagesCount,
                    "error": errorDescription.analyticsCode,
                    "errorDescription": errorDescription.analyticsMessage
                ]
            )
        )
        
        view?.update(
            appearance: .inform(
                .networkError(
                    .apiError(errorDescription),
                    buttonText: "OK") { [weak self] in
                        self?.view?.update(appearance: .history)
                    }
            )
        )
    }
    
    private func showEmptyFaxInform() {
        view?.update(
            appearance: .inform(
                .emptyFaxes(onTap: { [weak self] in
                    self?.router?.openTab(.fax)
                })
            )
        )
    }
    
    // delete all items
    
    func deleteAll() {
        router?.openAlertOptions(model: .confirmClearAll(onOk: { [weak self] in
            guard let self = self else { return }
            
            self.analytics.send(AnalyticsEventImpl(name: "tab2_clear_all_clicked"))
            
            self.view?.update(appearance: .loading)
            self.interactor.deleteAllHistory().sinkResult { [weak self] result in
                guard let self = self else { return }
                switch result {
                case .success:
                    self.view?.tableSource.deleteAll(animated: true)
                    self.showEmptyFaxInform()
                case let .failure(error):
                    self.justShowErrorInform(error)
                }
            }.store(in: &self.cancelBag)
            
        }), animated: true)
    }
    
    
    // delete single item
    
    private func deleteItem(id: String, sentAt: Int) {
        let allCurrentItems = view?.tableSource.items(in: .first) ?? []
        let historyItems = self.historyItems
        view?.update(appearance: .loading)
        
        analytics.send(
            AnalyticsEventImpl(
                name: "history_fax_delete",
                parameters: [
                    "fax_sent_date": sentAt
                ]
            )
        )
        
        interactor.delete(id: id).sinkResult({ [weak self] result in
            guard let self = self else { return }
            switch result {
            case .success:
                if historyItems.count == 1 {
                    self.showEmptyFaxInform()
                    self.view?.tableSource.deleteAll(animated: false)
                } else {
                    self.view?.update(appearance: .history)
                }
            case let .failure(error):
                self.justShowErrorInform(error)
                self.view?.tableSource.reload(section: .first, with: allCurrentItems, animated: false)
            }
        }).store(in: &cancelBag)
    }
    
    private func justShowErrorInform(_ error: NetworkError) {
        view?.update(
            appearance: .inform(
                .networkError(
                    error,
                    buttonText: "OK",
                    onTap: { [weak self] in
                        self?.view?.update(appearance: .history)
                    })
            )
        )
    }
    
    private var historyItems: [FaxHistoryTableItemModel] {
        view?.tableSource.items(in: .first).filter { $0.historyCellModel != nil } ?? []
    }
    
    private func sendCountItems() {
        analytics.send(AnalyticsEventImpl(name: "tab2_screen_shown", parameters: ["quantity": historyItems.count]))
    }
}

extension FaxHistoryPresenterImpl: FaxHistoryPresenter {
    func viewDidLoad() {
        fetchHistory()
        
        view?.tableSource.willDeleteItem.sink(receiveValue: { [weak self] model in
            guard let model = model.historyCellModel else { return }
            self?.deleteItem(id: model.id, sentAt: model.sentTimeTimestamp)
        }).store(in: &cancelBag)
        
        faxSendedPublisher.sinkSuccess { [weak self] _ in
            self?.fetchHistory()
        }.store(in: &cancelBag)
    }
    
    func viewDidAppear() {
        sendCountItems()
    }
    
    func pullToRefreshTriggered() {
        fetchHistory(showLoading: false)
    }
    
    func clearAllTapped() {
        deleteAll()
    }
}
