//
//  PaywallPresenter.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import Foundation
import Adapty

protocol PaywallPresenter: ViewOutput {
    func didTapDismiss()
    func didTapRestorePurchases()
    func didTapContinueButton()
    func didTapPrivacyPolicy()
    func didTapTermsOfUse()
    func didTapViewAllPlan()
    func getModel() -> PaywallModel?
}

final class PaywallPresenterImpl {
    
    weak var view: PaywallViewControllerInterface?
    private weak var router: PaywallRouter?
    private let purchaseService: PurchasesService
    private let analytics: AnalyticsService
    private let type: PaywallType
    
    private var premiumItems: [PremiumItemModel] = []
    private var paywall: AdaptyPaywall?
    private var products: [AdaptyPaywallProduct] = []
    
    init(
        router: PaywallRouter,
        purchaseService: PurchasesService = PurchasesService.shared,
        analytics: AnalyticsService = AnalyticsServiceImpl.shared,
        type: PaywallType = .standart
    ) {
        self.router = router
        self.purchaseService = purchaseService
        self.analytics = analytics
        self.type = type
    }
    
    private var selectedPremium: PremiumItemModel? {
        didSet {
            guard selectedPremium?.premiumId != oldValue?.premiumId else { return }
            selectedPremium.flatMap {
                view?.updateSelected(item: $0)
            }
            view?.setupViewAccordingPremiumModel(cost: (selectedPremium?.subtitle ?? "") + "." + L10n.Paywall.Sixth.cancelAnytime)
        }
    }
    
    private func loadProducts() {
        view?.update(isLoading: true)
        
        purchaseService.getPaywallAndProducts { [weak self] result in
            guard let self else { return }
            
            switch result {
            case let .success((paywall, products)):
                self.view?.update(isLoading: false)
                self.paywall = paywall
                self.products = products
                self.products = self.products.sorted { $0.skProduct.subscriptionPeriod?.unit.rawValue ?? 0 < $1.skProduct.subscriptionPeriod?.unit.rawValue ?? 1 }
                self.setupViewWithPaywall()
            case .failure(_):
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.loadProducts()
                }
            }
        }
    }
    
    private func setupViewWithPaywall() {
        guard let paywall = paywall else {
            return
        }
        
        let customPyload = paywall.typedCustomPayload
        let variant = customPyload?.paywallVariant ?? .first
        
        switch type {
        case .sending:
            view?.setup(variation: .sixth)
        case .standart:
            view?.setup(variation: variant)
        }
        
        premiumItems = products.map {
            .from(paywall: paywall,
                  product: $0,
                  onTap: { [weak self] model in
                    self?.selectedPremium = model
                  })
        }
        
        Adapty.logShowPaywall(paywall)
        view?.setup(premiumItems: premiumItems)
        
        selectedPremium = premiumItems.first(where: {
            $0.premiumId == customPyload?.initialProductId
        }) ?? premiumItems.first
        
        if variant == .fifth {
            view?.useLightColors()
        }
    }
    
    private func makePurchase() {
        guard let selectedPremium = selectedPremium else {
            return
        }
        
        guard var product = products.first(where: {
            $0.vendorProductId == selectedPremium.premiumId
        }) else {
            return
        }
        
        if !(view?.allPlansIsShowed ?? true) {
            for element in products {
                if element.subscriptionPeriod?.unit == .month {
                    product = element
                    break
                }
            }
        }

        view?.update(isLoading: true)
        purchaseService.purchase(product: product) { [weak self] isPurchased in
            self?.view?.update(isLoading: false)
            if isPurchased {
                self?.showPurchaseSucceed()
                self?.sendPurchaseSuceeded(type: product.vendorProductId)
            } else {
                self?.showFailToPurchaseAlert()
                self?.sendPurchaseFailed()
            }
        }
    }
    
    private func showPurchaseSucceed() {
        self.view?.showInformView(.purchaseSucceed(onTap: { [weak self] in
            let router = self?.router
            self?.closeScreen(completion: { [router] in
                router?.openTab(.fax)
            })
        }))
    }
    
    private func showFailToPurchaseAlert() {
        router?.openAlertOptions(model: .failedToPurchaseProduct(onRetry: { [weak self] in
            self?.makePurchase()
        }), animated: true)
    }
    
    private func closeScreen(completion: VoidClosure? = nil) {
        router?.closeLast(completion: completion)
        router?.lastClosed()
    }
    
    private func sendPurchaseSuceeded(type: String) {
        analytics.send(AnalyticsEventImpl(name: "subscription_purchased", parameters: ["type": type]))
    }
    
    private func sendPurchaseFailed() {
        analytics.send(AnalyticsEventImpl(name: "subscription_canceled"))
    }
}

extension PaywallPresenterImpl: PaywallPresenter {
    func viewDidLoad() {
//        let variant: PaywallViewVariation = .first // TODO: - Remove
//
//        view?.setup(variation: variant)
//
//        premiumItems = (0..<3).map { id in
//            PremiumItemModel(
//                topTipModel: nil,
//                premiumId: "\(id)",
//                title: "title",
//                subtitle: "subtitle",
//                price: "price",
//                onTap: { _ in })
//        }
//
//        view?.setup(premiumItems: premiumItems)
//        view?.updateSelected(item: premiumItems[0])
//        if variant == .fifth {
//            view?.useLightColors()
//        }
        
        loadProducts()
    }
    
    func didTapRestorePurchases() {
        view?.update(isLoading: true)
        purchaseService.restorePurchases { [weak self] status in
            self?.view?.update(isLoading: false)
            if status {
                self?.router?.openAlertOptions(model: .purchasesRestored(onOk: {
                    self?.closeScreen()
                }), animated: true)
            } else {
                self?.router?.openAlertOptions(model: .failedToRestorePurchases(), animated: true)
            }
        }
        analytics.send(AnalyticsEventImpl(name: "paywall_restore_clicked"))
    }
    
    func didTapContinueButton() {
        makePurchase()
    }
    
    func didTapPrivacyPolicy() {
        router?.openPdf(name: "privacy_policy", title: L10n.Paywall.privacyPolicy)
    }
    
    func didTapTermsOfUse() {
        router?.openPdf(name: "terms_of_use", title: L10n.Paywall.termsOfUse)
    }
    
    func didTapDismiss() {
        closeScreen()
        analytics.send(AnalyticsEventImpl(name: "paywall_closed"))
    }
    
    func didTapViewAllPlan() {
        analytics.send(AnalyticsEventImpl(name: "view_all_plans"))
    }
    
    func getModel() -> PaywallModel? {
        switch type {
        case .sending(let model):
           return model
        case .standart:
            return nil
        }
    }
}
