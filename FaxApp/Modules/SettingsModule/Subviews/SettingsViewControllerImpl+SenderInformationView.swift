//
//  SettingsViewControllerImpl+SenderInformationView.swift
//  FaxApp
//
//  Created by Eugene on 26.05.2022.
//

import UIKit

extension SettingsViewControllerImpl {
    final class SenderInformationView: UIView, UIThemable {
        
        private let titleLabel = Label(text: L10n.Settings.sender,
                                       font: .poppins(12, .semiBold))
        let nameInput = Input(placeholder: .init(text: L10n.Settings.name, color: .baboo), bgColor: .flerio)
        let phoneInput = Input(placeholder: .init(text: L10n.Settings.phone, color: .baboo), bgColor: .flerio)
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func apply(theme: UITheme) {
            nameInput.backgroundColor = .themed.goeridio.value
            phoneInput.backgroundColor = .themed.goeridio.value
        }
        
        private func configureSelf() {
            addSubview(titleLabel)
            addSubview(nameInput)
            addSubview(phoneInput)
            
            nameInput.borderColor = .xero
            phoneInput.borderColor = .xero
            
            [nameInput, phoneInput].forEach {
                $0.addDoneToolBar(action: nil)
            }
            
            titleLabel.pinEdges(toSuperviewEdges: [.left, .top])
            
            nameInput.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 12)
            nameInput.pinEdges(toSuperviewEdges: [.left, .right])
            
            phoneInput.pinEdge(.top, to: .bottom, of: nameInput, withOffset: 12)
            phoneInput.pinEdges(toSuperviewEdges: [.left, .right])
            
            pinEdge(.top, to: .top, of: titleLabel)
            pinEdge(.bottom, to: .bottom, of: phoneInput)
            
            themeProvider.register(observer: self)
        }
    }
}
