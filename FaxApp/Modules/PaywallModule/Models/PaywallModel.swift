//
//  PaywallModel.swift
//  FaxApp
//
//  Created by 123 on 28.06.2023.
//

import Foundation

struct PaywallModel {
    let time = FrontPagePDFGeneratorImpl.generateDateText(withTimeZone: false)
    let pages: Int
    let number: String
}
