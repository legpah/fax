//
//  FrontPdfPageViewController.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit
import PDFKit

protocol FrontPdfPageViewController: AnyObject {
    func setMessageCharactersLeft(text: String)
    func apply(atttibutesModel: FrontPageAttributesModel)
}

final class FrontPdfPageViewControllerImpl: UIViewController {
    
    private let contentView = UIView()
    private let titleLabel = Label(text: L10n.FrontPage.title, font: .poppins(12, .semiBold))
    private let titleInput = Input(placeholder: .init(text: L10n.FrontPage.name), bgColor: .goeridio)
    private let recipeintLabel = Label(text: L10n.FrontPage.sender, font: .poppins(12, .semiBold))
    private let fromInput = Input(placeholder: .init(text: L10n.FrontPage.from), bgColor: .goeridio)
    private let contactInput = Input(placeholder: .init(text: L10n.FrontPage.contact), bgColor: .goeridio)
    private let messageLabel = Label(text: L10n.FrontPage.message, font: .poppins(12, .semiBold))
    private let charactersLeftLabel = Label(font: .poppins(12), color: .wibgok, alignment: .right)
    private let messageInput = MultilineInput(placeholder: .init(text: L10n.FrontPage.typeHere), bgColor: .goeridio)
    private let previewButton = TrunButton(title: L10n.FrontPage.showPreview,
                                           titleColor: .tukesh,
                                           backroundColor: .bw,
                                           borderColor: .tukesh)
    private let createButton = TrunButton(title: L10n.FrontPage.create)
    private let backButton = TextButton(text: L10n.Paywall.close)
    private lazy var _trackingInputs = KeyboardOutsideTouchesResignerView(controller: self)
    private lazy var responderVisibilityManager = ResponderVisibilityManager(
        viewsToTrack: [messageInput],
        relativeToView: contentView,
        movingType: .bounds
    )
    
    private let presenter: FrontPdfPagePresenter
    
    init(presenter: FrontPdfPagePresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        responderVisibilityManager.start()
        presenter.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        apply(theme: .current)
    }
    
    private func configureUI() {
        title = L10n.FrontPage.frontPage
        navigationItem.setHidesBackButton(true, animated: true)
        
        view.addSubview(contentView)
        contentView.addSubview(titleLabel)
        contentView.addSubview(titleInput)
        contentView.addSubview(recipeintLabel)
        contentView.addSubview(fromInput)
        contentView.addSubview(contactInput)
        contentView.addSubview(messageLabel)
        contentView.addSubview(charactersLeftLabel)
        contentView.addSubview(messageInput)
        contentView.addSubview(previewButton)
        contentView.addSubview(createButton)
        navigationItem.setLeftBarButtonItems([UIBarButtonItem(customView: backButton)], animated: true)
        
        contentView.clipsToBounds = true
        contentView.layer.masksToBounds = true
        
        let inputs = [titleInput, fromInput, contactInput]
        _trackingInputs.trackKeyboard(of: inputs + [messageInput])
        inputs.forEach { $0.addDoneToolBar(action: nil) }
        messageInput.addDoneToolBar(action: nil)
        
        titleInput.maxCharactersCount = 28
        fromInput.maxCharactersCount = 50
        contactInput.maxCharactersCount = 50
        messageInput.maxCharactersCount = presenter.messageMaxCharactersCount
        
        titleInput.onTextChange = { [weak self] change in
            self?.presenter.didChangeTitle(change.text)
        }
        fromInput.onTextChange = { [weak self] change in
            self?.presenter.didChangeFrom(change.text)
        }
        contactInput.onTextChange = { [weak self] change in
            self?.presenter.didChangeContact(change.text)
        }
        messageInput.onTextChange = { [weak self] change in
            self?.presenter.didChangeMessage(change.text)
        }
        previewButton.onTap = { [weak self] in
            self?.presenter.didTapShowPreview()
        }
        createButton.onTap = { [weak self] in
            self?.presenter.didTapCreate()
        }
        backButton.onTap = { [weak self] in
            self?.navigationController?.popViewController(animated: true)
        }
        
        configureConstraints()
        
        themeProvider.register(observer: self)
        
    }
    
    private func configureConstraints() {
        contentView.pin(toTopLayoutGuideOf: self)
        contentView.pinEdges(toSuperviewEdges: [.left, .right])
        contentView.pin(toBottomLayoutGuideOf: self)
        
        titleLabel.pinEdge(toSuperviewEdge: .top, withInset: 24)
        titleLabel.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        
        titleInput.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 12)
        titleInput.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        
        recipeintLabel.pinEdge(.top, to: .bottom, of: titleInput, withOffset: 24)
        recipeintLabel.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        
        fromInput.pinEdge(.top, to: .bottom, of: recipeintLabel, withOffset: 12)
        fromInput.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        fromInput.setDimension(.height, toSize: 40)
        
        contactInput.pinEdge(.top, to: .bottom, of: fromInput, withOffset: 12)
        contactInput.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        contactInput.setDimension(.height, toSize: 40)
        
        messageLabel.pinEdge(.top, to: .bottom, of: contactInput, withOffset: 12)
        messageLabel.pinEdges(toSuperviewEdges: [.left], withInset: .layout.xInset)
        
        charactersLeftLabel.alignAxis(.y, toSameAxisOf: messageLabel)
        charactersLeftLabel.pinEdges(toSuperviewEdges: [.right], withInset: .layout.xInset)
        
        messageInput.pinEdge(.top, to: .bottom, of: charactersLeftLabel, withOffset: 12)
        messageInput.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        messageInput.setDimension(.height, toSize: 145, relation: .greaterThanOrEqual)
        
        previewButton.pinEdge(.top, to: .bottom, of: messageInput, withOffset: 12)
        previewButton.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        
        createButton.pinEdge(toSuperviewEdge: .bottom, withInset: 16)
        createButton.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
    }
    
    override func didMove(toParent parent: UIViewController?) {
        super.didMove(toParent: parent)

        if parent == nil {
            presenter.didTapBackButton()
        }
    }
    
}

extension FrontPdfPageViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        view.backgroundColor = .themed.flerio.value
        applyNavigationBarColor(.flerio)
    }
}

extension FrontPdfPageViewControllerImpl: FrontPdfPageViewController {
    func setMessageCharactersLeft(text: String) {
        charactersLeftLabel.text = text
    }
    
    func apply(atttibutesModel: FrontPageAttributesModel) {
        titleInput.text = atttibutesModel.title
        fromInput.text = atttibutesModel.from
        contactInput.text = atttibutesModel.contact
        messageInput.setText(atttibutesModel.message)
    }
}
