//
//  SettingsTabOpenRouterResolver.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import Foundation

extension CanOpenRouterResolverToken {
    static var settingsTabResolver: CanOpenScreenRouterResolver {
        SettingsTabResolver()
    }
}

fileprivate struct SettingsTabResolver: CanOpenScreenRouterResolver {
    func canOpen(router: ScreenRouter, from: ScreenRouter) -> Bool {
        switch RouterType(indentifiable: router.routerIdentifiable) {
        case .rootTab,
                .sendingFax,
                .onboarding,
                .contactsPicker,
                .frontPdfPage,
                .singleImagePicker,
                .documentPicker:
            return false
        case .alertOptions,
                .pdfPreview,
                .countryPhonesList,
                .paywall,
                .sendMail,
                .faq:
            return true
        }
    }
}
