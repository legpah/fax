//
//  FaxHistoryViewController+BottomButtonsBlockView.swift
//  FaxApp
//
//  Created by Eugene on 21.04.2022.
//

import UIKit

extension FaxMainPageViewControllerImpl {
    final class BottomButtonsBlockView: UIView {
        
        var onAddFileTap: VoidClosure?
        var onSendTap: VoidClosure?
        
        var isSendFileButtonActive: Bool {
            get { sendButton.isActive }
            set { sendButton.isActive = newValue }
        }
        
        private let addFileButton: TrunButton = {
            let font: UIFont = Locale.current.isDeutsch ? .poppins(14, .semiBold) : .poppins(16, .semiBold)
            let button = TrunButton(title: L10n.Main.addFile,
                                    titleColor: .tukesh,
                                    backroundColor: .bw,
                                    borderColor: .tukesh,
                                    titleInactiveColor: .gr23,
                                    backroundInactiveThemedColor: .bw,
                                    borderInactiveColor: .voiero,
                                    font: font,
                                    iconModel: TrunButton.IconModel(
                                        position: .left,
                                        offset: 8,
                                        sizelayout: .fixed(size: .symmetric(15)),
                                        image: .plusIcon
                                    ))
            return button
        }()
        
        private let sendButton: TrunButton = {
            let font: UIFont = Locale.current.isDeutsch ? .poppins(14, .semiBold) : .poppins(16, .semiBold)
            let button = TrunButton(
                title: L10n.Main.send,
                titleColor: .flerio,
                backroundColor: .tukesh,
                titleInactiveColor: .gr23,
                backroundInactiveThemedColor: .amber,
                borderInactiveColor: nil,
                font: font
            )
            return button
        }()
        
        private var storedConstrains: [NSLayoutConstraint] = []
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func toggleSendButton(isVisible: Bool, animated: Bool) {
            NSLayoutConstraint.deactivate(storedConstrains)
            if isVisible {
                storedConstrains += [addFileButton.match(.width, to: .width, of: self, withMultiplier: 0.47)]
                storedConstrains += [sendButton.match(.width, to: .width, of: self, withMultiplier: 0.47)]
            } else {
                storedConstrains += [addFileButton.pinEdge(toSuperviewEdge: .right)]
                storedConstrains += [sendButton.setDimension(.width, toSize: 0)]
            }
            
            UIView.animate(withDuration: animated ?.animations.defaultDuration : 0) {
                self.layoutIfNeeded()
                self.sendButton.alpha = isVisible ? 1 : 0
            }
        }
        
        private func configureSelf() {
            addSubview(addFileButton)
            addSubview(sendButton)
            addFileButton.onTap = { [weak self] in
                self?.onAddFileTap?()
            }
            sendButton.onTap = { [weak self] in
                self?.onSendTap?()
            }
            
            sendButton.bordersWidth = 0
            
            self.setDimension(.height, toSize: 48)
            addFileButton.pinEdges(toSuperviewEdges: [.left, .top, .bottom])
            sendButton.pinEdges(toSuperviewEdges: [.right, .top, .bottom])
        }
    }
}
