//
//  NavigationController+Extensions.swift
//  FaxApp
//
//  Created by Eugene on 1.06.2022.
//

import UIKit

extension UINavigationController {
    func applyBarColor(_ color: UIThemed<UIColor>) {
        navigationBar.backgroundColor = color.value
        navigationBar.barTintColor = color.value
    }
    
    func removeBottomBorder() {
        navigationBar.setBackgroundImage(UIImage(), for:.default)
        navigationBar.shadowImage = UIImage()
        navigationBar.layoutIfNeeded()
    }
    
    func restoreBottomBorder() {
        navigationBar.setBackgroundImage(nil, for:.default)
        navigationBar.shadowImage = nil
        navigationBar.layoutIfNeeded()
    }
}

extension UIViewController {
    func applyNavigationBarColor(_ color: UIThemed<UIColor>) {
        navigationController?.navigationBar.backgroundColor = color.value
        navigationController?.navigationBar.barTintColor = color.value
    }
    
    func removeNavigationBottomBorder() {
        self.navigationController?.removeBottomBorder()
    }
    
    func restoreNavigationBottomBorder() {
        self.navigationController?.restoreBottomBorder()
    }
}
