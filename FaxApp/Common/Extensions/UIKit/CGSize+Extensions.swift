//
//  CGSize+Extensions.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit

extension CGSize {
    static func symmetric(_ value: CGFloat) -> CGSize {
        CGSize(width: value, height: value)
    }
}
