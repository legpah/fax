//
//  SendMailRouter.swift
//  FaxApp
//
//  Created by Eugene on 24.05.2022.
//

import MessageUI

final class SendMailRouter: BaseScreenRouter, ScreenRouter {
    
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.sendMail
    let presentationType: ScreenPresentationType = .modal
    
    private lazy var delegate: Delegate = {
        let delegate = Delegate()
        delegate.router = self
        return delegate
    }()
    
    private lazy var _screen: UIViewController = {
        let view = MFMailComposeViewController()
        view.setToRecipients(["suppfax@gmail.com"])
        view.mailComposeDelegate = delegate
        view.overrideUserInterfaceStyle = UITheme.current.asSystemTheme
        return view
    }()
}

private final class Delegate: NSObject, MFMailComposeViewControllerDelegate {
    
    weak var router: CloseRouterCoordinator?
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        router?.closeLast()
        router?.lastClosed()
    }
}
