//
//  UpdateNotificationTokenEndpoint.swift
//  FaxApp
//
//  Created by Eugene on 30.05.2022.
//

import Foundation

struct UpdateNotificationTokenEndpoint: NetworkRequest {
    typealias ReturnType = EmptyResponse
    
    let path: String = "/user/add-notification-token"
    let method: HTTPMethod = .post
    let body: [String : Any]?
    
    init(token: String, deviceId: String) {
        body = [
            "notificationToken": token,
            "deviceCode": deviceId
        ]
    }
}
