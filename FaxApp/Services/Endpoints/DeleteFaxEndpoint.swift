//
//  DeleteFaxEndpoint.swift
//  FaxApp
//
//  Created by Eugene on 24.05.2022.
//

import Foundation

enum DeleteFaxEndpoint: NetworkRequest {
    case delete(id: String)
    case deleteAll
    
    typealias ReturnType = EmptyResponse
    
    var path: String {
        switch self {
        case .delete(let id):
            return "/fax/\(id)"
        case .deleteAll:
            return "/fax"
        }
    }
    
    var method: HTTPMethod { .delete }
}
