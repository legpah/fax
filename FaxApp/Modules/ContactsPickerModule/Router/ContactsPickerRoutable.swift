//
//  ContactsPickerRoutable.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import Foundation

protocol ContactsPickerRoutable: AnyObject {
    func openContactsPicker(output: ContactsPickerModuleOuput?)
}

extension ContactsPickerRoutable where Self: ScreenRouter  {
    func openContactsPicker(output: ContactsPickerModuleOuput?) {
        open(router: ContactsPickerRouterImpl(output: output))
    }
}
