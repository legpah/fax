//
//  SendFaxAccessServiceImpl.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation
import Combine

final class SendFaxAccessServiceImpl {
    private let api = ApiClient()
}

extension SendFaxAccessServiceImpl: SendFaxAccessService {
    func sendFaxAccess(pagesCount: Int) -> AnyPublisher<EmptyResponse, NetworkError> {
        let endpoint = SendFaxAccessEndpoint(pagesCount: pagesCount)
        return api.request(request: endpoint)
    }
}
