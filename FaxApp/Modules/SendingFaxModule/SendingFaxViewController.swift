//
//  SendingFaxViewController.swift
//  FaxApp
//
//  Created by Eugene on 20.05.2022.
//

import UIKit

enum SendingFaxViewControllerState {
    case preview
    case infromView(GenericInformViewModel, InformType)
    case sending
    
    enum InformType {
        case done
        case generic
        case subscriptionExpired
    }
}

protocol SendingFaxViewController: AnyObject {
    func updateControllerTitle(_ text: String)
    func update(state: SendingFaxViewControllerState)
}

final class SendingFaxViewControllerImpl: UIViewController {
    
    private let presenter: SendingFaxPresenter
    
    private let backBarButton = TextBarButton(text: L10n.Sending.back, position: .right)
    private let titleViewController = Label(font: .poppins(24, .semiBold), alignment: .center)
    private let recipientLabel = Label(text: L10n.Sending.recipient,
                                       font: .poppins(12, .semiBold))
    private let recipientText = TextContainerView()
    
    private let pagesLabel = Label(text: L10n.Sending.pages,
                                   font: .poppins(12, .semiBold))
    private let pagesText = TextContainerView()
    private let previewButton = Button(title: L10n.Sending.preview,
                                       titleColor: .tukesh,
                                       bgColor: nil,
                                       font: .poppins(12, .semiBold))
    private let deliveryTimeLabel = Label(text: L10n.Sending.deliveryTime,
                                          font: .poppins(12, .semiBold))
    private let deliveryTimeText = TextContainerView()
    private let deliveryTipLabel = Label(font: .poppins(10),
                                         color: .ibw,
                                         lines: 0)
    private let informView = GenericInformView()
    
    private let sendButton = TrunButton(
        title: L10n.Sending.sendNow,
        titleColor: .flerio,
        backroundColor: .tukesh
    )
    private let loadingSnipper = ActivityIndicator(color: .bw)
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return themeProvider.current.asStatusBarStyle
    }
    
    init(presenter: SendingFaxPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSelf()
        presenter.viewDidLoad()
    }
    
    private func configureSelf() {
        view.addSubview(backBarButton)
        view.addSubview(titleViewController)
        view.addSubview(recipientLabel)
        view.addSubview(recipientText)
        view.addSubview(pagesLabel)
        view.addSubview(pagesText)
        pagesText.addSubview(previewButton)
        view.addSubview(deliveryTimeLabel)
        view.addSubview(deliveryTimeText)
        view.addSubview(deliveryTipLabel)
        view.addSubview(sendButton)
        sendButton.addSubview(loadingSnipper)
        view.addSubview(informView)
        informView.alpha = 0
        
        titleViewController.adjustsFontSizeToFitWidth = true
        titleViewController.minimumScaleFactor = 0.7
        recipientText.label.text = presenter.recipientPhone
        pagesText.label.text = presenter.pagesNumber
        deliveryTimeText.label.text = presenter.deliveryTime
        deliveryTipLabel.alpha = 0.3
        deliveryTipLabel.text = L10n.Sending.deliveryTip(presenter.minPerPage)
        
        backBarButton.onTap = { [weak self] in
            self?.presenter.backButtonTapped()
        }
        previewButton.onTap = { [weak self] in
            self?.presenter.previewButtonTapped()
        }
        sendButton.onTap = { [weak self] in
            self?.presenter.sendButtonTapped()
        }
        informView.backButtonTap = { [weak self] in
            self?.update(state: .preview)
        }
        
        configureConstraints()
        
        themeProvider.register(observer: self)
        
    }
    
    private func configureConstraints() {
        backBarButton.pin(toTopLayoutGuideOf: self, withInset: 17)
        backBarButton.pinEdge(toSuperviewEdge: .left, withInset: .layout.xInset)
        backBarButton.setDimension(.width, toSize: 65)
        
        titleViewController.alignAxis(toSuperviewAxis: .x)
        titleViewController.alignAxis(.y, toSameAxisOf: backBarButton)
        titleViewController.pinEdge(.left, to: .right, of: backBarButton, withOffset: 10)
        
        recipientLabel.pinEdge(.top, to: .bottom, of: backBarButton, withOffset: 23)
        recipientText.pinEdge(.top, to: .bottom, of: recipientLabel, withOffset: 12)
        pagesLabel.pinEdge(.top, to: .bottom, of: recipientText, withOffset: 24)
        pagesText.pinEdge(.top, to: .bottom, of: pagesLabel, withOffset: 12)
        deliveryTimeLabel.pinEdge(.top, to: .bottom, of: pagesText, withOffset: 24)
        deliveryTimeText.pinEdge(.top, to: .bottom, of: deliveryTimeLabel, withOffset: 12)
        deliveryTipLabel.pinEdge(.top, to: .bottom, of: deliveryTimeText, withOffset: 8)
        
        previewButton.pinEdge(toSuperviewEdge: .right, withInset: 11)
        previewButton.alignAxis(toSuperviewAxis: .y)
        
        loadingSnipper.centerInSuperview()
        
        sendButton.pinEdge(toSuperviewEdge: .bottom, withInset: 48)
        
        [
            recipientLabel, recipientText, pagesLabel,
            pagesText, deliveryTimeLabel, deliveryTimeText,
            deliveryTipLabel, sendButton
        ].forEach {
            $0.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        }
        
        informView.pin(toTopLayoutGuideOf: self)
        informView.pinEdges(toSuperviewEdges: [.left, .right, .bottom])
    }
}

extension SendingFaxViewControllerImpl: UIThemable {
    func apply(theme: UITheme) {
        view.backgroundColor = .themed.flerio.value
        applyNavigationBarColor(.flerio)
    }
}

extension SendingFaxViewControllerImpl: SendingFaxViewController {
    func updateControllerTitle(_ text: String) {
        titleViewController.text = text
    }
    
    func update(state: SendingFaxViewControllerState) {
        func toggleInform(alpha: CGFloat) {
            UIView.animate(withDuration: .animations.defaultDuration) { [weak self] in
                self?.informView.alpha = alpha
            }
        }
        
        let responders = [self.view, backBarButton]
        switch state {
        case .preview:
            titleViewController.text = L10n.Sending.readyToSend
            responders.forEach { $0?.isUserInteractionEnabled = true }
            loadingSnipper.isAnimating = false
            sendButton.title = L10n.Sending.sendNow
            toggleInform(alpha: 0)
            
        case .sending:
            titleViewController.text = L10n.Sending.readyToSend
            responders.forEach { $0?.isUserInteractionEnabled = false }
            loadingSnipper.isAnimating = true
            sendButton.title = ""
            toggleInform(alpha: 0)
            
        case let .infromView(model, informType):
            responders.forEach { $0?.isUserInteractionEnabled = true }
            sendButton.title = ""
            loadingSnipper.isAnimating = false
            toggleInform(alpha: 1)
            switch informType {
            case .done:
                informView.hideNavigation(buttonIsHidden: true, titleIsHidden: false)
                titleViewController.text = L10n.Sending.done
                informView.setupTitle(text: L10n.Sending.done)
                informView.updateView(model: model)
            case .generic, .subscriptionExpired:
                informView.hideNavigation(buttonIsHidden: false, titleIsHidden: false)
                titleViewController.text = ""
                informView.setupTitle(text: "")
                informView.updateView(model: model.extendTap(action: { [weak self] in
                    self?.titleViewController.text = L10n.Sending.readyToSend
                }))
            }
        }
    }
}

extension SendingFaxViewControllerImpl {
    private final class TextContainerView: UIView, UIThemable {
        let label: Label
        
        init() {
            self.label = Label(text: "", font: .poppins(12, .light), color: .gundeshtalt)
            super.init(frame: .zero)
            setDimension(.height, toSize: 40)
            applyBordersShape(width: 1, radius: 8)
            addSubview(label)
            label.alignAxis(toSuperviewAxis: .y)
            label.pinEdge(toSuperviewEdge: .left, withInset: 12)
            themeProvider.register(observer: self)
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        func apply(theme: UITheme) {
            backgroundColor = .themed.tikruk.value
            applyBorders(color: .xero)
        }
    }
}
