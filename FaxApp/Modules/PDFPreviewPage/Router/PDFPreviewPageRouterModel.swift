//
//  PDFPreviewPageRouterModel.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import Foundation

struct PDFPreviewPageRouterModel {
    let viewTitle: String
    let pdfObject: PDFObject
    let canEditFrontPage: Bool
    let onDocumentEdited: IClosure<PDFObject>?
    
    init(viewTitle: String = L10n.Settings.preview,
         pdfObject: PDFObject,
         canEditFrontPage: Bool = false,
         onDocumentEdited: IClosure<PDFObject>? = nil) {
        self.viewTitle = viewTitle
        self.pdfObject = pdfObject
        self.canEditFrontPage = canEditFrontPage
        self.onDocumentEdited = onDocumentEdited
    }
}

extension PDFPreviewPageRouterModel {
    var isFontPageEditable: Bool {
        switch pdfObject.unerlyingSource {
        case .frontPage:
            return canEditFrontPage
        default:
            return false
        }
    }
}
