//
//  CountableDispatchGroup.swift
//  FaxApp
//
//  Created by Eugene on 26.04.2022.
//

import Foundation

final class CountableDispatchGroup {
    
    // MARK: - Private properties
    
    private var tasksCount = 0
    private let group = DispatchGroup()
    
    // MARK: - Internal properties
    
    func enter() {
        tasksCount += 1
        group.enter()
    }
    
    func leave() {
        guard tasksCount > 0 else {
            assertionFailureDebug("might be a bug")
            return
        }
        tasksCount -= 1
        group.leave()
    }
    
    func notify(queue: DispatchQueue = .main, execute: @escaping() -> Void) {
        group.notify(queue: queue, execute: execute)
    }
    
    func leaveAll() {
        (0..<tasksCount).forEach { _ in
            group.leave()
        }
        tasksCount = 0
    }
}
