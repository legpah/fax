//
//  String+HelperFunctions.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit

extension String {
    /// Возвращает строку, состояющую из строк `string`, соединенных с помощью разделителя `separator`.
    /// - Parameters:
    ///   - strings: Строки, которые необходимо соединить.
    ///   - separator: Разделитель строк. По умолчанию  (пробел).
    static func joining(_ strings: String?..., separator: String = " ") -> String {
        return strings
            .compactMap { $0 }
            .filter { !$0.isEmpty }
            .joined(separator: separator)
    }
    
    func onlyDidgits() -> String {
        self.replacingOccurrences(
            of: "\\D",
            with: "",
            options: .regularExpression
        )
    }
    
    func replaceDidgits(_ str: String) -> String {
        replacingOccurrences(
            of: "[0-9]",
            with: str,
            options: .regularExpression
        )
    }
    
    func localized(comment: String = "") -> String {
         NSLocalizedString(self, comment: comment)
    }
}


extension String {
    func height(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)
    
        return ceil(boundingBox.height)
    }

    func width(height: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [.font: font], context: nil)

        return ceil(boundingBox.width)
    }
}
