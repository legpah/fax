//
//  ContryPhonesStorage.swift
//  FaxApp
//
//  Created by Eugene on 27.04.2022.
//

import Foundation

protocol ContryPhonesStorageSetter {
    func setPhones(_ phones: [CountryPhone])
}

protocol ContryPhonesProvider {
    var phones: [CountryPhone] { get }
}

final class ContryPhonesStorage: ContryPhonesStorageSetter, ContryPhonesProvider {
    
    static let shared = ContryPhonesStorage()
    
    private init() {}
    
    private(set) var phones: [CountryPhone] = []
    
    func setPhones(_ phones: [CountryPhone]) {
        self.phones = phones
    }
}
