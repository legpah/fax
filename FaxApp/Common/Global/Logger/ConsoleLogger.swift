//
//  ConsoleLogger.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import Foundation

struct ConsoleLogger: Logger {
    func log(_ text: @autoclosure () -> String) {
        #if DEBUG
        print(text())
        #endif
    }
}
