//
//  SettingsOpenerRouter.swift
//  FaxApp
//
//  Created by 123 on 16.06.2023.
//

import Foundation
import UIKit

protocol SettingsOpenerRouter {
    func openSetings()
}

extension SettingsOpenerRouter {
    func openSetings() {
        if let appSettings = URL(string: UIApplication.openSettingsURLString), UIApplication.shared.canOpen(appSettings) {
            UIApplication.shared.open(appSettings)
        }
    }
}
