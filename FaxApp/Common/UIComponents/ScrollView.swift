//
//  ScrollView.swift
//  FaxApp
//
//  Created by Eugene on 13.05.2022.
//

import UIKit

/// Скролл с вью контента.
/// [Stack overflow question](https://stackoverflow.com/questions/19771205/uiscrollview-not-scrolling)
/// [Medium example](https://medium.com/@pradeep_chauhan/how-to-configure-a-uiscrollview-with-auto-layout-in-interface-builder-218dcb4022d7)
///
///
/// Для вертикального скролла первую сабвью необходимо прибить к верхнему краю `contentView` и последнюю сабвью к нижнему краю `contentView`
/// Для горизонтального скролла первую сабвью необходимо прибить к левому краю `contentView` и последнюю сабвью к правому краю `contentView`
final class ScrollView: UIScrollView {
    
    enum Direction {
        case vertical
        case horizontal
    }
    
    // MARK: - Internal properties
    
    let contentView = UIView()
    
    // MARK: - Init
    
    /// - Parameter direction: Направление скролла. От параметра зависит в какую сторону будет растягиваться `contentSize`, исходя из констрейнтов
    init(direction: Direction) {
        super.init(frame: .zero)
        self.showsVerticalScrollIndicator = false
        self.showsHorizontalScrollIndicator = false
        self.translatesAutoresizingMaskIntoConstraints = false
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        super.addSubview(self.contentView)
        self.configureConstraints(direction: direction)
    }
    
    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Overriden methods
    
    override func addSubview(_ view: UIView) {
        self.contentView.addSubview(view)
    }
    
    // MARK: - Private methods
    
    private func configureConstraints(direction: Direction) {
        let mainConstraints = [
            self.contentView.leftAnchor.constraint(equalTo: self.leftAnchor),
            self.contentView.topAnchor.constraint(equalTo: self.topAnchor),
            self.contentView.rightAnchor.constraint(equalTo: self.rightAnchor),
            self.contentView.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ]
        
        let directionConstraint: [NSLayoutConstraint] = {
            switch direction {
            case .vertical:
                return [self.contentView.widthAnchor.constraint(equalTo: self.widthAnchor)]
            case .horizontal:
                return [self.contentView.heightAnchor.constraint(equalTo: self.heightAnchor)]
            }
        }()
        
        NSLayoutConstraint.activate(mainConstraints + directionConstraint)
    }
}
