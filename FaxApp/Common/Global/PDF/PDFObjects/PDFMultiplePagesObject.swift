//
//  PDFMultiplePagesObject.swift
//  FaxApp
//
//  Created by Eugene on 26.04.2022.
//

import UIKit
import PDFKit
import Combine

final class PDFMultiplePagesObject: PDFObject {
    let unerlyingSource: PDFSource
    let pdfDocument: PDFDocument?
    let documentsCount: Int
    let hasFrontPage: Bool
    let originalFileTypes: [String]
    private(set) lazy var pdfData: Data? = pdfDocument?.dataRepresentation()
    var pdfPreview: AnyPublisher<UIImage?, Never> {
        imageSubject.eraseToAnyPublisher()
    }
    
    private let imageSubject = CurrentValueSubject<UIImage?, Never>(nil)
    
    init(sources: [PDFSource]) {
        let pdfs = sources.compactMap {
            $0.pdfObject().pdfData
        }
        originalFileTypes = sources.map { $0.pdfObject().unerlyingSource.analyticsDocumentType }
        hasFrontPage = sources.first(where: { $0.isFrontPage }) != nil
        self.documentsCount = sources.count
        let data = combinePdfs(pdfs) ?? Data()
        self.unerlyingSource = .pdf(data)
        self.pdfDocument = PDFDocument(data: data)
        generatePdfPreview { [weak self] image in
            self?.imageSubject.send(image)
        }
    }
}

fileprivate func combinePdfs(_ pdfs: [Data]) -> Data? {
    let out = NSMutableData()
    UIGraphicsBeginPDFContextToData(out, .zero, nil)

    guard let context = UIGraphicsGetCurrentContext() else {
        return nil
    }
    
    for pdf in pdfs {
        guard let dataProvider = CGDataProvider(data: pdf as CFData),
              let document = CGPDFDocument(dataProvider)
        else {
            continue
        }

        for pageNumber in 1...document.numberOfPages {
            guard let page = document.page(at: pageNumber) else { continue }
            var mediaBox = page.getBoxRect(.mediaBox)
            context.beginPage(mediaBox: &mediaBox)
            context.drawPDFPage(page)
            context.endPage()
        }
    }
    
    context.closePDF()
    UIGraphicsEndPDFContext()
    
    return out as Data
}
