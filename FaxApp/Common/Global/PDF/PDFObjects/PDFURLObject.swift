//
//  PDFURLObject.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import Combine
import Foundation

//final class PDFURLObject: PDFObject {
//
//    private(set) lazy var unerlyingSource: PDFSource = .url(url)
//
//    private var _data: Data?
//    private let url: URL?
//    private var isLoading: Bool = false
//
//    private var completions: [IClosure<Data?>] = []
//    private var cancellable: AnyCancellable?
//
//    init(url: URL?) {
//        self.url = url
//    }
//
//    func rawData(completion: @escaping IClosure<Data?>) {
//        if let _data = _data {
//            completion(_data)
//            return
//        }
//
//        guard let pdfUrl = url else {
//            completion(nil)
//            return
//        }
//
//        guard !isLoading else {
//            completions.append(completion)
//            return
//        }
//
//        isLoading = true
//        completions.append(completion)
//
//        cancellable = URLSession
//            .shared
//            .dataTaskPublisher(for: pdfUrl)
//            .sink(receiveCompletion: { _ in
//            }, receiveValue: { [weak self] value in
//                guard let self = self else { return }
//                self._data = value.data
//                self.isLoading = false
//                self.completions.forEach {
//                    $0(value.data)
//                }
//            })
//    }
//
//    func pdfView(completion: @escaping IClosure<PDFUIView>) {
//        rawData { data in
//            completion(
//                PDFUIView(data: data ?? Data())
//            )
//        }
//    }
//}
