//
//  FaxMainPageViewState.swift
//  FaxApp
//
//  Created by Eugene on 22.04.2022.
//

import Foundation

enum FaxMainPageViewState {
    case emptyDocuments
    case haveDocuments
}
