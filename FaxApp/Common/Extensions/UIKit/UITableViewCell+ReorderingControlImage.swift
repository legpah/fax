//
//  UITableViewCell+ReorderingControlImage.swift
//  FaxApp
//
//  Created by Eugene on 26.04.2022.
//

import UIKit

extension UITableViewCell {
    var reorderControlImageView: UIImageView? {
        let reorderControl = self.subviews.first { view -> Bool in
            view.classForCoder.description() == "UITableViewCellReorderControl"
        }
        return reorderControl?.subviews.first { view -> Bool in
            view is UIImageView
        } as? UIImageView
    }
    
    func applyReorderingImageControl(color: UIColor) {
        guard let reorderControlImageView = reorderControlImageView else {
            return
        }
        let image = reorderControlImageView.image?.withTintColor(color)
        reorderControlImageView.image = image
    }
}
