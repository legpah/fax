//
//  PaywallFirstVariantView.swift
//  FaxApp
//
//  Created by Eugene on 14.07.2022.
//

import Foundation
import UIKit

final class PaywallFirstVariantView: UIView, PaywallVariantView {
    var itemsContainer: PremiumItemsContainer {
        premiumItemsView
    }
    
    private let imageView = ImageView(themedImage: .paywallImage)
    private let premiumLabel = Label(text: L10n.Paywall.First.premium,
                                     font: .poppins(24, .semiBold),
                                     alignment: .center)
    private let subtitleLabel = Label(text: L10n.Paywall.First.withoutRestrictions,
                                      font: .poppins(16, .light),
                                      color: .gr9gr9,
                                      alignment: .center)
    private let continueButton = TrunButton(title: L10n.Paywall.continue,
                                            titleColor: .bw,
                                            backroundColor: .tukesh,
                                            borderColor: nil)
    private let premiumItemsView = PremiumItemsContainerImpl()
    
    private let isSmall: Bool
    
    init(isSmall: Bool) {
        self.isSmall = isSmall
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSelf() {
        addSubview(imageView)
        addSubview(premiumLabel)
        addSubview(subtitleLabel)
        addSubview(premiumItemsView)
        
        configureConstraints()
    }
    
    private func configureConstraints() {
        imageView.pinEdge(toSuperviewEdge: .top, withInset: isSmall ? 5 : 15)
        imageView.match(.height, to: .height, of: self, withMultiplier: isSmall ? 0.43 : 0.47)
        imageView.match(.width, to: .height, of: imageView)
        imageView.alignAxis(toSuperviewAxis: .x)
        
        premiumLabel.pinEdge(.top, to: .bottom, of: imageView, withOffset: isSmall ? 15 : 20)
        premiumLabel.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        
        subtitleLabel.pinEdge(.top, to: .bottom, of: premiumLabel)
        subtitleLabel.pinEdges(toSuperviewEdges: [.left, .right], withInset: .layout.xInset)
        
        premiumItemsView.pinEdge(.top, to: .bottom, of: subtitleLabel, withOffset: isSmall ? 30 : 43)
        premiumItemsView.pinEdges(toSuperviewEdges: [.left, .right])
        premiumItemsView.pinEdge(toSuperviewEdge: .bottom, withInset: 10, relation: .greaterThanOrEqual)
    }
}
