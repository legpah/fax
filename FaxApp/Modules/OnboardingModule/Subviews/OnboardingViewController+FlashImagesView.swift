//
//  OnboardingViewController+FlashImages.swift
//  FaxApp
//
//  Created by Eugene on 15.06.2022.
//

import UIKit

extension OnboardingViewControllerImpl {
    final class FlashImagesView: UIView {
        
        private let images: [UIImageView] = [
            UIImageView(image: Asset.onboarding1.image),
            UIImageView(image: Asset.onboarding2.image),
            UIImageView(image: Asset.onboarding3.image),
            UIImageView(image: Asset.onboarding4.image),
            UIImageView(image: Asset.onboarding5.image),
        ]
        
        private let edgeAlpha: CGFloat = 0.5
        private let middleAlpha: CGFloat = 1.0
        
        private var index = 0
        private var leftFrame: CGRect = .zero
        private var middleFrame: CGRect = .zero
        private var rightFrame: CGRect = .zero
        
        init() {
            super.init(frame: .zero)
            images.forEach {
                $0.frame = .zero
                addSubview($0)
            }
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        override func layoutSubviews() {
            super.layoutSubviews()
            guard frame != .zero, leftFrame == .zero else { return }
            
            let dsWidth: CGFloat = 445
            let dsHeight: CGFloat = 900
            let middleWidth = frame.width * 0.67
            let middleHeight = min(dsHeight * (middleWidth / dsWidth), frame.height)
            let sideWidth = middleWidth * 0.8
            let sideHeight = middleHeight * 0.8
            let sideY = (middleHeight - sideHeight) / 2
            
            leftFrame = CGRect(x: 0,
                               y: sideY,
                               width: sideWidth,
                               height: sideHeight)
            middleFrame = CGRect(x: (frame.width - middleWidth) / 2,
                                 y: 0,
                                 width: middleWidth,
                                 height: middleHeight)
            rightFrame = CGRect(x: frame.width - sideWidth,
                                y: sideY,
                                width: sideWidth,
                                height: sideHeight)
            
            setupInitialImages()
        }
        
        private func setupInitialImages() {
            self.sendSubviewToBack(images[1])
            images[0].frame = middleFrame
            images[1].frame = rightFrame
            images[1].alpha = edgeAlpha
        }
        
        func showNext(completion: VoidClosure?) {
            let currentIndex = index
            let nextIndex = index + 1
            guard let nextImage = images[safe: nextIndex] else {
                return
            }
            index = nextIndex
            
            let currentLeftImage = images[safe: currentIndex - 1]
            let currentImage = images[currentIndex]
            let nextRightImage = images[safe: nextIndex + 1]
            nextRightImage?.frame = rightFrame
            nextRightImage?.alpha = 0
            
            self.sendSubviewToBack(currentImage)
            self.bringSubviewToFront(nextImage)
            UIView.animate(
                withDuration: .animations.onboardingCarousel,
                delay: 0,
                options: [.curveLinear],
                animations: {
                    nextImage.alpha = self.middleAlpha
                    currentImage.alpha = self.edgeAlpha
                    currentLeftImage?.alpha = 0
                    nextRightImage?.alpha = self.edgeAlpha
                    
                    currentImage.frame = self.leftFrame
                    nextImage.frame = self.middleFrame
                }, completion: { _ in
                    completion?()
                    
                })
            
            UIView.animateKeyframes(
                withDuration: .animations.onboardingCarousel,
                delay: 0,
                options: [.calculationModeLinear]
            ) {
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1, animations: {
                    
                })
                
                UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 1, animations: {

                })
            }
            
//            UIView.animate(withDuration: 0, delay: .animations.onboardingCarousel / 2, animations: {
//                self.sendSubviewToBack(currentImage)
//                self.bringSubviewToFront(nextImage)
//            })
        }
    }
}
