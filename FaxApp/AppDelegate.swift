//
//  AppDelegate.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import UIKit
import SwiftUI

@main
final class AppDelegate: UIResponder, UIApplicationDelegate {
    
    private let appAnalytics = ApplicationAnalyticsService()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        UDAccessFactory.startupDate.value = Int(Date().timeIntervalSince1970)
        Fir.shared.configure()
        FirMessaging.shared.configure()
        Facebook.shared.applicationDidFinishLaunch(application, launchOptions)
        Appsflyer.shared.configure()
        Amplitudes.shared.configure()
        _ = PurchasesService.shared
        appAnalytics.applicationLaunched()
        
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        FirMessaging.shared.appRegisteredForRemoteNotifications(deviceToken: deviceToken)
    }
}

