//
//  APIClient.swift
//  FaxApp
//
//  Created by Eugene on 11.05.2022.
//

import Combine

final class ApiClient {
    
    enum Host {
        case dev
        case prod
        case prodIP
        
        var url: String {
            switch self {
                
            case .dev:
                return "http://5.45.75.43"
            case .prodIP:
                return "http://5.45.73.18"
            case .prod:
                return "https://faxservlink.xyz"
            }
        }
    }
    
    private let baseUrl = Host.prod.url
    private let userUIDProvider: UserUIDProvider
    private let networkService = NetworkService()
    private let analytics: AnalyticsService
    
    init(userUIDProvider: UserUIDProvider = UserUIDProviderImpl.shared,
         analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.userUIDProvider = userUIDProvider
        self.analytics = analytics
    }
    
    func request<Request: NetworkRequest>(
        request: Request
    ) -> AnyPublisher<Request.ReturnType, NetworkError> {
        guard var urlRequest = request.asURLRequest(baseURL: baseUrl) else {
            let error = NetworkError.wrongRequest
            send(event: .networkError(error, url: nil, method: request.method.rawValue))
            return Fail(error: error).eraseToAnyPublisher()
        }
        
        urlRequest.addValue(userUIDProvider.uuid, forHTTPHeaderField: "user-id")
        
        return networkService
            .request(request: urlRequest, decode: request.decode)
            .first()
            .mapError { [weak self] error -> NetworkError in
                self?.send(event: .networkError(error, url: urlRequest.url, method: request.method.rawValue))
                return error
            }
            .eraseToAnyPublisher()
    }
    
    private func send(event: NetworkAnalyticsEvent) {
        //analytics.send(event)
    }
}
