//
//  Facebook.swift
//  FaxApp
//
//  Created by Eugene on 17.05.2022.
//

import FBSDKCoreKit
import Combine

final class Facebook {
    
    static let shared = Facebook()
    private var cancelBag = Set<AnyCancellable>()
    
    private init() {}
    
    func applicationDidFinishLaunch(_ application: UIApplication,
                                    _ options: [UIApplication.LaunchOptionsKey: Any]?) {
        ApplicationDelegate.shared.initializeSDK()
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: options
        )
        
        #if DEBUG
        Settings.shared.enableLoggingBehavior(.appEvents)
        Settings.shared.enableLoggingBehavior(.networkRequests)
        Settings.shared.enableLoggingBehavior(.developerErrors)
       // Settings.shared.enableLoggingBehavior(.graphAPIDebugInfo)
        Settings.shared.enableLoggingBehavior(.accessTokens)
        #endif
        
        Settings.shared.isAutoLogAppEventsEnabled = true
        Settings.shared.isAdvertiserIDCollectionEnabled = true
        
        IDFATrackingManager.shared.idfaStatusPublisher.sink { status in
            Settings.shared.isAdvertiserTrackingEnabled = status
        }.store(in: &cancelBag)
    }
    
    func appDidBecomeActive() {
        AppEvents.shared.activateApp()
//        AppEvents.shared.logEvent(AppEvents.Name.customName, parameters: [:])
    }
    
    func applicationOpen(url: URL) {
        ApplicationDelegate.shared.application(
            UIApplication.shared,
            open: url,
            sourceApplication: nil,
            annotation: [UIApplication.OpenURLOptionsKey.annotation]
        )
    }
}

extension AppEvents.Name {
    static let customName: AppEvents.Name = AppEvents.Name(rawValue: "event_custom")
}
