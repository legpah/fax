//
//  FaxDeleteService.swift
//  FaxApp
//
//  Created by Eugene on 24.05.2022.
//

import Foundation
import Combine

protocol FaxDeleteService {
    func deleteAll() -> AnyPublisher<EmptyResponse, NetworkError>
    func delete(id: String) -> AnyPublisher<EmptyResponse, NetworkError>
}
