//
//  CountyPhonesListService.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import Foundation
import Combine

protocol CountyPhonesListService {
    var countryPhones: AnyPublisher<[CountryPhone], NetworkError> { get }
}
