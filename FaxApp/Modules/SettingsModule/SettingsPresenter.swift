//
//  SettingsPresenter.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import Foundation
import Combine
import UIKit

protocol SettingsPresenter: ViewOutput {
    func helpAndSupportTapped(_ option: SettingsHelpAndSupportOption)
    func themeChangeIconTapped()
    func nameDidChange(_ text: String)
    func phoneDidChange(_ text: String)
    func didTapNotifications(_ isOn: Bool)
}

final class SettingsPresenterImpl {
    
    weak var view: SettingsViewController?
    
    private weak var router: SettingsRouter?
    private let analytics: AnalyticsService
    private let themeSwitcher: UIThemeSwitcher
    private let name = UDAccess<String>(key: .userDefaults.senderName)
    private let phone = UDAccess<String>(key: .userDefaults.senderPhone)
    private let notificationsRegisterer: NotificationsRegisterer
    private var cancelBag = Set<AnyCancellable>()
    
    private var subscriptionState: SubscriptionViewSate = .loading {
        didSet {
            view?.setSubscriptionViewState(subscriptionState)
        }
    }
    
    init(router: SettingsRouter,
         analytics: AnalyticsService = AnalyticsServiceImpl.shared,
         notificationsRegisterer: NotificationsRegisterer = NotificationsRegistererImpl(),
         themeSwitcher: UIThemeSwitcher = UIThemeProviderImpl.shared) {
        self.analytics = analytics
        self.router = router
        self.notificationsRegisterer = notificationsRegisterer
        self.themeSwitcher = themeSwitcher
        subscribeOnSubscriptionState()
        setNotificationCenterObserver()
    }
    
    private func setNotificationCenterObserver() {
        NotificationCenter.default.addObserver(self, selector: #selector(setSwitcherFromSettings), name: UIApplication.willEnterForegroundNotification, object: nil)
    }
    
    @objc
    private func setSwitcherFromSettings() {
        view?.updateNotificationSwitcher(isOn: false)
        notificationsRegisterer.checkAuthorizationStatus(.authorized) { [weak self] in
            self?.view?.updateNotificationSwitcher(isOn: true)
        }
    }
    
    private func subscribeOnSubscriptionState() {
        PurchasesService.shared.subscriptionState.sinkSuccess { [weak self] state in
            guard let self = self else { return }
            switch state {
            case .loading:
                self.subscriptionState = .loading
            case let .subscribed(subscription):
                self.subscriptionState = .subscribed(
                    name: subscription.name,
                    expire: L10n.Settings.activeUntil(subscription.expiresAt),
                    renewable: subscription.renewable
                )
            case .notSubscribed:
                self.subscriptionState = .notSubscribed(onTap: { [weak self] in
                    self?.analytics.send(AnalyticsEventImpl(name: "tab3_subscription_clicked"))
                    self?.router?.openPaywallScreen()
                    self?.analytics.send(AnalyticsEventImpl(name: "paywall_shown", parameters: ["source": "settings"]))
                })
            }
        }.store(in: &cancelBag)
    }
    
    private func sendShowAnalytics() {
        
        func isExist(value: String?) -> String {
            value == nil ? "no" : "yes"
        }
        
        analytics.send(
            AnalyticsEventImpl(
                name: "tab3_screen_shown",
                parameters: [
                    "name": isExist(value: name.value),
                    "phone": isExist(value: phone.value)
                ]
            )
        )
    }
    
    private func removeObserver() {
        NotificationCenter.default.removeObserver(self)
    }
    
    deinit {
        removeObserver()
    }
}

extension SettingsPresenterImpl: SettingsPresenter {
    func viewDidLoad() {
        view?.setSubscriptionViewState(subscriptionState)
        
        view?.updateName(name.value ?? "")
        view?.updatePhone(phone.value ?? "")
    }
    
    func viewWillAppear() {
        setSwitcherFromSettings()
    }
    
    func viewDidAppear() {
        sendShowAnalytics()
    }
    
    func helpAndSupportTapped(_ option: SettingsHelpAndSupportOption) {
        sendHelpAndSupportClick(option)
        switch option {
        case .faq:
            router?.openFaqScreen()
        case .contactUs:
            router?.openSendMailScreen()
        case .privacyPolicy:
            router?.openPrivacyPolicy()
        case .rateApp:
            router?.openRateApp()
        }
    }
    
    func themeChangeIconTapped() {
        themeSwitcher.switchTheme()
    }
    
    func nameDidChange(_ text: String) {
        name.value = text
    }
    
    func phoneDidChange(_ text: String) {
        phone.value = text
    }
    
    func didTapNotifications(_ isOn: Bool) {
        if isOn {
            notificationsRegisterer.checkAuthorizationStatus(.notDetermined) { [weak self] in
                self?.notificationsRegisterer.requestAuthorization()
                return
            }
            notificationsRegisterer.checkAuthorizationStatus(.denied) { [weak self] in
                self?.router?.openSetings()
            }
        } else {
            router?.openSetings()
        }
        analytics.send(AnalyticsEventImpl(name: isOn ? "tab3_user_on_notifications" : "tab3_user_off_notifications"))
    }
}

private extension SettingsPresenterImpl {
    func sendHelpAndSupportClick(_ option: SettingsHelpAndSupportOption) {
        let name: String = {
            switch option {
            case .faq:
                return "tab3_faq_clicked"
            case .contactUs:
                return "tab3_contact_us_clicked"
            case .privacyPolicy:
                return "tab3_privacy_clicked"
            case .rateApp:
                return "tab3_rate_clicked"
            }
        }()
        analytics.send(AnalyticsEventImpl(name: name))
    }
}
