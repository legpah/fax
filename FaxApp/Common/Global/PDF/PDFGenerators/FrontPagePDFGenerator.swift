//
//  FrontPagePDFGenerator.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import PDFKit
import UIKit

protocol FrontPagePDFGenerator {
    func generate(from model: FrontPageAttributesModel) -> Data
}

final class FrontPagePDFGeneratorImpl: FrontPagePDFGenerator {
    
    private let xInset: CGFloat = 40
    private let xTextsInset: CGFloat = 181
    private lazy var contentWidth = PDFConstants.pageWidth - (xInset * 2)
    private lazy var textsContentWidth = PDFConstants.pageWidth - xInset - xTextsInset
    
    private var tempY: CGFloat = 0
    
    func generate(from model: FrontPageAttributesModel) -> Data {
        let format = UIGraphicsPDFRendererFormat()
        format.documentInfo = [
            String(kCGPDFContextAuthor): model.from
        ]
        
        let render = UIGraphicsPDFRenderer(bounds: PDFConstants.pageFrame,
                                           format: format)
        
        let data = render.pdfData { context in
            context.beginPage()
            
            let graphicContext = UIGraphicsGetCurrentContext()
            graphicContext?.setFillColor(UIThemed<UIColor>.ww.themed(model.theme).cgColor)
            graphicContext?.fill(PDFConstants.pageFrame)
            
            tempY = 0
            
            drawTitle(title: model.title, theme: model.theme)
            
            tempY += 24
            drawLR(L10n.Pdf.time, FrontPagePDFGeneratorImpl.generateDateText(withTimeZone: true), rFont: .poppins(16), theme: model.theme)
            
            tempY += 24
            drawLR(L10n.Pdf.from, model.from, rFont: .poppins(16), theme: model.theme)
            drawLR(L10n.Pdf.contact, model.contact, rFont: .poppins(16), theme: model.theme)
            
            tempY += 24
            drawLR(L10n.Pdf.message, model.message, rFont: .poppins(16, .light), theme: model.theme, paragraphStyle: messageParagraphStyle())
            
            tempY = 0
        }
        
        return data
    }
    
    private func drawTitle(title: String, theme: UITheme) {
        let font = UIFont.poppins(48, .semiBold)
        let titleString = NSAttributedString(string: title, attributes: [
            .font: font,
            .foregroundColor: UIThemed<UIColor>.dd.themed(theme),
        ])
        
        let titleY: CGFloat = 80 + UIFont.poppins(48, .semiBold).descender
        let titleHeight = title.height(width: contentWidth, font: font)
        let titleRect = CGRect(origin: .init(x: xInset, y: titleY), size: CGSize(width: contentWidth, height: titleHeight))
        
        titleString.draw(in: titleRect)
        
        tempY = titleY + titleHeight
    }
    
    private func drawLR(_ lText: String, _ rText: String, rFont: UIFont, theme: UITheme, paragraphStyle: NSMutableParagraphStyle? = nil) {
        let lString = NSAttributedString(string: lText, attributes: [
            .font: UIFont.poppins(16, .semiBold),
            .foregroundColor: UIThemed<UIColor>.dd.themed(theme),
        ])
        
        let lRect = CGRect(origin: .init(x: xInset, y: tempY), size: lString.size())
        lString.draw(in: lRect)
        
        var rAttributes: [NSAttributedString.Key: Any] = [
            .font: rFont,
            .foregroundColor: UIThemed<UIColor>.dd.themed(theme),
        ]
        if let paragraphStyle = paragraphStyle {
            rAttributes[.paragraphStyle] = paragraphStyle
        }
        let rString = NSAttributedString(string: rText, attributes: rAttributes)
        let rTextHeight = rText.height(width: textsContentWidth, font: rFont)
        let rTextRect = CGRect(origin: .init(x: xTextsInset, y: tempY), size: CGSize(width: textsContentWidth, height: rTextHeight))
        rString.draw(in: rTextRect)
        
        tempY += max(lRect.height, rTextHeight)
    }
    
    static func generateDateText(withTimeZone: Bool) -> String {
        let formatter = DateFormatter.fronPageDateFormatter
        let dateText = formatter.string(from: Date())
        if let timeZone = TimeZone.current.abbreviation(), withTimeZone {
            return "\(dateText) (\(timeZone))"
        } else {
            return dateText
        }
    }
    
    private func messageParagraphStyle() -> NSMutableParagraphStyle {
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        paragraphStyle.lineBreakMode = .byWordWrapping
        return paragraphStyle
    }
}


extension NSAttributedString {
    fileprivate func alignBottom(to another: NSAttributedString,
                                 with anotherRect: CGRect) -> CGFloat {
        guard let anotherFont = another.attribute(.font, at: 0, effectiveRange: nil) as? UIFont,
              let font = self.attribute(.font, at: 0, effectiveRange: nil) as? UIFont
        else {
            return anotherRect.minY
        }
        
        return (anotherRect.maxY + anotherFont.descender) - (self.size().height + font.descender)
    }
}
