//
//  UIView+RenderImage.swift
//  FaxApp
//
//  Created by Eugene on 15.04.2022.
//

import UIKit

extension UIView {
    func renderImage(withBlackFilter: Bool) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(frame.size, false, 0)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        context.saveGState()
        layer.render(in: context)
        context.restoreGState()
        guard let image = UIGraphicsGetImageFromCurrentImageContext() else { return nil }

        /*if withBlackFilter {
            guard var ciImage = CIImage(image: image) else { return nil }
            ciImage = ciImage.applyingFilter("CIColorControls", parameters: [kCIInputSaturationKey: 0, kCIInputContrastKey: 1.5])
            let ciContext = CIContext(options: nil)
            let cgImage = ciContext.createCGImage(ciImage, from: ciImage.extent)
            let filteredImage = UIImage(cgImage: cgImage!)
            UIGraphicsEndImageContext()
            return filteredImage
        }*/
        
        UIGraphicsEndImageContext()
        return image
    }
}
