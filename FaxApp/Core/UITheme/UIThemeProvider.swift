//
//  UIThemeProvider.swift
//  FaxApp
//
//  Created by Eugene on 12.04.2022.
//

import UIKit

protocol UIThemeProvider: AnyObject {
    var current: UITheme { get }
    func register<Observer: UIThemable>(observer: Observer, applyOnRegister: Bool)
    func unregister<Observer: UIThemable>(observer: Observer)
}

protocol UIThemeSwitcher: AnyObject {
    func switchTheme()
}

extension UIThemeProvider {
    func register<Observer: UIThemable>(observer: Observer, applyOnRegister: Bool = true) {
        self.register(observer: observer, applyOnRegister: applyOnRegister)
    }
}
