//
//  CountyPhoneInputRouter.swift
//  FaxApp
//
//  Created by Eugene on 4.05.2022.
//

import Foundation

typealias CountyPhoneInputRouter =
CountryPhonesListRoutable
& ContactsPickerRoutable
& AlertOptionsRoutable
