//
//  TextButton.swift
//  FaxApp
//
//  Created by Eugene on 1.06.2022.
//

import UIKit

final class TextButton: UIView {
    
    var onTap: VoidClosure?
    
    var title: String? {
        get { label.text }
        set { label.text = newValue }
    }
    
    private let label: Label
    
    init(text: String? = nil, font: UIFont = .poppins(14, .semiBold), insets: UIEdgeInsets = .zero) {
        self.label = Label(text: text,
                           font: font,
                           color: .tukesh,
                           alignment: .center,
                           lines: 0)
                           
        super.init(frame: .zero)
        addSubview(label)
        label.pinEdgesToSuperviewEdges(with: insets)
        addGestureRecognizer(
            UITapGestureRecognizer(target: self, action: #selector(didTap))
        )
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc
    private func didTap() {
        onTap?()
    }
}
