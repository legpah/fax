//
//  Constants+Animations.swift
//  FaxApp
//
//  Created by Eugene on 22.04.2022.
//

import Foundation

extension ConstantsToken.AnimationsToken {
    static let defaultDuration: TimeInterval = 0.23
    static let fastTransitionDuration: TimeInterval = 0.14
    static let onboardingCarousel: TimeInterval = 0.4
}

extension TimeInterval {
    static let animations = ConstantsToken.AnimationsToken.self
}
