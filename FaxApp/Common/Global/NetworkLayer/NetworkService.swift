//
//  NetworkService.swift
//  FaxApp
//
//  Created by Eugene on 11.05.2022.
//

import Foundation
import Combine

struct NetworkService {
    private let urlSession: URLSession
    
    public init(urlSession: URLSession = .shared) {
        self.urlSession = urlSession
    }
    
    func request<T: Decodable>(
        request: URLRequest,
        decode: @escaping (Data) throws -> T
    ) -> AnyPublisher<T, NetworkError> {
        return urlSession
            .dataTaskPublisher(for: request)
            .tryMap({ data, response -> Data in
                guard let response = response as? HTTPURLResponse else {
                    throw NetworkError.unknownError
                }
                
                printDebug("Network:\ncode\(response.statusCode)\n\(String.init(data: data, encoding: .utf8) ?? "No content")")
                
                if response.statusCode == .successCode {
                    if T.self is EmptyResponse.Type {
                        return try! JSONEncoder().encode(EmptyResponse())
                    } else {
                        return data
                    }
                } else if response.statusCode == .internalErrorCode {
                    if let internalError = try? internalError(data: data) {
                        throw NetworkError.apiError(internalError)
                    } else {
                        throw NetworkError.decodingInternalError
                    }
                } else {
                    throw NetworkError.httpError(code: response.statusCode)
                }
            })
            .tryMap({ data -> T in
                do {
                    return try decode(data)
                } catch {
                    throw error
                }
            })
            .mapError { error in
                if Int.noNetworkCodes.contains((error as NSError).code) {
                    return NetworkError.notInternetConnection
                }
                return handleError(error)
            }
            .receive(on: DispatchQueue.main)
            .eraseToAnyPublisher()
    }
    
    private func internalError(data: Data) throws -> ApiInternalError {
        let decoder = JSONDecoder()
        return try decoder.decode(ApiInternalError.self, from: data)
    }
    
    private func handleError(_ error: Error) -> NetworkError {
        switch error {
        case is Swift.DecodingError:
            return .decodingError
        case let error as NetworkError:
            return error
        default:
            return .unknownError
        }
    }
}

extension Int {
    fileprivate static let successCode = 200
    fileprivate static let internalErrorCode = 500
    fileprivate static let noNetworkCodes = [-1001, -1020, -1009, -1005]
}
