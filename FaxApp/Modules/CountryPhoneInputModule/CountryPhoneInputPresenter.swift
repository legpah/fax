//
//  PhoneInputPresenter.swift
//  FaxApp
//
//  Created by Eugene on 27.04.2022.
//

import Foundation
import Combine

protocol CountryPhoneInputModuleInput: AnyObject {
    var selectedCountry: CountryPhone? { get }
    var maskedPhoneRecipient: String { get }
}

protocol CountryPhoneInputModuleOuput: AnyObject {
    func countriesLoaded()
    func didChangeValidState(_ state: Bool)
}

protocol CountryPhoneInputPresenter: ViewOutput {
    func didChangePhone(_ text: String)
    func didChangePhoneInput(_ isFirstResponder: Bool, text: String)
    func didTapCountryButton()
    func didTapClear()
    func didTapContacts()
}

final class CountryPhoneInputPresenterImpl {
    
    weak var view: CountryPhoneInputViewController?
    
    private weak var router: CountyPhoneInputRouter?
    private weak var moduleOutput: CountryPhoneInputModuleOuput?
    private let interactor: CountryPhoneInputInteractor
    private let formatter = CountryPhoneInputFormatter()
    private var cancelBag = Set<AnyCancellable>()
    private var countries: [CountryPhone] = []
    private let analytics: AnalyticsService
    
    private var _selectedCountry: CountryPhone? {
        didSet {
            guard _selectedCountry != oldValue else { return }
            interactor.storeSelectedCountry(_selectedCountry)
            updateViewWithSelectedCountry(isLoading: _selectedCountry == nil)
        }
    }
    
    private var invalidInputState: CountryInputValidAppearance = .unspecified {
        didSet {
            view?.updateInvalidInput(invalidInputState)
            moduleOutput?.didChangeValidState(invalidInputState == .valid)
        }
    }
    
    private var lastMaskFormattedPhone: String = ""
    
    init(router: CountyPhoneInputRouter,
         moduleOutput: CountryPhoneInputModuleOuput,
         interactor: CountryPhoneInputInteractor = CountryPhoneInputInteractorImpl(),
         analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.router = router
        self.moduleOutput = moduleOutput
        self.interactor = interactor
        self.analytics = analytics
    }
    
    private func fetchCountryPhones() {
        interactor.countryPhones
            .retry(interval: 0.5, retries: .max)
            .map { countyPhones in
                return countyPhones.filter { $0.canSendFax }
            }
            .sinkSuccess({ [weak self] countyPhones in
                self?.handleFetchedCountryPhones(countyPhones)
            })
            .store(in: &cancelBag)
    }
    
    private func handleFetchedCountryPhones(_ countries: [CountryPhone]) {
        guard let selectedCountry = selectedCountyWith(countries: countries) else {
            return
        }
        
        self._selectedCountry = selectedCountry
        self.countries = countries
        moduleOutput?.countriesLoaded()
    }
    
    private func openCountriesPhonesScreen() {
        router?.openCountyPhonesList(onSelectCountry: { [weak self] country in
            self?._selectedCountry = country
            self?.analytics.send(AnalyticsEventImpl(name: "tab1_country_screen_country_choosed",
                                                    parameters: ["country": country.name]))
        })
    }
    
    private func updateViewWithSelectedCountry(isLoading: Bool) {
        if isLoading {
            self.view?.updatePhonePlaceholder(L10n.Country.loading)
        }
        view?.updateToLoadingState(isLoading: isLoading)
        guard let selectedCountry = _selectedCountry else {
            return
        }
        view?.updateCountyFlag(selectedCountry.flagUnicode)
        view?.updatePhoneCode(selectedCountry.code)
        view?.updatePhonePlaceholder(selectedCountry.mask)
    }
    
    private func selectedCountyWith(countries: [CountryPhone]) -> CountryPhone? {
        if let selectedCounty = interactor.storedSelectedCountry {
            if let remoteCountry = countries.first(where: { $0.name == selectedCounty.name }) {
                return remoteCountry
            } else {
                return countries.first
            }
        } else {
            return countries.first(where: {
                $0.name == .countries.usaName
            }) ?? countries.first
        }
    }
}

extension CountryPhoneInputPresenterImpl: CountryPhoneInputModuleInput {
    var selectedCountry: CountryPhone? {
        _selectedCountry
    }
    
    var maskedPhoneRecipient: String {
        "\(selectedCountry?.code ?? "") \(lastMaskFormattedPhone)"
    }
}

extension CountryPhoneInputPresenterImpl: CountryPhoneInputPresenter {
    func viewDidLoad() {
        updateViewWithSelectedCountry(isLoading: true)
        invalidInputState = .unspecified
        view?.updateClearButton(isVisible: false)
        fetchCountryPhones()
        
        SendFaxNotification.sendPublisher.sinkSuccess { [weak self] _ in
            self?.didTapClear()
        }.store(in: &cancelBag)
    }
    
    func didTapCountryButton() {
        openCountriesPhonesScreen()
        analytics.send(AnalyticsEventImpl(name: "tab1_country_clicked"))
    }
    
    func didChangePhone(_ text: String) {
        guard let selectedCountry = _selectedCountry else { return }
        let formatResult = formatter.format(text, country: selectedCountry)
        view?.updatePhoneText(formatResult.fomattedPhone)
        view?.updateClearButton(isVisible: formatResult.fomattedPhone.isNotEmpty)
        lastMaskFormattedPhone = formatResult.fomattedPhone
        if formatResult.fomattedPhone.isEmpty {
            invalidInputState = .unspecified
        } else {
            invalidInputState = formatResult.isValid ? .valid : .invalid
        }
    }
    
    func didChangePhoneInput(_ isFirstResponder: Bool, text: String) {
    }
    
    func didTapClear() {
        view?.updatePhoneText("")
        invalidInputState = .unspecified
        view?.updateClearButton(isVisible: false)
    }
    
    func didTapContacts() {
        router?.openContactsPicker(output: self)
        analytics.send(AnalyticsEventImpl(name: "tab1_contacts_clicked"))
    }
}

extension CountryPhoneInputPresenterImpl: ContactsPickerModuleOuput {
    func didPickContact(phoneNumber: String) {
        guard let currentCountry = _selectedCountry else {
            return
        }
        guard phoneNumber.starts(with: "+") else {
            router?.openAlertOptions(model: .failExtractCountyCode(), animated: true)
            return
        }
        
        let maybeCountry: CountryPhone? = {
            
            if phoneNumber.starts(with: String.countries.usaCanadaCode)
                && currentCountry.code == String.countries.usaCanadaCode {
                return _selectedCountry
            } else {
                return countries.first(where: {
                    phoneNumber.starts(with: $0.code)
                })
            }
        }()
        
        guard let country = maybeCountry else {
            router?.openAlertOptions(
                model: .unableToSendFax(onCheckOut: { [weak self] in
                    self?.openCountriesPhonesScreen()
                }),
                animated: true
            )
            return
        }
        
        _selectedCountry = country
        didChangePhone(phoneNumber.replacingOccurrences(of: country.code, with: ""))
    }
}
