//
//  FAQViewController.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import UIKit

typealias FAQDiffableSource = DiffableTableDataSource<FAQSectionModel, FAQCellModel>

protocol FAQViewController: AnyObject {
    
    var faqSource: FAQDiffableSource { get }
}

final class FAQViewControlerImpl: UIViewController {
    
    private let presenter: FAQPresenter
    private let tableView = TableView(registerCells: [
        FAQItemCell.self
    ])
    
    private lazy var dataSource = FAQDiffableSource(tableView: tableView)
    
    init(presenter: FAQPresenter) {
        self.presenter = presenter
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSelf()
        presenter.viewDidLoad()
    }
    
    private func configureSelf() {
        view.addSubview(tableView)
        tableView.estimatedRowHeight = 48
        tableView.separatorStyle = .none
        tableView.rowHeight = UITableView.automaticDimension
        
        dataSource.defaultRowAnimation = .fade
        
        configureCloseButton()
        cofigureConstraints()
        
        themeProvider.register(observer: self)
    }
    
    private func configureCloseButton() {
        let closeButton = TextBarButton(text: L10n.Paywall.close, position: .left)
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: closeButton)
        closeButton.onTap = { [weak self] in
            self?.presenter.didTapDismiss()
        }
    }
    
    private func cofigureConstraints() {
        tableView.pin(toTopLayoutGuideOf: self)
        tableView.pinEdges(toSuperviewEdges: [.left, .right])
        tableView.pin(toBottomLayoutGuideOf: self)
    }
    
    @objc
    private func closeTapped() {
        presenter.didTapDismiss()
    }
}

extension FAQViewControlerImpl: UIThemable {
    func apply(theme: UITheme) {
        view.backgroundColor = .themed.bw.value
        tableView.backgroundColor = .themed.bw.value
    }
}

extension FAQViewControlerImpl: FAQViewController {
    var faqSource: FAQDiffableSource {
        dataSource
    }
}

