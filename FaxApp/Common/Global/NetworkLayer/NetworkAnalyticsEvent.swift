//
//  NetworkAnalyticsEvent.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation

struct NetworkAnalyticsEvent: AnalyticsEvent {
    let name: String
    let parameters: [String : Any]?
}

extension NetworkAnalyticsEvent {
    static func networkError(
        _ error: NetworkError,
        url: URL?,
        method: String
    ) -> NetworkAnalyticsEvent {
        var params: [String: Any] = [
            "code": error.errorCode,
            "description": error.errorDescription,
            "http_method": method
        ]
        if let url = url {
            params["url"] = url.absoluteString
        } else {
            params["url"] = "incorrect url"
        }
        if let id = error.id {
            params["error_id"] = id
        }
        
        return NetworkAnalyticsEvent(
            name: "network_error",
            parameters: params
        )
    }
}
