//
//  GradientView.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import UIKit

final class GradientView: UIView {
    
    enum Direction: Int {
        case fromTopToBottom
        case fromBottomToTop
        case fromLeftToRight
        case fromRightToLeft
    }
    
    // MARK: - Internal properties
    
    var colors: [UIColor] {
        didSet { configureGradient() }
    }
    
    var direction: Direction {
        didSet { configureGradient() }
    }
    
    // MARK: - Private properties
    
    private var gradientLayer: CAGradientLayer {
        return self.layer as! CAGradientLayer
    }
    
    // MARK: - Life cycle
    
    init(direction: Direction, colors: [UIColor]) {
        self.direction = direction
        self.colors = colors
        super.init(frame: .zero)
        configureGradient()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override class var layerClass: AnyClass {
        return CAGradientLayer.self
    }
    
    // MARK: - Private methods
    
    private func configureGradient() {
        
        let (startPoint, endPoint): (CGPoint, CGPoint) = {
            switch direction {
            case .fromTopToBottom:
                return (CGPoint(x: 0, y: 0),
                        CGPoint(x: 0, y: 1))
            case .fromBottomToTop:
                return (CGPoint(x: 0, y: 1),
                        CGPoint(x: 0, y: 0))
            case .fromLeftToRight:
                return (CGPoint(x: 0, y: 0),
                        CGPoint(x: 1, y: 0))
            case .fromRightToLeft:
                return (CGPoint(x: 1, y: 0),
                        CGPoint(x: 0, y: 0))
            }
        }()
        
        gradientLayer.colors = self.colors.map { $0.cgColor }
        gradientLayer.startPoint = startPoint
        gradientLayer.endPoint = endPoint
    }
}
