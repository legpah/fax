//
//  SendFaxEndpoint.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation

struct SendFaxEndpoint: MultipartNetworkRequest {
    typealias ReturnType = SendFaxResponse
    
    let path: String = "/fax"
    let method: HTTPMethod = .post
    let body: [String : Any]?
    let files: [MultipartNetworkRequestFile]
    
    init(pdfData: Data,
         previewData: Data,
         recipient: String,
         pagesCount: Int) {
        files = [
            .init(name: "filePdf", fileData: pdfData, fileType: .pdf),
            .init(name: "filePreview", fileData: previewData, fileType: .jpeg)
        ]
        body = [
            "recipient": recipient,
            "userProperty": pagesCount
        ]
    }
}

struct SendFaxResponse: Decodable {
    let fileId: String
    let previewId: String
}
