//
//  FAQCellModelsProvider.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import Foundation

protocol FAQCellModelsProvider {
    func cellModels(onExpandTap: @escaping IOClosure<FAQCellModel, VoidClosure>) -> [FAQCellModel]
}

struct FAQCellModelsProviderImpl: FAQCellModelsProvider {
    
    func cellModels(
        onExpandTap: @escaping IOClosure<FAQCellModel, VoidClosure>
    ) -> [FAQCellModel] {
        [
            FAQCellModel(
                question: L10n.Faq.question1,
                answer: L10n.Faq.answer1,
                isExpanded: false,
                onExpandTap: onExpandTap
            ),
            FAQCellModel(
                question: L10n.Faq.question2,
                answer: L10n.Faq.answer2(L10n.Sending.preview.quoted, L10n.Main.send.quoted, L10n.Sending.sendNow.quoted),
                isExpanded: false,
                onExpandTap: onExpandTap
            ),
            FAQCellModel(
                question: L10n.Faq.question3,
                answer: L10n.Faq.answer3(L10n.Sending.preview.quoted, L10n.Sending.preview.quoted, L10n.Sending.readyToSend.quoted),
                isExpanded: false,
                onExpandTap: onExpandTap
            ),
            FAQCellModel(
                question: L10n.Faq.question4,
                answer: L10n.Faq.answer4,
                isExpanded: false,
                onExpandTap: onExpandTap
            ),
            FAQCellModel(
                question: L10n.Faq.question5,
                answer: L10n.Faq.answer5(L10n.Settings.title.quoted, L10n.Settings.contact.quoted, L10n.Settings.title.quoted),
                isExpanded: false,
                onExpandTap: onExpandTap
            ),
            FAQCellModel(
                question: L10n.Faq.question6,
                answer: L10n.Faq.answer6,
                isExpanded: false,
                onExpandTap: onExpandTap
            ),
            FAQCellModel(
                question: L10n.Faq.question7,
                answer: L10n.Faq.answer7(L10n.Settings.contact.quoted, L10n.Settings.contact.quoted),
                isExpanded: false,
                onExpandTap: onExpandTap
            ),
        ]
    }
}
