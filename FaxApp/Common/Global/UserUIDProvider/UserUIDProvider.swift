//
//  UserUIDProvider.swift
//  FaxApp
//
//  Created by Eugene on 10.05.2022.
//

import Foundation
import KeychainSwift

protocol UserUIDProvider: AnyObject {
    var uuid: String { get }
}

final class UserUIDProviderImpl: UserUIDProvider {
    
    static let shared = UserUIDProviderImpl()
    
    private init() {}
    
    var uuid: String {
        if let _uuid = _uuid {
            return _uuid
        } else {
            return restoreFromKeychain()
        }
    }
    
    private var _uuid: String?
    
    private let key = "_uuid"
    private func restoreFromKeychain() -> String {
        let keychain = KeychainSwift(keyPrefix: "com.supportlink.limited")
        keychain.synchronizable = true
        
        let stored = keychain.get(key) ?? createAndStoreUUID(keychain: keychain)
        _uuid = stored
        
        return stored
    }
    
    private func createAndStoreUUID(keychain: KeychainSwift) -> String {
        let uuid = UUID().uuidString
        keychain.set(uuid, forKey: key)
        return uuid
    }
}
