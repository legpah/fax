//
//  CountryPhonesListRouterModel.swift
//  FaxApp
//
//  Created by Eugene on 28.04.2022.
//

import Foundation

struct CountryPhonesListRouterModel {
    let selectedCountry: CountryPhone
    let onSelectCountry: IClosure<CountryPhone>
    
    init(selectedCountry: CountryPhone,
         onSelectCountry: @escaping IClosure<CountryPhone>) {
        self.selectedCountry = selectedCountry
        self.onSelectCountry = onSelectCountry
    }
}
