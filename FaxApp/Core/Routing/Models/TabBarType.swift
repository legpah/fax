//
//  TabBarType.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import Foundation

enum TabBarType: Int, CaseIterable {
    case fax
    case faxHistory
    case settings
}

extension Set where Element == TabBarType {
    static var allTabs: Set<TabBarType> = Set(TabBarType.allCases)
}

extension TabBarType: Comparable {
    static func < (lhs: TabBarType, rhs: TabBarType) -> Bool {
        lhs.rawValue < rhs.rawValue
    }
}

extension TabBarType {
    var analyticsIdentifier: String {
        switch self {
        case .fax:
            return "fax"
        case .faxHistory:
            return "history"
        case .settings:
            return "settings"
        }
    }
    
    var analyticsIndex: Int {
        (Self.allCases.firstIndex(of: self) ?? 0) + 1
    }
}
