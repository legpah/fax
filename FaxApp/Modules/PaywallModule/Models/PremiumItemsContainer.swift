//
//  PremiumItemsContainer.swift
//  FaxApp
//
//  Created by Eugene on 14.07.2022.
//

import UIKit

protocol PremiumItemsContainer {
    var allPlansIsShowed: Bool { get }
    func setup(premiumItems: [PremiumItemModel])
    func updateSelected(item: PremiumItemModel)
    func useLightColors()
    func allPlans(isShowed: Bool)
}

final class PremiumItemsContainerImpl: UIView, PremiumItemsContainer {
    
    private(set) var allPlansIsShowed: Bool = true
    private var premiumItemViews: [PremiumItemView] = []
    
    func setup(premiumItems: [PremiumItemModel]) {
        var topView: UIView = self
        premiumItemViews = premiumItems.enumerated().map { idx, model in
            let premiumView = PremiumItemView(model: model)
            self.addSubview(premiumView)
            premiumView.pinEdge(.top,
                                to: idx == 0 ? .top : .bottom,
                                of: topView,
                                withOffset: idx == 0 ? 0 : 12)
            premiumView.pinEdges(toSuperviewEdges: [.left, .right],
                                 withInset: .layout.xInset)
            if idx == premiumItems.count - 1 {
                premiumView.pinEdge(.bottom, to: .bottom, of: self)
            }
            topView = premiumView
            return premiumView
        }
        layoutIfNeeded()
    }
    
    func allPlans(isShowed: Bool) {
        allPlansIsShowed = isShowed
    }
    
    func updateSelected(item: PremiumItemModel) {
        premiumItemViews.forEach {
            $0.isSelected = item.premiumId == $0.model.premiumId
        }
    }
    
    func useLightColors() {
        premiumItemViews.forEach { $0.useLightColors = true }
    }
}
