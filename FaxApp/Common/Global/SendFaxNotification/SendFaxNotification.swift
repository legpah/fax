//
//  SendFaxNotification.swift
//  FaxApp
//
//  Created by Eugene on 31.05.2022.
//

import Combine
import Foundation

typealias PagesCount = Int

enum SendFaxNotification {
    static var sendPublisher: AnyPublisher<PagesCount, Never> {
        sendSubject.eraseToAnyPublisher()
    }
    
    private static let sendSubject = PassthroughSubject<PagesCount, Never>()
    
    static func notifyFaxSended(pagesCount: PagesCount) {
        sendSubject.send(pagesCount)
    }
   
}
 
enum SendFaxErrorNotification {
    static var sendPublisher: AnyPublisher<Error, Never> {
        sendSubject.eraseToAnyPublisher()
    }
    
    private static let sendSubject = PassthroughSubject<Error, Never>()
    
    static func notifyErrorSended(error: Error) {
        sendSubject.send(error)
    }
}
 
