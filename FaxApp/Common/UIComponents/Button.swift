//
//  Button.swift
//  FaxApp
//
//  Created by Eugene on 07.04.2022.
//

import UIKit

final class Button: UIView {
    
    var onTap: VoidClosure? {
        didSet {
            onTapChanged()
        }
    }
    
    var isActive: Bool = true {
        didSet {
            guard isActive != oldValue else { return }
            apply(theme: .current)
        }
    }
    
    var title: String {
        didSet { label.text = title }
    }
    
    var textAlignment: NSTextAlignment? {
        didSet {
            textAlignment.flatMap { label.textAlignment = $0 }
        }
    }
    
    private let label: Label
    private let bgColor: UIThemed<UIColor>?
    var titleColor: UIThemed<UIColor> {
        didSet { apply(theme: .current) }
    }
    private let bgInactiveColor: UIThemed<UIColor>?
    private let titleInactiveColor: UIThemed<UIColor>?
    
    init(
        title: String,
        titleColor: UIThemed<UIColor>,
        bgColor: UIThemed<UIColor>?,
        font: UIFont = .poppins(16, .semiBold),
        bgInactiveColor: UIThemed<UIColor>? = nil,
        titleInactiveColor: UIThemed<UIColor>? = nil
    ) {
        self.label = Label(text: title,
                           font: font,
                           color: titleColor,
                           alignment: .left,
                           lines: 0)
        self.title = title
        self.bgColor = bgColor
        self.titleColor = titleColor
        self.bgInactiveColor = bgInactiveColor
        self.titleInactiveColor = titleInactiveColor
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSelf() {
        addSubview(label)
        label.pinEdgesToSuperviewEdges()
        
        themeProvider.register(observer: self)
    }
    
    private func onTapChanged() {
        if onTap == nil {
            self.gestureRecognizers?.removeAll()
        } else {
            self.addGestureRecognizer(
                UITapGestureRecognizer(target: self, action: #selector(handleTap))
            )
        }
    }
    
    @objc
    private func handleTap() {
        if isActive {
            onTap?()
        }
    }
}

extension Button: UIThemable {
    func apply(theme: UITheme) {
        self.backgroundColor = isActive
        ? bgColor?.value
        : bgInactiveColor?.value ?? bgColor?.value
        
        if isActive {
            label.color = titleColor
        } else {
            label.color = titleInactiveColor ?? titleColor
        }
    }
}
