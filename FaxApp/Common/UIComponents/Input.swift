//
//  Input.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import UIKit

class Input: UITextField {
    
    // MARK: - Public properties
    
    var shouldHideOnReturnTap = false
    
    var maxCharactersCount: Int = .max - 1
    
    var onFirstResponderChange: IClosure<FirstResponderChange>?
    
    var onTextChange: IClosure<TextChange>?
    
    var textInsets: UIEdgeInsets = .symmetric(x: 12, y: 11) {
        didSet { self.setNeedsDisplay() }
    }
    
    var borderRadius: CGFloat = 8 {
        didSet { updateBordersShape() }
    }
    
    var bordersWidth: CGFloat = 1 {
        didSet { updateBordersShape() }
    }
    
    var borderColor: UIThemed<UIColor>? = .lanka {
        didSet { applyBorders(color: borderColor) }
    }
    
    var placeholderText: String? {
        get { placeholderModel?.text }
        set {
            placeholderModel?.text = newValue ?? ""
            updatePlaceholder()
        }
    }
    
    // MARK: - Private properties
    
    private let toolBar = UIToolbar()
    private var placeholderModel: Placeholder?
    private let color: UIThemed<UIColor>
    private let bgColor: UIThemed<UIColor>
    private var onToolbarDoneTap: VoidClosure?
    
    // MARK: - Init
    
    init(font: UIFont = .poppins(12),
         placeholder: Placeholder? = nil,
         color: UIThemed<UIColor> = .ibw,
         bgColor: UIThemed<UIColor> = .lanka,
         keyboard: UIKeyboardType = .default,
         alignment: NSTextAlignment = .left,
         height: CGFloat = 40) {
        self.color = color
        self.placeholderModel = placeholder
        self.bgColor = bgColor
        super.init(frame: .zero)
        self.font = font
        autocapitalizationType = .none
        autocorrectionType = .no
        textAlignment = alignment
        delegate = self
        keyboardType = keyboard
        
        updateBordersShape()
        updatePlaceholder()
        setDimension(.height, toSize: height)
        themeProvider.register(observer: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // MARK: - Life cycle
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        super.hitTest(point, with: event)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textInsetsDependedByConstantText)
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textInsetsDependedByConstantText)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: textInsetsDependedByConstantText)
    }
    
    private var textInsetsDependedByConstantText: UIEdgeInsets {
        textInsets
//        guard constantText != nil else { return textInsets }
//        return UIEdgeInsets(top: textInsets.top,
//                            left: textInsets.left + constantTextLabel.frame.width,
//                            bottom: textInsets.bottom,
//                            right: textInsets.right)
    }
    
    
    // MARK: - Internal methods
    
    func addDoneToolBar(title: String = "Done", action: VoidClosure?) {
        
        onToolbarDoneTap = action
        
        guard self.inputAccessoryView == nil else { return }
        
        toolBar.items = [
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil),
            UIBarButtonItem(title: title, style: .done, target: self, action: #selector(handleDoneTap))
        ]
        
        toolBar.sizeToFit()
        self.inputAccessoryView = toolBar
    }
    
    // MARK: - Private methods
    
    @objc
    private func handleDoneTap() {
        resignFirstResponder()
        onToolbarDoneTap?()
    }
    
    private func updateBordersShape() {
        applyBordersShape(width: bordersWidth, radius: borderRadius)
    }
    
    private func updatePlaceholder() {
        guard let placeholder = self.placeholderModel else {
            attributedPlaceholder = nil
            return
        }
        
        let parafraphStyle = NSMutableParagraphStyle()
        parafraphStyle.alignment = self.textAlignment
        
        attributedPlaceholder = NSAttributedString(
            string: placeholder.text,
            attributes: [
                .font: placeholder.font,
                .foregroundColor: placeholder.color.value,
                .paragraphStyle: parafraphStyle,
            ]
        )
    }
}

extension Input: UIThemable {
    func apply(theme: UITheme) {
        self.textColor = color.value
        self.backgroundColor = bgColor.value
        applyBorders(color: borderColor)
        updatePlaceholder()
        
        keyboardAppearance = theme.asKeyboardAppearance
        
        switch theme {
        case .light:
            toolBar.barStyle = .default
        case .dark:
            toolBar.barStyle = .black
        }
    }
}

extension Input: UITextFieldDelegate {
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        onFirstResponderChange?(
            FirstResponderChange(isActive: true, text: textField.text ?? "")
        )
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        guard shouldHideOnReturnTap else { return true }
        resignFirstResponder()
        onFirstResponderChange?(
            FirstResponderChange(isActive: false, text: textField.text ?? "")
        )
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard var text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        
        guard newLength <= maxCharactersCount else { return false }
        
        if newLength > text.count {
            text.append(contentsOf: string)
            DispatchQueue.main.async {
                self.onTextChange?(TextChange(text: text, editAction: .addition))
            }
        } else if newLength < text.count {
            text = String(text.prefix(newLength))
            DispatchQueue.main.async {
                self.onTextChange?(TextChange(text: text, editAction: .deletion))
            }
        }
        
        return true
    }
}

extension Input {
    struct TextChange {
        let text: String
        let editAction: EditAction
    }
    
    enum EditAction {
        case addition
        case deletion
    }
    
    struct FirstResponderChange {
        let isActive: Bool
        let text: String
    }
}

extension Input {
    struct Placeholder {
        var text: String
        let color: UIThemed<UIColor>
        let font: UIFont
        
        init(text: String,
             color: UIThemed<UIColor> = .baboo,
             font: UIFont = .poppins(12, .light)) {
            self.text = text
            self.color = color
            self.font = font
        }
    }
}
