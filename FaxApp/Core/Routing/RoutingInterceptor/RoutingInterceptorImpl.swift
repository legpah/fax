//
//  RoutingInterceptorImpl.swift
//  FaxApp
//
//  Created by Eugene on 19.04.2022.
//

import Foundation

struct RoutingInterceptorImpl: RoutingInterceptor {
    
    private let analytics: AnalyticsService
    
    init(analytics: AnalyticsService = AnalyticsServiceImpl.shared) {
        self.analytics = analytics
    }
    
    func interceptRoute(from: ScreenRouter, to: ScreenRouter) -> ScreenRouter? {
        let token = RoutingInterceptorToken.self
        let interceptor: RoutingInterceptor
        
        switch RouterType(identifier: to.routerIdentifiable.identifier) {
        case let .singleImagePicker(source):
            interceptor = token.toSingleImagePicker(source: source)
        case .onboarding:
            interceptor = token.toOnboardingInterceptor()
        case .sendMail:
            interceptor = token.toSendMail()
        default:
            interceptor = token.passThroughRouterInterceptor
        }
        
        let toRouter = interceptor.interceptRoute(from: from, to: to)
        
        //sendEvent(from: from, to: toRouter)
        
        return toRouter
    }
    
    private func sendEvent(from: ScreenRouter, to: ScreenRouter?) {
        guard let to = to else {
            return
        }
        
        let event = AnalyticsEventImpl(name: "screen_opened", parameters: [
            "from": from.routerIdentifiable.identifier,
            "to": to.routerIdentifiable.identifier,
        ])
        analytics.send(event)
    }
}
