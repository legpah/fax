//
//  SettingsViewControllerImpl+SettingsHelpAndSupportView.swift
//  FaxApp
//
//  Created by Eugene on 20.04.2022.
//

import UIKit

extension SettingsViewControllerImpl {
    final class HelpAndSupportView: UIView {
        
        var onOptionTap: IClosure<SettingsHelpAndSupportOption>?
        
        private let title = Label(text: L10n.Settings.help, font: .poppins(12, .semiBold))
        private let stackView = UIStackView()
        
        init() {
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(title)
            addSubview(stackView)
            
            stackView.axis = .vertical
            stackView.distribution = .equalSpacing
            stackView.applyBordersShape(width: 1, radius: 8)
            
            configureArrangedSubviews()
            configureConstraints()
            
            themeProvider.register(observer: self)
        }
        
        private func configureArrangedSubviews() {
            SettingsHelpAndSupportOption.allCases.enumerated().forEach { idx, value in
                let params = value.buttonParams
                let button = ItemView(
                    icon: params.image,
                    title: params.title,
                    hasArrow: value == .faq,
                    hasBottomBorder: idx != SettingsHelpAndSupportOption.allCases.count - 1
                )
                
                button.onTap = { [weak self] in
                    self?.onOptionTap?(value)
                }
                
                stackView.addArrangedSubview(button)
            }
        }
        
        private func configureConstraints() {
            title.pinEdges(toSuperviewEdges: [.left, .top])
            
            stackView.pinEdge(.top, to: .bottom, of: title, withOffset: 12)
            stackView.pinEdges(toSuperviewEdges: [.left, .right])
            
            self.pinEdge(.bottom, to: .bottom, of: stackView)
        }
    }
}

extension SettingsViewControllerImpl.HelpAndSupportView: UIThemable {
    func apply(theme: UITheme) {
        stackView.applyBorders(color: .xero)
    }
}

extension SettingsHelpAndSupportOption {
    fileprivate typealias ButtonParams = (title: String, image: UIThemed<UIImage>)
    fileprivate var buttonParams: ButtonParams {
        switch self {
        case .faq:
            return (L10n.Settings.faq, .faq)
        case .contactUs:
            return (L10n.Settings.contact, .contactUs)
        case .privacyPolicy:
            return (L10n.Settings.privacy, .privacyPolicy)
        case .rateApp:
            return (L10n.Settings.rate, .rate)
        }
    }
}

extension SettingsViewControllerImpl.HelpAndSupportView {
    final class ItemView: UIView, UIThemable {
        
        var onTap: VoidClosure?
        
        private let icon: UIThemed<UIImage>
        private let iconImage = UIImageView()
        private let titleLabel: Label
        private let arrowImage = UIImageView()
        private let borderView = UIView()
        private let hasArrow: Bool
        private let hasBottomBorder: Bool
        
        init(icon: UIThemed<UIImage>,
             title: String,
             hasArrow: Bool,
             hasBottomBorder: Bool) {
            self.icon = icon
            self.titleLabel = Label(text: title, font: .poppins(14))
            self.hasArrow = hasArrow
            self.hasBottomBorder = hasBottomBorder
            super.init(frame: .zero)
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(iconImage)
            addSubview(titleLabel)
            if hasArrow {
                addSubview(arrowImage)
            }
            
            setDimension(.height, toSize: 48)
            
            iconImage.pinEdge(toSuperviewEdge: .left, withInset: 16)
            iconImage.alignView(y: .center(offset: 0))
            
            titleLabel.pinEdge(toSuperviewEdge: .left, withInset: 45)
            titleLabel.alignView(y: .center(offset: 0))
            
            if hasArrow {
                arrowImage.alignView(x: .right(offset: 16))
                arrowImage.alignView(y: .center(offset: 0))
            }
            
            if hasBottomBorder {
                addSubview(borderView)
                borderView.setDimension(.height, toSize: 1)
                borderView.pinEdgesToSuperviewEdges(excludingEdge: .top)
            }
            
            addGestureRecognizer(
                UITapGestureRecognizer(target: self, action: #selector(didTap))
            )
            
            themeProvider.register(observer: self)
        }
        
        func apply(theme: UITheme) {
            iconImage.image = icon.value
            backgroundColor = .themed.goeridio.value
            if hasArrow {
                arrowImage.image = .themed.arrowRight.value
            }
            if hasBottomBorder {
                borderView.backgroundColor = .themed.xero.value
            }
        }
        
        @objc
        private func didTap() {
            onTap?()
        }
    }
}

