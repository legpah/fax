//
//  SendingFaxRouterModel.swift
//  FaxApp
//
//  Created by Eugene on 23.05.2022.
//

import Foundation

struct SendingFaxRouterModel {
    let recipientPhone: String
    let recepientCountry: String
    let pdfObject: PDFMultiplePagesObject
}

