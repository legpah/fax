//
//  KeyboardListenerWrapper.swift
//  FaxApp
//
//  Created by Eugene on 14.06.2022.
//

import UIKit

final class ResponderVisibilityManager {
    
    enum RelativeViewMovingType {
        case transform
        case bounds
    }
    
    // MARK: - Internal properties
    
    /// Расстояние между `relativeToView` и клавиатурой
    var bottomOffset: CGFloat
    
    // MARK: - Private properties
    
    private enum State {
        case holding
        case listening
    }
    
    private let viewsToTrack: [UIView]
    private let relativeToView: UIView
    private let movingType: RelativeViewMovingType
    private let listener = KeyboardListener()
    private var state: State = .holding
    
    private var anyFirstResponderView: UIView? {
        return viewsToTrack.first(where: { $0.isFirstResponder })
    }
    
    // MARK: - Init
    
    init(viewsToTrack: [UIView],
         relativeToView: UIView,
         movingType: RelativeViewMovingType,
         bottomOffset: CGFloat = 10) {
        self.bottomOffset = bottomOffset
        self.viewsToTrack = viewsToTrack
        self.movingType = movingType
        self.relativeToView = relativeToView
        configureEventsHandlers()
    }
    
    // MARK: - Internal methods
    
    func start() {
        guard state != .listening else {
            return
        }
        state = .listening
        listener.start()
    }
    
    func stop() {
        guard state != .holding else {
            return
        }
        state = .holding
        listener.stop()
    }
    
    // MARK: - Private methods
    
    private func configureEventsHandlers() {
        listener
            .on(event: .willShow) { [weak self] options in
            self?.onKeyboardShow(options: options)
        }
        .on(event: .willHide) { [weak self] options in
            self?.onKeyboardHide(options: options)
        }
    }
    
    private func onKeyboardShow(options: KeyboardListener.KeyboardOptions) {
        guard let firstResponderView = anyFirstResponderView else {
            return
        }
        
        let firstResponderViewMaxY = firstResponderView.globalPoint.y + firstResponderView.frame.height
        guard firstResponderViewMaxY + bottomOffset >= options.endFrame.origin.y else {
            return
        }
        
        let relativeViewYTranslation = options.endFrame.origin.y - (firstResponderViewMaxY + bottomOffset)
        
        UIView.animate(withDuration: options.animationDuration) {
            switch self.movingType {
            case .transform:
                self.relativeToView.transform = .init(translationX: 0, y: relativeViewYTranslation)
            case .bounds:
                self.relativeToView.bounds.origin.y = self.relativeToView.bounds.origin.y - relativeViewYTranslation
            }
        }
    }
    
    private func onKeyboardHide(options: KeyboardListener.KeyboardOptions) {
        UIView.animate(withDuration: options.animationDuration) {
            switch self.movingType {
            case .transform:
                self.relativeToView.transform = .identity
            case .bounds:
                self.relativeToView.bounds.origin.y = 0
            }
        }
    }
}
