//
//  FaxHistorySectionModel.swift
//  FaxApp
//
//  Created by Eugene on 29.04.2022.
//

import UIKit

enum FaxHistorySectionModel: Int, DiffableTableSection {
    
    var sectionIndex: Int { rawValue }
    
    case first
    
    func createHeader() -> UIView? {
        nil
    }
}
