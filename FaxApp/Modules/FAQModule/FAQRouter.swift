//
//  FAQRouter.swift
//  FaxApp
//
//  Created by Eugene on 24.04.2022.
//

import UIKit

protocol FAQRouter: CloseRouterCoordinator {}

final class FAQRouterImpl: BaseScreenRouter, ScreenRouter {
    
    var screen: UIViewController { _screen }
    let routerIdentifiable: RouterIdentifiable = RouterType.faq
    let presentationType: ScreenPresentationType = .modal

    private lazy var _screen: UIViewController = {
        let presenter = FAQPresenterImpl(router: self)
        let view = FAQViewControlerImpl(presenter: presenter)
        presenter.view = view
        view.navigationItem.title = L10n.Faq.title
        return NavigationController(rootViewController: view, bgColor: .bw)
    }()
}

extension FAQRouterImpl: FAQRouter {}
