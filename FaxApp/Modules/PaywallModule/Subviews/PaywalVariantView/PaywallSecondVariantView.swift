//
//  PaywallSecondVariantView.swift
//  FaxApp
//
//  Created by Eugene on 14.07.2022.
//

import Foundation
import UIKit

final class PaywallSecondVariantView: UIView, PaywallVariantView {
    var itemsContainer: PremiumItemsContainer {
        premiumItemsView
    }
    
    private lazy var titleLabel = Label(
        text: L10n.Paywall.Second.title,
        font: .poppins(size.value(36, 24, 24), .semiBold),
        lines: 0
    )
    private let reverseKeyImage = ImageView(themedImage: .paywallReverseKey)
    private let starsContainer = UIView()
    private lazy var firstStarView = StarItemView(
        size: size,
        title: L10n.Paywall.Second.Star.First.title,
        subtitle:  L10n.Paywall.Second.Star.First.subtitle
    )
    private lazy var secondStarView = StarItemView(
        size: size,
        title: L10n.Paywall.Second.Star.Second.title,
        subtitle: L10n.Paywall.Second.Star.Second.subtitle
    )
    private lazy var thirdStarView = StarItemView(
        size: size,
        title: L10n.Paywall.Second.Star.Third.title,
        subtitle:  L10n.Paywall.Second.Star.Third.subtitle
    )
    
    private let premiumItemsView = PremiumItemsContainerImpl()
    
    private let size: PaywallSizeVariant
    
    init(size: PaywallSizeVariant) {
        self.size = size
        super.init(frame: .zero)
        configureSelf()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func configureSelf() {
        addSubview(titleLabel)
        addSubview(starsContainer)
        addSubview(reverseKeyImage)
        starsContainer.addSubview(firstStarView)
        starsContainer.addSubview(secondStarView)
        if size != .small {
            starsContainer.addSubview(thirdStarView)
        }
        addSubview(premiumItemsView)
        
        configureConstraints()
    }
    
    private func configureConstraints() {
        titleLabel.pinEdge(toSuperviewEdge: .top, withInset: size.value(25, 25, 16))
        titleLabel.pinEdges(toSuperviewEdges: [.left, .right], withInset: 24)
        
        reverseKeyImage.pinEdge(toSuperviewEdge: .right)
        reverseKeyImage.setDimensions(
            to: size.value(
                CGSize(width: 153, height: 270),
                CGSize(width: 108, height: 237),
                CGSize(width: 108, height: 237)
            )
        )
        reverseKeyImage.alignAxis(.y, toSameAxisOf: starsContainer)
        
        starsContainer.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: size.value(42, 33, 33))
        starsContainer.pinEdge(toSuperviewEdge: .left, withInset: 24)
        starsContainer.pinEdge(.right, to: .left, of: reverseKeyImage, withOffset: size.value(10, 0, 0))
        
        firstStarView.pinEdges(toSuperviewEdges: [.top, .left, .right])
        
        secondStarView.pinEdge(.top, to: .bottom, of: firstStarView, withOffset: size.value(24, 17, 16))
        secondStarView.pinEdges(toSuperviewEdges: [.left, .right])
        if size == .small {
            secondStarView.pinEdge(toSuperviewEdge: .bottom)
        }
        
        if size != .small {
            thirdStarView.pinEdge(.top, to: .bottom, of: secondStarView, withOffset: 16)
            thirdStarView.pinEdges(toSuperviewEdges: [.left, .right])
            thirdStarView.pinEdge(toSuperviewEdge: .bottom)
        }
        
        premiumItemsView.pinEdge(.top, to: .bottom, of: starsContainer, withOffset: size.value(40, 30, 28))
        premiumItemsView.pinEdges(toSuperviewEdges: [.left, .right])
        premiumItemsView.pinEdge(toSuperviewEdge: .bottom, withInset: 5, relation: .greaterThanOrEqual)
    }
}

extension PaywallSecondVariantView {
    fileprivate final class StarItemView: UIView {
        
        private let size: PaywallSizeVariant
        private let starImage = ImageView(themedImage: .paywallStar)
        private lazy var titleLabel = Label(
            font: .poppins(size.value(22, 18, 18), .semiBold),
            lines: 0
        )
        private let subtitleLabel = Label(
            font: .poppins(12),
            lines: 0
        )
        
        init(size: PaywallSizeVariant, title: String, subtitle: String) {
            self.size = size
            super.init(frame: .zero)
            titleLabel.text = title
            subtitleLabel.text = subtitle
            configureSelf()
        }
        
        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
        
        private func configureSelf() {
            addSubview(starImage)
            addSubview(titleLabel)
            addSubview(subtitleLabel)
            
            starImage.setDimensions(to: CGSize(width: 27, height: 24))
            starImage.pinEdges(toSuperviewEdges: [.left, .top])
            
            titleLabel.pinEdge(toSuperviewEdge: .top)
            titleLabel.pinEdge(.left, to: .right, of: starImage, withOffset: 16)
            titleLabel.pinEdge(toSuperviewEdge: .right)
            
            subtitleLabel.pinEdge(.top, to: .bottom, of: titleLabel, withOffset: 4)
            subtitleLabel.pinEdge(.left, to: .left, of: titleLabel)
            subtitleLabel.pinEdge(toSuperviewEdge: .right)
            
            self.pinEdge(.bottom, to: .bottom, of: subtitleLabel)
        }
    }
}
