//
//  PDFUIView.swift
//  FaxApp
//
//  Created by Eugene on 15.04.2022.
//

import UIKit
import PDFKit

final class PDFUIView: UIView {
    
    var scrollView: UIScrollView? {
        pdfView.subviews.first as? UIScrollView
    }
    
    var autoScales: Bool = true {
        didSet { pdfView.autoScales = autoScales }
    }
    
    var pdfBackgroundColor: UIColor = .clear {
        didSet { pdfView.backgroundColor = pdfBackgroundColor }
    }
    
    var document: PDFDocument? {
        get { pdfView.document }
        set { pdfView.document = newValue }
    }
    
    var onPageNumberChange: IClosure<Int>?
    var onPdfLoad: VoidClosure?
    
    private let pdfView = PDFView(frame: PDFConstants.pageFrame)
    private var isFirstNumberChange: Bool = true
    
    init() {
        super.init(frame: PDFConstants.pageFrame)
        configurePDFView()
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(didChangePage),
            name: NSNotification.Name.PDFViewPageChanged,
            object: nil
        )
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func scrollToTop(animated: Bool) {
        self.scrollView?.setContentOffset(.zero, animated: animated)
    }
    
    private func configurePDFView() {
        addSubview(pdfView)
        pdfView.pageShadowsEnabled = false
        pdfView.autoScales = autoScales
        pdfView.backgroundColor = pdfBackgroundColor
        pdfView.pinEdgesToSuperviewEdges()
    }
    
    @objc
    private func didChangePage() {
        if isFirstNumberChange && pdfView.visiblePages.isNotEmpty {
            onPdfLoad?()
            isFirstNumberChange = false
        }
        
        if let currentPage = pdfView.currentPage {
            let pageNumber = pdfView.document?.index(for: currentPage) ?? 0
            onPageNumberChange?(pageNumber + 1)
        }
    }
}
