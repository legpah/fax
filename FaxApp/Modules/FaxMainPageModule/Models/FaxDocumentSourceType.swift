//
//  FaxDocumentSourceType.swift
//  FaxApp
//
//  Created by Eugene on 14.04.2022.
//

import Foundation

enum FaxDocumentSourceType: CaseIterable {
    case frontPage
    case camera
    case scanner
    case photos
    case files
    
    var localize: String {
        switch self {
        case .frontPage:
            return L10n.Main.Alert.page
        case .camera:
            return L10n.Main.Alert.camera
        case .scanner:
            return L10n.Main.Alert.scanner
        case .photos:
            return L10n.Main.Alert.gallery
        case .files:
            return L10n.Main.Alert.icloud
        }
    }
}

extension FaxDocumentSourceType {
    init?(string: String) {
        guard let type = FaxDocumentSourceType.allCases.first(where: { $0.localize.lowercased() == string.lowercased() }) else {
            return nil
        }
        self = type
    }
}
