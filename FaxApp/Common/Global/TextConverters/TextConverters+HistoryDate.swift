//
//  TextConverters+HistoryDate.swift
//  FaxApp
//
//  Created by Eugene on 27.05.2022.
//

import Foundation

extension TextConvertersTag {
    static func historyDate(timestamp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        
        let calendar = Calendar.current
        if calendar.isDateInToday(date) {
            let time = DateFormatter.hhmmFormatter.string(from: date)
            return "\(L10n.History.today), \(time)"
        } else if calendar.isDateInYesterday(date) {
            let time = DateFormatter.hhmmFormatter.string(from: date)
            return "\(L10n.History.yesterday), \(time)"
        } else {
            return DateFormatter.historyDateFormatter.string(from: date)
        }
    }
}
