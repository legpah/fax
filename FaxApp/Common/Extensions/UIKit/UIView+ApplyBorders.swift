//
//  UIView+ApplyBorders.swift
//  FaxApp
//
//  Created by Eugene on 13.04.2022.
//

import UIKit

extension UIView {
    func applyBorders(color: UIThemed<UIColor>?) {
        layer.borderColor = color?.value.cgColor
    }
    
    func applyBordersShape(width: CGFloat,
                           radius: CGFloat,
                           masksToBounds: Bool = true) {
        layer.borderWidth = width
        layer.cornerRadius = radius
        layer.masksToBounds = masksToBounds
    }
}
