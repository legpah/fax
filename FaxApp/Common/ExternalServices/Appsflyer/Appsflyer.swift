//
//  Appsflyer.swift
//  FaxApp
//
//  Created by Eugene on 18.05.2022.
//

import Foundation
import AppsFlyerLib
import Combine
import Adapty

final class Appsflyer: NSObject {
    
    static let shared = Appsflyer()
    
    private var cancelBag = Set<AnyCancellable>()
    
    func configure() {
        AppsFlyerLib.shared().appsFlyerDevKey = "FVNQ5k6ARz9gfd8sEjR3sS"
        AppsFlyerLib.shared().appleAppID = "1622799610"
        AppsFlyerLib.shared().waitForATTUserAuthorization(timeoutInterval: 80)
        AppsFlyerLib.shared().customerUserID = UserUIDProviderImpl.shared.uuid
        AppsFlyerLib.shared().delegate = self
        
        #if DEBUG
        AppsFlyerLib.shared().isDebug = true
        #else
        AppsFlyerLib.shared().isDebug = false
        #endif
        
        IDFATrackingManager.shared.idfaStatusPublisher.sink { status in
            AppsFlyerLib.shared().disableAdvertisingIdentifier = !status
        }.store(in: &cancelBag)
    }
    
    func appDidBecomeActive() {
        AppsFlyerLib.shared().start()
    }
    
}

extension Appsflyer: AppsFlyerLibDelegate {
    
    func onConversionDataFail(_ error: Error) {
        debugPrint(error)
    }
    
    func onConversionDataSuccess(_ conversionInfo: [AnyHashable : Any]) {
        Adapty.updateAttribution(conversionInfo, source: .appsflyer, networkUserId: AppsFlyerLib.shared().getAppsFlyerUID())
    }
    
}
