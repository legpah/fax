// swiftlint:disable all
// Generated using SwiftGen — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command file_length implicit_return prefer_self_in_static_references

// MARK: - Strings

// swiftlint:disable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:disable nesting type_body_length type_name vertical_whitespace_opening_braces
internal enum L10n {
  internal enum Alerts {
    internal enum Country {
      /// Check out
      internal static let button = L10n.tr("Localizable", "alerts.country.button", fallback: "Check out")
      internal enum Code {
        /// Phone number should stars with '+' symbol
        internal static let subtitle = L10n.tr("Localizable", "alerts.country.code.subtitle", fallback: "Phone number should stars with '+' symbol")
        /// Unable to determine country code
        internal static let title = L10n.tr("Localizable", "alerts.country.code.title", fallback: "Unable to determine country code")
      }
      internal enum Unable {
        /// Check out available counties
        internal static let subTitle = L10n.tr("Localizable", "alerts.country.unable.subTitle", fallback: "Check out available counties")
        /// This country is not available to fax
        internal static let title = L10n.tr("Localizable", "alerts.country.unable.title", fallback: "This country is not available to fax")
      }
    }
    internal enum Documents {
      /// Choose document source
      internal static let title = L10n.tr("Localizable", "alerts.documents.title", fallback: "Choose document source")
    }
    internal enum History {
      internal enum Clear {
        /// You can't revert this action
        internal static let subTitle = L10n.tr("Localizable", "alerts.history.clear.subTitle", fallback: "You can't revert this action")
        /// Are you sure?
        internal static let title = L10n.tr("Localizable", "alerts.history.clear.title", fallback: "Are you sure?")
      }
    }
    internal enum MailPicker {
      /// Check your login in apple's mail app
      internal static let subTitle = L10n.tr("Localizable", "alerts.mailPicker.subTitle", fallback: "Check your login in apple's mail app")
      /// Unable to open mail app
      internal static let title = L10n.tr("Localizable", "alerts.mailPicker.title", fallback: "Unable to open mail app")
    }
    internal enum Main {
      internal enum Delete {
        /// Confirm
        internal static let confirm = L10n.tr("Localizable", "alerts.main.delete.confirm", fallback: "Confirm")
        /// All added files will be deleted from the current set. Are you sure?
        internal static let title = L10n.tr("Localizable", "alerts.main.delete.title", fallback: "All added files will be deleted from the current set. Are you sure?")
      }
      internal enum Gallery {
        /// Settings
        internal static let settings = L10n.tr("Localizable", "alerts.main.gallery.settings", fallback: "Settings")
        /// Permission denied, please allow our app permission through Settings in your phone
        internal static let subTitle = L10n.tr("Localizable", "alerts.main.gallery.subTitle", fallback: "Permission denied, please allow our app permission through Settings in your phone")
        /// Permission Error
        internal static let title = L10n.tr("Localizable", "alerts.main.gallery.title", fallback: "Permission Error")
      }
      internal enum Limit {
        /// You may send %d or less pages at once
        internal static func subtitle(_ p1: Int) -> String {
          return L10n.tr("Localizable", "alerts.main.limit.subtitle", p1, fallback: "You may send %d or less pages at once")
        }
        /// Pages limit exceeded
        internal static let title = L10n.tr("Localizable", "alerts.main.limit.title", fallback: "Pages limit exceeded")
      }
      internal enum SizeLimit {
        /// The maximum fax size must not exceed %f MB
        internal static func subTitle(_ p1: Float) -> String {
          return L10n.tr("Localizable", "alerts.main.sizeLimit.subTitle", p1, fallback: "The maximum fax size must not exceed %f MB")
        }
        /// Fax size limit exceeded
        internal static let title = L10n.tr("Localizable", "alerts.main.sizeLimit.title", fallback: "Fax size limit exceeded")
      }
      internal enum Unexpected {
        /// Unexpected error occured!
        internal static let title = L10n.tr("Localizable", "alerts.main.unexpected.title", fallback: "Unexpected error occured!")
      }
    }
    internal enum Paywall {
      internal enum Purchase {
        /// Retry
        internal static let retry = L10n.tr("Localizable", "alerts.paywall.purchase.retry", fallback: "Retry")
        /// Failed to purchase subscription!
        internal static let title = L10n.tr("Localizable", "alerts.paywall.purchase.title", fallback: "Failed to purchase subscription!")
      }
      internal enum Restore {
        /// Purchase restored successfully!
        internal static let successfull = L10n.tr("Localizable", "alerts.paywall.restore.successfull", fallback: "Purchase restored successfully!")
        /// Failed to restore purchases
        internal static let title = L10n.tr("Localizable", "alerts.paywall.restore.title", fallback: "Failed to restore purchases")
      }
    }
    internal enum Picker {
      /// Unable to open unsupported camera source
      internal static let title = L10n.tr("Localizable", "alerts.picker.title", fallback: "Unable to open unsupported camera source")
    }
    internal enum Sending {
      internal enum Push {
        /// Cancel
        internal static let cancel = L10n.tr("Localizable", "alerts.sending.push.cancel", fallback: "Cancel")
        /// Ok, if you want to enable push notifications, go to settings
        internal static let nextTitle = L10n.tr("Localizable", "alerts.sending.push.nextTitle", fallback: "Ok, if you want to enable push notifications, go to settings")
        /// No
        internal static let no = L10n.tr("Localizable", "alerts.sending.push.no", fallback: "No")
        /// Do you want to know when the recipient will receive your fax?
        internal static let title = L10n.tr("Localizable", "alerts.sending.push.title", fallback: "Do you want to know when the recipient will receive your fax?")
        /// Yes
        internal static let yes = L10n.tr("Localizable", "alerts.sending.push.yes", fallback: "Yes")
      }
    }
  }
  internal enum Boarding {
    /// And images to send
    internal static let andImagesToSend = L10n.tr("Localizable", "boarding.and_images_to_send", fallback: "And images to send")
    /// Before sending
    internal static let beforeSending = L10n.tr("Localizable", "boarding.before_sending", fallback: "Before sending")
    /// Choose between
    internal static let chooseBetween = L10n.tr("Localizable", "boarding.choose_between", fallback: "Choose between")
    /// From iphone
    internal static let fromIphone = L10n.tr("Localizable", "boarding.from_iphone", fallback: "From iphone")
    /// Light and Dark mode
    internal static let lightAndDarkMode = L10n.tr("Localizable", "boarding.light_and_dark_mode", fallback: "Light and Dark mode")
    /// Preview your Fax
    internal static let previewYourFax = L10n.tr("Localizable", "boarding.preview_your_fax", fallback: "Preview your Fax")
    /// Send Faxes
    internal static let sendFaxes = L10n.tr("Localizable", "boarding.send_Faxes", fallback: "Send Faxes")
    /// Track all
    internal static let trackAll = L10n.tr("Localizable", "boarding.track_all", fallback: "Track all")
    /// Upload any documents
    internal static let uploadAnyDocuments = L10n.tr("Localizable", "boarding.upload_any_documents", fallback: "Upload any documents")
    /// Your sendings
    internal static let yourSendings = L10n.tr("Localizable", "boarding.your_sendings", fallback: "Your sendings")
  }
  internal enum Country {
    /// Loading...
    internal static let loading = L10n.tr("Localizable", "country.loading", fallback: "Loading...")
    /// Search...
    internal static let search = L10n.tr("Localizable", "country.search", fallback: "Search...")
    /// Country
    internal static let title = L10n.tr("Localizable", "country.title", fallback: "Country")
    /// Enter valid number
    internal static let validNumber = L10n.tr("Localizable", "country.valid_number", fallback: "Enter valid number")
  }
  internal enum Error {
    internal enum SmtWrong {
      /// We have tried to send your fax repeatedly, but fax machine did not connect to recipient. Check your internet connection and try again.
      internal static let subtitle = L10n.tr("Localizable", "error.smt_wrong.subtitle", fallback: "We have tried to send your fax repeatedly, but fax machine did not connect to recipient. Check your internet connection and try again.")
      /// Something went wrong
      internal static let title = L10n.tr("Localizable", "error.smt_wrong.title", fallback: "Something went wrong")
    }
    internal enum WrongNumber {
      /// Wrong phone number
      internal static let title = L10n.tr("Localizable", "error.wrong_number.title", fallback: "Wrong phone number")
    }
  }
  internal enum Faq {
    /// 
    /// This app allows you to send faxes using your IOS device. The recepient will get a message like from real fax machine.
    /// 
    internal static let answer1 = L10n.tr("Localizable", "faq.answer1", fallback: "\nThis app allows you to send faxes using your IOS device. The recepient will get a message like from real fax machine.\n")
    /// 
    /// You should have active subscription to send fax.
    /// If your subscription is activated, follow the steps below:
    /// - Choose a region of reciepent’s fax number
    /// - Enter the recepient’s fax number or choose it from your contacts
    /// - Upload a document you want to send
    /// - Click %@ to check a file before sending
    /// - Click %@ to check the main information of sending
    /// - Click %@ to send Fax
    /// 
    internal static func answer2(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
      return L10n.tr("Localizable", "faq.answer2", String(describing: p1), String(describing: p2), String(describing: p3), fallback: "\nYou should have active subscription to send fax.\nIf your subscription is activated, follow the steps below:\n- Choose a region of reciepent’s fax number\n- Enter the recepient’s fax number or choose it from your contacts\n- Upload a document you want to send\n- Click %@ to check a file before sending\n- Click %@ to check the main information of sending\n- Click %@ to send Fax\n")
    }
    /// 
    /// There are some oppotunities to do this:
    /// - Click %@ at the left top of main screen
    /// - Click on the icon of uploaded document on the main screen
    /// - Click %@ on the %@ screen
    /// 
    internal static func answer3(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
      return L10n.tr("Localizable", "faq.answer3", String(describing: p1), String(describing: p2), String(describing: p3), fallback: "\nThere are some oppotunities to do this:\n- Click %@ at the left top of main screen\n- Click on the icon of uploaded document on the main screen\n- Click %@ on the %@ screen\n")
    }
    /// 
    /// It depends on recipient’s line quality and the number of pages you sent. As usual, it takes 2 minute to send one page of a document.
    /// 
    internal static let answer4 = L10n.tr("Localizable", "faq.answer4", fallback: "\nIt depends on recipient’s line quality and the number of pages you sent. As usual, it takes 2 minute to send one page of a document.\n")
    /// 
    /// If you have not received a notification about your fax, make sure that you enable notifications of our app in your device settings.
    /// If you didn’t allow the app to send you notification, you can easily solve this issue, just follow the steps bellow:
    /// - Go to %@ screen
    /// - Switch the slider in Notifications
    /// - Still have a problem? Contact us by a %@ button in %@ screen
    /// 
    internal static func answer5(_ p1: Any, _ p2: Any, _ p3: Any) -> String {
      return L10n.tr("Localizable", "faq.answer5", String(describing: p1), String(describing: p2), String(describing: p3), fallback: "\nIf you have not received a notification about your fax, make sure that you enable notifications of our app in your device settings.\nIf you didn’t allow the app to send you notification, you can easily solve this issue, just follow the steps bellow:\n- Go to %@ screen\n- Switch the slider in Notifications\n- Still have a problem? Contact us by a %@ button in %@ screen\n")
    }
    /// 
    /// We store you faxes for 90 days. Your confidence is very imortant for us, that’s why we permanently delete all you sendings after 90 days.
    /// 
    internal static let answer6 = L10n.tr("Localizable", "faq.answer6", fallback: "\nWe store you faxes for 90 days. Your confidence is very imortant for us, that’s why we permanently delete all you sendings after 90 days.\n")
    /// 
    /// We are really sorry for this. You always can contact us by a %@ button in %@ screen. We will aswer you as soon as possible.
    /// 
    internal static func answer7(_ p1: Any, _ p2: Any) -> String {
      return L10n.tr("Localizable", "faq.answer7", String(describing: p1), String(describing: p2), fallback: "\nWe are really sorry for this. You always can contact us by a %@ button in %@ screen. We will aswer you as soon as possible.\n")
    }
    /// 
    /// It depends on recipient’s line quality and the number of pages you sent. As usual, it takes 2 minute to send one page of a document.
    /// 
    internal static let answer8 = L10n.tr("Localizable", "faq.answer8", fallback: "\nIt depends on recipient’s line quality and the number of pages you sent. As usual, it takes 2 minute to send one page of a document.\n")
    /// What is the application for?
    internal static let question1 = L10n.tr("Localizable", "faq.question1", fallback: "What is the application for?")
    /// How to send a Fax?
    internal static let question2 = L10n.tr("Localizable", "faq.question2", fallback: "How to send a Fax?")
    /// How can I preview a fax before sending?
    internal static let question3 = L10n.tr("Localizable", "faq.question3", fallback: "How can I preview a fax before sending?")
    /// How much time does it take to send a fax?
    internal static let question4 = L10n.tr("Localizable", "faq.question4", fallback: "How much time does it take to send a fax?")
    /// If there was no notification, does it mean that the fax was not delivered?
    internal static let question5 = L10n.tr("Localizable", "faq.question5", fallback: "If there was no notification, does it mean that the fax was not delivered?")
    /// How long is history of sendings kept?
    internal static let question6 = L10n.tr("Localizable", "faq.question6", fallback: "How long is history of sendings kept?")
    /// I have not found any answer for my question
    internal static let question7 = L10n.tr("Localizable", "faq.question7", fallback: "I have not found any answer for my question")
    /// How much time does it take to send a fax?
    internal static let question8 = L10n.tr("Localizable", "faq.question8", fallback: "How much time does it take to send a fax?")
    /// FAQ
    internal static let title = L10n.tr("Localizable", "faq.title", fallback: "FAQ")
  }
  internal enum FrontPage {
    /// %d characters left
    internal static func charactersLeft(_ p1: Int) -> String {
      return L10n.tr("Localizable", "frontPage.characters_left", p1, fallback: "%d characters left")
    }
    /// Contact
    internal static let contact = L10n.tr("Localizable", "frontPage.contact", fallback: "Contact")
    /// Create
    internal static let create = L10n.tr("Localizable", "frontPage.create", fallback: "Create")
    /// From
    internal static let from = L10n.tr("Localizable", "frontPage.from", fallback: "From")
    /// Front Page
    internal static let frontPage = L10n.tr("Localizable", "frontPage.front_page", fallback: "Front Page")
    /// Message
    internal static let message = L10n.tr("Localizable", "frontPage.message", fallback: "Message")
    /// Name
    internal static let name = L10n.tr("Localizable", "frontPage.name", fallback: "Name")
    /// Document preview
    internal static let preview = L10n.tr("Localizable", "frontPage.preview", fallback: "Document preview")
    /// Sender
    internal static let sender = L10n.tr("Localizable", "frontPage.sender", fallback: "Sender")
    /// Show preview
    internal static let showPreview = L10n.tr("Localizable", "frontPage.show_preview", fallback: "Show preview")
    /// Title
    internal static let title = L10n.tr("Localizable", "frontPage.title", fallback: "Title")
    /// Type here...
    internal static let typeHere = L10n.tr("Localizable", "frontPage.type_here", fallback: "Type here...")
  }
  internal enum GenericInform {
    internal enum Done {
      /// It will take about %@ minutes to send your fax. You will receive a notification when the fax is delivered. You can check the status of your send in the %@ screen
      internal static func subTitle(_ p1: Any, _ p2: Any) -> String {
        return L10n.tr("Localizable", "genericInform.done.subTitle", String(describing: p1), String(describing: p2), fallback: "It will take about %@ minutes to send your fax. You will receive a notification when the fax is delivered. You can check the status of your send in the %@ screen")
      }
      /// Fax accepted for sending
      internal static let title = L10n.tr("Localizable", "genericInform.done.title", fallback: "Fax accepted for sending")
    }
    internal enum Error {
      internal enum Expired {
        /// Unfortunately, your subscription has expired and you can't send faxes by our app. Please renew your subscription here to restore functions.
        internal static let subTitle = L10n.tr("Localizable", "genericInform.error.expired.subTitle", fallback: "Unfortunately, your subscription has expired and you can't send faxes by our app. Please renew your subscription here to restore functions.")
        /// Subscription expired
        internal static let title = L10n.tr("Localizable", "genericInform.error.expired.title", fallback: "Subscription expired")
      }
      internal enum Number {
        /// The number of recipient is incorrect. Make sure that you entered the correct number and try again.
        internal static let subTitle = L10n.tr("Localizable", "genericInform.error.number.subTitle", fallback: "The number of recipient is incorrect. Make sure that you entered the correct number and try again.")
        /// Wrong phone number
        internal static let title = L10n.tr("Localizable", "genericInform.error.number.title", fallback: "Wrong phone number")
      }
      internal enum Wrong {
        /// Something went wrong
        internal static let title = L10n.tr("Localizable", "genericInform.error.wrong.title", fallback: "Something went wrong")
      }
    }
    internal enum History {
      /// You haven’t sent a fax yet
      internal static let empty = L10n.tr("Localizable", "genericInform.history.empty", fallback: "You haven’t sent a fax yet")
      internal enum Empty {
        /// Need to send Fax?
        internal static let button = L10n.tr("Localizable", "genericInform.history.empty.button", fallback: "Need to send Fax?")
      }
    }
    internal enum NoInternet {
      /// Please stable your network connection and try again
      internal static let subTitle = L10n.tr("Localizable", "genericInform.no_internet.subTitle", fallback: "Please stable your network connection and try again")
      /// Internet connection lost
      internal static let title = L10n.tr("Localizable", "genericInform.no_internet.title", fallback: "Internet connection lost")
    }
    internal enum Purchase {
      /// Start Faxing
      internal static let button = L10n.tr("Localizable", "genericInform.purchase.button", fallback: "Start Faxing")
      /// Our team created this app to help people reduce the time spent queuing at FedEx and other similar companies. We hope that this application will help you save your time and make sending fax for you as convenient as possible.
      internal static let subTitle = L10n.tr("Localizable", "genericInform.purchase.subTitle", fallback: "Our team created this app to help people reduce the time spent queuing at FedEx and other similar companies. We hope that this application will help you save your time and make sending fax for you as convenient as possible.")
      /// Thank you for your purchase!
      internal static let title = L10n.tr("Localizable", "genericInform.purchase.title", fallback: "Thank you for your purchase!")
    }
  }
  internal enum History {
    /// Clear all
    internal static let clear = L10n.tr("Localizable", "history.clear", fallback: "Clear all")
    /// Delivering %d pages
    internal static func deliveringCount(_ p1: Int) -> String {
      return L10n.tr("Localizable", "history.delivering_count", p1, fallback: "Delivering %d pages")
    }
    /// Delivering 1 page
    internal static let deliveringCount1 = L10n.tr("Localizable", "history.delivering_count_1", fallback: "Delivering 1 page")
    /// We permanently remove your faxes from history after 90 days, as your privacy and security is our number one priority.
    internal static let desc = L10n.tr("Localizable", "history.desc", fallback: "We permanently remove your faxes from history after 90 days, as your privacy and security is our number one priority.")
    /// Not delivered
    internal static let notDelivered = L10n.tr("Localizable", "history.not_delivered", fallback: "Not delivered")
    /// Sended PDF
    internal static let sended = L10n.tr("Localizable", "history.sended", fallback: "Sended PDF")
    /// Sent %d pages
    internal static func sentCount(_ p1: Int) -> String {
      return L10n.tr("Localizable", "history.sent_count", p1, fallback: "Sent %d pages")
    }
    /// Sent 1 page
    internal static let sentCount1 = L10n.tr("Localizable", "history.sent_count_1", fallback: "Sent 1 page")
    /// History
    internal static let title = L10n.tr("Localizable", "history.title", fallback: "History")
    /// Today
    internal static let today = L10n.tr("Localizable", "history.today", fallback: "Today")
    /// Try again
    internal static let tryAgain = L10n.tr("Localizable", "history.try_again", fallback: "Try again")
    /// Yesterday
    internal static let yesterday = L10n.tr("Localizable", "history.yesterday", fallback: "Yesterday")
  }
  internal enum Main {
    /// Add file
    internal static let addFile = L10n.tr("Localizable", "main.add_file", fallback: "Add file")
    /// Clear
    internal static let clear = L10n.tr("Localizable", "main.clear", fallback: "Clear")
    /// Document
    internal static let document = L10n.tr("Localizable", "main.document", fallback: "Document")
    /// Document preview
    internal static let documentPreview = L10n.tr("Localizable", "main.document_preview", fallback: "Document preview")
    /// Documents
    internal static let documents = L10n.tr("Localizable", "main.documents", fallback: "Documents")
    /// Edit
    internal static let edit = L10n.tr("Localizable", "main.edit", fallback: "Edit")
    /// Fax
    internal static let fax = L10n.tr("Localizable", "main.fax", fallback: "Fax")
    /// of
    internal static let of = L10n.tr("Localizable", "main.of", fallback: "of")
    /// Page
    internal static let page = L10n.tr("Localizable", "main.page", fallback: "Page")
    /// Pages
    internal static let pages = L10n.tr("Localizable", "main.pages", fallback: "Pages")
    /// You have not added files yet
    internal static let placeholder = L10n.tr("Localizable", "main.placeholder", fallback: "You have not added files yet")
    /// Preview
    internal static let preview = L10n.tr("Localizable", "main.preview", fallback: "Preview")
    /// Recipient
    internal static let recipient = L10n.tr("Localizable", "main.recipient", fallback: "Recipient")
    /// Send
    internal static let send = L10n.tr("Localizable", "main.send", fallback: "Send")
    internal enum Alert {
      /// Camera
      internal static let camera = L10n.tr("Localizable", "main.alert.camera", fallback: "Camera")
      /// Gallery
      internal static let gallery = L10n.tr("Localizable", "main.alert.gallery", fallback: "Gallery")
      /// iCloud
      internal static let icloud = L10n.tr("Localizable", "main.alert.icloud", fallback: "iCloud")
      /// Create Front Page
      internal static let page = L10n.tr("Localizable", "main.alert.page", fallback: "Create Front Page")
      /// Scan documents
      internal static let scanner = L10n.tr("Localizable", "main.alert.scanner", fallback: "Scan documents")
    }
  }
  internal enum Paywall {
    /// Close
    internal static let close = L10n.tr("Localizable", "paywall.close", fallback: "Close")
    /// Continue
    internal static let `continue` = L10n.tr("Localizable", "paywall.continue", fallback: "Continue")
    /// Privacy policy
    internal static let privacyPolicy = L10n.tr("Localizable", "paywall.privacy_policy", fallback: "Privacy policy")
    /// Restore
    internal static let restore = L10n.tr("Localizable", "paywall.restore", fallback: "Restore")
    /// Terms of use
    internal static let termsOfUse = L10n.tr("Localizable", "paywall.terms_of_use", fallback: "Terms of use")
    internal enum Fifth {
      /// Premium access
      /// to all
      /// features
      internal static let title = L10n.tr("Localizable", "paywall.fifth.title", fallback: "Premium access\nto all\nfeatures")
      /// Premium access
      /// to all
      /// features
      internal static let title2 = L10n.tr("Localizable", "paywall.fifth.title2", fallback: "Premium access\nto all\nfeatures")
      /// Premium access
      /// to all features
      internal static let title3 = L10n.tr("Localizable", "paywall.fifth.title3", fallback: "Premium access\nto all features")
      internal enum Star {
        internal enum First {
          /// Send as much faxes as you need with active subscription
          internal static let subtitle = L10n.tr("Localizable", "paywall.fifth.star.first.subtitle", fallback: "Send as much faxes as you need with active subscription")
          /// Unlimited number of faxes
          internal static let title = L10n.tr("Localizable", "paywall.fifth.star.first.title", fallback: "Unlimited number of faxes")
        }
        internal enum Second {
          /// Fast and easy worldwide Fax sendings from your phone
          internal static let subtitle = L10n.tr("Localizable", "paywall.fifth.star.second.subtitle", fallback: "Fast and easy worldwide Fax sendings from your phone")
          /// Send Fax to 25+ countries
          internal static let title = L10n.tr("Localizable", "paywall.fifth.star.second.title", fallback: "Send Fax to 25+ countries")
        }
      }
    }
    internal enum First {
      /// PREMIUM
      internal static let premium = L10n.tr("Localizable", "paywall.first.premium", fallback: "PREMIUM")
      /// Without restrictions
      internal static let withoutRestrictions = L10n.tr("Localizable", "paywall.first.without_restrictions", fallback: "Without restrictions")
    }
    internal enum Fourth {
      /// Cancel
      /// Anytime
      internal static let cancelTitle = L10n.tr("Localizable", "paywall.fourth.cancelTitle", fallback: "Cancel\nAnytime")
      /// Premium
      /// Access
      internal static let lockTitle = L10n.tr("Localizable", "paywall.fourth.lockTitle", fallback: "Premium\nAccess")
      /// No more time-wasting in a queue at fax offices! With our app, you can easily send faxes to more than 25 countries with no restrictions!
      internal static let subtitle = L10n.tr("Localizable", "paywall.fourth.subtitle", fallback: "No more time-wasting in a queue at fax offices! With our app, you can easily send faxes to more than 25 countries with no restrictions!")
      /// Unlimited Access
      /// to all features
      internal static let title = L10n.tr("Localizable", "paywall.fourth.title", fallback: "Unlimited Access\nto all features")
    }
    internal enum Second {
      /// Premium access to all features
      internal static let title = L10n.tr("Localizable", "paywall.second.title", fallback: "Premium access to all features")
      internal enum Star {
        internal enum First {
          /// Send as much faxes as you need with active subscription
          internal static let subtitle = L10n.tr("Localizable", "paywall.second.star.first.subtitle", fallback: "Send as much faxes as you need with active subscription")
          /// Unlimited number of faxes
          internal static let title = L10n.tr("Localizable", "paywall.second.star.first.title", fallback: "Unlimited number of faxes")
        }
        internal enum Second {
          /// Fast and easy worldwide Fax sendings from your phone
          internal static let subtitle = L10n.tr("Localizable", "paywall.second.star.second.subtitle", fallback: "Fast and easy worldwide Fax sendings from your phone")
          /// Send Fax to 25+ countries
          internal static let title = L10n.tr("Localizable", "paywall.second.star.second.title", fallback: "Send Fax to 25+ countries")
        }
        internal enum Third {
          /// Check a status or find a document from previous delivery
          internal static let subtitle = L10n.tr("Localizable", "paywall.second.star.third.subtitle", fallback: "Check a status or find a document from previous delivery")
          /// Track all sendings
          internal static let title = L10n.tr("Localizable", "paywall.second.star.third.title", fallback: "Track all sendings")
        }
      }
    }
    internal enum Sixth {
      /// access
      /// to
      internal static let access = L10n.tr("Localizable", "paywall.sixth.access", fallback: "access\nto")
      /// View all plans
      internal static let allPlans = L10n.tr("Localizable", "paywall.sixth.allPlans", fallback: "View all plans")
      ///  Cancel anytime
      internal static let cancelAnytime = L10n.tr("Localizable", "paywall.sixth.cancelAnytime", fallback: " Cancel anytime")
      /// Contact
      internal static let contact = L10n.tr("Localizable", "paywall.sixth.contact", fallback: "Contact")
      /// Delivery
      /// tracking
      internal static let delivery = L10n.tr("Localizable", "paywall.sixth.delivery", fallback: "Delivery\ntracking")
      /// all features
      internal static let features = L10n.tr("Localizable", "paywall.sixth.features", fallback: "all features")
      /// No
      /// limits
      internal static let limit = L10n.tr("Localizable", "paywall.sixth.limit", fallback: "No\nlimits")
      /// Pages
      internal static let pages = L10n.tr("Localizable", "paywall.sixth.pages", fallback: "Pages")
      /// Press
      internal static let press = L10n.tr("Localizable", "paywall.sixth.press", fallback: "Press")
      /// 
      /// and send a fax
      internal static let sendFax = L10n.tr("Localizable", "paywall.sixth.sendFax", fallback: "\nand send a fax")
      /// No more time-wasting in a queue
      /// at fax offices!
      internal static let subtitle = L10n.tr("Localizable", "paywall.sixth.subtitle", fallback: "No more time-wasting in a queue\nat fax offices!")
      /// Time
      internal static let time = L10n.tr("Localizable", "paywall.sixth.time", fallback: "Time")
      /// Unlimited
      internal static let unlim = L10n.tr("Localizable", "paywall.sixth.unlim", fallback: "Unlimited")
      /// Send
      /// worldwide
      internal static let worldwide = L10n.tr("Localizable", "paywall.sixth.worldwide", fallback: "Send\nworldwide")
    }
    internal enum Third {
      /// Premium access
      /// to all features
      internal static let title = L10n.tr("Localizable", "paywall.third.title", fallback: "Premium access\nto all features")
      internal enum Check {
        internal enum First {
          /// Unlimited number of faxes
          internal static let title = L10n.tr("Localizable", "paywall.third.check.first.title", fallback: "Unlimited number of faxes")
        }
        internal enum Second {
          /// Fast and easy worldwide Fax delivery
          internal static let title = L10n.tr("Localizable", "paywall.third.check.second.title", fallback: "Fast and easy worldwide Fax delivery")
        }
        internal enum Third {
          /// Sent fax tracking
          internal static let title = L10n.tr("Localizable", "paywall.third.check.third.title", fallback: "Sent fax tracking")
        }
      }
    }
  }
  internal enum Pdf {
    /// Contact:
    internal static let contact = L10n.tr("Localizable", "pdf.contact", fallback: "Contact:")
    /// From:
    internal static let from = L10n.tr("Localizable", "pdf.from", fallback: "From:")
    /// Message:
    internal static let message = L10n.tr("Localizable", "pdf.message", fallback: "Message:")
    /// Time:
    internal static let time = L10n.tr("Localizable", "pdf.time", fallback: "Time:")
  }
  internal enum Purchase {
    /// Monthly
    internal static let month = L10n.tr("Localizable", "purchase.month", fallback: "Monthly")
    /// Weekly
    internal static let week = L10n.tr("Localizable", "purchase.week", fallback: "Weekly")
    /// Yearly
    internal static let year = L10n.tr("Localizable", "purchase.year", fallback: "Yearly")
  }
  internal enum Sending {
    /// Back
    internal static let back = L10n.tr("Localizable", "sending.back", fallback: "Back")
    /// Delivery time
    internal static let deliveryTime = L10n.tr("Localizable", "sending.delivery_time", fallback: "Delivery time")
    /// Each page will take about %@ minutes to send. We will make up to 5 attempts to deliver your fax.
    internal static func deliveryTip(_ p1: Any) -> String {
      return L10n.tr("Localizable", "sending.delivery_tip", String(describing: p1), fallback: "Each page will take about %@ minutes to send. We will make up to 5 attempts to deliver your fax.")
    }
    /// Done
    internal static let done = L10n.tr("Localizable", "sending.done", fallback: "Done")
    /// Go to paywall
    internal static let goToPaywall = L10n.tr("Localizable", "sending.go_to_paywall", fallback: "Go to paywall")
    /// minutes
    internal static let minutes = L10n.tr("Localizable", "sending.minutes", fallback: "minutes")
    /// Pages
    internal static let pages = L10n.tr("Localizable", "sending.pages", fallback: "Pages")
    /// Preview
    internal static let preview = L10n.tr("Localizable", "sending.preview", fallback: "Preview")
    /// Ready to send
    internal static let readyToSend = L10n.tr("Localizable", "sending.ready_to_send", fallback: "Ready to send")
    /// Recipient
    internal static let recipient = L10n.tr("Localizable", "sending.recipient", fallback: "Recipient")
    /// Retry
    internal static let retry = L10n.tr("Localizable", "sending.retry", fallback: "Retry")
    /// Send now
    internal static let sendNow = L10n.tr("Localizable", "sending.send_now", fallback: "Send now")
  }
  internal enum Settings {
    /// Active until %@
    internal static func activeUntil(_ p1: Any) -> String {
      return L10n.tr("Localizable", "settings.active_until", String(describing: p1), fallback: "Active until %@")
    }
    /// Advanced
    internal static let advanced = L10n.tr("Localizable", "settings.advanced", fallback: "Advanced")
    /// Buy subscription
    internal static let buy = L10n.tr("Localizable", "settings.buy", fallback: "Buy subscription")
    /// Change your settings to renew subscription automatically
    internal static let change = L10n.tr("Localizable", "settings.change", fallback: "Change your settings to renew subscription automatically")
    /// Contact Us
    internal static let contact = L10n.tr("Localizable", "settings.contact", fallback: "Contact Us")
    /// Edit Front page
    internal static let edit = L10n.tr("Localizable", "settings.edit", fallback: "Edit Front page")
    /// FAQ
    internal static let faq = L10n.tr("Localizable", "settings.faq", fallback: "FAQ")
    /// Help and Support
    internal static let help = L10n.tr("Localizable", "settings.help", fallback: "Help and Support")
    /// Name
    internal static let name = L10n.tr("Localizable", "settings.name", fallback: "Name")
    /// Notifications
    internal static let notifications = L10n.tr("Localizable", "settings.notifications", fallback: "Notifications")
    /// Phone
    internal static let phone = L10n.tr("Localizable", "settings.phone", fallback: "Phone")
    /// Fax Preview
    internal static let preview = L10n.tr("Localizable", "settings.preview", fallback: "Fax Preview")
    /// Privacy Policy
    internal static let privacy = L10n.tr("Localizable", "settings.privacy", fallback: "Privacy Policy")
    /// Rate App
    internal static let rate = L10n.tr("Localizable", "settings.rate", fallback: "Rate App")
    /// Subscription renews automatically
    internal static let renews = L10n.tr("Localizable", "settings.renews", fallback: "Subscription renews automatically")
    /// Sender information
    internal static let sender = L10n.tr("Localizable", "settings.sender", fallback: "Sender information")
    /// Settings
    internal static let title = L10n.tr("Localizable", "settings.title", fallback: "Settings")
  }
}
// swiftlint:enable explicit_type_interface function_parameter_count identifier_name line_length
// swiftlint:enable nesting type_body_length type_name vertical_whitespace_opening_braces

// MARK: - Implementation Details

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg..., fallback value: String) -> String {
    let format = BundleToken.bundle.localizedString(forKey: key, value: value, table: table)
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

// swiftlint:disable convenience_type
private final class BundleToken {
  static let bundle: Bundle = {
    #if SWIFT_PACKAGE
    return Bundle.module
    #else
    return Bundle(for: BundleToken.self)
    #endif
  }()
}
// swiftlint:enable convenience_type
